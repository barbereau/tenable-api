# InlineResponse409

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conflicts** | [**list[InlineResponse409Conflicts]**](vm__InlineResponse409Conflicts.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


