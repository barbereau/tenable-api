# coding: utf-8

"""
    Web Application Scanning v1

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.waslegacy.configuration import Configuration


class InlineResponse20026Output(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'ports': 'object',
        'has_attachment': 'int',
        'custom_description': 'str',
        'plugin_output': 'str',
        'hosts': 'str',
        'severity': 'int'
    }

    attribute_map = {
        'ports': 'ports',
        'has_attachment': 'has_attachment',
        'custom_description': 'custom_description',
        'plugin_output': 'plugin_output',
        'hosts': 'hosts',
        'severity': 'severity'
    }

    def __init__(self, ports=None, has_attachment=None, custom_description=None, plugin_output=None, hosts=None, severity=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20026Output - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._ports = None
        self._has_attachment = None
        self._custom_description = None
        self._plugin_output = None
        self._hosts = None
        self._severity = None
        self.discriminator = None

        if ports is not None:
            self.ports = ports
        if has_attachment is not None:
            self.has_attachment = has_attachment
        if custom_description is not None:
            self.custom_description = custom_description
        if plugin_output is not None:
            self.plugin_output = plugin_output
        if hosts is not None:
            self.hosts = hosts
        if severity is not None:
            self.severity = severity

    @property
    def ports(self):
        """Gets the ports of this InlineResponse20026Output.  # noqa: E501


        :return: The ports of this InlineResponse20026Output.  # noqa: E501
        :rtype: object
        """
        return self._ports

    @ports.setter
    def ports(self, ports):
        """Sets the ports of this InlineResponse20026Output.


        :param ports: The ports of this InlineResponse20026Output.  # noqa: E501
        :type ports: object
        """

        self._ports = ports

    @property
    def has_attachment(self):
        """Gets the has_attachment of this InlineResponse20026Output.  # noqa: E501

        If the value is `1`, the plugin output contains files that may be exported.  # noqa: E501

        :return: The has_attachment of this InlineResponse20026Output.  # noqa: E501
        :rtype: int
        """
        return self._has_attachment

    @has_attachment.setter
    def has_attachment(self, has_attachment):
        """Sets the has_attachment of this InlineResponse20026Output.

        If the value is `1`, the plugin output contains files that may be exported.  # noqa: E501

        :param has_attachment: The has_attachment of this InlineResponse20026Output.  # noqa: E501
        :type has_attachment: int
        """

        self._has_attachment = has_attachment

    @property
    def custom_description(self):
        """Gets the custom_description of this InlineResponse20026Output.  # noqa: E501

        A custom description of the plugin.  # noqa: E501

        :return: The custom_description of this InlineResponse20026Output.  # noqa: E501
        :rtype: str
        """
        return self._custom_description

    @custom_description.setter
    def custom_description(self, custom_description):
        """Sets the custom_description of this InlineResponse20026Output.

        A custom description of the plugin.  # noqa: E501

        :param custom_description: The custom_description of this InlineResponse20026Output.  # noqa: E501
        :type custom_description: str
        """

        self._custom_description = custom_description

    @property
    def plugin_output(self):
        """Gets the plugin_output of this InlineResponse20026Output.  # noqa: E501

        The text of the plugin output.  # noqa: E501

        :return: The plugin_output of this InlineResponse20026Output.  # noqa: E501
        :rtype: str
        """
        return self._plugin_output

    @plugin_output.setter
    def plugin_output(self, plugin_output):
        """Sets the plugin_output of this InlineResponse20026Output.

        The text of the plugin output.  # noqa: E501

        :param plugin_output: The plugin_output of this InlineResponse20026Output.  # noqa: E501
        :type plugin_output: str
        """

        self._plugin_output = plugin_output

    @property
    def hosts(self):
        """Gets the hosts of this InlineResponse20026Output.  # noqa: E501

        Other hosts with the same output.  # noqa: E501

        :return: The hosts of this InlineResponse20026Output.  # noqa: E501
        :rtype: str
        """
        return self._hosts

    @hosts.setter
    def hosts(self, hosts):
        """Sets the hosts of this InlineResponse20026Output.

        Other hosts with the same output.  # noqa: E501

        :param hosts: The hosts of this InlineResponse20026Output.  # noqa: E501
        :type hosts: str
        """

        self._hosts = hosts

    @property
    def severity(self):
        """Gets the severity of this InlineResponse20026Output.  # noqa: E501

        The severity of the output.  # noqa: E501

        :return: The severity of this InlineResponse20026Output.  # noqa: E501
        :rtype: int
        """
        return self._severity

    @severity.setter
    def severity(self, severity):
        """Sets the severity of this InlineResponse20026Output.

        The severity of the output.  # noqa: E501

        :param severity: The severity of this InlineResponse20026Output.  # noqa: E501
        :type severity: int
        """

        self._severity = severity

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20026Output):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20026Output):
            return True

        return self.to_dict() != other.to_dict()
