# InlineResponse20035

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**export_uuid** | **str** | The UUID of the assets export job. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


