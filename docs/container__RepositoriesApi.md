# tenableapi.containerv2.RepositoriesApi

All URIs are relative to *https://cloud.tenable.com/container-security/api/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**container_security_v2_delete_repository**](container__RepositoriesApi.md#container_security_v2_delete_repository) | **DELETE** /repositories/{name} | Delete repository
[**container_security_v2_get_repository_details**](container__RepositoriesApi.md#container_security_v2_get_repository_details) | **GET** /repositories/{name} | Get repository details
[**container_security_v2_list_repositories**](container__RepositoriesApi.md#container_security_v2_list_repositories) | **GET** /repositories | List repositories


# **container_security_v2_delete_repository**
> container_security_v2_delete_repository(name)

Delete repository

Deletes a Tenable.io Container Security repository.<p>Requires SCAN OPERATOR [24] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv2
from tenableapi.containerv2.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com/container-security/api/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv2.Configuration(
    host = "https://cloud.tenable.com/container-security/api/v2"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv2.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv2.RepositoriesApi(api_client)
    name = 'name_example' # str | The name of the repository to delete.

    try:
        # Delete repository
        api_instance.container_security_v2_delete_repository(name)
    except ApiException as e:
        print("Exception when calling RepositoriesApi->container_security_v2_delete_repository: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The name of the repository to delete. | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Returned if Tenable.io successfully deletes the specified repository. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**404** | Returned if Tenable.io cannot find the specified repository. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_v2_get_repository_details**
> RepositoryDetails container_security_v2_get_repository_details(name)

Get repository details

Returns details for a Tenable.io Container Security repository.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv2
from tenableapi.containerv2.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com/container-security/api/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv2.Configuration(
    host = "https://cloud.tenable.com/container-security/api/v2"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv2.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv2.RepositoriesApi(api_client)
    name = 'name_example' # str | 

    try:
        # Get repository details
        api_response = api_instance.container_security_v2_get_repository_details(name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling RepositoriesApi->container_security_v2_get_repository_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  | 

### Return type

[**RepositoryDetails**](container__RepositoryDetails.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the repository details. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**404** | Returned if Tenable.io cannot find the specified repository. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_v2_list_repositories**
> RepositoryListResponse container_security_v2_list_repositories(image_name=image_name, name_contains=name_contains, offset=offset, limit=limit)

List repositories

Returns a list of image repositories in your Tenable.io Container Security instance. Use the query parameters to filter the list. <p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv2
from tenableapi.containerv2.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com/container-security/api/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv2.Configuration(
    host = "https://cloud.tenable.com/container-security/api/v2"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv2.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv2.RepositoriesApi(api_client)
    image_name = 'image_name_example' # str | The repository name to filter on. Tenable.io Container Security returns only the repositories with names that exactly match the parameter value. The value is case-sensitive. (optional)
name_contains = 'name_contains_example' # str | The partial repository name to filter on. Tenable.io Container Security returns the images with names that contain the parameter value. The value is case-sensitive. (optional)
offset = 0 # int | The number of skipped records in the returned result set. Must be in the int32 format. (optional) (default to 0)
limit = 50 # int | The number of records to include in the result set. Must be in the int32 format. Default is 50. The maximum limit is 1,000. (optional) (default to 50)

    try:
        # List repositories
        api_response = api_instance.container_security_v2_list_repositories(image_name=image_name, name_contains=name_contains, offset=offset, limit=limit)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling RepositoriesApi->container_security_v2_list_repositories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image_name** | **str**| The repository name to filter on. Tenable.io Container Security returns only the repositories with names that exactly match the parameter value. The value is case-sensitive. | [optional] 
 **name_contains** | **str**| The partial repository name to filter on. Tenable.io Container Security returns the images with names that contain the parameter value. The value is case-sensitive. | [optional] 
 **offset** | **int**| The number of skipped records in the returned result set. Must be in the int32 format. | [optional] [default to 0]
 **limit** | **int**| The number of records to include in the result set. Must be in the int32 format. Default is 50. The maximum limit is 1,000. | [optional] [default to 50]

### Return type

[**RepositoryListResponse**](container__RepositoryListResponse.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | success |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

