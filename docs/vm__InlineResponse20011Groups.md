# InlineResponse20011Groups

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the agent group to which the agent belongs. | [optional] 
**id** | **int** | The unique ID of the agent group to which the agent belongs. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


