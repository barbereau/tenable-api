# InlineObject6

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the exclusion. | [optional] 
**description** | **str** | The description of the exclusion. | [optional] 
**schedule** | [**ScannersScannerIdAgentsExclusionsSchedule**](vm__ScannersScannerIdAgentsvm__ExclusionsSchedule.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


