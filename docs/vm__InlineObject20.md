# InlineObject20

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the exclusion. | [optional] 
**description** | **str** | The description of the exclusion. | [optional] 
**members** | **str** | The targets that you want excluded from scans. Specify multiple targets as a comma-separated string. Targets can be in the following formats:  - an individual IPv4 address (192.168.1.1)  - a range of IPv4 addresses (192.168.1.1-192.168.1.255)  - CIDR notation (192.168.2.0/24)  - a fully-qualified domain name (FQDN) (host.domain.com) | [optional] 
**schedule** | [**ExclusionsSchedule**](vm__ExclusionsSchedule.md) |  | [optional] 
**network_id** | **str** | The ID of the network object associated with scanners where Tenable.io applies the exclusion. The default network ID is &#x60;00000000-0000-0000-0000-000000000000&#x60;. To determine the ID of a custom network, use the [GET /networks](ref:networks-list) endpoint. For more information about network objects, see [Manage Networks](doc:manage-networks-tio). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


