# Page

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** | The name of the product. | [optional] 
**page_slug** | **str** | Product page slug, for example, &#x60;nessus&#x60;. | [optional] 
**description** | **str** | The description of a product. | [optional] 
**files_index_url** | **str** | The URL to list the product files available for download. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


