# InlineObject35

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**force_plugin_update** | **int** | Pass 1 to force a plugin update. | [optional] 
**force_ui_update** | **int** | Pass 1 to force a UI update. | [optional] 
**finish_update** | **int** | Pass 1 to reboot the scanner and run the latest software update (only valid if automatic updates are disabled). | [optional] 
**registration_code** | **str** | Sets the registration code for the scanner. | [optional] 
**aws_update_interval** | **int** | Specifies how often, in minutes, the scanner checks in with Tenable.io (Amazon Web Services scanners only). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


