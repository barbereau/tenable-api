# tenableapi.vm.SolutionsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**solutions_filters**](vm__SolutionsApi.md#solutions_filters) | **GET** /solutions/filters | List solutions filters
[**solutions_list**](vm__SolutionsApi.md#solutions_list) | **GET** /solutions | List solutions
[**solutions_list_assets**](vm__SolutionsApi.md#solutions_list_assets) | **GET** /solutions/{solution_id}/assets | List assets for solution
[**solutions_list_vulns**](vm__SolutionsApi.md#solutions_list_vulns) | **GET** /solutions/{solution_id}/vulnerabilities | List vulnerabilities for solution


# **solutions_filters**
> InlineResponse20083 solutions_filters()

List solutions filters

Returns a list of filters you can use to refine the results from the [GET /solutions](ref:listSolutions) endpoint.  This endpoint requires a Lumin license. For more information, see [Lumin](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/Lumin.htm).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.SolutionsApi(api_client)
    
    try:
        # List solutions filters
        api_response = api_instance.solutions_filters()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling SolutionsApi->solutions_filters: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20083**](vm__InlineResponse20083.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of filters for solutions. |  -  |
**404** | Returned if Tenable.io cannot find the specified solution. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **solutions_list**
> InlineResponse20080 solutions_list(limit=limit, offset=offset, sort=sort, f=f, ft=ft)

List solutions

Returns a filterable, sortable, paginated list of solutions.  This endpoint requires a Lumin license. For more information, see [Lumin](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/Lumin.htm).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.SolutionsApi(api_client)
    limit = 56 # int | The numbers of items to return. The default is 20. (optional)
offset = 56 # int | The starting record to retrieve. If this parameter is not supplied, the default value is `0`. (optional)
sort = 'vpr_max:desc' # str | The field or fields to sort the results on and the sort order, for example, `sort=vpr_max:desc`. Default sort order is `asc`. If you specify multiple fields, fields must be separated by commas (`sort=field1:value,field2:value`).  By default, Tenable.io sorts the solutions list by `vpr_max` (descending), `asset_count` (descending), and `vulnerability_instance_count` (descending). (optional)
f = 'acr_score:gt:6' # str | A filter condition in the `field:operator:value` format, for example, `f=acr_score:gt:6`. For supported filter fields, use the [GET /solutions/filters](ref:solutions-filters) endpoint. (optional)
ft = 'and|or' # str | If multiple `f` parameters are present, specifies whether Tenable.io applies `AND` or `OR` to conditions. Supported values are `and` and `or`. If you omit this parameter when using multiple `f` parameters, Tenable.io applies `AND` by default. (optional)

    try:
        # List solutions
        api_response = api_instance.solutions_list(limit=limit, offset=offset, sort=sort, f=f, ft=ft)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling SolutionsApi->solutions_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The numbers of items to return. The default is 20. | [optional] 
 **offset** | **int**| The starting record to retrieve. If this parameter is not supplied, the default value is &#x60;0&#x60;. | [optional] 
 **sort** | **str**| The field or fields to sort the results on and the sort order, for example, &#x60;sort&#x3D;vpr_max:desc&#x60;. Default sort order is &#x60;asc&#x60;. If you specify multiple fields, fields must be separated by commas (&#x60;sort&#x3D;field1:value,field2:value&#x60;).  By default, Tenable.io sorts the solutions list by &#x60;vpr_max&#x60; (descending), &#x60;asset_count&#x60; (descending), and &#x60;vulnerability_instance_count&#x60; (descending). | [optional] 
 **f** | **str**| A filter condition in the &#x60;field:operator:value&#x60; format, for example, &#x60;f&#x3D;acr_score:gt:6&#x60;. For supported filter fields, use the [GET /solutions/filters](ref:solutions-filters) endpoint. | [optional] 
 **ft** | **str**| If multiple &#x60;f&#x60; parameters are present, specifies whether Tenable.io applies &#x60;AND&#x60; or &#x60;OR&#x60; to conditions. Supported values are &#x60;and&#x60; and &#x60;or&#x60;. If you omit this parameter when using multiple &#x60;f&#x60; parameters, Tenable.io applies &#x60;AND&#x60; by default. | [optional] 

### Return type

[**InlineResponse20080**](vm__InlineResponse20080.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of solutions. |  -  |
**400** | Returned if your request message is invalid. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **solutions_list_assets**
> InlineResponse20081 solutions_list_assets(solution_id)

List assets for solution

Returns a list of affected assets for which Tenable recommends the specified solution.   This endpoint requires a Lumin license. For more information, see [Lumin](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/Lumin.htm).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.SolutionsApi(api_client)
    solution_id = 'solution_id_example' # str | The UUID of the solution.

    try:
        # List assets for solution
        api_response = api_instance.solutions_list_assets(solution_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling SolutionsApi->solutions_list_assets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **solution_id** | [**str**](.md)| The UUID of the solution. | 

### Return type

[**InlineResponse20081**](vm__InlineResponse20081.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of assets for which Tenable recommends the specified solution. |  -  |
**404** | Returned if Tenable.io cannot find the specified solution. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **solutions_list_vulns**
> InlineResponse20082 solutions_list_vulns(solution_id)

List vulnerabilities for solution

Returns a list of vulnerabilities that the specified solution addresses.  This endpoint requires a Lumin license. For more information, see [Lumin](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/Lumin.htm).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.SolutionsApi(api_client)
    solution_id = 'solution_id_example' # str | The UUID of solution.

    try:
        # List vulnerabilities for solution
        api_response = api_instance.solutions_list_vulns(solution_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling SolutionsApi->solutions_list_vulns: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **solution_id** | [**str**](.md)| The UUID of solution. | 

### Return type

[**InlineResponse20082**](vm__InlineResponse20082.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of vulnerabilities that the specified solution addresses. |  -  |
**404** | Returned if Tenable.io cannot find the specified solution. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

