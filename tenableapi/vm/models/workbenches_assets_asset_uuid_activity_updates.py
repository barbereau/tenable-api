# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class WorkbenchesAssetsAssetUuidActivityUpdates(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'method': 'str',
        '_property': 'str',
        'value': 'str'
    }

    attribute_map = {
        'method': 'method',
        '_property': 'property',
        'value': 'value'
    }

    def __init__(self, method=None, _property=None, value=None, local_vars_configuration=None):  # noqa: E501
        """WorkbenchesAssetsAssetUuidActivityUpdates - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._method = None
        self.__property = None
        self._value = None
        self.discriminator = None

        if method is not None:
            self.method = method
        if _property is not None:
            self._property = _property
        if value is not None:
            self.value = value

    @property
    def method(self):
        """Gets the method of this WorkbenchesAssetsAssetUuidActivityUpdates.  # noqa: E501

        The update method. Possible values include:   - add—A scan identified a new software application installed on the asset.  - remove—Tenable.io identified the specified application as expired and removed it from the installed_software attribute of the asset. Tenable.io considers an application detection expired if no scan detects the application within 30 days of the scan that originally detected the application.  # noqa: E501

        :return: The method of this WorkbenchesAssetsAssetUuidActivityUpdates.  # noqa: E501
        :rtype: str
        """
        return self._method

    @method.setter
    def method(self, method):
        """Sets the method of this WorkbenchesAssetsAssetUuidActivityUpdates.

        The update method. Possible values include:   - add—A scan identified a new software application installed on the asset.  - remove—Tenable.io identified the specified application as expired and removed it from the installed_software attribute of the asset. Tenable.io considers an application detection expired if no scan detects the application within 30 days of the scan that originally detected the application.  # noqa: E501

        :param method: The method of this WorkbenchesAssetsAssetUuidActivityUpdates.  # noqa: E501
        :type method: str
        """

        self._method = method

    @property
    def _property(self):
        """Gets the _property of this WorkbenchesAssetsAssetUuidActivityUpdates.  # noqa: E501

        The name of the updated attribute.  # noqa: E501

        :return: The _property of this WorkbenchesAssetsAssetUuidActivityUpdates.  # noqa: E501
        :rtype: str
        """
        return self.__property

    @_property.setter
    def _property(self, _property):
        """Sets the _property of this WorkbenchesAssetsAssetUuidActivityUpdates.

        The name of the updated attribute.  # noqa: E501

        :param _property: The _property of this WorkbenchesAssetsAssetUuidActivityUpdates.  # noqa: E501
        :type _property: str
        """

        self.__property = _property

    @property
    def value(self):
        """Gets the value of this WorkbenchesAssetsAssetUuidActivityUpdates.  # noqa: E501

        The updated value of the attribute.  # noqa: E501

        :return: The value of this WorkbenchesAssetsAssetUuidActivityUpdates.  # noqa: E501
        :rtype: str
        """
        return self._value

    @value.setter
    def value(self, value):
        """Sets the value of this WorkbenchesAssetsAssetUuidActivityUpdates.

        The updated value of the attribute.  # noqa: E501

        :param value: The value of this WorkbenchesAssetsAssetUuidActivityUpdates.  # noqa: E501
        :type value: str
        """

        self._value = value

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, WorkbenchesAssetsAssetUuidActivityUpdates):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, WorkbenchesAssetsAssetUuidActivityUpdates):
            return True

        return self.to_dict() != other.to_dict()
