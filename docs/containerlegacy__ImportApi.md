# tenableapi.containerv1.ImportApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**container_security_import_delete_import_by_id**](containerlegacy__ImportApi.md#container_security_import_delete_import_by_id) | **DELETE** /container-security/api/v1/import/{id} | Delete import
[**container_security_import_import**](containerlegacy__ImportApi.md#container_security_import_import) | **POST** /container-security/api/v1/import | Create import
[**container_security_import_list_imports**](containerlegacy__ImportApi.md#container_security_import_list_imports) | **GET** /container-security/api/v1/import/list | List imports
[**container_security_import_run_import_by_id**](containerlegacy__ImportApi.md#container_security_import_run_import_by_id) | **POST** /container-security/api/v1/import/{id}/run | Run import 
[**container_security_import_test_connection**](containerlegacy__ImportApi.md#container_security_import_test_connection) | **POST** /container-security/api/v1/import/{id}/test | Test connection to Tenable.io
[**container_security_import_update_import_by_id**](containerlegacy__ImportApi.md#container_security_import_update_import_by_id) | **POST** /container-security/api/v1/import/{id} | Update import


# **container_security_import_delete_import_by_id**
> InlineResponse2001 container_security_import_delete_import_by_id(id)

Delete import

**Deprecated!** Tenable.io Container Security API v1 is deprecated. For images import, use Tenable.io connectors. For more information, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/containersecurity/Content/ContainerSecurity/ConfigureConnectors.htm). Deletes an import by ID.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ImportApi(api_client)
    id = 56 # int | The ID of the import that you want to delete.

    try:
        # Delete import
        api_response = api_instance.container_security_import_delete_import_by_id(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ImportApi->container_security_import_delete_import_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the import that you want to delete. | 

### Return type

[**InlineResponse2001**](containerlegacy__InlineResponse2001.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the ID of the deleted import. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_import_import**
> InlineResponse2001 container_security_import_import(inline_object)

Create import

**Deprecated!** Tenable.io Container Security API v1 is deprecated. For images import, use Tenable.io connectors. For more information, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/containersecurity/Content/ContainerSecurity/ConfigureConnectors.htm). Creates an import.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ImportApi(api_client)
    inline_object = tenableapi.containerv1.InlineObject() # InlineObject | 

    try:
        # Create import
        api_response = api_instance.container_security_import_import(inline_object)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ImportApi->container_security_import_import: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object** | [**InlineObject**](containerlegacy__InlineObject.md)|  | 

### Return type

[**InlineResponse2001**](containerlegacy__InlineResponse2001.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the ID you specified in the request. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_import_list_imports**
> object container_security_import_list_imports()

List imports

**Deprecated!** Tenable.io Container Security API v1 is deprecated. For images import, use Tenable.io connectors. For more information, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/containersecurity/Content/ContainerSecurity/ConfigureConnectors.htm). Returns a list of all imports.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ImportApi(api_client)
    
    try:
        # List imports
        api_response = api_instance.container_security_import_list_imports()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ImportApi->container_security_import_list_imports: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns an array of information about each import that has been performed. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_import_run_import_by_id**
> InlineResponse2001 container_security_import_run_import_by_id(id)

Run import 

**Deprecated!** Tenable.io Container Security API v1 is deprecated. For images import, use Tenable.io connectors. For more information, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/containersecurity/Content/ContainerSecurity/ConfigureConnectors.htm). Runs an import by ID.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ImportApi(api_client)
    id = 56 # int | The ID of the import that you want to run.

    try:
        # Run import 
        api_response = api_instance.container_security_import_run_import_by_id(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ImportApi->container_security_import_run_import_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the import that you want to run. | 

### Return type

[**InlineResponse2001**](containerlegacy__InlineResponse2001.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the ID of the import you want to run. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_import_test_connection**
> InlineResponse2001 container_security_import_test_connection(id)

Test connection to Tenable.io

**Deprecated!** Tenable.io Container Security API v1 is deprecated. For images import, use Tenable.io connectors. For more information, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/containersecurity/Content/ContainerSecurity/ConfigureConnectors.htm). Tests your connection to Tenable.io Container Security.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ImportApi(api_client)
    id = 56 # int | A test ID. You can specify any integer as the ID. Tenable.io Container Security uses this value for the test only.

    try:
        # Test connection to Tenable.io
        api_response = api_instance.container_security_import_test_connection(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ImportApi->container_security_import_test_connection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A test ID. You can specify any integer as the ID. Tenable.io Container Security uses this value for the test only. | 

### Return type

[**InlineResponse2001**](containerlegacy__InlineResponse2001.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the ID you specified in the request. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_import_update_import_by_id**
> InlineResponse2001 container_security_import_update_import_by_id(id, inline_object1)

Update import

**Deprecated!** Tenable.io Container Security API v1 is deprecated. For images import, use Tenable.io connectors. For more information, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/containersecurity/Content/ContainerSecurity/ConfigureConnectors.htm). Updates an import by ID.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ImportApi(api_client)
    id = 56 # int | The ID of the import that you want to update.
inline_object1 = tenableapi.containerv1.InlineObject1() # InlineObject1 | 

    try:
        # Update import
        api_response = api_instance.container_security_import_update_import_by_id(id, inline_object1)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ImportApi->container_security_import_update_import_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the import that you want to update. | 
 **inline_object1** | [**InlineObject1**](containerlegacy__InlineObject1.md)|  | 

### Return type

[**InlineResponse2001**](containerlegacy__InlineResponse2001.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the ID you specified in the request. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

