# InlineResponse2009

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creation_date** | **int** | The creation date for the scanner in Unix time. | [optional] 
**group** | **bool** | True if the scanner is in a group; false, otherwise. | [optional] 
**id** | **int** | The unique ID of the scanner. | [optional] 
**uuid** | **str** | The UUID of the scanner. | [optional] 
**last_connect** | **int** | The last_connect time in Unix time. | [optional] 
**last_modification_date** | **int** | The last_modification_date in Unix time. | [optional] 
**linked** | **int** | 1 if the scanner is linked; 0, otherwise. | [optional] 
**name** | **str** | The user-defined name of the scanner. | [optional] 
**type** | **str** | The type of scanner (managed_webapp). | [optional] 
**status** | **str** | The status of the scanner (on or off). | [optional] 
**scan_count** | **int** | The current number of running scans on the scanner. | [optional] 
**engine_version** | **str** | The version of the scanner. | [optional] 
**owner** | **str** | The owner of the scanner. | [optional] 
**key** | **str** | A alphanumeric sequence of characters used when linking a scanner to Tenable.io. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


