# InlineResponse20092ReferenceInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The source of the reference information about the vulnerability. Possible values include:  - bid—Bugtraq (Symantec Connect)  - cert—CERT/CC Vulnerability Notes Database  - cve—NIST National Vulnerability Database (NVD)  - edb-id—The Exploit Database  - iava—information assurance vulnerability alert  - osvdb—Open Sourced Vulnerability Database | [optional] 
**url** | **str** | The URL of the source site, if available. | [optional] 
**values** | **list[str]** | The unique identifier(s) for the vulnerability at the source. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


