# InlineObject36

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | **str** | An action to perform on a scan. Valid values are &#x60;stop&#x60;, &#x60;pause&#x60;, and &#x60;resume&#x60;. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


