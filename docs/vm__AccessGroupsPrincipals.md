# AccessGroupsPrincipals

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | (Required) The type of principal. Valid values include:  - user—Grants access to the user you specify.  - group—Grants access to all users assigned to the user group you specify. | [optional] 
**principal_id** | **str** | The UUID of a user or user group. This parameter is required if the request omits the &#x60;principal_name&#x60; parameter. | [optional] 
**principal_name** | **str** | The name of the user or user group. This parameter is required if the request omits the &#x60;principal_id&#x60; parameter. If a request includes both &#x60;principal_id&#x60; and &#x60;principal_name&#x60;, Tenable.io assigns the user or user group to the access group based on the &#x60;principal_id&#x60; parameter, and ignores the &#x60;principal_name&#x60; parameter in the request. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


