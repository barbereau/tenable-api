# InlineResponse20092RiskInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**risk_factor** | **str** | The risk factor associated with the plugin. Possible values are: &#x60;Low&#x60;, &#x60;Medium&#x60;, &#x60;High&#x60;, or &#x60;Critical&#x60;. See the &#x60;risk_factor&#x60; attribute in [Tenable Plugin Attributes](doc:tenable-plugin-attributes). | [optional] 
**cvss_vector** | **str** | The raw CVSSv2 metrics for the vulnerability. For more information, see CVSSv2 documentation. | [optional] 
**cvss_base_score** | **str** | The CVSSv2 base score (intrinsic and fundamental characteristics of a vulnerability that are constant over time and user environments). | [optional] 
**cvss_temporal_vector** | **str** | The raw CVSSv2 temporal metrics for the vulnerability. | [optional] 
**cvss_temporal_score** | **str** | The CVSSv2 temporal score (characteristics of a vulnerability that change over time but not among user environments). | [optional] 
**cvss3_vector** | **str** | The raw CVSSv3 metrics for the vulnerability. For more information, see CVSSv3 documentation. | [optional] 
**cvss3_base_score** | **str** | The CVSSv3 base score (intrinsic and fundamental characteristics of a vulnerability that are constant over time and user environments). | [optional] 
**cvss3_temporal_vector** | **str** | CVSSv3 temporal metrics for the vulnerability. | [optional] 
**cvss3_temporal_score** | **str** | The CVSSv3 temporal score (characteristics of a vulnerability that change over time but not among user environments). | [optional] 
**stig_severity** | **str** | Security Technical Implementation Guide (STIG) severity code for the vulnerability. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


