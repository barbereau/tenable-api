# InlineResponse20017

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | **object** |  | [optional] 
**hosts** | [**list[InlineResponse20017Hosts]**](waslegacy__InlineResponse20017Hosts.md) |  | [optional] 
**notes** | [**list[InlineResponse20017Notes]**](waslegacy__InlineResponse20017Notes.md) |  | [optional] 
**vulnerabilities** | [**list[InlineResponse20017Vulnerabilities]**](waslegacy__InlineResponse20017Vulnerabilities.md) |  | [optional] 
**filters** | [**list[InlineResponse20017Filters]**](waslegacy__InlineResponse20017Filters.md) |  | [optional] 
**history** | [**list[InlineResponse20017History]**](waslegacy__InlineResponse20017History.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


