# ScansScanIdSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the scan. | [optional] 
**description** | **str** | The description of the scan. | [optional] 
**policy_id** | **int** | The unique ID of the policy to use. | [optional] 
**folder_id** | **int** | The unique ID of the destination folder for the scan. | [optional] 
**scanner_id** | **int** | The unique ID of the scanner to use. | [optional] 
**enabled** | **bool** | If &#x60;true&#x60;, the schedule for the scan is enabled. | 
**launch** | **str** | When to launch the scan. (Valid values are DAILY, WEEKLY, MONTHLY, YEARLY.) | [optional] 
**starttime** | **str** | The starting time and date for the scan in the following format: YYYYMMDDTHHMMSS. | [optional] 
**rrules** | **str** | Expects a string of three values separated by semi-colons. The frequency (FREQ&#x3D;ONETIME or DAILY or WEEKLY or MONTHLY or YEARLY), the interval (INTERVAL&#x3D;1 or 2 or 3 ... x), and the days of the week (BYDAY&#x3D;SU,MO,TU,WE,TH,FR,SA). To create a scan that runs every three weeks on Monday Wednesday and Friday the string would be &#x60;FREQ&#x3D;WEEKLY;INTERVAL&#x3D;3;BYDAY&#x3D;MO,WE,FR&#x60; | [optional] 
**timezone** | **str** | The timezone for the scan schedule. | [optional] 
**text_targets** | **str** | A single URL to scan. Required for non-agent scans if no target groups are provided. | 
**emails** | **str** | A comma-separated list of accounts who will receive the email summary report. | [optional] 
**acls** | **list[str]** | An array containing permissions to apply to the scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


