# InlineResponse20083

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | [**list[InlineResponse20083Filters]**](vm__InlineResponse20083Filters.md) | A list of filters you can use to refine [GET /solutions](ref:listSolutions) results. | [optional] 
**sort** | [**InlineResponse20039Sort**](vm__InlineResponse20039vm__Sort.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


