# InlineResponse20080Solutions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The ID of the solution. | [optional] 
**title** | **str** | The title of the solution. | [optional] 
**description** | **str** | A brief description of the solution. | [optional] 
**family** | **str** | The solution family that includes the solution. | [optional] 
**vpr_max** | **float** | The highest Vulnerability Priority Rating (VPR) score of any vulnerability addressed by the solution. For more information about VPR, see [Vulnerability Priority Rating Drivers](doc:vpr-drivers-tio). | [optional] 
**cvss_max** | **float** | The highest CVSS score of any vulnerability addressed by the solution. | [optional] 
**asset_count** | **int** | The number of distinct assets with at least one vulnerability addressed by the solution. | [optional] 
**assets** | **list[str]** | A list of assets where Tenable recommends you apply the solution. Tenable.io returns a maximum of 500 assets for an individual solution. If Tenable.io identifies more than 500 assets for a solution, this list is empty and the &#x60;assets_truncated&#x60; attribute is &#x60;true&#x60;. | [optional] 
**assets_truncated** | **bool** | Indicates whether the assets where Tenable recommends you apply the solution is more than 500 (&#x60;true&#x60;). If this value is &#x60;true&#x60;, the &#x60;assets&#x60; array for the solution is an empty list. | [optional] 
**vulnerability_count** | **int** | The total number of vulnerabilities addressed by the solution. Tenable identifies distinct vulnerabilities based on the Tenable Vulnerability Indicator (TVI). Tenable assigns a TVI (TVI-####-#####) to all unique, publicly disclosed vulnerabilities to uniquely identify an individual vulnerability on your network. | [optional] 
**vulnerabilities** | **list[str]** | A list of vulnerabilities addressed by the solution. Tenable.io returns a maximum of 500 vulnerabilities for an individual solution. If Tenable.io identifies more than 500 vulnerabilities for a solution, this list is empty and the &#x60;vulnerabilities_truncated&#x60; attribute is &#x60;true&#x60;. | [optional] 
**vulnerabilities_truncated** | **bool** | Indicates whether the vulnerabilities addressed by the solution is more than 500 (&#x60;true&#x60;). If this value is &#x60;true&#x60;, the &#x60;vulnerabilities&#x60; array for the solution is an empty list. | [optional] 
**vulnerability_instance_count** | **int** | The number of vulnerability instances for the specified assets. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


