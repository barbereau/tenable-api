# tenableapi.mssp.AccountsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**io_mssp_accounts_list**](mssp__AccountsApi.md#io_mssp_accounts_list) | **GET** /mssp/accounts | List customer accounts


# **io_mssp_accounts_list**
> ChildAccountListResponse io_mssp_accounts_list()

List customer accounts

Returns a list of customer accounts in the Tenable.io MSSP Portal. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.mssp
from tenableapi.mssp.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.mssp.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.mssp.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.mssp.AccountsApi(api_client)
    
    try:
        # List customer accounts
        api_response = api_instance.io_mssp_accounts_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccountsApi->io_mssp_accounts_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ChildAccountListResponse**](mssp__ChildAccountListResponse.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of customer accounts in the Tenable.io MSSP Portal. A customer account contains information about a customer instance of a Tenable.io product, including product license information and user-defined notes that can include valuable internal tracking or customer contact information. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in the Tenable.io MSSP Portal. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

