# InlineResponse20010

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agents** | **list[str]** | The agents in the group. The agent records can be filtered, sorted, and paginated. | [optional] 
**creation_date** | **int** | The creation date of the agent group in unixtime. | [optional] 
**id** | **int** | The unique ID of the agent group. | [optional] 
**last_modification_date** | **int** | The last modification date for the agent group in unixtime. | [optional] 
**name** | **str** | The name of the agent group. | [optional] 
**owner** | **str** | The username for the owner of the agent group. | [optional] 
**owner_id** | **str** | The unique ID of the owner of the agent group. | [optional] 
**owner_name** | **str** | The name for the owner of the agent group. | [optional] 
**owner_uuid** | **str** | The UUID of the owner of the agent group. | [optional] 
**pagination** | **object** |  | [optional] 
**shared** | **int** | The shared status of the agent group. | [optional] 
**user_permissions** | **int** | The sharing permissions for the agent group. | [optional] 
**uuid** | **str** | The UUID of the agent group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


