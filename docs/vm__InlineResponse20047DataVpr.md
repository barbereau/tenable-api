# InlineResponse20047DataVpr

Information about the Vulnerability Priority Rating (VPR) for the plugin.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**score** | **float** | The Vulnerability Priority Rating (VPR) for the vulnerability. If a plugin is designed to detect multiple vulnerabilities, the VPR represents the highest value calculated for a vulnerability associated with the plugin. For more information, see &lt;a href&#x3D;\&quot;https://docs.tenable.com/cloud/Content/Analysis/RiskMetrics.htm\&quot; target&#x3D;\&quot;_blank\&quot;&gt;Severity vs. VPR&lt;/a&gt; in the &lt;i&gt;Tenable.io Vulnerability Management User Guide&lt;/i&gt;. | [optional] 
**drivers** | **object** | The key drivers Tenable uses to calculate a vulnerability&#39;s VPR. For more information, see &lt;a href&#x3D;\&quot;/docs/vpr-drivers-tio\&quot;&gt;Vulnerability Priority Rating Drivers&lt;/a&gt;. | [optional] 
**updated** | **str** | The ISO timestamp when Tenable.io last imported the VPR for this vulnerability. Tenable.io imports updated VPR values every time you run a scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


