# InlineResponse20013AcrDrivers

Information about an asset characteristic that factored into the ACR score calculation.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driver_name** | **str** | The type of characteristic. | [optional] 
**driver_value** | **list[str]** | The characteristic value. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


