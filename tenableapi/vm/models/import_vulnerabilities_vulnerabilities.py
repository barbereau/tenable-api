# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class ImportVulnerabilitiesVulnerabilities(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'tenable_plugin_id': 'str',
        'cve': 'str',
        'port': 'int',
        'protocol': 'str',
        'authenticated': 'bool',
        'first_found': 'int',
        'last_found': 'int',
        'last_fixed': 'int',
        'output': 'str'
    }

    attribute_map = {
        'tenable_plugin_id': 'tenable_plugin_id',
        'cve': 'cve',
        'port': 'port',
        'protocol': 'protocol',
        'authenticated': 'authenticated',
        'first_found': 'first_found',
        'last_found': 'last_found',
        'last_fixed': 'last_fixed',
        'output': 'output'
    }

    def __init__(self, tenable_plugin_id=None, cve=None, port=None, protocol=None, authenticated=None, first_found=None, last_found=None, last_fixed=None, output=None, local_vars_configuration=None):  # noqa: E501
        """ImportVulnerabilitiesVulnerabilities - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._tenable_plugin_id = None
        self._cve = None
        self._port = None
        self._protocol = None
        self._authenticated = None
        self._first_found = None
        self._last_found = None
        self._last_fixed = None
        self._output = None
        self.discriminator = None

        if tenable_plugin_id is not None:
            self.tenable_plugin_id = tenable_plugin_id
        if cve is not None:
            self.cve = cve
        if port is not None:
            self.port = port
        if protocol is not None:
            self.protocol = protocol
        if authenticated is not None:
            self.authenticated = authenticated
        if first_found is not None:
            self.first_found = first_found
        if last_found is not None:
            self.last_found = last_found
        if last_fixed is not None:
            self.last_fixed = last_fixed
        if output is not None:
            self.output = output

    @property
    def tenable_plugin_id(self):
        """Gets the tenable_plugin_id of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501

        The ID of the Nessus plugin that identified the vulnerability. This parameter is required if the vulnerability object does not specify a cve value.  # noqa: E501

        :return: The tenable_plugin_id of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :rtype: str
        """
        return self._tenable_plugin_id

    @tenable_plugin_id.setter
    def tenable_plugin_id(self, tenable_plugin_id):
        """Sets the tenable_plugin_id of this ImportVulnerabilitiesVulnerabilities.

        The ID of the Nessus plugin that identified the vulnerability. This parameter is required if the vulnerability object does not specify a cve value.  # noqa: E501

        :param tenable_plugin_id: The tenable_plugin_id of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :type tenable_plugin_id: str
        """

        self._tenable_plugin_id = tenable_plugin_id

    @property
    def cve(self):
        """Gets the cve of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501

        The Common Vulnerability and Exposure (CVE) ID for the vulnerability. This parameter is required if the vulnerability object does not specify a `tenable_plugin_id` value.  # noqa: E501

        :return: The cve of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :rtype: str
        """
        return self._cve

    @cve.setter
    def cve(self, cve):
        """Sets the cve of this ImportVulnerabilitiesVulnerabilities.

        The Common Vulnerability and Exposure (CVE) ID for the vulnerability. This parameter is required if the vulnerability object does not specify a `tenable_plugin_id` value.  # noqa: E501

        :param cve: The cve of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :type cve: str
        """

        self._cve = cve

    @property
    def port(self):
        """Gets the port of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501

        The port the scanner used to communicate with the asset.  # noqa: E501

        :return: The port of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :rtype: int
        """
        return self._port

    @port.setter
    def port(self, port):
        """Sets the port of this ImportVulnerabilitiesVulnerabilities.

        The port the scanner used to communicate with the asset.  # noqa: E501

        :param port: The port of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :type port: int
        """

        self._port = port

    @property
    def protocol(self):
        """Gets the protocol of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501

        The protocol the scanner used to communicate with the asset.  # noqa: E501

        :return: The protocol of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :rtype: str
        """
        return self._protocol

    @protocol.setter
    def protocol(self, protocol):
        """Sets the protocol of this ImportVulnerabilitiesVulnerabilities.

        The protocol the scanner used to communicate with the asset.  # noqa: E501

        :param protocol: The protocol of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :type protocol: str
        """

        self._protocol = protocol

    @property
    def authenticated(self):
        """Gets the authenticated of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501

        A value specifying whether the scan that identified the vulnerability was an authenticated (credentialed) scan.  # noqa: E501

        :return: The authenticated of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :rtype: bool
        """
        return self._authenticated

    @authenticated.setter
    def authenticated(self, authenticated):
        """Sets the authenticated of this ImportVulnerabilitiesVulnerabilities.

        A value specifying whether the scan that identified the vulnerability was an authenticated (credentialed) scan.  # noqa: E501

        :param authenticated: The authenticated of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :type authenticated: bool
        """

        self._authenticated = authenticated

    @property
    def first_found(self):
        """Gets the first_found of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501

        The date (in Unix time) when a scan first identified the vulnerability on the asset.  # noqa: E501

        :return: The first_found of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :rtype: int
        """
        return self._first_found

    @first_found.setter
    def first_found(self, first_found):
        """Sets the first_found of this ImportVulnerabilitiesVulnerabilities.

        The date (in Unix time) when a scan first identified the vulnerability on the asset.  # noqa: E501

        :param first_found: The first_found of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :type first_found: int
        """

        self._first_found = first_found

    @property
    def last_found(self):
        """Gets the last_found of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501

        The date (in Unix time) when a scan last identified the vulnerability on the asset.  # noqa: E501

        :return: The last_found of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :rtype: int
        """
        return self._last_found

    @last_found.setter
    def last_found(self, last_found):
        """Sets the last_found of this ImportVulnerabilitiesVulnerabilities.

        The date (in Unix time) when a scan last identified the vulnerability on the asset.  # noqa: E501

        :param last_found: The last_found of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :type last_found: int
        """

        self._last_found = last_found

    @property
    def last_fixed(self):
        """Gets the last_fixed of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501

        The date (in Unix time) when the vulnerability state was changed to `fixed`. Tenable.io updates the vulnerability state to fixed when a scan no longer detects a previously detected vulnerability on the asset.  # noqa: E501

        :return: The last_fixed of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :rtype: int
        """
        return self._last_fixed

    @last_fixed.setter
    def last_fixed(self, last_fixed):
        """Sets the last_fixed of this ImportVulnerabilitiesVulnerabilities.

        The date (in Unix time) when the vulnerability state was changed to `fixed`. Tenable.io updates the vulnerability state to fixed when a scan no longer detects a previously detected vulnerability on the asset.  # noqa: E501

        :param last_fixed: The last_fixed of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :type last_fixed: int
        """

        self._last_fixed = last_fixed

    @property
    def output(self):
        """Gets the output of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501

        (Required) The text output of the scanner, up to 2,000 maximum characters.  # noqa: E501

        :return: The output of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :rtype: str
        """
        return self._output

    @output.setter
    def output(self, output):
        """Sets the output of this ImportVulnerabilitiesVulnerabilities.

        (Required) The text output of the scanner, up to 2,000 maximum characters.  # noqa: E501

        :param output: The output of this ImportVulnerabilitiesVulnerabilities.  # noqa: E501
        :type output: str
        """

        self._output = output

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ImportVulnerabilitiesVulnerabilities):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, ImportVulnerabilitiesVulnerabilities):
            return True

        return self.to_dict() != other.to_dict()
