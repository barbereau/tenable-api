# InlineResponse20064Filters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The short name of the filter. | [optional] 
**readable_name** | **str** | The long name of the filter. | [optional] 
**operators** | **list[str]** | The comparison options for the filter. | [optional] 
**control** | [**InlineResponse20064Control**](vm__InlineResponse20064Control.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


