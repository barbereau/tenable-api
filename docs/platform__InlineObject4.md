# InlineObject4

Specify `true` or `false` to grant or revoke authorizations.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_permitted** | **bool** | Indicates whether API access is authorized for the user. | 
**password_permitted** | **bool** | Indicates whether user name and password login is authorized for the user. | 
**saml_permitted** | **bool** | Indicates whether SSO with SAML is authorized for the user. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


