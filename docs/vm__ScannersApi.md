# tenableapi.vm.ScannersApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**scanners_control_scans**](vm__ScannersApi.md#scanners_control_scans) | **POST** /scanners/{scanner_id}/scans/{scan_uuid}/control | Allow control of running scans
[**scanners_delete**](vm__ScannersApi.md#scanners_delete) | **DELETE** /scanners/{scanner_id} | Delete scanner
[**scanners_details**](vm__ScannersApi.md#scanners_details) | **GET** /scanners/{scanner_id} | Get scanner details
[**scanners_edit**](vm__ScannersApi.md#scanners_edit) | **PUT** /scanners/{scanner_id} | Update scanner
[**scanners_get_aws_targets**](vm__ScannersApi.md#scanners_get_aws_targets) | **GET** /scanners/{scanner_id}/aws-targets | List AWS scan targets
[**scanners_get_scanner_key**](vm__ScannersApi.md#scanners_get_scanner_key) | **GET** /scanners/{scanner_id}/key | Get scanner key
[**scanners_get_scans**](vm__ScannersApi.md#scanners_get_scans) | **GET** /scanners/{scanner_id}/scans | List running scans
[**scanners_list**](vm__ScannersApi.md#scanners_list) | **GET** /scanners | List scanners
[**scanners_toggle_link_state**](vm__ScannersApi.md#scanners_toggle_link_state) | **PUT** /scanners/{scanner_id}/link | Toggle scanner link state


# **scanners_control_scans**
> object scanners_control_scans(scanner_id, scan_uuid, inline_object36)

Allow control of running scans

Allows control of scans that are currently running on a scanner.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannersApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.
scan_uuid = 'scan_uuid_example' # str | The UUID of the scan.
inline_object36 = tenableapi.vm.InlineObject36() # InlineObject36 | 

    try:
        # Allow control of running scans
        api_response = api_instance.scanners_control_scans(scanner_id, scan_uuid, inline_object36)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannersApi->scanners_control_scans: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 
 **scan_uuid** | **str**| The UUID of the scan. | 
 **inline_object36** | [**InlineObject36**](vm__InlineObject36.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully completes the specified action. |  -  |
**403** | Returned if you do not have permission to perform the specified action. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan or scanner. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanners_delete**
> object scanners_delete(scanner_id)

Delete scanner

Deletes and unlinks a scanner from Tenable.io.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannersApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.

    try:
        # Delete scanner
        api_response = api_instance.scanners_delete(scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannersApi->scanners_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deletes/unlinks the scanner. |  -  |
**403** | Returned if you attempt to delete the local scanner. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to delete the scanner. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanners_details**
> InlineResponse20056 scanners_details(scanner_id)

Get scanner details

Returns details for the specified scanner.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannersApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.

    try:
        # Get scanner details
        api_response = api_instance.scanners_details(scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannersApi->scanners_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 

### Return type

[**InlineResponse20056**](vm__InlineResponse20056.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scanner details. |  -  |
**403** | Returned if you do not have permission to view the specified scanner. |  -  |
**404** | Returned if Tenable.io cannot find the specified scanner. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanners_edit**
> InlineResponse20058 scanners_edit(scanner_id, inline_object35)

Update scanner

Updates the specified scanner.   You cannot use this endpoint to assign the scanner to a network object. Instead, use the [POST /networks/{network_id}/scanners/{scanner_uuid}](ref:networks-assign-scanner) endpoint.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannersApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.
inline_object35 = tenableapi.vm.InlineObject35() # InlineObject35 | 

    try:
        # Update scanner
        api_response = api_instance.scanners_edit(scanner_id, inline_object35)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannersApi->scanners_edit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 
 **inline_object35** | [**InlineObject35**](vm__InlineObject35.md)|  | 

### Return type

[**InlineResponse20058**](vm__InlineResponse20058.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully updates the specified scanner. |  -  |
**403** | Returned if you attempt to update a cloud scanner where you don&#39;t have edit permissions, or if you attempt to set a registration code for an Amazon Web Services scanner. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encounters an internal error while attempting to update the scanner. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanners_get_aws_targets**
> InlineResponse20060 scanners_get_aws_targets(scanner_id)

List AWS scan targets

Lists AWS scan targets if the requested scanner is an Amazon Web Services scanner.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannersApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.

    try:
        # List AWS scan targets
        api_response = api_instance.scanners_get_aws_targets(scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannersApi->scanners_get_aws_targets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 

### Return type

[**InlineResponse20060**](vm__InlineResponse20060.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the AWS target list for the specified scanner. |  -  |
**403** | Returned if you do not have permission to view scanner data. |  -  |
**404** | Returned if Tenable.io cannot find the specified scanner, or if the scanner is not an AWS scanner. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanners_get_scanner_key**
> InlineResponse20059 scanners_get_scanner_key(scanner_id)

Get scanner key

Gets the key of the requested scanner.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannersApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.

    try:
        # Get scanner key
        api_response = api_instance.scanners_get_scanner_key(scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannersApi->scanners_get_scanner_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 

### Return type

[**InlineResponse20059**](vm__InlineResponse20059.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scanner key. |  -  |
**403** | Returned if you do not have permission to view scanner data. |  -  |
**404** | Returned if Tenable.io cannot find the specified scanner. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanners_get_scans**
> InlineResponse20061 scanners_get_scans(scanner_id)

List running scans

Lists scans running on the requested scanner.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannersApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.

    try:
        # List running scans
        api_response = api_instance.scanners_get_scans(scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannersApi->scanners_get_scans: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 

### Return type

[**InlineResponse20061**](vm__InlineResponse20061.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the list of scans running on the specified scanner. |  -  |
**403** | Returned if you do not have permission to view scanner data. |  -  |
**404** | Returned if Tenable.io cannot find the specified scanner. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanners_list**
> list[InlineResponse20056] scanners_list()

List scanners

Returns the scanner list.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannersApi(api_client)
    
    try:
        # List scanners
        api_response = api_instance.scanners_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannersApi->scanners_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse20056]**](vm__InlineResponse20056.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scanner list. |  -  |
**403** | Returned if you do not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanners_toggle_link_state**
> object scanners_toggle_link_state(scanner_id, inline_object37)

Toggle scanner link state

Enables or disables the link state of the scanner identified by `scanner_id`.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannersApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.
inline_object37 = tenableapi.vm.InlineObject37() # InlineObject37 | 

    try:
        # Toggle scanner link state
        api_response = api_instance.scanners_toggle_link_state(scanner_id, inline_object37)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannersApi->scanners_toggle_link_state: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 
 **inline_object37** | [**InlineObject37**](vm__InlineObject37.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully updates the link state of the scanner. |  -  |
**403** | Returned if you attempt to edit a cloud scanner where you do not have edit permissions. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

