# InlineResponse20081

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | **list[str]** | A list of asset UUIDs. | [optional] 
**pagination** | [**InlineResponse20080Pagination**](vm__InlineResponse20080Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


