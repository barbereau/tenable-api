# InlineResponse200AccessGroups

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**container_uuid** | **str** | The UUID of your Tenable.io instance. | [optional] 
**created_at** | **str** | An ISO timestamp indicating the date and time on which the access group was created. | [optional] 
**updated_at** | **str** | An ISO timestamp indicating the time and date on which the access group was last modified. | [optional] 
**id** | **str** | The UUID of the access group. | [optional] 
**name** | **str** | The name of the access group. This name must be:   * Unique within your Tenable.io instance.   * A maximum of 255 characters.   * Alphanumeric, but can include limited special characters (underscore, dash, parenthesis, brackets, colon). | [optional] 
**all_assets** | **bool** | Specifies whether the access group is the system-provided All Assets access group:   - If &#x60;true&#x60;, the access group is the All Assets access group. The only change you can make to this access group is to refine user membership in the group. For more information, see descriptions of the all_users and principals parameters for the [PUT /access-groups/{id}](ref:io-v1-access-groups-edit) endpoint.  - If &#x60;false&#x60;, the access group is a user-defined access group, and you can change all parameters for the group. This parameter is &#x60;false&#x60; for all access groups you create. | [optional] 
**all_users** | **bool** | Specifies whether assets in the access group can be viewed by all or only some users in your organization:  - If &#x60;true&#x60;, all users in your organization have Can View access to the assets defined in the rules parameter. If &#x60;true&#x60; in a [POST /access-groups](ref:io-v1-access-groups-create) or [PUT /access-groups/{id}](ref:io-v1-access-groups-edit) request, Tenable.io ignores any principal parameters in the request.   - If &#x60;false&#x60;, only specified users have Can View access to the assets defined in the rules parameter. You define which users or user groups have access in the principals parameter of a [POST /access-groups](ref:io-v1-access-groups-create) or [PUT /access-groups/{id}](ref:io-v1-access-groups-edit) request.   **Note:** If a [PUT /access-groups/{id}]ref:io-v1-access-groups-edit) endpoint request sets this parameter to &#x60;true&#x60; for an access group where the parameter was previously set to &#x60;false&#x60;, Tenable.io removes all principal data previously associated with the access group. | [optional] 
**status** | **str** | The status of the process evaluating and assigning assets to the access group. Possible values are:   - PROCESSING—Tenable.io is currently evaluating assets against the asset rules for the access group. For an indication of evaluation progress, see the &#x60;processing_percent_complete&#x60; attribute for the access group.  - COMPLETED—Tenable.io has successfully completed its evaluation of assets against the asset rules for the group.  - ERROR—Tenable.io encountered an error while evaluating assets against asset rules for the access group. Rule validation typically prevents this status from occurring. However, if you encounter an ERROR status, Tenable recommends that you delete the existing asset rules, then recreate the rules after a short time has elapsed. | [optional] 
**created_by_uuid** | **str** | The UUID of the user who created the access group. | [optional] 
**created_by_name** | **str** | The name of the user who created the access group. | [optional] 
**updated_by_uuid** | **str** | The UUID of the user who last modified the access group. | [optional] 
**updated_by_name** | **str** | The name of the user who last modified the access group. | [optional] 
**processing_percent_complete** | **int** | The percentage of assets that Tenable.io has evaluated against the asset rules for the access group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


