# ReportScanTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**template_id** | **str** | The UUID of the Tenable-provided template. | [optional] 
**name** | **str** | The name of the Tenable-provided template. | [optional] 
**description** | **str** | The description of the Tenable-provided template. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


