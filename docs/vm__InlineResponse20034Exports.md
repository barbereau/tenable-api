# InlineResponse20034Exports

Information about the export job.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** | The UUID for the export request. | [optional] 
**status** | **str** | The status of the export request. Possible values include:  - QUEUED—Tenable.io has queued the export request until it completes other requests currently in process.  - PROCESSING—Tenable.io has started processing the export request.  - FINISHED—Tenable.io has completed processing the export request. The list of chunks is complete.  - CANCELLED—An administrator has cancelled the export request.  - ERROR—Tenable.io encountered an error while processing the export request. Tenable recommends that you retry the request. If the status persists on retry, contact Support. | [optional] 
**chunks_available** | **list[int]** | A list of completed chunks available for download. | [optional] 
**total_chunks** | **int** | The total number of chunks associated with the export job as a whole. | [optional] 
**finished_chunks** | **int** | The number of chunks that have been processed and are available for download. | [optional] 
**filters** | **object** | The filters used in the export job request. For a list of possible filters, see the [POST /vulns/export](ref:exports-vulns-export-request-export) and [POST /assets/export](ref:exports-assets-request-export) endpoints. | [optional] 
**num_assets_per_chunk** | **int** | The number of assets contained in each export chunk. | [optional] 
**created** | **int** | The Unix timestamp when the export job was created. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


