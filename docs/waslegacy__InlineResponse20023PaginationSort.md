# InlineResponse20023PaginationSort

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The field on which Tenable.io sorts the results. | [optional] 
**order** | **str** | The direction of the sort order. Supported values are &#x60;asc&#x60; (ascending) and &#x60;desc&#x60; (descending). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


