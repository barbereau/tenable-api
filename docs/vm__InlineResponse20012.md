# InlineResponse20012

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**task_id** | **str** | The UUID of the task. | [optional] 
**container_uuid** | **str** | The UUID of the container where the task is operating. | [optional] 
**status** | **str** | State of the task.  \&quot;NEW\&quot; means that the task was created, but has not yet started running.  \&quot;RUNNING\&quot; means that the task is in progress.  \&quot;COMPLETED\&quot; means that the task is done.  \&quot;FAILED\&quot; means that there was an error completing the task.  \&quot;STALE\&quot; means that the task has not been updated in a long time. | [optional] 
**message** | **str** | An informative, human-readable message about the state of the task. | [optional] 
**start_time** | **int** | Start time of the task in unix time milliseconds. | [optional] 
**end_time** | **int** | End time of the task in unix time milliseconds, if the task is finished. | [optional] 
**last_update_time** | **int** | Last time progress was made on executing the task in unix time milliseconds. | [optional] 
**total_work_units** | **int** | Total amount of work which the task will attempt to complete. | [optional] 
**total_work_units_completed** | **int** | Total amount of work that the task has completed. | [optional] 
**completion_percentage** | **int** | total_work_units_completed divided by total_work_units. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


