# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20072History(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'time_end': 'int',
        'scan_uuid': 'str',
        'id': 'int',
        'is_archived': 'bool',
        'time_start': 'int',
        'visibility': 'str',
        'targets': 'InlineResponse20072Targets',
        'status': 'str'
    }

    attribute_map = {
        'time_end': 'time_end',
        'scan_uuid': 'scan_uuid',
        'id': 'id',
        'is_archived': 'is_archived',
        'time_start': 'time_start',
        'visibility': 'visibility',
        'targets': 'targets',
        'status': 'status'
    }

    def __init__(self, time_end=None, scan_uuid=None, id=None, is_archived=None, time_start=None, visibility=None, targets=None, status=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20072History - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._time_end = None
        self._scan_uuid = None
        self._id = None
        self._is_archived = None
        self._time_start = None
        self._visibility = None
        self._targets = None
        self._status = None
        self.discriminator = None

        if time_end is not None:
            self.time_end = time_end
        if scan_uuid is not None:
            self.scan_uuid = scan_uuid
        if id is not None:
            self.id = id
        if is_archived is not None:
            self.is_archived = is_archived
        if time_start is not None:
            self.time_start = time_start
        if visibility is not None:
            self.visibility = visibility
        if targets is not None:
            self.targets = targets
        if status is not None:
            self.status = status

    @property
    def time_end(self):
        """Gets the time_end of this InlineResponse20072History.  # noqa: E501

        The Unix timestamp when the scan finished running.  # noqa: E501

        :return: The time_end of this InlineResponse20072History.  # noqa: E501
        :rtype: int
        """
        return self._time_end

    @time_end.setter
    def time_end(self, time_end):
        """Sets the time_end of this InlineResponse20072History.

        The Unix timestamp when the scan finished running.  # noqa: E501

        :param time_end: The time_end of this InlineResponse20072History.  # noqa: E501
        :type time_end: int
        """

        self._time_end = time_end

    @property
    def scan_uuid(self):
        """Gets the scan_uuid of this InlineResponse20072History.  # noqa: E501

        The UUID for the specific scan run.  # noqa: E501

        :return: The scan_uuid of this InlineResponse20072History.  # noqa: E501
        :rtype: str
        """
        return self._scan_uuid

    @scan_uuid.setter
    def scan_uuid(self, scan_uuid):
        """Sets the scan_uuid of this InlineResponse20072History.

        The UUID for the specific scan run.  # noqa: E501

        :param scan_uuid: The scan_uuid of this InlineResponse20072History.  # noqa: E501
        :type scan_uuid: str
        """

        self._scan_uuid = scan_uuid

    @property
    def id(self):
        """Gets the id of this InlineResponse20072History.  # noqa: E501

        The unique identifier for the specific scan run.  # noqa: E501

        :return: The id of this InlineResponse20072History.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20072History.

        The unique identifier for the specific scan run.  # noqa: E501

        :param id: The id of this InlineResponse20072History.  # noqa: E501
        :type id: int
        """

        self._id = id

    @property
    def is_archived(self):
        """Gets the is_archived of this InlineResponse20072History.  # noqa: E501

        Indicates whether the scan results are older than 60 days (`true`). If this parameter is `true`, Tenable.io returns limited data for the scan run. For complete scan results that are older than 60 days, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead.  # noqa: E501

        :return: The is_archived of this InlineResponse20072History.  # noqa: E501
        :rtype: bool
        """
        return self._is_archived

    @is_archived.setter
    def is_archived(self, is_archived):
        """Sets the is_archived of this InlineResponse20072History.

        Indicates whether the scan results are older than 60 days (`true`). If this parameter is `true`, Tenable.io returns limited data for the scan run. For complete scan results that are older than 60 days, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead.  # noqa: E501

        :param is_archived: The is_archived of this InlineResponse20072History.  # noqa: E501
        :type is_archived: bool
        """

        self._is_archived = is_archived

    @property
    def time_start(self):
        """Gets the time_start of this InlineResponse20072History.  # noqa: E501

        The Unix timestamp when the scan started running.  # noqa: E501

        :return: The time_start of this InlineResponse20072History.  # noqa: E501
        :rtype: int
        """
        return self._time_start

    @time_start.setter
    def time_start(self, time_start):
        """Sets the time_start of this InlineResponse20072History.

        The Unix timestamp when the scan started running.  # noqa: E501

        :param time_start: The time_start of this InlineResponse20072History.  # noqa: E501
        :type time_start: int
        """

        self._time_start = time_start

    @property
    def visibility(self):
        """Gets the visibility of this InlineResponse20072History.  # noqa: E501

        The visibility of the scan results in workbenches (`public` or `private`).  # noqa: E501

        :return: The visibility of this InlineResponse20072History.  # noqa: E501
        :rtype: str
        """
        return self._visibility

    @visibility.setter
    def visibility(self, visibility):
        """Sets the visibility of this InlineResponse20072History.

        The visibility of the scan results in workbenches (`public` or `private`).  # noqa: E501

        :param visibility: The visibility of this InlineResponse20072History.  # noqa: E501
        :type visibility: str
        """

        self._visibility = visibility

    @property
    def targets(self):
        """Gets the targets of this InlineResponse20072History.  # noqa: E501


        :return: The targets of this InlineResponse20072History.  # noqa: E501
        :rtype: InlineResponse20072Targets
        """
        return self._targets

    @targets.setter
    def targets(self, targets):
        """Sets the targets of this InlineResponse20072History.


        :param targets: The targets of this InlineResponse20072History.  # noqa: E501
        :type targets: InlineResponse20072Targets
        """

        self._targets = targets

    @property
    def status(self):
        """Gets the status of this InlineResponse20072History.  # noqa: E501

        The status of the scan. For a list of possible values, see [Scan Status](doc:scan-status-tio).  # noqa: E501

        :return: The status of this InlineResponse20072History.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this InlineResponse20072History.

        The status of the scan. For a list of possible values, see [Scan Status](doc:scan-status-tio).  # noqa: E501

        :param status: The status of this InlineResponse20072History.  # noqa: E501
        :type status: str
        """

        self._status = status

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20072History):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20072History):
            return True

        return self.to_dict() != other.to_dict()
