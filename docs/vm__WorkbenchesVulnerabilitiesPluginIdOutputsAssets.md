# WorkbenchesVulnerabilitiesPluginIdOutputsAssets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hostname** | **str** | The host name of the asset. | [optional] 
**id** | **str** | The ID of the asset. | [optional] 
**uuid** | **str** | The UUID of the asset. Use this value as the unique key for the asset. | [optional] 
**netbios_name** | **str** | The NetBios name of the asset. | [optional] 
**fqdn** | **str** | The FQDN of the asset. | [optional] 
**ipv4** | **str** | The IPV4 of the asset. | [optional] 
**first_seen** | **datetime** | Indicates when the asset was discovered by a scan. | [optional] 
**last_seen** | **datetime** | Indicates when the asset was last observed by a scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


