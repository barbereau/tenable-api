# tenableapi.was.VulnerabilitiesApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_v2_vulns_list**](was__VulnerabilitiesApi.md#was_v2_vulns_list) | **GET** /was/v2/vulnerabilities | List vulnerabilities


# **was_v2_vulns_list**
> InlineResponse2004 was_v2_vulns_list(plugin_id=plugin_id, application_uri=application_uri, asset_id=asset_id, order_by=order_by, ordering=ordering, page=page, size=size)

List vulnerabilities

Returns a list of vulnerabilities detected by Tenable.io Web Application Scanning API v2 scans.<p>Requires BASIC [16] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.VulnerabilitiesApi(api_client)
    plugin_id = 56 # int | The ID of a plugin associated with a vulnerability. (optional)
application_uri = 'https://www.tenable.com' # str | The URI of the web application to filter on.   **Deprecated:** This parameter is deprecated and will be retired on 2021/03/01. Tenable recommends that you use the `asset_id` parameter instead. Please update any existing integrations that your organization has. (optional)
asset_id = 'b018aed8-554f-4965-9b05-994eaa66d459' # str | The UUID of the web application asset to filter on. (optional)
order_by = 'created_at' # str | The field used to order the query results. (optional)
ordering = 'asc' # str | The sort order applied when sorting by the `order_by` parameter. Values include:  - `asc`  - `desc`  If your request omits the `ordering` query parameter, this value defaults to `asc`. (optional) (default to 'asc')
page = 0 # int | The starting record to retrieve. Use in combination with the `size` parameter to paginate results. The default value is `0`. (optional) (default to 0)
size = 10 # int | The page size of the query results. Use in combination with the `page` parameter to paginate results. The default value is `10`. (optional) (default to 10)

    try:
        # List vulnerabilities
        api_response = api_instance.was_v2_vulns_list(plugin_id=plugin_id, application_uri=application_uri, asset_id=asset_id, order_by=order_by, ordering=ordering, page=page, size=size)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VulnerabilitiesApi->was_v2_vulns_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plugin_id** | **int**| The ID of a plugin associated with a vulnerability. | [optional] 
 **application_uri** | **str**| The URI of the web application to filter on.   **Deprecated:** This parameter is deprecated and will be retired on 2021/03/01. Tenable recommends that you use the &#x60;asset_id&#x60; parameter instead. Please update any existing integrations that your organization has. | [optional] 
 **asset_id** | [**str**](.md)| The UUID of the web application asset to filter on. | [optional] 
 **order_by** | **str**| The field used to order the query results. | [optional] 
 **ordering** | **str**| The sort order applied when sorting by the &#x60;order_by&#x60; parameter. Values include:  - &#x60;asc&#x60;  - &#x60;desc&#x60;  If your request omits the &#x60;ordering&#x60; query parameter, this value defaults to &#x60;asc&#x60;. | [optional] [default to &#39;asc&#39;]
 **page** | **int**| The starting record to retrieve. Use in combination with the &#x60;size&#x60; parameter to paginate results. The default value is &#x60;0&#x60;. | [optional] [default to 0]
 **size** | **int**| The page size of the query results. Use in combination with the &#x60;page&#x60; parameter to paginate results. The default value is &#x60;10&#x60;. | [optional] [default to 10]

### Return type

[**InlineResponse2004**](was__InlineResponse2004.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of vulnerabilities. |  -  |
**400** | Returned if your request specifies invalid query parameter values. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

