# InlineObject55

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the group. | 
**members** | **str** | The members of the group. A comma-separated list of FQDNs or IP address ranges that you want to scan. | 
**acls** | [**list[InlineResponse20046]**](vm__InlineResponse20046.md) | An array containing permissions to apply to the group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


