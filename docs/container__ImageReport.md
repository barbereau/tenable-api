# ImageReport

An image vulnerability report.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**os_release_name** | **str** | The image operating system release name. | [optional] 
**malware** | [**list[Malware]**](container__Malware.md) | A list of malware files identified by the Tenable.io Container Security scan in the image. | [optional] 
**sha256** | **str** | The image SHA256 hash. | [optional] 
**os** | **str** | The image operating system. | [optional] 
**risk_score** | **int** | The image risk score on a scale of 1-10. For more information about the risk score metric, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/containersecurity/Content/ContainerSecurity/RiskMetrics.htm). | [optional] 
**findings** | [**list[Finding]**](container__Finding.md) | A list of vulnerabilities that Tenable.io Container Security identified in the image. | [optional] 
**os_version** | **str** | The image operating system version. | [optional] 
**created_at** | **datetime** | An ISO timestamp indicating the date and time when the image was created, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**installed_packages** | [**list[InstalledPackage]**](container__Installedcontainer__Package.md) | A list of installed software packages for the image. | [optional] 
**platform** | **str** | The image platform, for example, &#x60;docker&#x60;. | [optional] 
**image_name** | **str** | The name of the image. | [optional] 
**updated_at** | **datetime** | An ISO timestamp indicating the date and time when the image was last uploaded, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**digest** | **str** | The image digest. | [optional] 
**tag** | **str** | The tag identifying the image version. **Note**: Image tags are not equivalent to Tenable.io [asset tags](ref:tags). | [optional] 
**potentially_unwanted_programs** | [**list[UnwantedProgram]**](container__UnwantedProgram.md) | A list of potentially unwanted programs for the image. | [optional] 
**docker_image_id** | **str** | The image Docker ID. | [optional] 
**os_architecture** | **str** | An image processor architecture, for example, &#x60;AMD64&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


