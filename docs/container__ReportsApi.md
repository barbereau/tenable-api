# tenableapi.containerv2.ReportsApi

All URIs are relative to *https://cloud.tenable.com/container-security/api/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**container_security_v2_get_image_report**](container__ReportsApi.md#container_security_v2_get_image_report) | **GET** /reports/{repository}/{image}/{tag} | Get image report


# **container_security_v2_get_image_report**
> ImageReport container_security_v2_get_image_report(repository, image, tag)

Get image report

Returns a vulnerability report for the specified image.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv2
from tenableapi.containerv2.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com/container-security/api/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv2.Configuration(
    host = "https://cloud.tenable.com/container-security/api/v2"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv2.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv2.ReportsApi(api_client)
    repository = 'repository_example' # str | The name of the Tenable.io Container Security repository where the image is stored. The value is case-sensitive.
image = 'image_example' # str | The name of the image. The value is case-sensitive.
tag = 'tag_example' # str | The tag identifying the image version. The value is case-sensitive. **Note**: Image tags are not equivalent to Tenable.io [asset tags](ref:tags).

    try:
        # Get image report
        api_response = api_instance.container_security_v2_get_image_report(repository, image, tag)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ReportsApi->container_security_v2_get_image_report: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **repository** | **str**| The name of the Tenable.io Container Security repository where the image is stored. The value is case-sensitive. | 
 **image** | **str**| The name of the image. The value is case-sensitive. | 
 **tag** | **str**| The tag identifying the image version. The value is case-sensitive. **Note**: Image tags are not equivalent to Tenable.io [asset tags](ref:tags). | 

### Return type

[**ImageReport**](container__ImageReport.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a vulnerability scan report for the image. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**404** | Returned if Tenable.io cannot find the specified image or the report for the image is not ready. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

