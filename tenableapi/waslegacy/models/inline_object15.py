# coding: utf-8

"""
    Web Application Scanning v1

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.waslegacy.configuration import Configuration


class InlineObject15(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'format': 'str',
        'password': 'str',
        'chapters': 'str'
    }

    attribute_map = {
        'format': 'format',
        'password': 'password',
        'chapters': 'chapters'
    }

    def __init__(self, format=None, password=None, chapters=None, local_vars_configuration=None):  # noqa: E501
        """InlineObject15 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._format = None
        self._password = None
        self._chapters = None
        self.discriminator = None

        self.format = format
        if password is not None:
            self.password = password
        if chapters is not None:
            self.chapters = chapters

    @property
    def format(self):
        """Gets the format of this InlineObject15.  # noqa: E501

        The file format to use. For Web Application Scanning, supported export formats are Nessus, CSV, and DB.  # noqa: E501

        :return: The format of this InlineObject15.  # noqa: E501
        :rtype: str
        """
        return self._format

    @format.setter
    def format(self, format):
        """Sets the format of this InlineObject15.

        The file format to use. For Web Application Scanning, supported export formats are Nessus, CSV, and DB.  # noqa: E501

        :param format: The format of this InlineObject15.  # noqa: E501
        :type format: str
        """
        if self.local_vars_configuration.client_side_validation and format is None:  # noqa: E501
            raise ValueError("Invalid value for `format`, must not be `None`")  # noqa: E501
        allowed_values = ["nessus", "csv", "db"]  # noqa: E501
        if self.local_vars_configuration.client_side_validation and format not in allowed_values:  # noqa: E501
            raise ValueError(
                "Invalid value for `format` ({0}), must be one of {1}"  # noqa: E501
                .format(format, allowed_values)
            )

        self._format = format

    @property
    def password(self):
        """Gets the password of this InlineObject15.  # noqa: E501

        The password used to encrypt database exports (\\*Required when exporting as DB).  # noqa: E501

        :return: The password of this InlineObject15.  # noqa: E501
        :rtype: str
        """
        return self._password

    @password.setter
    def password(self, password):
        """Sets the password of this InlineObject15.

        The password used to encrypt database exports (\\*Required when exporting as DB).  # noqa: E501

        :param password: The password of this InlineObject15.  # noqa: E501
        :type password: str
        """

        self._password = password

    @property
    def chapters(self):
        """Gets the chapters of this InlineObject15.  # noqa: E501

        The chapters to include in the export (expecting a semi-colon delimited string comprised of some combination of the following options: vuln\\_hosts\\_summary, vuln\\_by\\_host, compliance\\_exec, remediations, vuln\\_by\\_plugin, compliance)  # noqa: E501

        :return: The chapters of this InlineObject15.  # noqa: E501
        :rtype: str
        """
        return self._chapters

    @chapters.setter
    def chapters(self, chapters):
        """Sets the chapters of this InlineObject15.

        The chapters to include in the export (expecting a semi-colon delimited string comprised of some combination of the following options: vuln\\_hosts\\_summary, vuln\\_by\\_host, compliance\\_exec, remediations, vuln\\_by\\_plugin, compliance)  # noqa: E501

        :param chapters: The chapters of this InlineObject15.  # noqa: E501
        :type chapters: str
        """

        self._chapters = chapters

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineObject15):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineObject15):
            return True

        return self.to_dict() != other.to_dict()
