# InlineObject26

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the network object. This name must be unique within your Tenable.io instance, cannot duplicate the name of a previously deleted network, and cannot be &#x60;default&#x60;.      **Note:** You can add a maximum of 50,000 network objects to an individual Tenable.io instance. | 
**description** | **str** | The description of the network object. | [optional] 
**assets_ttl_days** | **int** | The number of days to wait before assets age out. Assets will be permanently deleted if they are not seen on a scan within the specified number of days.&lt;ul&gt;&lt;li&gt;Minimum value: 90&lt;/li&gt;&lt;li&gt;Maximum value: 365&lt;/li&gt;&lt;/ul&gt; **Warning:** If you enable this option, Tenable.io immediately deletes assets in the specified network that have not been seen for the specified number of days. All asset records and associated vulnerabilities are deleted and cannot be recovered. The deleted assets no longer count towards [your license](https://docs.tenable.com/earlyaccess/tenableio/vulnerabilitymanagement/Content/GettingStarted/Licenses.htm). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


