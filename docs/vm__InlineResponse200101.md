# InlineResponse200101

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | The export processing status, for example, READY or LOADING. | [optional] 
**progress_total** | **str** | The total number of items included in export. | [optional] 
**progress** | **str** | The number of already processed items. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


