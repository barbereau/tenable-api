# InlineResponse20018Credentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** | The UUID of the managed credential object. | [optional] 
**name** | **str** | The name of the managed credential object. You specify the name when you create or update the managed credential. | [optional] 
**description** | **str** | The definition of the managed credential object. You can specify the description when you create or update the managed credential. | [optional] 
**category** | [**InlineResponse20018Category**](vm__InlineResponse20018Category.md) |  | [optional] 
**type** | [**InlineResponse20018Type**](vm__InlineResponse20018Type.md) |  | [optional] 
**created_date** | **str** | The date (in Unix time) when the managed credential object was created. | [optional] 
**created_by** | [**InlineResponse20018CreatedBy**](vm__InlineResponse20018CreatedBy.md) |  | [optional] 
**last_used_by** | [**InlineResponse20018LastUsedBy**](vm__InlineResponse20018LastUsedBy.md) |  | [optional] 
**permissions** | **int** | A value specifying the permissions granted to the user or user group for the credential. For possible values, see \&quot;Credential Roles\&quot; in [Permissions](doc:permissions). | [optional] 
**user_permissions** | **int** | The permissions for the managed credential that are assigned to the user account submitting the API request. For possible values, see \&quot;Credential Roles\&quot; in [Permissions](doc:permissions). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


