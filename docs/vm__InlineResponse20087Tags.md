# InlineResponse20087Tags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_uuid** | **str** | The UUID of the asset where the tag is assigned. | [optional] 
**value** | **str** | The tag value (the second half of the category:value pair). | [optional] 
**value_uuid** | **str** | The UUID of the tag value.  **Note:** A tag UUID is technically assigned to the tag value only (the second half of the category:value pair), but the API commands use this value to represent the whole &#x60;category:value&#x60; pair. | [optional] 
**category_name** | **str** | The tag category name (the first half of the category:value pair). | [optional] 
**category_uuid** | **str** | The UUID of the tag category. Use this value to create future tags in the same category. | [optional] 
**created_at** | **str** | An ISO timestamp indicating the date and time on which the was assigned to an asset, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**created_by** | **str** | The name of the user who assigned the tag to the asset. | [optional] 
**source** | **str** | The tag type:  - static—A user must manually apply the tag to assets.  - dynamic—Tenable.io automatically applies the tag based on asset attribute rules. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


