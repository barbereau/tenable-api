# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20095(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'id': 'str',
        'severities': 'list[WorkbenchesAssetsVulnerabilitiesSeverities]',
        'total': 'int',
        'fqdn': 'list[str]',
        'ipv4': 'list[str]',
        'ipv6': 'list[str]',
        'last_seen': 'str',
        'netbios_name': 'list[str]',
        'agent_name': 'list[str]'
    }

    attribute_map = {
        'id': 'id',
        'severities': 'severities',
        'total': 'total',
        'fqdn': 'fqdn',
        'ipv4': 'ipv4',
        'ipv6': 'ipv6',
        'last_seen': 'last_seen',
        'netbios_name': 'netbios_name',
        'agent_name': 'agent_name'
    }

    def __init__(self, id=None, severities=None, total=None, fqdn=None, ipv4=None, ipv6=None, last_seen=None, netbios_name=None, agent_name=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20095 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._id = None
        self._severities = None
        self._total = None
        self._fqdn = None
        self._ipv4 = None
        self._ipv6 = None
        self._last_seen = None
        self._netbios_name = None
        self._agent_name = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if severities is not None:
            self.severities = severities
        if total is not None:
            self.total = total
        if fqdn is not None:
            self.fqdn = fqdn
        if ipv4 is not None:
            self.ipv4 = ipv4
        if ipv6 is not None:
            self.ipv6 = ipv6
        if last_seen is not None:
            self.last_seen = last_seen
        if netbios_name is not None:
            self.netbios_name = netbios_name
        if agent_name is not None:
            self.agent_name = agent_name

    @property
    def id(self):
        """Gets the id of this InlineResponse20095.  # noqa: E501

        The UUID of the asset.  # noqa: E501

        :return: The id of this InlineResponse20095.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20095.

        The UUID of the asset.  # noqa: E501

        :param id: The id of this InlineResponse20095.  # noqa: E501
        :type id: str
        """

        self._id = id

    @property
    def severities(self):
        """Gets the severities of this InlineResponse20095.  # noqa: E501

        A count of vulnerabilities by severity.  # noqa: E501

        :return: The severities of this InlineResponse20095.  # noqa: E501
        :rtype: list[WorkbenchesAssetsVulnerabilitiesSeverities]
        """
        return self._severities

    @severities.setter
    def severities(self, severities):
        """Sets the severities of this InlineResponse20095.

        A count of vulnerabilities by severity.  # noqa: E501

        :param severities: The severities of this InlineResponse20095.  # noqa: E501
        :type severities: list[WorkbenchesAssetsVulnerabilitiesSeverities]
        """

        self._severities = severities

    @property
    def total(self):
        """Gets the total of this InlineResponse20095.  # noqa: E501

        The total number of vulnerabilities detected on the asset.  # noqa: E501

        :return: The total of this InlineResponse20095.  # noqa: E501
        :rtype: int
        """
        return self._total

    @total.setter
    def total(self, total):
        """Sets the total of this InlineResponse20095.

        The total number of vulnerabilities detected on the asset.  # noqa: E501

        :param total: The total of this InlineResponse20095.  # noqa: E501
        :type total: int
        """

        self._total = total

    @property
    def fqdn(self):
        """Gets the fqdn of this InlineResponse20095.  # noqa: E501

        A list of fully-qualified domain names (FQDNs) for the asset.  # noqa: E501

        :return: The fqdn of this InlineResponse20095.  # noqa: E501
        :rtype: list[str]
        """
        return self._fqdn

    @fqdn.setter
    def fqdn(self, fqdn):
        """Sets the fqdn of this InlineResponse20095.

        A list of fully-qualified domain names (FQDNs) for the asset.  # noqa: E501

        :param fqdn: The fqdn of this InlineResponse20095.  # noqa: E501
        :type fqdn: list[str]
        """

        self._fqdn = fqdn

    @property
    def ipv4(self):
        """Gets the ipv4 of this InlineResponse20095.  # noqa: E501

        A list of ipv4 addresses for the asset.  # noqa: E501

        :return: The ipv4 of this InlineResponse20095.  # noqa: E501
        :rtype: list[str]
        """
        return self._ipv4

    @ipv4.setter
    def ipv4(self, ipv4):
        """Sets the ipv4 of this InlineResponse20095.

        A list of ipv4 addresses for the asset.  # noqa: E501

        :param ipv4: The ipv4 of this InlineResponse20095.  # noqa: E501
        :type ipv4: list[str]
        """

        self._ipv4 = ipv4

    @property
    def ipv6(self):
        """Gets the ipv6 of this InlineResponse20095.  # noqa: E501

        A list of ipv6 addresses for the asset.  # noqa: E501

        :return: The ipv6 of this InlineResponse20095.  # noqa: E501
        :rtype: list[str]
        """
        return self._ipv6

    @ipv6.setter
    def ipv6(self, ipv6):
        """Sets the ipv6 of this InlineResponse20095.

        A list of ipv6 addresses for the asset.  # noqa: E501

        :param ipv6: The ipv6 of this InlineResponse20095.  # noqa: E501
        :type ipv6: list[str]
        """

        self._ipv6 = ipv6

    @property
    def last_seen(self):
        """Gets the last_seen of this InlineResponse20095.  # noqa: E501

        The ISO timestamp of the scan that most recently detected the asset.  # noqa: E501

        :return: The last_seen of this InlineResponse20095.  # noqa: E501
        :rtype: str
        """
        return self._last_seen

    @last_seen.setter
    def last_seen(self, last_seen):
        """Sets the last_seen of this InlineResponse20095.

        The ISO timestamp of the scan that most recently detected the asset.  # noqa: E501

        :param last_seen: The last_seen of this InlineResponse20095.  # noqa: E501
        :type last_seen: str
        """

        self._last_seen = last_seen

    @property
    def netbios_name(self):
        """Gets the netbios_name of this InlineResponse20095.  # noqa: E501

        The NetBIOS name for the asset.  # noqa: E501

        :return: The netbios_name of this InlineResponse20095.  # noqa: E501
        :rtype: list[str]
        """
        return self._netbios_name

    @netbios_name.setter
    def netbios_name(self, netbios_name):
        """Sets the netbios_name of this InlineResponse20095.

        The NetBIOS name for the asset.  # noqa: E501

        :param netbios_name: The netbios_name of this InlineResponse20095.  # noqa: E501
        :type netbios_name: list[str]
        """

        self._netbios_name = netbios_name

    @property
    def agent_name(self):
        """Gets the agent_name of this InlineResponse20095.  # noqa: E501

        The names of any Nessus agents that scanned and identified the asset.  # noqa: E501

        :return: The agent_name of this InlineResponse20095.  # noqa: E501
        :rtype: list[str]
        """
        return self._agent_name

    @agent_name.setter
    def agent_name(self, agent_name):
        """Sets the agent_name of this InlineResponse20095.

        The names of any Nessus agents that scanned and identified the asset.  # noqa: E501

        :param agent_name: The agent_name of this InlineResponse20095.  # noqa: E501
        :type agent_name: list[str]
        """

        self._agent_name = agent_name

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20095):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20095):
            return True

        return self.to_dict() != other.to_dict()
