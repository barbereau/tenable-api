# InputErrorResponseValueStaticValueChangedInInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**given** | **str** | Given value. | 
**required** | **str** | Required value. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


