# ScanSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rrule** | **str** | A valid &#x60;rrule&#x60; string per RFC 5545. | 
**starttime** | **str** | A valid &#x60;dtstart&#x60; string per RFC 5545, minus TZID (see timezone below) if different than UTC, in which case use Z at the end. For example, &#x60;19970105T083000 or 19970105T083000Z (utc)&#x60;. | 
**timezone** | **str** | A valid &#x60;TZID&#x60; per RFC 5545. If empty this value defaults to UTC. For example, &#x60;America/New_York&#x60;. | [optional] 
**enabled** | **bool** | Indicates whether a schedule is enabled (&#x60;true&#x60;) or disabled (&#x60;false&#x60;). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


