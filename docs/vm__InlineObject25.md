# InlineObject25

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the folder. **Note:** Tenable.io does not allow the following special characters in folder names: &#x60;( ) [ ] : ; &#x3D; + / | \\ ? , ^ % &amp; $&#x60; | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


