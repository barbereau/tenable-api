# UserTemplateInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the user-defined template. | 
**description** | **str** | The description for the user-defined template. | [optional] 
**owner_id** | **str** | The UUID of the owner of the user-defined template. | 
**default_permissions** | [**PermissionLevel**](was__PermissionLevel.md) |  | 
**results_visibility** | [**ResultsVisibility**](was__ResultsVisibility.md) |  | 
**permissions** | [**list[Permissions]**](was__Permissions.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


