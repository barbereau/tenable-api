# ScansSettingsAcls

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**permissions** | **int** | The scan permission. For more information, see [Permissions](doc:permissions). | [optional] 
**owner** | **int** | A value that indicates whether the user or user group specified in the object owns the scan. Possible values include: &#x60;null&#x60; (system-owned permissions), &#x60;0&#x60; (the user is not the owner of the scan), &#x60;1&#x60; (the user is the owner of the scan). | [optional] 
**display_name** | **str** | The name of the user or group granted the specified permissions, as it appears in the Tenable.io user interface. | [optional] 
**name** | **str** | The name of the user or group granted the specified permissions. | [optional] 
**id** | **int** | A number representing the order in which the user or user groups display in the Permissions tab in the Tenable.io user interface. | [optional] 
**type** | **str** | The type of scan permissions: &#x60;default&#x60; (default permissions for the scan), &#x60;user&#x60; (permissions for an individual user), or &#x60;group&#x60; (permissions for a user group). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


