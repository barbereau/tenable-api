# tenableapi.vm.NetworksApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**io_networks_asset_count_details**](vm__NetworksApi.md#io_networks_asset_count_details) | **GET** /networks/{network_id}/assets-not-seen-in/{num_days} | Get network asset count
[**networks_assign_scanner**](vm__NetworksApi.md#networks_assign_scanner) | **POST** /networks/{network_id}/scanners/{scanner_uuid} | Assign scanners
[**networks_assign_scanner_bulk**](vm__NetworksApi.md#networks_assign_scanner_bulk) | **POST** /networks/{network_id}/scanners | Bulk assign scanners
[**networks_create**](vm__NetworksApi.md#networks_create) | **POST** /networks | Create network
[**networks_delete**](vm__NetworksApi.md#networks_delete) | **DELETE** /networks/{network_id} | Delete network
[**networks_details**](vm__NetworksApi.md#networks_details) | **GET** /networks/{network_id} | Get network details
[**networks_list**](vm__NetworksApi.md#networks_list) | **GET** /networks | List networks
[**networks_list_assignable_scanners**](vm__NetworksApi.md#networks_list_assignable_scanners) | **GET** /networks/{network_id}/assignable-scanners | List assignable scanners
[**networks_list_scanners**](vm__NetworksApi.md#networks_list_scanners) | **GET** /networks/{network_id}/scanners | List network scanners
[**networks_update**](vm__NetworksApi.md#networks_update) | **PUT** /networks/{network_id} | Update network


# **io_networks_asset_count_details**
> InlineResponse20044 io_networks_asset_count_details(network_id, num_days)

Get network asset count

Returns the total number of assets in the network along with the number of assets that have not been seen for the specified number of days.<p>Requires SCAN MANAGER [40] user permissions.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.NetworksApi(api_client)
    network_id = 'network_id_example' # str | The UUID of the network object for which you want to view details.
num_days = 56 # int | Return a count of assets that have not been seen for the specified number of days.<ul><li>Minimum value: 1</li><li>Maximum value: 365</li></ul> 

    try:
        # Get network asset count
        api_response = api_instance.io_networks_asset_count_details(network_id, num_days)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling NetworksApi->io_networks_asset_count_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| The UUID of the network object for which you want to view details. | 
 **num_days** | **int**| Return a count of assets that have not been seen for the specified number of days.&lt;ul&gt;&lt;li&gt;Minimum value: 1&lt;/li&gt;&lt;li&gt;Maximum value: 365&lt;/li&gt;&lt;/ul&gt;  | 

### Return type

[**InlineResponse20044**](vm__InlineResponse20044.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the total number of assets in the network along with the number of assets that have not been seen for the specified number of days. |  -  |
**400** | Returned if the parameters in your request were invalid. For example, if &#x60;num_days&#x60; is out of range (min: 1, max: 365). |  -  |
**404** | Returned if Tenable.io cannot find a network object with the specified UUID. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **networks_assign_scanner**
> object networks_assign_scanner(network_id, scanner_uuid)

Assign scanners

Associates a scanner or scanner group with a network object. Use this endpoint to:<ul><li>Assign a scanner or scanner group to a custom network object.</li><li>Return a scanner or scanner group to the default network object.</li></ul><p>Requires ADMINISTRATOR [64] user permissions.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.NetworksApi(api_client)
    network_id = 'network_id_example' # str | The UUID of the network object where you want to assign a scanner or scanner group.
scanner_uuid = 'scanner_uuid_example' # str | The UUID of the scanner or scanner group you want to assign to the network object. To get UUID values, use the [GET /networks/{network_id}/assignable-scanners](ref:networks-list-assignable-scanners) endpoint.

    try:
        # Assign scanners
        api_response = api_instance.networks_assign_scanner(network_id, scanner_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling NetworksApi->networks_assign_scanner: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| The UUID of the network object where you want to assign a scanner or scanner group. | 
 **scanner_uuid** | **str**| The UUID of the scanner or scanner group you want to assign to the network object. To get UUID values, use the [GET /networks/{network_id}/assignable-scanners](ref:networks-list-assignable-scanners) endpoint. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully assigned the scanner or scanner group to the network object. |  -  |
**400** | Returned if you attempt to assign an AWS scanner to the network object. For AWS assets, use network segmentation processes offered by AWS instead. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have sufficient permissions to assign a scanner or scanner group to the specified network object. |  -  |
**404** | Returned if Tenable.io could not find either the network object, the scanner object, or the scanner group object you specified. |  -  |
**409** | Returned if Tenable.io determines that assigning the scanner or scanner group causes a network conflict. A response message with this code includes a response body under the following conditions:   * The scanner you attempted to assign is a member of more than one scanner group.   * The scanner group you attempted to assign contains one or more scanners that belong to another scanner group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **networks_assign_scanner_bulk**
> object networks_assign_scanner_bulk(network_id, inline_object28)

Bulk assign scanners

Bulk assigns scanners and scanner groups to a custom network. The data in this request payload overwrites the full list of specified scanners and scanner groups previously assigned to the specified network object. If the payload excludes scanners or scanner groups previously assigned to the specified network object, Tenable.io returns those scanners and scanner groups to the default network. You cannot use this endpoint to bulk return scanners and scanner groups from multiple custom networks to the default network.<p>Requires ADMINISTRATOR [64] user permissions.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.NetworksApi(api_client)
    network_id = 'network_id_example' # str | The UUID of the network object where you want to bulk assign scanners and scanner groups.
inline_object28 = tenableapi.vm.InlineObject28() # InlineObject28 | 

    try:
        # Bulk assign scanners
        api_response = api_instance.networks_assign_scanner_bulk(network_id, inline_object28)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling NetworksApi->networks_assign_scanner_bulk: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| The UUID of the network object where you want to bulk assign scanners and scanner groups. | 
 **inline_object28** | [**InlineObject28**](vm__InlineObject28.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully assigned the scanners and scanner groups to the network object. |  -  |
**400** | Returned if you attempt to assign an AWS scanner to the network object. For AWS assets, use network segmentation processes offered by AWS instead. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have sufficient permissions to assign a scanner or scanner group to the specified network object. |  -  |
**404** | Returned if Tenable.io could not find either the network object, the scanner object, or the scanner group object you specified. |  -  |
**409** | Returned if Tenable.io determines that assigning the scanner or scanner group causes a network conflict. A response message with this code includes a response body under the following conditions:   * A scanner you attempted to assign is a member of more than one scanner group.   * A scanner group you attempted to assign contains one or more scanners that belong to another scanner group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **networks_create**
> InlineResponse20043Networks networks_create(inline_object26)

Create network

Creates a network object that you associate with scanners and scanner groups.   **Note:** You cannot add AWS assets to network objects. For AWS assets, use the network segmentation provided by AWS instead.<p>Requires ADMINISTRATOR [64] user permissions.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.NetworksApi(api_client)
    inline_object26 = tenableapi.vm.InlineObject26() # InlineObject26 | 

    try:
        # Create network
        api_response = api_instance.networks_create(inline_object26)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling NetworksApi->networks_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object26** | [**InlineObject26**](vm__InlineObject26.md)|  | 

### Return type

[**InlineResponse20043Networks**](vm__InlineResponse20043Networks.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates a network object. |  -  |
**400** | Returned if Tenable.io encounters invalid JSON in the request body or if an invalid &#x60;assets_ttl_days&#x60; value was declared (min: 90, max: 365). |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have permission to create network objects. |  -  |
**409** | Returned if a network object with the same name already exists. |  -  |
**415** | Returned if the request payload is in an unsupported format. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **networks_delete**
> object networks_delete(network_id)

Delete network

Deletes the specified network object. Before you delete a network object, consider moving assets to a different network using the bulk asset move endpoint ([POST /api/v2/assets/bulk-jobs/move-to-network](ref:assets-bulk-move)). If you delete a network object, Tenable.io:  - Returns the scanners and scanner groups associated with the deleted object to the default network object.  - Retains any asset records for the deleted network until the assets age out of your licensed assets count.   **Note:** You can view deleted network objects using the `includeDeleted` filter in a [GET /networks](ref:networks-list) request. <p>Requires ADMINISTRATOR [64] user permissions.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.NetworksApi(api_client)
    network_id = 'network_id_example' # str | UUID for the network object you want to delete. You cannot delete the default network object.

    try:
        # Delete network
        api_response = api_instance.networks_delete(network_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling NetworksApi->networks_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| UUID for the network object you want to delete. You cannot delete the default network object. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deleted the specified network object. |  -  |
**400** | Returned if you attempted to delete the default network object. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have sufficient permissions to delete the specified network object. |  -  |
**404** | Returned if Tenable.io could not find the network object you specified. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **networks_details**
> InlineResponse20043Networks networks_details(network_id)

Get network details

Returns the details of the specified network object.<p>Requires ADMINISTRATOR [64] user permissions.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.NetworksApi(api_client)
    network_id = 'network_id_example' # str | The UUID of the network object for which you want to view details.

    try:
        # Get network details
        api_response = api_instance.networks_details(network_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling NetworksApi->networks_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| The UUID of the network object for which you want to view details. | 

### Return type

[**InlineResponse20043Networks**](vm__InlineResponse20043Networks.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the details of the specified network object. |  -  |
**404** | Returned if Tenable.io cannot find a network object with the specified UUID. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **networks_list**
> InlineResponse20043 networks_list(f=f, ft=ft, limit=limit, offset=offset, sort=sort, include_deleted=include_deleted)

List networks

Lists network objects for your organization.<p>Requires ADMINISTRATOR [64] user permissions.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.NetworksApi(api_client)
    f = 'f_example' # str | A filter condition in the following format: `field:operator:value`. For network objects, you can only filter on the `name` field, using the following operators:  * eq—The name of the returned network object is equal to the text you specify.  * neq—The returned list of network objects excludes the network object where the name is equal to the text you specify.  * match—The returned list includes network objects where the name contains the text you specify at least partially.  You can specify multiple `f` parameters, separated by ampersand (&) characters. If you specify multiple `f` parameters, use the `ft` parameter to specify how Tenable.io applies the multiple filter conditions. (optional)
ft = 'ft_example' # str | The operator that Tenable.io applies if multiple \\`f\\` parameters are present. The `OR` operator is the only supported value. If you omit this parameter and multiple `f` parameters are present, Tenable.io applies the `OR` operator by default. (optional)
limit = 56 # int | Maximum number of objects requested (or service imposed limit if not in request). (optional)
offset = 56 # int | Offset from request (or zero). (optional)
sort = [tenableapi.vm.Sort()] # list[Sort] | Objects that specify the sort order for the returned data. (optional)
include_deleted = True # bool | Indicates whether Tenable.io includes deleted network objects in the response message. Deleted network objects contain the additional attributes, `deleted` and `deleted_by`, which specifies the date (in Unix time) when the network object was deleted and the UUID of the user that deleted the network object. (optional)

    try:
        # List networks
        api_response = api_instance.networks_list(f=f, ft=ft, limit=limit, offset=offset, sort=sort, include_deleted=include_deleted)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling NetworksApi->networks_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **f** | **str**| A filter condition in the following format: &#x60;field:operator:value&#x60;. For network objects, you can only filter on the &#x60;name&#x60; field, using the following operators:  * eq—The name of the returned network object is equal to the text you specify.  * neq—The returned list of network objects excludes the network object where the name is equal to the text you specify.  * match—The returned list includes network objects where the name contains the text you specify at least partially.  You can specify multiple &#x60;f&#x60; parameters, separated by ampersand (&amp;) characters. If you specify multiple &#x60;f&#x60; parameters, use the &#x60;ft&#x60; parameter to specify how Tenable.io applies the multiple filter conditions. | [optional] 
 **ft** | **str**| The operator that Tenable.io applies if multiple \\&#x60;f\\&#x60; parameters are present. The &#x60;OR&#x60; operator is the only supported value. If you omit this parameter and multiple &#x60;f&#x60; parameters are present, Tenable.io applies the &#x60;OR&#x60; operator by default. | [optional] 
 **limit** | **int**| Maximum number of objects requested (or service imposed limit if not in request). | [optional] 
 **offset** | **int**| Offset from request (or zero). | [optional] 
 **sort** | [**list[Sort]**](vm__Sort.md)| Objects that specify the sort order for the returned data. | [optional] 
 **include_deleted** | **bool**| Indicates whether Tenable.io includes deleted network objects in the response message. Deleted network objects contain the additional attributes, &#x60;deleted&#x60; and &#x60;deleted_by&#x60;, which specifies the date (in Unix time) when the network object was deleted and the UUID of the user that deleted the network object. | [optional] 

### Return type

[**InlineResponse20043**](vm__InlineResponse20043.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of network objects. |  -  |
**400** | Returned if the query parameters in your request were invalid. |  -  |
**403** | Returned if you do not have sufficient permissions to list network objects. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **networks_list_assignable_scanners**
> InlineResponse20045 networks_list_assignable_scanners(network_id)

List assignable scanners

Lists all scanners and scanner groups not yet assigned to a custom network object. This list includes all scanner groups within the default network AND any scanners within the default network that do not belong to a scanner group.<p>Requires ADMINISTRATOR [64] user permissions.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.NetworksApi(api_client)
    network_id = 'network_id_example' # str | The UUID of the default network object.

    try:
        # List assignable scanners
        api_response = api_instance.networks_list_assignable_scanners(network_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling NetworksApi->networks_list_assignable_scanners: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| The UUID of the default network object. | 

### Return type

[**InlineResponse20045**](vm__InlineResponse20045.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of assignable scanners and scanner groups in the default network object. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have sufficient permissions to list scanners and scanner groups for the default network object. |  -  |
**404** | Returned if Tenable.io could not find the network object you specified. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **networks_list_scanners**
> InlineResponse20045 networks_list_scanners(network_id)

List network scanners

Lists all scanners and scanner groups belonging to the specified network object.<p>Requires ADMINISTRATOR [64] user permissions.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.NetworksApi(api_client)
    network_id = 'network_id_example' # str | The UUID of the network object for which you want to list assigned scanners and scanner groups.

    try:
        # List network scanners
        api_response = api_instance.networks_list_scanners(network_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling NetworksApi->networks_list_scanners: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| The UUID of the network object for which you want to list assigned scanners and scanner groups. | 

### Return type

[**InlineResponse20045**](vm__InlineResponse20045.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of scanners and scanner groups for the specified network object. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have sufficient permissions to list scanners and scanner groups for the specified network object. |  -  |
**404** | Returned if Tenable.io could not find the network object you specified. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **networks_update**
> InlineResponse20043Networks networks_update(network_id, inline_object27)

Update network

Updates the name or description of a network object.  **Note:** The `name` body parameter is required when updating the specified network with this endpoint. To retrieve the `name`, use the [GET /networks/{network_id}](ref:networks-details) endpoint.<p>Requires ADMINISTRATOR [64] user permissions.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.NetworksApi(api_client)
    network_id = 'network_id_example' # str | The UUID of the network object you want to update. You cannot update the default network object.
inline_object27 = tenableapi.vm.InlineObject27() # InlineObject27 | 

    try:
        # Update network
        api_response = api_instance.networks_update(network_id, inline_object27)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling NetworksApi->networks_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| The UUID of the network object you want to update. You cannot update the default network object. | 
 **inline_object27** | [**InlineObject27**](vm__InlineObject27.md)|  | 

### Return type

[**InlineResponse20043Networks**](vm__InlineResponse20043Networks.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns successfully updated details of the network object. |  -  |
**400** | Returned if Tenable.io encounters invalid JSON in the request body, if you attempted to change the default network object, or if an invalid &#x60;assets_ttl_days&#x60; value was declared (min: 90, max: 365). |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have permission to update the specified network object. |  -  |
**404** | Returned if Tenable.io could not find the specified network object, either because the object does not exist or because the object has been deleted. |  -  |
**409** | Returned if a network object with the same name already exists. |  -  |
**415** | Returned if the request payload is in an unsupported format. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

