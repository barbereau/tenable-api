# tenableapi.waslegacy.PoliciesApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_policies_configure**](waslegacy__PoliciesApi.md#was_policies_configure) | **PUT** /policies/{policy_id} | Update policy
[**was_policies_copy**](waslegacy__PoliciesApi.md#was_policies_copy) | **POST** /policies/{policy_id}/copy | Copy policy
[**was_policies_create**](waslegacy__PoliciesApi.md#was_policies_create) | **POST** /policies | Create policy
[**was_policies_delete**](waslegacy__PoliciesApi.md#was_policies_delete) | **DELETE** /policies/{policy_id} | Delete policy
[**was_policies_details**](waslegacy__PoliciesApi.md#was_policies_details) | **GET** /policies/{policy_id} | List policy details
[**was_policies_export**](waslegacy__PoliciesApi.md#was_policies_export) | **GET** /policies/{policy_id}/export | Export policy
[**was_policies_import**](waslegacy__PoliciesApi.md#was_policies_import) | **POST** /policies/import | Import policy
[**was_policies_list**](waslegacy__PoliciesApi.md#was_policies_list) | **GET** /policies | List policies


# **was_policies_configure**
> object was_policies_configure(policy_id)

Update policy

Updates the parameters of a policy.<p>Requires CAN EDIT [32] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.PoliciesApi(api_client)
    policy_id = 56 # int | The ID of the policy to change.

    try:
        # Update policy
        api_response = api_instance.was_policies_configure(policy_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PoliciesApi->was_policies_configure: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **policy_id** | **int**| The ID of the policy to change. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io changed the policy configuration. |  -  |
**404** | Returned if the policy does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an error while saving the configuration. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_policies_copy**
> object was_policies_copy(policy_id)

Copy policy

Copies a policy.<p>Requires CAN EDIT [32] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.PoliciesApi(api_client)
    policy_id = 56 # int | The ID of the policy to copy.

    try:
        # Copy policy
        api_response = api_instance.was_policies_copy(policy_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PoliciesApi->was_policies_copy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **policy_id** | **int**| The ID of the policy to copy. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the policy object with the ID and name properties set. |  -  |
**403** | Returned if the user does not have permission to copy the policy. |  -  |
**404** | Returned if the policy does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io failed to copy the policy. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_policies_create**
> InlineResponse20011 was_policies_create(inline_object5)

Create policy

Creates a policy.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.PoliciesApi(api_client)
    inline_object5 = tenableapi.waslegacy.InlineObject5() # InlineObject5 | 

    try:
        # Create policy
        api_response = api_instance.was_policies_create(inline_object5)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PoliciesApi->was_policies_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object5** | [**InlineObject5**](waslegacy__InlineObject5.md)|  | 

### Return type

[**InlineResponse20011**](waslegacy__InlineResponse20011.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io saved the policy successfully. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an error while saving the policy. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_policies_delete**
> object was_policies_delete(policy_id)

Delete policy

Deletes a policy.<p>Requires CAN EDIT [32] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.PoliciesApi(api_client)
    policy_id = 56 # int | The ID of the policy to delete.

    try:
        # Delete policy
        api_response = api_instance.was_policies_delete(policy_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PoliciesApi->was_policies_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **policy_id** | **int**| The ID of the policy to delete. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io deleted the policy. |  -  |
**403** | Returned if the user does not have permission to delete the policy. |  -  |
**404** | Returned if the policy does not exist. |  -  |
**405** | Returned if the policy is in use by a scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_policies_details**
> InlineResponse20012 was_policies_details(policy_id)

List policy details

Returns the details for the given policy.<p>Requires CAN USE [32] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.PoliciesApi(api_client)
    policy_id = 56 # int | The ID of the policy to retrieve.

    try:
        # List policy details
        api_response = api_instance.was_policies_details(policy_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PoliciesApi->was_policies_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **policy_id** | **int**| The ID of the policy to retrieve. | 

### Return type

[**InlineResponse20012**](waslegacy__InlineResponse20012.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the policy details. This response can be edited and passed directly to the [PUT /policies/{policy_id}](ref:was-policies-configure) endpoint. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_policies_export**
> object was_policies_export(policy_id)

Export policy

Exports the given policy.<p>Requires CAN EDIT [32] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.PoliciesApi(api_client)
    policy_id = 56 # int | The ID of the policy to export.

    try:
        # Export policy
        api_response = api_instance.was_policies_export(policy_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PoliciesApi->was_policies_export: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **policy_id** | **int**| The ID of the policy to export. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the policy in nessus (XML) format. |  -  |
**403** | Returned if the user does not have permission to export the policy. |  -  |
**404** | Returned if the policy does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_policies_import**
> InlineResponse20013 was_policies_import(inline_object6)

Import policy

Imports an existing policy uploaded using POST /file/upload (.nessus format only).<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.PoliciesApi(api_client)
    inline_object6 = tenableapi.waslegacy.InlineObject6() # InlineObject6 | 

    try:
        # Import policy
        api_response = api_instance.was_policies_import(inline_object6)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PoliciesApi->was_policies_import: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object6** | [**InlineObject6**](waslegacy__InlineObject6.md)|  | 

### Return type

[**InlineResponse20013**](waslegacy__InlineResponse20013.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the policy object. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if the server failed to import the policy. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_policies_list**
> list[InlineResponse20010] was_policies_list()

List policies

Returns a list of policies, including non-WAS policies.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.PoliciesApi(api_client)
    
    try:
        # List policies
        api_response = api_instance.was_policies_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PoliciesApi->was_policies_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse20010]**](waslegacy__InlineResponse20010.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the policy list. |  -  |
**403** | Returned if the user does not have permission to view the policy list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

