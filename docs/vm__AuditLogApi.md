# tenableapi.vm.AuditLogApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**audit_log_events**](vm__AuditLogApi.md#audit_log_events) | **GET** /audit-log/v1/events | View audit log


# **audit_log_events**
> list[InlineResponse20017] audit_log_events(f=f, limit=limit)

View audit log

This endpoint requests a list of events. Events can include the following:  - audit.log.view—The system received and processed an audit-log request.  - session.create—The system created a session for the user. This event can be triggered by user login or authentication using an API key.  - session.delete—The session expired, or the user ended the session.  - session.impersonation.end—An administrator ended a session where they impersonated another user.  - session.impersonation.start—An administrator started a session where they impersonated another user.  - user.authenticate.api-keys—The user authenticated a session start using an API key.  - user.authenticate.mfa—The two-factor authentication challenge was successful, and login allowed.  - user.authenticate.password—The user authenticated a session start using a password.  - user.create—An administrator created a new user account.  - user.delete—An administrator deleted the user account.  - user.impersonation.end—An administrator stopped impersonating another user.  - user.impersonation.start—An administrator started impersonating another user.  - user.logout—The user logged out of the session.  - user.update—Either an administrator or the user updated the user account.  You can specify various filters to limit the events that are returned, as well as the number of events. By default, a maximum of 50 events is returned.  **Note:** If you configure SSO authentication, Tenable.io does not log user actions to the audit log. This information may be available from the identity services provider you use. For more information, see [SSO Authentication](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/ConfigureSSO.htm).<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AuditLogApi(api_client)
    f = 'f_example' # str | A filter condition in the `field.operator:value` format. Filter conditions can include: * date.gt:&lt;YYYY-MM-DD>—Tenable.io returns events only if the date when the events occurred is after the date you specify. For example: `f=date.gt:2017-12-31` * date.lt:&lt;YYYY-MM-DD>—Tenable.io returns events only if the date when the events occurred is before the date you specify. For example: `f=date.lt:2017-12-31` * actor_id.match:&lt;UUID>—Tenable.io returns only the events with a matching actor UUID. For example: `f=actor_id.match:6000a811-8422-4096-83d3-e4d44f44b97d` * target_id.match:&lt;UUID>—Tenable.io returns only the events with a matching target UUID. For example: `f=target_id.match:6000a811-8422-4096-83d3-e4d44f44b97d`  You can specify multiple `f` parameters, separated by ampersand (&) characters. For example: `?f=date.gt:2018-12-31&f=date.lt:2019-12-31&f=actor_id.match:50f84b7f-d1d3-4182-bb46-79cf5c51812e&limit=5000` (optional)
limit = 56 # int | Sets the limit for how many events Tenable.io should return by the call. By default, this value is 50. For example: `limit=5000` (optional)

    try:
        # View audit log
        api_response = api_instance.audit_log_events(f=f, limit=limit)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AuditLogApi->audit_log_events: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **f** | **str**| A filter condition in the &#x60;field.operator:value&#x60; format. Filter conditions can include: * date.gt:&amp;lt;YYYY-MM-DD&gt;—Tenable.io returns events only if the date when the events occurred is after the date you specify. For example: &#x60;f&#x3D;date.gt:2017-12-31&#x60; * date.lt:&amp;lt;YYYY-MM-DD&gt;—Tenable.io returns events only if the date when the events occurred is before the date you specify. For example: &#x60;f&#x3D;date.lt:2017-12-31&#x60; * actor_id.match:&amp;lt;UUID&gt;—Tenable.io returns only the events with a matching actor UUID. For example: &#x60;f&#x3D;actor_id.match:6000a811-8422-4096-83d3-e4d44f44b97d&#x60; * target_id.match:&amp;lt;UUID&gt;—Tenable.io returns only the events with a matching target UUID. For example: &#x60;f&#x3D;target_id.match:6000a811-8422-4096-83d3-e4d44f44b97d&#x60;  You can specify multiple &#x60;f&#x60; parameters, separated by ampersand (&amp;) characters. For example: &#x60;?f&#x3D;date.gt:2018-12-31&amp;f&#x3D;date.lt:2019-12-31&amp;f&#x3D;actor_id.match:50f84b7f-d1d3-4182-bb46-79cf5c51812e&amp;limit&#x3D;5000&#x60; | [optional] 
 **limit** | **int**| Sets the limit for how many events Tenable.io should return by the call. By default, this value is 50. For example: &#x60;limit&#x3D;5000&#x60; | [optional] 

### Return type

[**list[InlineResponse20017]**](vm__InlineResponse20017.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the audit log. |  -  |
**403** | Returned if you do not have permission to view the audit log. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

