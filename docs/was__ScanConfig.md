# ScanConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings** | [**ConfigSettings**](was__ConfigSettings.md) |  | 
**config_id** | **str** | The UUID of the scan configuration. | 
**container_id** | **str** | The UUID of your organization&#39;s Tenable.io Web Application Scanning instance. | 
**owner_id** | **str** | The UUID of owner of the scan configuration. | 
**template_id** | **str** | The UUID of the Tenable-provided configuration template from which this configuration was derived. | [optional] 
**user_template_id** | **str** | The UUID of the user-defined template resource. | [optional] [readonly] 
**name** | **str** | The name of the scan configuration. | 
**target** | **str** | The URL of the target web application. | 
**description** | **str** | The description of the scan configuration. | [optional] 
**created_at** | **datetime** | An ISO timestamp indicating the date and time when the scan configuration was created, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | 
**updated_at** | **datetime** | An ISO timestamp indicating the date and time when the scan configuration was last updated, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | 
**scanner_id** | **int** | The ID of the scanner (if the &#x60;type&#x60; is set to &#x60;managed_webapp&#x60;), or scanner group (if the type is &#x60;pool&#x60; or &#x60;local&#x60;) that performs the scan. | 
**schedule** | [**ScanSchedule**](was__ScanSchedule.md) |  | [optional] 
**default_permissions** | [**PermissionLevel**](was__PermissionLevel.md) |  | 
**results_visibility** | [**ResultsVisibility**](was__ResultsVisibility.md) |  | 
**permissions** | [**list[Permissions]**](was__Permissions.md) |  | 
**notifications** | [**Notifications**](was__Notifications.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


