# tenableapi.vm.ExportsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**exports_assets_download_chunk**](vm__ExportsApi.md#exports_assets_download_chunk) | **GET** /assets/export/{export_uuid}/chunks/{chunk_id} | Download assets chunk
[**exports_assets_export_cancel**](vm__ExportsApi.md#exports_assets_export_cancel) | **POST** /assets/export/{export_uuid}/cancel | Cancel asset export
[**exports_assets_export_status**](vm__ExportsApi.md#exports_assets_export_status) | **GET** /assets/export/{export_uuid}/status | Get assets export status
[**exports_assets_export_status_recent**](vm__ExportsApi.md#exports_assets_export_status_recent) | **GET** /assets/export/status | Get asset export jobs
[**exports_assets_request_export**](vm__ExportsApi.md#exports_assets_request_export) | **POST** /assets/export | Export assets
[**exports_vulns_download_chunk**](vm__ExportsApi.md#exports_vulns_download_chunk) | **GET** /vulns/export/{export_uuid}/chunks/{chunk_id} | Download vulnerabilities chunk
[**exports_vulns_export_cancel**](vm__ExportsApi.md#exports_vulns_export_cancel) | **POST** /vulns/export/{export_uuid}/cancel | Cancel vuln export
[**exports_vulns_export_status**](vm__ExportsApi.md#exports_vulns_export_status) | **GET** /vulns/export/{export_uuid}/status | Get vulnerabilities export status
[**exports_vulns_export_status_recent**](vm__ExportsApi.md#exports_vulns_export_status_recent) | **GET** /vulns/export/status | Get vuln export jobs
[**exports_vulns_request_export**](vm__ExportsApi.md#exports_vulns_request_export) | **POST** /vulns/export | Export vulnerabilities


# **exports_assets_download_chunk**
> InlineResponse20037 exports_assets_download_chunk(export_uuid, chunk_id)

Download assets chunk

Downloads exported assets chunk by ID. Tenable.io processes the chunks in parallel, so chunk IDs may not be arranged sequentially in the completed output. Chunks are available for download for up to 24 hours after they have been created. Tenable.io returns a 404 message for expired chunks. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExportsApi(api_client)
    export_uuid = 'export_uuid_example' # str | The UUID of the export request.
chunk_id = 56 # int | The ID of the asset chunk you want to export.

    try:
        # Download assets chunk
        api_response = api_instance.exports_assets_download_chunk(export_uuid, chunk_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExportsApi->exports_assets_download_chunk: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **export_uuid** | **str**| The UUID of the export request. | 
 **chunk_id** | **int**| The ID of the asset chunk you want to export. | 

### Return type

[**InlineResponse20037**](vm__InlineResponse20037.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the file is downloaded successfully. The response body excludes an attribute if the attribute is empty in the asset record. |  -  |
**400** | Returned if the chunk ID is invalid or the chunk is not ready for download. |  -  |
**403** | Returned if you do not have permission to export assets. |  -  |
**404** | Returned if Tenable.io cannot find a chunk with the specified UUID, or if the chunk with the specified UUID has expired. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exports_assets_export_cancel**
> InlineResponse20033 exports_assets_export_cancel(export_uuid)

Cancel asset export

Cancels the specified export job. If you cancel an export job, Tenable.io finishes any chunk that is currently processing, terminates the processing of any unprocessed chunks, and updates the job status to `CANCELLED`. If a cancelled job includes completed chunks, you can download those chunks for three days after cancellation.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExportsApi(api_client)
    export_uuid = 'export_uuid_example' # str | The UUID for the export request.

    try:
        # Cancel asset export
        api_response = api_instance.exports_assets_export_cancel(export_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExportsApi->exports_assets_export_cancel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **export_uuid** | **str**| The UUID for the export request. | 

### Return type

[**InlineResponse20033**](vm__InlineResponse20033.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully cancels the specified export request. |  -  |
**400** | Returned if your request message is invalid. |  -  |
**404** | Returned if Tenable.io cannot find an export job with the specified UUID. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exports_assets_export_status**
> InlineResponse20036 exports_assets_export_status(export_uuid)

Get assets export status

Returns the status of an assets export request. Tenable.io processes the chunks in parallel, so the chunks may not complete in order. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExportsApi(api_client)
    export_uuid = 'export_uuid_example' # str | The UUID for the export request.

    try:
        # Get assets export status
        api_response = api_instance.exports_assets_export_status(export_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExportsApi->exports_assets_export_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **export_uuid** | **str**| The UUID for the export request. | 

### Return type

[**InlineResponse20036**](vm__InlineResponse20036.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the status of the specified export job. |  -  |
**403** | Returned if you do not have permission to view the export status. |  -  |
**404** | Returned if Tenable.io cannot find an export with the specified UUID. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exports_assets_export_status_recent**
> InlineResponse20034 exports_assets_export_status_recent()

Get asset export jobs

Retrieves a list of asset export jobs. This list includes the 1,000 most recent export jobs regardless of status. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExportsApi(api_client)
    
    try:
        # Get asset export jobs
        api_response = api_instance.exports_assets_export_status_recent()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExportsApi->exports_assets_export_status_recent: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20034**](vm__InlineResponse20034.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of recent asset export jobs. |  -  |
**400** | Returned if your request message is invalid. |  -  |
**403** | Returned if you do not have permissions for the request. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exports_assets_request_export**
> InlineResponse20035 exports_assets_request_export(inline_object22)

Export assets

Exports all assets that match the request criteria.  **Important!** For more information on using this endpoint, see guidelines and limitations described in [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExportsApi(api_client)
    inline_object22 = tenableapi.vm.InlineObject22() # InlineObject22 | 

    try:
        # Export assets
        api_response = api_instance.exports_assets_request_export(inline_object22)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExportsApi->exports_assets_request_export: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object22** | [**InlineObject22**](vm__InlineObject22.md)|  | 

### Return type

[**InlineResponse20035**](vm__InlineResponse20035.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully queues an export request. |  -  |
**400** | Returned if your request message contains any invalid filters or is itself invalid. |  -  |
**403** | Returned if you do not have permission to export assets. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exports_vulns_download_chunk**
> InlineResponse20032 exports_vulns_download_chunk(export_uuid, chunk_id)

Download vulnerabilities chunk

Downloads exported vulnerabilities chunk by ID as a JSON file. The response content type is `application/octet-stream`.  Chunks are available for download for up to 24 hours after they have been created. Tenable.io returns a 404 message for expired chunks. Tenable.io processes the chunks in parallel, so chunk IDs may not be arranged sequentially in the completed output. Export chunks do not include an attribute if that attribute is empty in the vulnerability record.  A successful response message contains attributes that correspond to CVSS metrics; these metrics are described fully in the following documents:<ul><li>[A Complete Guide to the Common Vulnerability Scoring System, Version 2.0](https://www.first.org/cvss/v2/guide)</li><li>[Common Vulnerability Scoring System v3.0: Specification Document](https://www.first.org/cvss/specification-document)</li></ul><p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExportsApi(api_client)
    export_uuid = 'export_uuid_example' # str | The UUID of the export request.
chunk_id = 56 # int | The ID of the chunk you want to export.

    try:
        # Download vulnerabilities chunk
        api_response = api_instance.exports_vulns_download_chunk(export_uuid, chunk_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExportsApi->exports_vulns_download_chunk: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **export_uuid** | **str**| The UUID of the export request. | 
 **chunk_id** | **int**| The ID of the chunk you want to export. | 

### Return type

[**InlineResponse20032**](vm__InlineResponse20032.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if file is downloaded successfully. |  -  |
**400** | Returned if the chunk ID is invalid or the chunk is not ready for download. |  -  |
**403** | Returned if you do not have permission to export vulnerabilities. |  -  |
**404** | Returned if Tenable.io cannot find a chunk with the specified UUID. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exports_vulns_export_cancel**
> InlineResponse20033 exports_vulns_export_cancel(export_uuid)

Cancel vuln export

Cancels the specified export job. If you cancel an export job, Tenable.io finishes any chunk that is currently processing, terminates the processing of any unprocessed chunks, and updates the job status to `CANCELLED`. If a cancelled job includes completed chunks, you can download those chunks for three days after cancellation. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExportsApi(api_client)
    export_uuid = 'export_uuid_example' # str | The UUID for the export request.

    try:
        # Cancel vuln export
        api_response = api_instance.exports_vulns_export_cancel(export_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExportsApi->exports_vulns_export_cancel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **export_uuid** | **str**| The UUID for the export request. | 

### Return type

[**InlineResponse20033**](vm__InlineResponse20033.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully cancels the specified export request. |  -  |
**400** | Returned if Tenable.io cannot cancel the request. |  -  |
**401** | Returned if Tenable.io cannot find an export job for the specified UUID. |  -  |
**403** | Returned if you do not have permission to cancel export jobs. |  -  |
**404** | Returned if Tenable.io cannot find an export with the specified UUID. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exports_vulns_export_status**
> InlineResponse20031 exports_vulns_export_status(export_uuid)

Get vulnerabilities export status

Returns the status of a vulnerability export request. Tenable.io processes the chunks in parallel, so the chunks may not complete in order.   **Note:** Output for an individual plugin is limited to 1,024 KB (1 MB). <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExportsApi(api_client)
    export_uuid = 'export_uuid_example' # str | The UUID for the export request.

    try:
        # Get vulnerabilities export status
        api_response = api_instance.exports_vulns_export_status(export_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExportsApi->exports_vulns_export_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **export_uuid** | **str**| The UUID for the export request. | 

### Return type

[**InlineResponse20031**](vm__InlineResponse20031.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the status of the specified export job. |  -  |
**403** | Returned if you do not have permission to view the export status. |  -  |
**404** | Returned if Tenable.io cannot find an export job with the specified UUID. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exports_vulns_export_status_recent**
> InlineResponse20034 exports_vulns_export_status_recent()

Get vuln export jobs

Retrieves a list of vulnerability export jobs. This list includes the 1,000 most recent export jobs regardless of status. However, this list includes completed jobs only if the job completed in the previous three days. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExportsApi(api_client)
    
    try:
        # Get vuln export jobs
        api_response = api_instance.exports_vulns_export_status_recent()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExportsApi->exports_vulns_export_status_recent: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20034**](vm__InlineResponse20034.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of recent vulnerability export jobs. |  -  |
**400** | Returned if your request message is invalid. |  -  |
**403** | Returned if you do not have permissions for the request. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exports_vulns_request_export**
> InlineResponse20030 exports_vulns_request_export(inline_object21)

Export vulnerabilities

Exports vulnerabilities that match the request criteria.   **Important!** For more information on using this endpoint, see guidelines and limitations described in [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio).<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExportsApi(api_client)
    inline_object21 = tenableapi.vm.InlineObject21() # InlineObject21 | 

    try:
        # Export vulnerabilities
        api_response = api_instance.exports_vulns_request_export(inline_object21)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExportsApi->exports_vulns_request_export: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object21** | [**InlineObject21**](vm__InlineObject21.md)|  | 

### Return type

[**InlineResponse20030**](vm__InlineResponse20030.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully queues an export request. |  -  |
**400** | Returned if your request message contains an invalid filter or a host.target filter that exceeds supported limits. |  -  |
**403** | Returned if you do not have permission to export vulnerabilities. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

