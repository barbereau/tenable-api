# InlineResponse20032CvssTemporalVector

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exploitability** | **str** | The CVSSv2 Exploitability (E) temporal metric for the vulnerability the plugin covers. Possible values include:  - U—Unproven  - POC—Proof-of-Concept  - F—Functional  - H—High  - ND—Not Defined | [optional] 
**remediation_level** | **str** | The CVSSv2 Remediation Level (RL) temporal metric for the vulnerability the plugin covers. Possible values include:   - OF—Official Fix  - TF—Temporary Fix  - W—Workaround  - U—Unavailable  - ND—Not Defined | [optional] 
**report_confidence** | **str** | The CVSSv2 Report Confidence (RC) temporal metric for the vulnerability the plugin covers. Possible values include:   - UC—Unconfirmed  - UR—Uncorroborated  - C—Confirmed  - ND—Not Defined | [optional] 
**raw** | **str** | The complete &#x60;cvss_temporal_vector&#x60; metrics and result values for the vulnerability the plugin covers in a condensed and coded format. For example, &#x60;E:U/RL:OF/RC:C&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


