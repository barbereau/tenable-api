# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20023Credentials(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'id': 'str',
        'category': 'str',
        'default_expand': 'bool',
        'types': 'list[InlineResponse20023Types]'
    }

    attribute_map = {
        'id': 'id',
        'category': 'category',
        'default_expand': 'default_expand',
        'types': 'types'
    }

    def __init__(self, id=None, category=None, default_expand=None, types=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20023Credentials - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._id = None
        self._category = None
        self._default_expand = None
        self._types = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if category is not None:
            self.category = category
        if default_expand is not None:
            self.default_expand = default_expand
        if types is not None:
            self.types = types

    @property
    def id(self):
        """Gets the id of this InlineResponse20023Credentials.  # noqa: E501

        The system name that uniquely identifies the category in Tenable.io.  # noqa: E501

        :return: The id of this InlineResponse20023Credentials.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20023Credentials.

        The system name that uniquely identifies the category in Tenable.io.  # noqa: E501

        :param id: The id of this InlineResponse20023Credentials.  # noqa: E501
        :type id: str
        """

        self._id = id

    @property
    def category(self):
        """Gets the category of this InlineResponse20023Credentials.  # noqa: E501

        The display name for the category in the user interface.  # noqa: E501

        :return: The category of this InlineResponse20023Credentials.  # noqa: E501
        :rtype: str
        """
        return self._category

    @category.setter
    def category(self, category):
        """Sets the category of this InlineResponse20023Credentials.

        The display name for the category in the user interface.  # noqa: E501

        :param category: The category of this InlineResponse20023Credentials.  # noqa: E501
        :type category: str
        """

        self._category = category

    @property
    def default_expand(self):
        """Gets the default_expand of this InlineResponse20023Credentials.  # noqa: E501

        A value specifying whether the list of credential types in the category appears as expanded by default in the user interface.  # noqa: E501

        :return: The default_expand of this InlineResponse20023Credentials.  # noqa: E501
        :rtype: bool
        """
        return self._default_expand

    @default_expand.setter
    def default_expand(self, default_expand):
        """Sets the default_expand of this InlineResponse20023Credentials.

        A value specifying whether the list of credential types in the category appears as expanded by default in the user interface.  # noqa: E501

        :param default_expand: The default_expand of this InlineResponse20023Credentials.  # noqa: E501
        :type default_expand: bool
        """

        self._default_expand = default_expand

    @property
    def types(self):
        """Gets the types of this InlineResponse20023Credentials.  # noqa: E501

        Supported configuration settings for an individual credential type.  # noqa: E501

        :return: The types of this InlineResponse20023Credentials.  # noqa: E501
        :rtype: list[InlineResponse20023Types]
        """
        return self._types

    @types.setter
    def types(self, types):
        """Sets the types of this InlineResponse20023Credentials.

        Supported configuration settings for an individual credential type.  # noqa: E501

        :param types: The types of this InlineResponse20023Credentials.  # noqa: E501
        :type types: list[InlineResponse20023Types]
        """

        self._types = types

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20023Credentials):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20023Credentials):
            return True

        return self.to_dict() != other.to_dict()
