# InlineObject10

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alt_targets** | **list[str]** | If specified, the target will be scanned instead of the default. Value shall be an array with a single URL. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


