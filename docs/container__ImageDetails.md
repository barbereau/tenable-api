# ImageDetails

The image details.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the image. | [optional] 
**repository** | **str** | The name of the Tenable.io Container Security repository where the image is stored. | [optional] 
**tag** | **str** | The tag identifying the image version. **Note**: Image tags are not equivalent to Tenable.io [asset tags](ref:tags). | [optional] 
**digest** | **str** | A content-addressable image identifier. | [optional] 
**status** | **str** | The image analysis status. Status values can include:  - never_scanned  - scanned  - scan_failed | [optional] 
**score** | **float** | The image risk score. For more information about the risk score metric, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/containersecurity/Content/ContainerSecurity/RiskMetrics.htm). | [optional] 
**number_of_vulns** | **int** | The number of known vulnerabilities for the image. | [optional] 
**number_of_malware** | **int** | The number of known malware exploits for image. | [optional] 
**uploaded_at** | **str** | An ISO timestamp indicating the date and time when the image was uploaded to Tenable.io Container Security, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**last_scanned** | **str** | An ISO timestamp indicating the date and time when Tenable.io Container Security last scanned the image, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**layers** | [**list[Layer]**](container__Layer.md) | The layers that represent the history of changes to the image. | [optional] 
**report_url** | **str** | The URL of the latest available image analysis report. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


