# AuditLogV1EventsTarget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The UUID of the target entity. For example, a user UUID. | [optional] 
**name** | **str** | The name of the target entity. For example, a username. | [optional] 
**type** | **str** | The type of entity that was the target of the action. For example, a user. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


