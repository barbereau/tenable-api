# InlineResponse20047

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**InlineResponse20047Data**](vm__InlineResponse20047Data.md) |  | [optional] 
**size** | **int** | The number of records in the returned result set. | [optional] 
**params** | [**InlineResponse20047Params**](vm__InlineResponse20047Params.md) |  | [optional] 
**total_count** | **int** | The total number of available plugin records after Tenable.io applies the last_updated filter. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


