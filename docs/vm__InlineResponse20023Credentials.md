# InlineResponse20023Credentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The system name that uniquely identifies the category in Tenable.io. | [optional] 
**category** | **str** | The display name for the category in the user interface. | [optional] 
**default_expand** | **bool** | A value specifying whether the list of credential types in the category appears as expanded by default in the user interface. | [optional] 
**types** | [**list[InlineResponse20023Types]**](vm__InlineResponse20023Types.md) | Supported configuration settings for an individual credential type. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


