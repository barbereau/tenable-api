# InlineResponse20020

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the managed credential object. You specify the name when you create or update the managed credential. | [optional] 
**description** | **str** | The definition of the managed credential object. You specify the description when you create or update the managed credential. | [optional] 
**category** | [**InlineResponse20018Category**](vm__InlineResponse20018Category.md) |  | [optional] 
**type** | [**InlineResponse20018Type**](vm__InlineResponse20018Type.md) |  | [optional] 
**ad_hoc** | **bool** | A value specifying how a user creates a managed credential in the user interface. If &#x60;true&#x60;, the user created the credential during the scan configuration. If &#x60;false&#x60;, the user created the credential independently from scan configuration. | [optional] 
**user_permissions** | **int** | The permissions for the managed credential that are assigned to the user account submitting the API request. For possible values, see \&quot;Credential Roles\&quot; in [Permissions](doc:permissions). | [optional] 
**settings** | **object** | The configuration settings for the credential. The parameters of this object vary depending on the credential type. For more information, see [Determine Settings for a Credential Type](doc:determine-settings-for-credential-type). | [optional] 
**permissions** | [**list[CredentialsPermissions]**](vm__CredentialsPermissions.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


