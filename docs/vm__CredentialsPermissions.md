# CredentialsPermissions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grantee_uuid** | **str** | The UUID of the user or user group granted permissions for the managed credential.     This parameter is required when assigning CAN USE (32) or CAN EDIT (64) permissions for a managed credential. | [optional] 
**type** | **str** | A value specifying whether the grantee is a user (&#x60;user&#x60;) or a user group (&#x60;group&#x60;).    This parameter is required when assigning CAN USE (32) or CAN EDIT (64) permissions for a managed credential. | [optional] 
**permissions** | **int** | A value specifying the permissions granted to the user or user group for the credential. Possible values are:  - 32—The user can view credential information and use the credential in scans. Corresponds to the **Can Use** permission in the user interface.  - 64—The user can view and edit credential settings, delete the credential, and use the credential in scans. Corresponds to the **Can Edit** permission in the user interface.    This parameter is required when assigning CAN USE (32) or CAN EDIT (64) permissions for a managed credential. | [optional] 
**name** | **str** | The name of the user or user group that you want to grant permissions for the managed credential.    This parameter is optional when assigning CAN USE (32) or CAN EDIT (64) permissions for a managed credential. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


