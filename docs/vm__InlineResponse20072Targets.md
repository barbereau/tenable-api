# InlineResponse20072Targets

The target parameters used to launch the scan.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom** | **bool** | If &#x60;true&#x60;, custom parameters were used to launch the scan. | [optional] 
**default** | **bool** | If &#x60;true&#x60;, default parameters were used to launch the scan.. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


