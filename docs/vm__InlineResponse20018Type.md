# InlineResponse20018Type

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The system name that uniquely identifies the credential type. | [optional] 
**name** | **str** | The display name for the credential type in the user interface. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


