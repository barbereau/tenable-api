# Notifications

Contact information used to send scan notifications upon scan completion.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**emails** | **list[str]** | A list of email addresses. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


