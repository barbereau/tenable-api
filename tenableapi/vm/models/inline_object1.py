# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineObject1(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'name': 'str',
        'all_assets': 'bool',
        'all_users': 'bool',
        'rules': 'list[AccessGroupsRules]',
        'principals': 'list[AccessGroupsPrincipals]'
    }

    attribute_map = {
        'name': 'name',
        'all_assets': 'all_assets',
        'all_users': 'all_users',
        'rules': 'rules',
        'principals': 'principals'
    }

    def __init__(self, name=None, all_assets=None, all_users=None, rules=None, principals=None, local_vars_configuration=None):  # noqa: E501
        """InlineObject1 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._name = None
        self._all_assets = None
        self._all_users = None
        self._rules = None
        self._principals = None
        self.discriminator = None

        if name is not None:
            self.name = name
        if all_assets is not None:
            self.all_assets = all_assets
        if all_users is not None:
            self.all_users = all_users
        if rules is not None:
            self.rules = rules
        if principals is not None:
            self.principals = principals

    @property
    def name(self):
        """Gets the name of this InlineObject1.  # noqa: E501

        The name of the access group you want to modify.  # noqa: E501

        :return: The name of this InlineObject1.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineObject1.

        The name of the access group you want to modify.  # noqa: E501

        :param name: The name of this InlineObject1.  # noqa: E501
        :type name: str
        """

        self._name = name

    @property
    def all_assets(self):
        """Gets the all_assets of this InlineObject1.  # noqa: E501

        Specifies whether the access group you want to modify is the All Assets group or a user-defined group:   * If you want to refine membership in the All Assets access group (the only change you can make to the All Assets group), this parameter must be `true`. Tenable.io ignores any rules parameters in your request, but overwrrites existing principals parameters with those in the request based on the `all_users` and principals parameters in the request.   * If you want to modify a user-defined access group, this parameter must be `false`. Tenable.io overwrites the existing rules parameters with the rules parameters you specify in this request, and overwrites existing principals parameters based on the `all_users` and principals parameters in the request.  # noqa: E501

        :return: The all_assets of this InlineObject1.  # noqa: E501
        :rtype: bool
        """
        return self._all_assets

    @all_assets.setter
    def all_assets(self, all_assets):
        """Sets the all_assets of this InlineObject1.

        Specifies whether the access group you want to modify is the All Assets group or a user-defined group:   * If you want to refine membership in the All Assets access group (the only change you can make to the All Assets group), this parameter must be `true`. Tenable.io ignores any rules parameters in your request, but overwrrites existing principals parameters with those in the request based on the `all_users` and principals parameters in the request.   * If you want to modify a user-defined access group, this parameter must be `false`. Tenable.io overwrites the existing rules parameters with the rules parameters you specify in this request, and overwrites existing principals parameters based on the `all_users` and principals parameters in the request.  # noqa: E501

        :param all_assets: The all_assets of this InlineObject1.  # noqa: E501
        :type all_assets: bool
        """

        self._all_assets = all_assets

    @property
    def all_users(self):
        """Gets the all_users of this InlineObject1.  # noqa: E501

        Specifies whether assets in the access group can be viewed by all or only some users in your organization:   * If `true`, all users in your organization have Can View access to the assets defined in the rules parameter. Tenable.io ignores any principal parameters in your request.   * If `false`, only specified users have Can View access to the assets defined in the rules parameter. You define which users or user groups have access in the principals parameter of the request.      If you omit this parameter, Tenable.io sets the parameter to `false` by default.  # noqa: E501

        :return: The all_users of this InlineObject1.  # noqa: E501
        :rtype: bool
        """
        return self._all_users

    @all_users.setter
    def all_users(self, all_users):
        """Sets the all_users of this InlineObject1.

        Specifies whether assets in the access group can be viewed by all or only some users in your organization:   * If `true`, all users in your organization have Can View access to the assets defined in the rules parameter. Tenable.io ignores any principal parameters in your request.   * If `false`, only specified users have Can View access to the assets defined in the rules parameter. You define which users or user groups have access in the principals parameter of the request.      If you omit this parameter, Tenable.io sets the parameter to `false` by default.  # noqa: E501

        :param all_users: The all_users of this InlineObject1.  # noqa: E501
        :type all_users: bool
        """

        self._all_users = all_users

    @property
    def rules(self):
        """Gets the rules of this InlineObject1.  # noqa: E501

        An array of asset rules. Tenable.io uses these rules to assign assets to the access group. You can specify a maximum of 1,000 rules for an individual access group. If you specify multiple rules for an access group, Tenable.io assigns an asset to the access group if the asset matches any of the rules. You can only add rules to access groups if the `all_assets` parameter is set to `false`.  # noqa: E501

        :return: The rules of this InlineObject1.  # noqa: E501
        :rtype: list[AccessGroupsRules]
        """
        return self._rules

    @rules.setter
    def rules(self, rules):
        """Sets the rules of this InlineObject1.

        An array of asset rules. Tenable.io uses these rules to assign assets to the access group. You can specify a maximum of 1,000 rules for an individual access group. If you specify multiple rules for an access group, Tenable.io assigns an asset to the access group if the asset matches any of the rules. You can only add rules to access groups if the `all_assets` parameter is set to `false`.  # noqa: E501

        :param rules: The rules of this InlineObject1.  # noqa: E501
        :type rules: list[AccessGroupsRules]
        """

        self._rules = rules

    @property
    def principals(self):
        """Gets the principals of this InlineObject1.  # noqa: E501

        An array of principals. Each principal represents a user or user group assigned to the access group. You cannot add an access group as a principal to another access group.  # noqa: E501

        :return: The principals of this InlineObject1.  # noqa: E501
        :rtype: list[AccessGroupsPrincipals]
        """
        return self._principals

    @principals.setter
    def principals(self, principals):
        """Sets the principals of this InlineObject1.

        An array of principals. Each principal represents a user or user group assigned to the access group. You cannot add an access group as a principal to another access group.  # noqa: E501

        :param principals: The principals of this InlineObject1.  # noqa: E501
        :type principals: list[AccessGroupsPrincipals]
        """

        self._principals = principals

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineObject1):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineObject1):
            return True

        return self.to_dict() != other.to_dict()
