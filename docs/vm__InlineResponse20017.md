# InlineResponse20017

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The unique ID of the event. | [optional] 
**action** | **str** | The action that was taken by the user. | [optional] 
**crud** | **str** | Indicates whether the action taken was creating (c), reading (r), updating (u), or deleting (d) an entity. | [optional] 
**is_failure** | **bool** | Indicates whether the action the user took succeeded or failed. Tenable.io logs an event regardless of whether a user action succeeds. | [optional] 
**received** | **str** | The date and time the event occured in ISO 8601 format. | [optional] 
**description** | **str** | A description of the event. | [optional] 
**actor** | [**AuditLogV1EventsActor**](vm__AuditLogV1EventsActor.md) |  | [optional] 
**is_anonymous** | **bool** | Indicates whether the action was performed anonymously. | [optional] 
**target** | [**AuditLogV1EventsTarget**](vm__AuditLogV1EventsTarget.md) |  | [optional] 
**fields** | [**AuditLogV1EventsFields**](vm__AuditLogV1EventsFields.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


