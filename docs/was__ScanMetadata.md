# ScanMetadata

Summary statistics for the scan results.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**queued_urls** | **int** | The number of web application URLs queued for the scan. | [optional] 
**crawled_urls** | **int** | The number of web application URLs crawled by the scan. | [optional] 
**queued_pages** | **int** | The number of web application pages queued for the scan. | [optional] 
**audited_pages** | **int** | The number of pages audited by the scan. | [optional] 
**request_count** | **int** | The number of requests the scan made to the target web application. | [optional] 
**response_time** | **int** | The average response time (in milliseconds) from the web application. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


