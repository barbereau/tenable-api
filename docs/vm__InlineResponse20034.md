# InlineResponse20034

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exports** | [**list[InlineResponse20034Exports]**](vm__InlineResponse20034Exports.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


