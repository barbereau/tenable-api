# InlineResponse20029

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schedule** | [**ExclusionsSchedule**](vm__ExclusionsSchedule.md) |  | [optional] 
**id** | **int** | The unique ID of the exclusion. | [optional] 
**name** | **str** | The name of the exclusion. | [optional] 
**description** | **str** | The description of the exclusion. | [optional] 
**members** | **str** | The targets that you want excluded from scans. Specify multiple targets as a comma-separated string. Targets can be in the following formats:  - an individual IPv4 address (192.168.1.1)  - a range of IPv4 addresses (192.168.1.1-192.168.1.255)  - CIDR notation (192.168.2.0/24)  - a fully-qualified domain name (FQDN) (host.domain.com) | [optional] 
**creation_date** | **int** | The creation date of the exclusion in Unix time. | [optional] 
**network_id** | **str** | The ID of the network object associated with scanners where Tenable.io applies the exclusion. The default network ID is &#x60;00000000-0000-0000-0000-000000000000&#x60;. For more information about network objects, see [Manage Networks](doc:manage-networks-tio). | [optional] 
**last_modification_date** | **int** | The last modification date for the exclusion in Unix time. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


