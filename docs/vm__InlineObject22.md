# InlineObject22

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chunk_size** | **int** | Specifies the number of assets per exported chunk. The range is 100-10000. If you specify a value outside of that range, Tenable.io returns a 400 error. Using smaller chunks size can improve performance. | 
**filters** | [**AssetsExportFilters**](vm__AssetsExportFilters.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


