# InlineObject6

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**verification_code** | **str** | The verification code sent in the send-verification request. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


