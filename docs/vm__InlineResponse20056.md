# InlineResponse20056

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creation_date** | **int** | The Unix timestamp when the scanner was created. This attribute specifies the original creation date if the scanner was migrated. | [optional] 
**distro** | **str** | The scanner operating system distribution. | [optional] 
**group** | **bool** | Indicates whether the scanner belongs to a scanner group (&#39;true&#39;) or not (&#39;false&#39;). | [optional] 
**hostname** | **str** | The hostname of the scanner. | [optional] 
**id** | **int** | The unique ID of the scanner. | [optional] 
**ip_addresses** | **list[str]** | A list of IP addresses associated with the scanner. | [optional] 
**key** | **str** | The linking key, that is, the alpha-numeric sequence of characters you use to link a scanner to Tenable.io. For more information about linking a scanner to Tenable.io, see the [Tenable.io Vulnerability Management Guide](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/LinkaSensor.htm). | [optional] 
**last_connect** | **str** | The Unix timestamp when any of the scanner&#39;s tasks have provided its last update. | [optional] 
**last_modification_date** | **int** | The Unix timestamp when the scanner was last modified. | [optional] 
**engine_version** | **str** | The version of the scanner. | [optional] 
**loaded_plugin_set** | **str** | The current plugin set on the scanner. | [optional] 
**registration_code** | **str** | The registration code of the scanner. | [optional] 
**license** | [**ScannerGroupsGroupIdScannersLicense**](vm__ScannerGroupsGroupIdScannersLicense.md) |  | [optional] 
**linked** | **int** | Specifies whether you disabled (&#x60;0&#x60;) or enabled (&#x60;1&#x60;) the scanner. For more information, see the [PUT /scanners/{scanner_id}/link](ref:scanners-toggle-link-state) endpoint. | [optional] 
**name** | **str** | The user-defined name of the scanner. | [optional] 
**network_name** | **str** | The name of the network object associated with the scanner. For more information about network objects, see [Manage Networks](doc:manage-networks-tio). | [optional] 
**num_hosts** | **int** | The number of hosts that the scanner&#39;s analysis has discovered. | [optional] 
**num_scans** | **int** | The number of scans (tasks) the scanner is currently executing. | [optional] 
**num_sessions** | **int** | The number of active sessions between the scanner and hosts. | [optional] 
**num_tcp_sessions** | **int** | The number of active TCP sessions between the scanner and hosts. | [optional] 
**owner** | **str** | The owner of the scanner. | [optional] 
**owner_id** | **int** | The ID of the owner of the scanner. | [optional] 
**owner_name** | **str** | The username of the owner of the scanner. | [optional] 
**owner_uuid** | **str** | The UUID of the owner of the scanner. | [optional] 
**platform** | **str** | The platform of the scanner. | [optional] 
**pool** | **bool** | Indicates whether the scanner is part of a scanner group (&#39;true&#39;) or not (&#39;false&#39;). For more information about scanner groups, see the [Scanner Groups](ref:scanner-groups) endpoints. | [optional] 
**scan_count** | **int** | The number of scans that the scanner is currently running. | [optional] 
**shared** | **bool** | Indicates whether anyone other than the scanner owner has explicit access to the scanner (&#x60;1&#x60;). | [optional] 
**source** | **str** | Always set to &#x60;service&#x60;. | [optional] 
**status** | **str** | The status of the scanner (&#x60;on&#x60; or &#x60;off&#x60;). | [optional] 
**timestamp** | **int** | Equivalent to the &#x60;last_modification_date&#x60; attribute. | [optional] 
**type** | **str** | The type of scanner (&#x60;local&#x60;, &#x60;managed&#x60;, &#x60;managed_pvs&#x60;, &#x60;pool&#x60;, &#x60;remote&#x60;, or &#x60;webapp&#x60;). | [optional] 
**ui_build** | **int** | The backend build of Nessus that is running on the scanner. | [optional] 
**ui_version** | **str** | The backend version of Nessus that is running on the scanner. | [optional] 
**user_permissions** | **int** | The permissions you (the current user) have been assigned for the scanner. See [Permissions](doc:permissions). | [optional] 
**uuid** | **str** | The UUID of the scanner. | [optional] 
**supports_remote_logs** | **bool** | Indicates if the scanner supports remote logging. | [optional] 
**supports_webapp** | **bool** | Indicates if the scanner supports web application scanning. | [optional] 
**aws_update_interval** | **int** | Specifies how often, in minutes, the scanner checks in with Tenable.io (Amazon Web Services scanners only). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


