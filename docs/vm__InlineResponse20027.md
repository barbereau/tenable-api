# InlineResponse20027

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_was** | **bool** | If &#x60;true&#x60;, the template can be used for Web Application Scanning only. For Vulnerability Management templates, this value is always &#x60;null&#x60;. | [optional] 
**title** | **str** | The long name of the template. | [optional] 
**name** | **str** | The short name of the template. | [optional] 
**is_agent** | **bool** | If &#x60;true&#x60;, the template is for agent scans. | [optional] 
**filter_attributes** | [**list[InlineResponse20025FilterAttributes]**](vm__InlineResponse20025FilterAttributes.md) |  | [optional] 
**settings** | [**InlineResponse20025Settings**](vm__InlineResponse20025Settings.md) |  | [optional] 
**credentials** | **object** | Credentials that grant the scanner access to the target system without requiring an agent. Credentialed scans can perform a wider variety of checks than non-credentialed scans, which can result in more accurate scan results. This facilitates scanning of a very large network to determine local exposures or compliance violations. You can configure credentials for Cloud Services, Database, Host, Miscellaneous, Mobile Device Management, and Plaintext Authentication. | [optional] 
**compliance** | **object** | Plugins options enables you to select security checks by Plugin Family or individual plugins checks. | [optional] 
**plugins** | **object** | The settings for compliance audit checks. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


