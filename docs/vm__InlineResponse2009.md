# InlineResponse2009

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The unique ID of the exclusion. | [optional] 
**name** | **str** | The name of the exclusion. | [optional] 
**description** | **str** | The description of the exclusion. | [optional] 
**creation_date** | **int** | The creation date of the exclusion in unixtime. | [optional] 
**last_modification_date** | **int** | The last modification date for the exclusion in unixtime. | [optional] 
**schedule** | [**ScannersScannerIdAgentsExclusionsSchedule**](vm__ScannersScannerIdAgentsvm__ExclusionsSchedule.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


