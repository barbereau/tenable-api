# InlineResponse20072History

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time_end** | **int** | The Unix timestamp when the scan finished running. | [optional] 
**scan_uuid** | **str** | The UUID for the specific scan run. | [optional] 
**id** | **int** | The unique identifier for the specific scan run. | [optional] 
**is_archived** | **bool** | Indicates whether the scan results are older than 60 days (&#x60;true&#x60;). If this parameter is &#x60;true&#x60;, Tenable.io returns limited data for the scan run. For complete scan results that are older than 60 days, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead. | [optional] 
**time_start** | **int** | The Unix timestamp when the scan started running. | [optional] 
**visibility** | **str** | The visibility of the scan results in workbenches (&#x60;public&#x60; or &#x60;private&#x60;). | [optional] 
**targets** | [**InlineResponse20072Targets**](vm__InlineResponse20072Targets.md) |  | [optional] 
**status** | **str** | The status of the scan. For a list of possible values, see [Scan Status](doc:scan-status-tio). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


