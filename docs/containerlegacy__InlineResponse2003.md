# InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**image_name** | **str** |  | [optional] 
**docker_image_id** | **str** |  | [optional] 
**tag** | **str** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**platform** | **str** |  | [optional] 
**findings** | **list[object]** |  | [optional] 
**malware** | **list[object]** |  | [optional] 
**potentially_unwanted_programs** | **list[object]** |  | [optional] 
**sha256** | **str** |  | [optional] 
**os** | **str** |  | [optional] 
**os_version** | **str** |  | [optional] 
**os_architecture** | **str** |  | [optional] 
**os_release_name** | **str** |  | [optional] 
**installed_packages** | **list[object]** |  | [optional] 
**risk_score** | **int** |  | [optional] 
**digest** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


