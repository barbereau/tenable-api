# InlineObject28

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scanner_uuids** | **list[str]** | A list of UUIDs for the scanners and scanner groups you want to bulk move to a network object. To get values for this list, use the [GET /networks/{network_id}/assignable-scanners](ref:networks-list-assignable-scanners) endpoint. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


