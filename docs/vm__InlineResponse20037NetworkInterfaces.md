# InlineResponse20037NetworkInterfaces

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the interface. | [optional] 
**mac_address** | **list[str]** | The MAC addresses of the interface. | [optional] 
**ipv4** | **list[str]** | One or more IPv4 addresses belonging to the interface. | [optional] 
**ipv6** | **list[str]** | One or more IPv6 addresses belonging to the interface. | [optional] 
**fqdn** | **list[str]** | One or more FQDN belonging to the interface. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


