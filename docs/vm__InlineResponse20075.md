# InlineResponse20075

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**output** | [**list[InlineResponse20075Output]**](vm__InlineResponse20075Output.md) |  | [optional] 
**info** | [**InlineResponse20075Info**](vm__InlineResponse20075Info.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


