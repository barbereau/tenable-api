# tenableapi.containerv1.ContainersApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**container_security_containers_image_inventory**](containerlegacy__ContainersApi.md#container_security_containers_image_inventory) | **GET** /container-security/api/v1/container/{imageID}/status | Get image inventory
[**container_security_containers_list_containers**](containerlegacy__ContainersApi.md#container_security_containers_list_containers) | **GET** /container-security/api/v1/container/list | List containers


# **container_security_containers_image_inventory**
> InlineResponse200 container_security_containers_image_inventory(image_id)

Get image inventory

**Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images](ref:container-security-v2-list-images) endpoint instead. Returns an inventory of an image by ID.<p>Requires BASIC [16] user permissions. See [Permissions](docs/permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ContainersApi(api_client)
    image_id = 'image_id_example' # str | The ID of the image that you want to inventory.

    try:
        # Get image inventory
        api_response = api_instance.container_security_containers_image_inventory(image_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ContainersApi->container_security_containers_image_inventory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image_id** | **str**| The ID of the image that you want to inventory. | 

### Return type

[**InlineResponse200**](containerlegacy__InlineResponse200.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the inventory of the image you specified. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_containers_list_containers**
> object container_security_containers_list_containers()

List containers

**Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images](ref:container-security-v2-list-images) endpoint instead. Lists all containers.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ContainersApi(api_client)
    
    try:
        # List containers
        api_response = api_instance.container_security_containers_list_containers()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ContainersApi->container_security_containers_list_containers: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns an array of containers. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

