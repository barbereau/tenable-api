# ChildAccountResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** | The UUID of the customer account in the Tenable.io MSSP Portal. | 
**container_name** | **str** | The name of the customer account in the Tenable.io MSSP Portal. | [optional] 
**region** | **str** | The Tenable.io AWS cloud region that contains the customer account. Customer account regions correspond to the regions used for [cloud scanners](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/Sensors.htm). To avoid latency issues, Tenable recommends that scans in the customer instance use scanners in the same region as the customer account. | [optional] 
**licensed_assets** | **int** | The number of assets currently counted against the customer&#39;s Tenable.io license. | [optional] 
**licensed_assets_limit** | **int** | The number of assets which the customer is licensed to scan. | [optional] 
**licensed_apps** | **list[str]** | List of licensed Tenable applications. The applications can include: &lt;ul&gt;&lt;li&gt;pci—Tenable.io PCI/ASV&lt;/li&gt;&lt;li&gt;was—Tenable.io Web Application Scanning&lt;/li&gt;&lt;li&gt;consec—Tenable.io Container Security&lt;/li&gt;&lt;li&gt;lumin—Tenable Lumin&lt;/li&gt;&lt;/ul&gt; | [optional] 
**notes** | **str** | User-defined text; for example, internal tracking or customer contact information. | [optional] 
**logo_uuid** | **str** | The UUID of the logo assigned to the customer account in the Tenable.io MSSP Portal. This attribute is only present if a custom logo is assigned to the account. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


