# tenableapi.was.ScansApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_v2_scans_delete**](was__ScansApi.md#was_v2_scans_delete) | **DELETE** /was/v2/scans/{scan_id} | Delete scan
[**was_v2_scans_details**](was__ScansApi.md#was_v2_scans_details) | **GET** /was/v2/scans/{scan_id} | Get scan details
[**was_v2_scans_details_vulns**](was__ScansApi.md#was_v2_scans_details_vulns) | **GET** /was/v2/scans/{scan_id}/vulnerabilities | List vulnerabilities for scan
[**was_v2_scans_download_export**](was__ScansApi.md#was_v2_scans_download_export) | **GET** /was/v2/scans/{scan_id}/report | Download exported scan
[**was_v2_scans_export**](was__ScansApi.md#was_v2_scans_export) | **PUT** /was/v2/scans/{scan_id}/report | Export scan results
[**was_v2_scans_launch**](was__ScansApi.md#was_v2_scans_launch) | **POST** /was/v2/configs/{config_id}/scans | Launch scan
[**was_v2_scans_list**](was__ScansApi.md#was_v2_scans_list) | **GET** /was/v2/scans | List scans
[**was_v2_scans_notes_list**](was__ScansApi.md#was_v2_scans_notes_list) | **GET** /was/v2/scans/{scan_id}/notes | Get scan notes
[**was_v2_scans_status_update**](was__ScansApi.md#was_v2_scans_status_update) | **PATCH** /was/v2/scans/{scan_id} | Update scan status


# **was_v2_scans_delete**
> was_v2_scans_delete(scan_id)

Delete scan

Removes the specified scan and all vulnerabilities it detected. This request creates an asynchronous deletion job.<p>Requires SCAN MANAGER [40] user permissions and CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The UUID of the scan for which you want to delete.

    try:
        # Delete scan
        api_instance.was_v2_scans_delete(scan_id)
    except ApiException as e:
        print("Exception when calling ScansApi->was_v2_scans_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | [**str**](.md)| The UUID of the scan for which you want to delete. | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returned if Tenable.io Web Application Scanning successfully creates the deletion job. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**409** | Returned if you attempt to delete a running scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_scans_details**
> Scan was_v2_scans_details(scan_id)

Get scan details

Returns scan details.<p>Requires BASIC [16] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The UUID of the scan for which you want to view details.

    try:
        # Get scan details
        api_response = api_instance.was_v2_scans_details(scan_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_v2_scans_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | [**str**](.md)| The UUID of the scan for which you want to view details. | 

### Return type

[**Scan**](was__Scan.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns scan details. |  -  |
**400** | Returned if your request specifies an invalid scan ID. |  -  |
**404** | Returned if Tenable.io Web Application Scanning cannot find the specified scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_scans_details_vulns**
> InlineResponse2003 was_v2_scans_details_vulns(scan_id, order_by=order_by, ordering=ordering, page=page, size=size)

List vulnerabilities for scan

Returns a list of vulnerabilities for the specified scan. <p>Requires BASIC [16] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The UUID of the scan for which you want to view vulnerabilities.
order_by = 'created_at' # str | The field used to order the query results. (optional)
ordering = 'asc' # str | The sort order applied when sorting by the `order_by` parameter. Values include:  - `asc`  - `desc`  If your request omits the `ordering` query parameter, this value defaults to `asc`. (optional) (default to 'asc')
page = 0 # int | The starting record to retrieve. Use in combination with the `size` parameter to paginate results. The default value is `0`. (optional) (default to 0)
size = 10 # int | The page size of the query results. Use in combination with the `page` parameter to paginate results. The default value is `10`. (optional) (default to 10)

    try:
        # List vulnerabilities for scan
        api_response = api_instance.was_v2_scans_details_vulns(scan_id, order_by=order_by, ordering=ordering, page=page, size=size)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_v2_scans_details_vulns: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | [**str**](.md)| The UUID of the scan for which you want to view vulnerabilities. | 
 **order_by** | **str**| The field used to order the query results. | [optional] 
 **ordering** | **str**| The sort order applied when sorting by the &#x60;order_by&#x60; parameter. Values include:  - &#x60;asc&#x60;  - &#x60;desc&#x60;  If your request omits the &#x60;ordering&#x60; query parameter, this value defaults to &#x60;asc&#x60;. | [optional] [default to &#39;asc&#39;]
 **page** | **int**| The starting record to retrieve. Use in combination with the &#x60;size&#x60; parameter to paginate results. The default value is &#x60;0&#x60;. | [optional] [default to 0]
 **size** | **int**| The page size of the query results. Use in combination with the &#x60;page&#x60; parameter to paginate results. The default value is &#x60;10&#x60;. | [optional] [default to 10]

### Return type

[**InlineResponse2003**](was__InlineResponse2003.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of scan vulnerabilities for the requested scan. |  -  |
**400** | Returned if your request specifies an invalid scan UUID. |  -  |
**403** | Returned if you do not have permissions for the scan. For more information, see [Permissions](doc:permissions). |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_scans_download_export**
> ScanReport was_v2_scans_download_export(scan_id, content_type)

Download exported scan

Downloads a scan report for the specified scan.  **Note:** A 404 Not Found is returned if the requested report is not yet ready for download. <p>Requires BASIC [16] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The UUID of the scan for which you want to view a report.
content_type = 'content_type_example' # str | The format you want the report returned in. You can request reports in one of the following formats:<ul><li>application/json</li><li>application/pdf</li><li>text/csv</li><li>text/html</li><li>text/xml</li>

    try:
        # Download exported scan
        api_response = api_instance.was_v2_scans_download_export(scan_id, content_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_v2_scans_download_export: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | [**str**](.md)| The UUID of the scan for which you want to view a report. | 
 **content_type** | **str**| The format you want the report returned in. You can request reports in one of the following formats:&lt;ul&gt;&lt;li&gt;application/json&lt;/li&gt;&lt;li&gt;application/pdf&lt;/li&gt;&lt;li&gt;text/csv&lt;/li&gt;&lt;li&gt;text/html&lt;/li&gt;&lt;li&gt;text/xml&lt;/li&gt; | 

### Return type

[**ScanReport**](was__ScanReport.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/pdf, text/csv, text/html, text/xml

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A scan report in HTML, PDF, JSON, CSV, or XML format. |  -  |
**400** | Returned if your request specifies an invalid scan UUID. |  -  |
**403** | Returned if you do not have permissions for the scan. For more information, see [Permissions](doc:permissions). |  -  |
**404** | Returned if the requested report is not yet ready for download. |  -  |
**415** | Returned if your request contains an invalid &#x60;Content-Type&#x60; header. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_scans_export**
> was_v2_scans_export(scan_id, content_type)

Export scan results

Generates a scan report for the specified scan. <p>Requires BASIC [16] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The UUID of the scan for which you want to generate a report.
content_type = 'content_type_example' # str | The format you want the report returned in. You can request reports in one of the following formats:<ul><li>application/json</li><li>application/pdf</li><li>text/csv</li><li>text/html</li><li>text/xml</li>

    try:
        # Export scan results
        api_instance.was_v2_scans_export(scan_id, content_type)
    except ApiException as e:
        print("Exception when calling ScansApi->was_v2_scans_export: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | [**str**](.md)| The UUID of the scan for which you want to generate a report. | 
 **content_type** | **str**| The format you want the report returned in. You can request reports in one of the following formats:&lt;ul&gt;&lt;li&gt;application/json&lt;/li&gt;&lt;li&gt;application/pdf&lt;/li&gt;&lt;li&gt;text/csv&lt;/li&gt;&lt;li&gt;text/html&lt;/li&gt;&lt;li&gt;text/xml&lt;/li&gt; | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the scan report generation is complete. |  -  |
**202** | Returned if scan report export request was accepted. |  -  |
**400** | Returned if your request specifies an invalid scan UUID. |  -  |
**403** | Returned if you do not have permissions for the scan. For more information, see [Permissions](doc:permissions). |  -  |
**415** | Returned if your request contains an invalid &#x60;Content-Type&#x60; header. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_scans_launch**
> InlineResponse202 was_v2_scans_launch(config_id)

Launch scan

Launches a scan using the specified configuration.<p>Requires SCAN OPERATOR [24] user permissions and CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ScansApi(api_client)
    config_id = 'config_id_example' # str | The UUID of the scan configuration to use to launch a scan.

    try:
        # Launch scan
        api_response = api_instance.was_v2_scans_launch(config_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_v2_scans_launch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **config_id** | [**str**](.md)| The UUID of the scan configuration to use to launch a scan. | 

### Return type

[**InlineResponse202**](was__InlineResponse202.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returned if Tenable.io Web Application Scanning successfully launches the scan. |  -  |
**403** | Returned if you do not have either user permissions to launch the scan or scan permissions to use the scan configuration. For more information, see [Permissions](doc:permissions). |  -  |
**404** | Returned if Tenable.io Web Application Scanning cannot find the specified scan configuration. |  -  |
**409** | Returned if a scan is already is a running for the specified configuration. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in Tenable.io Web Application Scanning. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_scans_list**
> InlineResponse2001 was_v2_scans_list(config_id=config_id, order_by=order_by, ordering=ordering, page=page, size=size)

List scans

Returns a list of scans. <p>Requires BASIC [16] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ScansApi(api_client)
    config_id = 'b018aed8-554f-4965-9b05-994eaa66d459' # str | The UUID of the scan configuration. (optional)
order_by = 'created_at' # str | The field used to order the query results. (optional)
ordering = 'asc' # str | The sort order applied when sorting by the `order_by` parameter. Values include:  - `asc`  - `desc`  If your request omits the `ordering` query parameter, this value defaults to `asc`. (optional) (default to 'asc')
page = 0 # int | The starting record to retrieve. Use in combination with the `size` parameter to paginate results. The default value is `0`. (optional) (default to 0)
size = 10 # int | The page size of the query results. Use in combination with the `page` parameter to paginate results. The default value is `10`. (optional) (default to 10)

    try:
        # List scans
        api_response = api_instance.was_v2_scans_list(config_id=config_id, order_by=order_by, ordering=ordering, page=page, size=size)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_v2_scans_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **config_id** | [**str**](.md)| The UUID of the scan configuration. | [optional] 
 **order_by** | **str**| The field used to order the query results. | [optional] 
 **ordering** | **str**| The sort order applied when sorting by the &#x60;order_by&#x60; parameter. Values include:  - &#x60;asc&#x60;  - &#x60;desc&#x60;  If your request omits the &#x60;ordering&#x60; query parameter, this value defaults to &#x60;asc&#x60;. | [optional] [default to &#39;asc&#39;]
 **page** | **int**| The starting record to retrieve. Use in combination with the &#x60;size&#x60; parameter to paginate results. The default value is &#x60;0&#x60;. | [optional] [default to 0]
 **size** | **int**| The page size of the query results. Use in combination with the &#x60;page&#x60; parameter to paginate results. The default value is &#x60;10&#x60;. | [optional] [default to 10]

### Return type

[**InlineResponse2001**](was__InlineResponse2001.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of scans. |  -  |
**400** | Returned if your request specifies invalid query parameter values. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_scans_notes_list**
> InlineResponse2002 was_v2_scans_notes_list(scan_id, order_by=order_by, ordering=ordering, page=page, size=size)

Get scan notes

Returns a list of notes for the specified scan. <p>Requires BASIC [16] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The UUID of the scan for which you want to view notes.
order_by = 'created_at' # str | The field used to order the query results. (optional)
ordering = 'asc' # str | The sort order applied when sorting by the `order_by` parameter. Values include:  - `asc`  - `desc`  If your request omits the `ordering` query parameter, this value defaults to `asc`. (optional) (default to 'asc')
page = 0 # int | The starting record to retrieve. Use in combination with the `size` parameter to paginate results. The default value is `0`. (optional) (default to 0)
size = 10 # int | The page size of the query results. Use in combination with the `page` parameter to paginate results. The default value is `10`. (optional) (default to 10)

    try:
        # Get scan notes
        api_response = api_instance.was_v2_scans_notes_list(scan_id, order_by=order_by, ordering=ordering, page=page, size=size)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_v2_scans_notes_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | [**str**](.md)| The UUID of the scan for which you want to view notes. | 
 **order_by** | **str**| The field used to order the query results. | [optional] 
 **ordering** | **str**| The sort order applied when sorting by the &#x60;order_by&#x60; parameter. Values include:  - &#x60;asc&#x60;  - &#x60;desc&#x60;  If your request omits the &#x60;ordering&#x60; query parameter, this value defaults to &#x60;asc&#x60;. | [optional] [default to &#39;asc&#39;]
 **page** | **int**| The starting record to retrieve. Use in combination with the &#x60;size&#x60; parameter to paginate results. The default value is &#x60;0&#x60;. | [optional] [default to 0]
 **size** | **int**| The page size of the query results. Use in combination with the &#x60;page&#x60; parameter to paginate results. The default value is &#x60;10&#x60;. | [optional] [default to 10]

### Return type

[**InlineResponse2002**](was__InlineResponse2002.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of scan notes for the requested scan. |  -  |
**400** | Returned if your request specifies an invalid scan UUID. |  -  |
**403** | Returned if you do not have permissions for the scan. For more information, see [Permissions](doc:permissions). |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_scans_status_update**
> was_v2_scans_status_update(scan_id, inline_object)

Update scan status

Update the `requested_action` attribute for a scan. The requested action must be valid for the scan's current status. For example, you can stop a scan if it has a status of `running`. Otherwise, Tenable.io Web Application Scanning returns a 409 reponse code. This request creates an asynchronous update job.<p>Requires SCAN OPERATOR [24] user permissions and CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The UUID of the scan for which you want to update status.
inline_object = tenableapi.was.InlineObject() # InlineObject | 

    try:
        # Update scan status
        api_instance.was_v2_scans_status_update(scan_id, inline_object)
    except ApiException as e:
        print("Exception when calling ScansApi->was_v2_scans_status_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | [**str**](.md)| The UUID of the scan for which you want to update status. | 
 **inline_object** | [**InlineObject**](was__InlineObject.md)|  | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returned if Tenable.io Web Application Scanning successfully creates an update job. |  -  |
**404** | Returned if Tenable.io Web Application Scanning cannot find the specified scan. |  -  |
**409** | Returned if the requested action update is invalid for the current scan status. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

