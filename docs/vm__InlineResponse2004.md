# InlineResponse2004

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_groups** | [**InlineResponse2004AccessGroups**](vm__InlineResponse2004AccessGroups.md) |  | [optional] 
**pagination** | [**InlineResponse200Pagination**](vm__InlineResponse200Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


