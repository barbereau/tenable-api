# InlineResponse2005ParamsSubAccounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role_arn** | **str** | The Amazon Resource Name (ARN) of the role generated based on the associated account ID. | [optional] 
**external_id** | **str** | The UUID of your Tenable.io instance used by AWS to identify it as a client application. You can obtain the UUID of your Tenable.io account using the [GET /users/{user_id}](ref:users-details) endpoint. The UUID corresponds to the &#x60;container_uuid&#x60; attribute of the response message for that endpoint. | [optional] 
**trails** | [**list[InlineResponse2005ParamsTrails]**](platform__InlineResponse2005ParamsTrails.md) | For keyless AWS connectors, a list of AWS cloudtrails associated with the account. | [optional] 
**incremental_mode** | **bool** | Indicates whether a connector has completed the initial full import successfully. If the value is &#x60;true&#x60;, then the connector is in incremental mode where it imports assets based on events instead of enumerating all assets in the account every single time. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


