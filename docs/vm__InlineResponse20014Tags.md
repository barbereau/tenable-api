# InlineResponse20014Tags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag_uuid** | **str** | The UUID of the tag. | [optional] 
**tag_key** | **str** | The tag category (the first half of the category:value pair). | [optional] 
**tag_value** | **str** | The tag value (the second half of the category:value pair). | [optional] 
**added_by** | **str** | The UUID of the user who assigned the tag to the asset. | [optional] 
**added_at** | **str** | The ISO timestamp when the tag was assigned to the asset. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


