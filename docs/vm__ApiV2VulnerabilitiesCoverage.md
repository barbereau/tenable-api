# ApiV2VulnerabilitiesCoverage

.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ids** | **str** | A string or range of IDs that each represents a check that the scan used to detect vulnerabilities you are importing. This parameter supports Tenable plugin checks only. For more information, see [Plugins](https://www.tenable.com/plugins). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


