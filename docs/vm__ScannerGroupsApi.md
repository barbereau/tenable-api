# tenableapi.vm.ScannerGroupsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**io_scanner_groups_list_routes**](vm__ScannerGroupsApi.md#io_scanner_groups_list_routes) | **GET** /scanner-groups/{group_id}/routes | List scan routes
[**io_scanner_groups_update_routes**](vm__ScannerGroupsApi.md#io_scanner_groups_update_routes) | **PUT** /scanner-groups/{group_id}/routes | Update scan routes
[**scanner_groups_add_scanner**](vm__ScannerGroupsApi.md#scanner_groups_add_scanner) | **POST** /scanner-groups/{group_id}/scanners/{scanner_id} | Add scanner to scanner group
[**scanner_groups_create**](vm__ScannerGroupsApi.md#scanner_groups_create) | **POST** /scanner-groups | Create scanner group
[**scanner_groups_delete**](vm__ScannerGroupsApi.md#scanner_groups_delete) | **DELETE** /scanner-groups/{group_id} | Delete a scanner group
[**scanner_groups_delete_scanner**](vm__ScannerGroupsApi.md#scanner_groups_delete_scanner) | **DELETE** /scanner-groups/{group_id}/scanners/{scanner_id} | Remove scanner from scanner group
[**scanner_groups_details**](vm__ScannerGroupsApi.md#scanner_groups_details) | **GET** /scanner-groups/{group_id} | List scanner group details
[**scanner_groups_edit**](vm__ScannerGroupsApi.md#scanner_groups_edit) | **PUT** /scanner-groups/{group_id} | Update scanner group
[**scanner_groups_list**](vm__ScannerGroupsApi.md#scanner_groups_list) | **GET** /scanner-groups | List scanner groups
[**scanner_groups_list_scanners**](vm__ScannerGroupsApi.md#scanner_groups_list_scanners) | **GET** /scanner-groups/{group_id}/scanners | List scanners within scanner group


# **io_scanner_groups_list_routes**
> list[InlineResponse20057] io_scanner_groups_list_routes(group_id)

List scan routes

List the hostnames, wildcards, IP addresses, and IP address ranges that Tenable.io matches against targets in auto-routed scans. For more information about supported route formats, see [Supported Scan Routing Target Formats](doc:manage-scan-routing-tio#section-supported-scan-routing-target-formats). <p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group. Corresponds to the `id` element in the 200 response for the [GET /scanner-groups](ref:scanner-groups-list) endpoint.

    try:
        # List scan routes
        api_response = api_instance.io_scanner_groups_list_routes(group_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->io_scanner_groups_list_routes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. Corresponds to the &#x60;id&#x60; element in the 200 response for the [GET /scanner-groups](ref:scanner-groups-list) endpoint. | 

### Return type

[**list[InlineResponse20057]**](vm__InlineResponse20057.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scan routes list for the specified scanner group. |  -  |
**403** | Returned if you do not have permission to view the scan routes list. |  -  |
**404** | Returned if Tenable.io cannot find the specified scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_scanner_groups_update_routes**
> object io_scanner_groups_update_routes(group_id, inline_object34)

Update scan routes

Updates the hostnames, hostname wildcards, IP addresses, and IP address ranges that Tenable.io matches against targets in auto-routed scans. For more information about supported route formats, see [Supported Scan Routing Target Formats](doc:manage-scan-routing-tio#section-supported-scan-routing-target-formats). <p>Requires SCAN MANAGER [40] user permissions and CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group. Corresponds to the `id` element in the 200 response for the [GET /scanner-groups](ref:scanner-groups-list) endpoint.
inline_object34 = tenableapi.vm.InlineObject34() # InlineObject34 | 

    try:
        # Update scan routes
        api_response = api_instance.io_scanner_groups_update_routes(group_id, inline_object34)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->io_scanner_groups_update_routes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. Corresponds to the &#x60;id&#x60; element in the 200 response for the [GET /scanner-groups](ref:scanner-groups-list) endpoint. | 
 **inline_object34** | [**InlineObject34**](vm__InlineObject34.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully updates the scan routes for the specified scanner group. |  -  |
**400** | Returned if one or more scan routes are in an unrecognized or illegal format, or more than 10,000 scan routes are assigned to a single scanner group. |  -  |
**404** | Returned if Tenable.io cannot find the specified scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanner_groups_add_scanner**
> object scanner_groups_add_scanner(group_id, scanner_id)

Add scanner to scanner group

Adds a scanner to the scanner group. You can use the [GET /scanner-groups/{group_id}/scanners](ref:scanner-groups-list-scanners) endpoint to verify that the scanner has been added to the specified scanner group.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.
scanner_id = 56 # int | The ID of the scanner to add to the scanner group.

    try:
        # Add scanner to scanner group
        api_response = api_instance.scanner_groups_add_scanner(group_id, scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->scanner_groups_add_scanner: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 
 **scanner_id** | **int**| The ID of the scanner to add to the scanner group. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully adds the scanner to the scanner group. |  -  |
**400** | Returned if you attempt to add a scanner group to another scanner group. |  -  |
**409** | Returned if you attempt to add a scanner to a scanner group that the scanner is already a member of. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to add the scanner to the scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanner_groups_create**
> InlineResponse20055 scanner_groups_create(inline_object32)

Create scanner group

Creates a new scanner group. You cannot use this endpoint to:<ul><li>Assign the new scanner group to a network object. Tenable.io automatically assigns new scanner groups to the default network object. To assign a scanner group to a network object, use the [POST /networks/{network_id}/scanners/{scanner_uuid}](ref:networks-assign-scanner) endpoint.</li><li>Configure scan routes for the scanner group. Instead, use the [PUT /scanner-groups/group_id/routes](ref:io-scanner-groups-update-routes) endpoint.</li></ul><p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannerGroupsApi(api_client)
    inline_object32 = tenableapi.vm.InlineObject32() # InlineObject32 | 

    try:
        # Create scanner group
        api_response = api_instance.scanner_groups_create(inline_object32)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->scanner_groups_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object32** | [**InlineObject32**](vm__InlineObject32.md)|  | 

### Return type

[**InlineResponse20055**](vm__InlineResponse20055.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates the scanner group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to create the scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanner_groups_delete**
> object scanner_groups_delete(group_id)

Delete a scanner group

Deletes a scanner group.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.

    try:
        # Delete a scanner group
        api_response = api_instance.scanner_groups_delete(group_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->scanner_groups_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deletes the specified scanner group. |  -  |
**404** | Returned if Tenable.io cannot find the specified scanner group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to delete the scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanner_groups_delete_scanner**
> object scanner_groups_delete_scanner(group_id, scanner_id)

Remove scanner from scanner group

Remove a scanner from the scanner group.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.
scanner_id = 56 # int | The ID of the scanner to remove from the scanner group.

    try:
        # Remove scanner from scanner group
        api_response = api_instance.scanner_groups_delete_scanner(group_id, scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->scanner_groups_delete_scanner: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 
 **scanner_id** | **int**| The ID of the scanner to remove from the scanner group. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully removes the scanner from the scanner group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to remove the scanner from the scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanner_groups_details**
> InlineResponse20055 scanner_groups_details(group_id)

List scanner group details

Returns details for the specified scanner group.  **Note:** This endpoint does not return details about scan routes configured for the scanner group. For scan routes, use the [GET /scanner-groups/group_id/routes](ref:io-scanner-groups-list-routes) endpoint instead.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.

    try:
        # List scanner group details
        api_response = api_instance.scanner_groups_details(group_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->scanner_groups_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 

### Return type

[**InlineResponse20055**](vm__InlineResponse20055.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scanner group details. |  -  |
**403** | Returned if you do not have permission to view the scanner group. |  -  |
**404** | Returned if Tenable.io cannot find the specified scanner group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanner_groups_edit**
> InlineResponse20055 scanner_groups_edit(group_id, inline_object33)

Update scanner group

Updates a scanner group. You cannot use this endpoint to:<ul><li>Assign a scanner group to a network object. Instead, use the [POST /networks/{network_id}/scanners/{scanner_uuid}](ref:networks-assign-scanner) endpoint.</li><li>Update scan routes configured for the scanner group. Instead, use the [PUT /scanner-groups/group_id/routes](ref:io-scanner-groups-update-routes) endpoint.</li></ul><p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.
inline_object33 = tenableapi.vm.InlineObject33() # InlineObject33 | 

    try:
        # Update scanner group
        api_response = api_instance.scanner_groups_edit(group_id, inline_object33)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->scanner_groups_edit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 
 **inline_object33** | [**InlineObject33**](vm__InlineObject33.md)|  | 

### Return type

[**InlineResponse20055**](vm__InlineResponse20055.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully updates the scanner group. |  -  |
**404** | Returned if Tenable.io cannot find the specified scanner group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to update the scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanner_groups_list**
> InlineResponse20055 scanner_groups_list()

List scanner groups

Lists scanner groups for your Tenable.io instance.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannerGroupsApi(api_client)
    
    try:
        # List scanner groups
        api_response = api_instance.scanner_groups_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->scanner_groups_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20055**](vm__InlineResponse20055.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scanner group list. |  -  |
**403** | Returned if you do not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scanner_groups_list_scanners**
> list[InlineResponse20056] scanner_groups_list_scanners(group_id)

List scanners within scanner group

Lists scanners associated with the scanner group.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.

    try:
        # List scanners within scanner group
        api_response = api_instance.scanner_groups_list_scanners(group_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->scanner_groups_list_scanners: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 

### Return type

[**list[InlineResponse20056]**](vm__InlineResponse20056.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the list of scanners in the group. |  -  |
**403** | Returned if you do not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

