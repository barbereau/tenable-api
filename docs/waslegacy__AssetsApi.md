# tenableapi.waslegacy.AssetsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_assets_asset_info**](waslegacy__AssetsApi.md#was_assets_asset_info) | **GET** /assets/{asset_uuid} | Get asset information
[**was_assets_import**](waslegacy__AssetsApi.md#was_assets_import) | **POST** /import/assets | Import asset list
[**was_assets_import_job_info**](waslegacy__AssetsApi.md#was_assets_import_job_info) | **GET** /import/asset-jobs/{asset_import_job_uuid} | Get import job information
[**was_assets_list_assets**](waslegacy__AssetsApi.md#was_assets_list_assets) | **GET** /assets | List assets
[**was_assets_list_import_jobs**](waslegacy__AssetsApi.md#was_assets_list_import_jobs) | **GET** /import/asset-jobs | List asset import jobs


# **was_assets_asset_info**
> InlineResponse200 was_assets_asset_info(asset_uuid)

Get asset information

Gets information about the specified asset.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.AssetsApi(api_client)
    asset_uuid = 'asset_uuid_example' # str | The UUID of the asset.

    try:
        # Get asset information
        api_response = api_instance.was_assets_asset_info(asset_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->was_assets_asset_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_uuid** | **str**| The UUID of the asset. | 

### Return type

[**InlineResponse200**](waslegacy__InlineResponse200.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of assets. |  -  |
**403** | Returned if the user does not have permission to view information about an asset. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_assets_import**
> InlineResponse2001 was_assets_import(inline_object)

Import asset list

Imports a list of assets in JSON format. The request cannot exceed 5 MB. Each asset object requires a value for at least one of the following properties: fqdn, ipv4, netbios_name, mac_address.<p>Requires CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.AssetsApi(api_client)
    inline_object = tenableapi.waslegacy.InlineObject() # InlineObject | 

    try:
        # Import asset list
        api_response = api_instance.was_assets_import(inline_object)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->was_assets_import: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object** | [**InlineObject**](waslegacy__InlineObject.md)|  | 

### Return type

[**InlineResponse2001**](waslegacy__InlineResponse2001.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the import job UUID. |  -  |
**400** | Returned if the user submitted a bad request. |  -  |
**403** | Returned if the user does not have permission to import assets. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_assets_import_job_info**
> InlineResponse2002 was_assets_import_job_info(asset_import_job_uuid)

Get import job information

Gets information about the specified import job.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.AssetsApi(api_client)
    asset_import_job_uuid = 'asset_import_job_uuid_example' # str | The UUID of the asset import job.

    try:
        # Get import job information
        api_response = api_instance.was_assets_import_job_info(asset_import_job_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->was_assets_import_job_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_import_job_uuid** | **str**| The UUID of the asset import job. | 

### Return type

[**InlineResponse2002**](waslegacy__InlineResponse2002.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns information about the specified import job. |  -  |
**403** | Returned if the user does not have permission to list asset import jobs. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_assets_list_assets**
> list[InlineResponse200] was_assets_list_assets()

List assets

Lists up to 5000 assets, including non-WAS assets.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.AssetsApi(api_client)
    
    try:
        # List assets
        api_response = api_instance.was_assets_list_assets()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->was_assets_list_assets: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse200]**](waslegacy__InlineResponse200.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of assets. |  -  |
**403** | Returned if the user does not have permission to list assets. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_assets_list_import_jobs**
> list[InlineResponse2002] was_assets_list_import_jobs()

List asset import jobs

Lists asset import jobs.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.AssetsApi(api_client)
    
    try:
        # List asset import jobs
        api_response = api_instance.was_assets_list_import_jobs()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->was_assets_list_import_jobs: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse2002]**](waslegacy__InlineResponse2002.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of asset import jobs. |  -  |
**403** | Returned if the user does not have permission to list asset import jobs. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

