# InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_count** | **int** | The number of users in the group. | [optional] 
**permissions** | **int** | The permissions for the group. | [optional] 
**name** | **str** | The name of the group. | [optional] 
**uuid** | **str** | The UUID for the group. | [optional] 
**id** | **int** | The unique ID of the group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


