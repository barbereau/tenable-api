# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20092ReferenceInformation(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'name': 'str',
        'url': 'str',
        'values': 'list[str]'
    }

    attribute_map = {
        'name': 'name',
        'url': 'url',
        'values': 'values'
    }

    def __init__(self, name=None, url=None, values=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20092ReferenceInformation - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._name = None
        self._url = None
        self._values = None
        self.discriminator = None

        if name is not None:
            self.name = name
        if url is not None:
            self.url = url
        if values is not None:
            self.values = values

    @property
    def name(self):
        """Gets the name of this InlineResponse20092ReferenceInformation.  # noqa: E501

        The source of the reference information about the vulnerability. Possible values include:  - bid—Bugtraq (Symantec Connect)  - cert—CERT/CC Vulnerability Notes Database  - cve—NIST National Vulnerability Database (NVD)  - edb-id—The Exploit Database  - iava—information assurance vulnerability alert  - osvdb—Open Sourced Vulnerability Database  # noqa: E501

        :return: The name of this InlineResponse20092ReferenceInformation.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse20092ReferenceInformation.

        The source of the reference information about the vulnerability. Possible values include:  - bid—Bugtraq (Symantec Connect)  - cert—CERT/CC Vulnerability Notes Database  - cve—NIST National Vulnerability Database (NVD)  - edb-id—The Exploit Database  - iava—information assurance vulnerability alert  - osvdb—Open Sourced Vulnerability Database  # noqa: E501

        :param name: The name of this InlineResponse20092ReferenceInformation.  # noqa: E501
        :type name: str
        """

        self._name = name

    @property
    def url(self):
        """Gets the url of this InlineResponse20092ReferenceInformation.  # noqa: E501

        The URL of the source site, if available.  # noqa: E501

        :return: The url of this InlineResponse20092ReferenceInformation.  # noqa: E501
        :rtype: str
        """
        return self._url

    @url.setter
    def url(self, url):
        """Sets the url of this InlineResponse20092ReferenceInformation.

        The URL of the source site, if available.  # noqa: E501

        :param url: The url of this InlineResponse20092ReferenceInformation.  # noqa: E501
        :type url: str
        """

        self._url = url

    @property
    def values(self):
        """Gets the values of this InlineResponse20092ReferenceInformation.  # noqa: E501

        The unique identifier(s) for the vulnerability at the source.  # noqa: E501

        :return: The values of this InlineResponse20092ReferenceInformation.  # noqa: E501
        :rtype: list[str]
        """
        return self._values

    @values.setter
    def values(self, values):
        """Sets the values of this InlineResponse20092ReferenceInformation.

        The unique identifier(s) for the vulnerability at the source.  # noqa: E501

        :param values: The values of this InlineResponse20092ReferenceInformation.  # noqa: E501
        :type values: list[str]
        """

        self._values = values

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20092ReferenceInformation):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20092ReferenceInformation):
            return True

        return self.to_dict() != other.to_dict()
