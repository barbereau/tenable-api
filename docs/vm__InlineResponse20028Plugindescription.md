# InlineResponse20028Plugindescription

The detailed information for a Tenable.io plugin.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**severity** | **str** | The severity level of the vulnerabilities targeted by the plugin | [optional] 
**pluginname** | **str** | The name of the plugin. | [optional] 
**pluginattributes** | **object** | The attributes of the plugin, including synopsis, description, solution, and risk information. | [optional] 
**pluginfamily** | **str** | The name of the plugin family. | [optional] 
**pluginid** | **int** | The ID of the plugin. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


