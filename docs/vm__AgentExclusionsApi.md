# tenableapi.vm.AgentExclusionsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**agent_exclusions_create**](vm__Agentvm__ExclusionsApi.md#agent_exclusions_create) | **POST** /scanners/{scanner_id}/agents/exclusions | Create agent exclusion
[**agent_exclusions_delete**](vm__Agentvm__ExclusionsApi.md#agent_exclusions_delete) | **DELETE** /scanners/{scanner_id}/agents/exclusions/{exclusion_id} | Delete agent exclusion
[**agent_exclusions_details**](vm__Agentvm__ExclusionsApi.md#agent_exclusions_details) | **GET** /scanners/{scanner_id}/agents/exclusions/{exclusion_id} | Get agent exclusion details
[**agent_exclusions_edit**](vm__Agentvm__ExclusionsApi.md#agent_exclusions_edit) | **PUT** /scanners/{scanner_id}/agents/exclusions/{exclusion_id} | Update agent exclusion
[**agent_exclusions_list**](vm__Agentvm__ExclusionsApi.md#agent_exclusions_list) | **GET** /scanners/{scanner_id}/agents/exclusions | List agent exclusions


# **agent_exclusions_create**
> InlineResponse2009 agent_exclusions_create(scanner_id, inline_object5)

Create agent exclusion

Creates a new agent exclusion.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentExclusionsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner
inline_object5 = tenableapi.vm.InlineObject5() # InlineObject5 | 

    try:
        # Create agent exclusion
        api_response = api_instance.agent_exclusions_create(scanner_id, inline_object5)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentExclusionsApi->agent_exclusions_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner | 
 **inline_object5** | [**InlineObject5**](vm__InlineObject5.md)|  | 

### Return type

[**InlineResponse2009**](vm__InlineResponse2009.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the agent exclusion has been created. |  -  |
**400** | Returned if an argument is missing or invalid. |  -  |
**403** | Returned if you do not have permission to create an exclusion. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to create the exclusion. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_exclusions_delete**
> object agent_exclusions_delete(exclusion_id, scanner_id)

Delete agent exclusion

Deletes an agent exclusion.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentExclusionsApi(api_client)
    exclusion_id = 56 # int | The ID of the exclusion to delete.
scanner_id = 56 # int | The ID of the scanner

    try:
        # Delete agent exclusion
        api_response = api_instance.agent_exclusions_delete(exclusion_id, scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentExclusionsApi->agent_exclusions_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exclusion_id** | **int**| The ID of the exclusion to delete. | 
 **scanner_id** | **int**| The ID of the scanner | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the exclusion has been successfully deleted. |  -  |
**403** | Returned if you do not have permission to delete the exclusion. |  -  |
**404** | Returned if Tenable.io cannot find the specified exclusion. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_exclusions_details**
> InlineResponse2009 agent_exclusions_details(exclusion_id, scanner_id)

Get agent exclusion details

Returns details for the specified agent exclusion.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentExclusionsApi(api_client)
    exclusion_id = 56 # int | The ID of the exclusion.
scanner_id = 56 # int | The ID of the scanner

    try:
        # Get agent exclusion details
        api_response = api_instance.agent_exclusions_details(exclusion_id, scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentExclusionsApi->agent_exclusions_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exclusion_id** | **int**| The ID of the exclusion. | 
 **scanner_id** | **int**| The ID of the scanner | 

### Return type

[**InlineResponse2009**](vm__InlineResponse2009.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the exclusion details. |  -  |
**403** | Returned if you do not have permission to view the exclusion. |  -  |
**404** | Returned if Tenable.io cannot find the specified exclusion. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_exclusions_edit**
> object agent_exclusions_edit(exclusion_id, scanner_id, inline_object6)

Update agent exclusion

Updates an agent exclusion.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentExclusionsApi(api_client)
    exclusion_id = 56 # int | The ID of the exclusion to edit.
scanner_id = 56 # int | The ID of the scanner
inline_object6 = tenableapi.vm.InlineObject6() # InlineObject6 | 

    try:
        # Update agent exclusion
        api_response = api_instance.agent_exclusions_edit(exclusion_id, scanner_id, inline_object6)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentExclusionsApi->agent_exclusions_edit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exclusion_id** | **int**| The ID of the exclusion to edit. | 
 **scanner_id** | **int**| The ID of the scanner | 
 **inline_object6** | [**InlineObject6**](vm__InlineObject6.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the exclusion has been modified. |  -  |
**403** | Returned if you do not have permission to modify the exclusion. |  -  |
**404** | Returned if Tenable.io cannot find the specified exclusion. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to change the exclusion. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_exclusions_list**
> list[InlineResponse2009] agent_exclusions_list(scanner_id)

List agent exclusions

Returns the list of current agent exclusions.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentExclusionsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner

    try:
        # List agent exclusions
        api_response = api_instance.agent_exclusions_list(scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentExclusionsApi->agent_exclusions_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner | 

### Return type

[**list[InlineResponse2009]**](vm__InlineResponse2009.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the exclusions. |  -  |
**403** | Returned if you do not have permission to view the exclusions. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

