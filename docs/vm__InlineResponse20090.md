# InlineResponse20090

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_uuid** | **str** | This attribute is always empty. An empty value does not indicate an error condition. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


