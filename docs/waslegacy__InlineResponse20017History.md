# InlineResponse20017History

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alt_targets_used** | **bool** | If &#x60;true&#x60;, the scan was not launched with a target list. This parameter is &#x60;true&#x60; for agent scans. | [optional] 
**scheduler** | **int** | If &#x60;true&#x60;, the scan was launched automatically from a schedule. | [optional] 
**status** | **str** | The status of the historical data. | [optional] 
**type** | **str** | The type of scan: local, remote, or agent. | [optional] 
**uuid** | **str** | The UUID of the historical data. | [optional] 
**last_modification_date** | **int** | The last modification date for the historical data in Unix time. | [optional] 
**creation_date** | **int** | The creation date for the historical data in Unix time. | [optional] 
**owner_id** | **int** | The unique ID of the owner of the scan. | [optional] 
**history_id** | **int** | The unique ID of the historical data. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


