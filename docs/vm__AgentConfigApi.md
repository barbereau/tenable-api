# tenableapi.vm.AgentConfigApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**agent_config_details**](vm__AgentConfigApi.md#agent_config_details) | **GET** /scanners/{scanner_id}/agents/config | Get agent configuration
[**agent_config_edit**](vm__AgentConfigApi.md#agent_config_edit) | **PUT** /scanners/{scanner_id}/agents/config | Update agent configuration


# **agent_config_details**
> InlineResponse2008 agent_config_details(scanner_id)

Get agent configuration

Returns the configuration of agents associated with a specific scanner. Agent configuration controls agent settings for global agent software update enablement and agent auto-expiration.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentConfigApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.

    try:
        # Get agent configuration
        api_response = api_instance.agent_config_details(scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentConfigApi->agent_config_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 

### Return type

[**InlineResponse2008**](vm__InlineResponse2008.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the agent configuration. |  -  |
**403** | Returned if you do not have permission to view the agent configuration. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_config_edit**
> InlineResponse2008 agent_config_edit(scanner_id, inline_object4)

Update agent configuration

Updates the configuration of agents associated with a specific scanner. <p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentConfigApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.
inline_object4 = tenableapi.vm.InlineObject4() # InlineObject4 | 

    try:
        # Update agent configuration
        api_response = api_instance.agent_config_edit(scanner_id, inline_object4)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentConfigApi->agent_config_edit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 
 **inline_object4** | [**InlineObject4**](vm__InlineObject4.md)|  | 

### Return type

[**InlineResponse2008**](vm__InlineResponse2008.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the agent configuration has been updated. |  -  |
**403** | Returned if you do not have permission to update the agent config. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to update the agent configuration. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

