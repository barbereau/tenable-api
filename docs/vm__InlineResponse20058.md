# InlineResponse20058

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owner_uuid** | **str** | The UUID of the owner of the scanner. | [optional] 
**created** | **int** | The Unix timestamp when you linked the scanner to the Tenable.io instance. | [optional] 
**modified** | **int** | The Unix timestamp when the scanner configuration was last modified. | [optional] 
**container_uuid** | **str** | The UUID of the Tenable.io instance to which the scanner is linked. | [optional] 
**uuid** | **str** | The UUID of the scanner. | [optional] 
**id** | **int** | The unique ID of the scanner. | [optional] 
**name** | **str** | The user-defined name of the scanner. | [optional] 
**network_name** | **str** | The name of the network object associated with the scanner. For more information about network objects, see [Manage Networks](doc:manage-networks-tio). | [optional] 
**default_permissions** | **int** | The default permissions value set for the scanner. (Possible values are: 16, 24, 32, 40, and 64) | [optional] 
**shared** | **bool** | Indicates whether anyone other than the scanner owner has explicit access to the scanner (&#x60;1&#x60;). | [optional] 
**user_permissions** | **int** | The permissions you (the current user) have been assigned for the scanner. See [Permissions](doc:permissions). | [optional] 
**key** | **str** | The linking key, that is, the alpha-numeric sequence of characters you use to link a scanner to Tenable.io. For more information about linking a scanner to Tenable.io, see the [Tenable.io Vulnerability Management Guide](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/LinkaSensor.htm). | [optional] 
**status** | **str** | The status of the scanner (&#x60;on&#x60; or &#x60;off&#x60;). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


