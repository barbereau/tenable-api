# tenableapi.containerv1.ReportsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**container_security_reports_nessus_report_by_container_id**](containerlegacy__ReportsApi.md#container_security_reports_nessus_report_by_container_id) | **GET** /container-security/api/v1/reports/nessus/show | Get Nessus report for container
[**container_security_reports_report_by_container_id**](containerlegacy__ReportsApi.md#container_security_reports_report_by_container_id) | **GET** /container-security/api/v1/reports/show | Get container report
[**container_security_reports_report_by_image_digest**](containerlegacy__ReportsApi.md#container_security_reports_report_by_image_digest) | **GET** /container-security/api/v1/reports/by_image_digest | Get image digest report
[**container_security_reports_report_by_image_id**](containerlegacy__ReportsApi.md#container_security_reports_report_by_image_id) | **GET** /container-security/api/v1/reports/by_image | Get image report


# **container_security_reports_nessus_report_by_container_id**
> object container_security_reports_nessus_report_by_container_id(id)

Get Nessus report for container

**Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images/{repository}/{image}/{tag}](ref:container-security-v2-get-image-report) endpoint instead. Returns a Nessus report for a container that you specify by ID. Note: If you do not have the container_id, you can call the list-containers endpoint.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ReportsApi(api_client)
    id = 56 # int | The ID of the container for which you want a report.

    try:
        # Get Nessus report for container
        api_response = api_instance.container_security_reports_nessus_report_by_container_id(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ReportsApi->container_security_reports_nessus_report_by_container_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the container for which you want a report. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the Nessus report for the container you specified. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_reports_report_by_container_id**
> InlineResponse2003 container_security_reports_report_by_container_id(container_id)

Get container report

**Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images/{repository}/{image}/{tag}](ref:container-security-v2-get-image-report) endpoint instead. Returns a report in JSON format for a container that you specify by ID. Note: If you do not have the container_id, you can call the list-containers endpoint.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ReportsApi(api_client)
    container_id = 56 # int | The ID of the container for which you want a report.

    try:
        # Get container report
        api_response = api_instance.container_security_reports_report_by_container_id(container_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ReportsApi->container_security_reports_report_by_container_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **container_id** | **int**| The ID of the container for which you want a report. | 

### Return type

[**InlineResponse2003**](containerlegacy__InlineResponse2003.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the report for the container you specified. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_reports_report_by_image_digest**
> InlineResponse2003 container_security_reports_report_by_image_digest(image_digest)

Get image digest report

**Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images/{repository}/{image}/{tag}](ref:container-security-v2-get-image-report) endpoint instead. Returns a report in JSON format for an image digest.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ReportsApi(api_client)
    image_digest = 'image_digest_example' # str | The image digest of the image for which you want a report.

    try:
        # Get image digest report
        api_response = api_instance.container_security_reports_report_by_image_digest(image_digest)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ReportsApi->container_security_reports_report_by_image_digest: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image_digest** | **str**| The image digest of the image for which you want a report. | 

### Return type

[**InlineResponse2003**](containerlegacy__InlineResponse2003.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the report for the image you specified. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_reports_report_by_image_id**
> InlineResponse2003 container_security_reports_report_by_image_id(image_id)

Get image report

**Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images/{repository}/{image}/{tag}](ref:container-security-v2-get-image-report) endpoint instead. Returns a report in JSON format for an image that you specify by ID. Note: If you do not have the image_id, you can call the list-images endpoint.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.ReportsApi(api_client)
    image_id = 'image_id_example' # str | The ID of the image for which you want a report.

    try:
        # Get image report
        api_response = api_instance.container_security_reports_report_by_image_id(image_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ReportsApi->container_security_reports_report_by_image_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image_id** | **str**| The ID of the image for which you want a report. | 

### Return type

[**InlineResponse2003**](containerlegacy__InlineResponse2003.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the report for the image you specified. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

