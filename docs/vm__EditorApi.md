# tenableapi.vm.EditorApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**editor_audits**](vm__EditorApi.md#editor_audits) | **GET** /editor/{type}/{object_id}/audits/{file_id} | Download audit file
[**editor_details**](vm__EditorApi.md#editor_details) | **GET** /editor/{type}/{id} | Get configuration details
[**editor_list_templates**](vm__EditorApi.md#editor_list_templates) | **GET** /editor/{type}/templates | List templates
[**editor_plugin_description**](vm__EditorApi.md#editor_plugin_description) | **GET** /editor/policy/{policy_id}/families/{family_id}/plugins/{plugin_id} | Get plugin details
[**editor_template_details**](vm__EditorApi.md#editor_template_details) | **GET** /editor/{type}/templates/{template_uuid} | Get template details


# **editor_audits**
> file editor_audits(type, object_id, file_id)

Download audit file

Downloads the specified custom audit file associated with the scan or policy. The file ID can be found in the scan or policy details using the /editor/{type}/{object_id} endpoint.<p>Requires CAN EDIT [32] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.EditorApi(api_client)
    type = 'type_example' # str | The type of template to retrieve (scan or policy).
object_id = 56 # int | The unique ID of the object.
file_id = 56 # int | The ID of the file to export.

    try:
        # Download audit file
        api_response = api_instance.editor_audits(type, object_id, file_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling EditorApi->editor_audits: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| The type of template to retrieve (scan or policy). | 
 **object_id** | **int**| The unique ID of the object. | 
 **file_id** | **int**| The ID of the file to export. | 

### Return type

**file**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the audit file. |  -  |
**403** | Returned if you do not have permission to export the audit file. |  -  |
**404** | Returned if Tenable.io cannot find the specified audit file. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **editor_details**
> InlineResponse20025 editor_details(type, id)

Get configuration details

Gets the configuration details for the scan or user-defined template (policy).<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.EditorApi(api_client)
    type = 'type_example' # str | The type of object (`scan` or `policy`).
id = 56 # int | The unique ID of the scan or user-defined template (policy).

    try:
        # Get configuration details
        api_response = api_instance.editor_details(type, id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling EditorApi->editor_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| The type of object (&#x60;scan&#x60; or &#x60;policy&#x60;). | 
 **id** | **int**| The unique ID of the scan or user-defined template (policy). | 

### Return type

[**InlineResponse20025**](vm__InlineResponse20025.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the object data. Note that the returned attributes can vary depending on the Tenable-provided template used to create the scan or user-defined template (policy). |  -  |
**403** | Returned if you do not have permission to open the object. |  -  |
**404** | Returned if Tenable.io cannot find the specified object. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **editor_list_templates**
> list[InlineResponse20026] editor_list_templates(type)

List templates

Lists Tenable-provided scan templates. Tenable provides a number of scan templates to facilitate the creation of scans and scan policies. For a full description of these scan templates, see the [*Tenable.io Vulnerability Management Guide*](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Scans/Templates.htm). <p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.EditorApi(api_client)
    type = 'type_example' # str | The type of templates to retrieve (scan or policy).

    try:
        # List templates
        api_response = api_instance.editor_list_templates(type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling EditorApi->editor_list_templates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| The type of templates to retrieve (scan or policy). | 

### Return type

[**list[InlineResponse20026]**](vm__InlineResponse20026.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the template list. |  -  |
**403** | Returned if you do not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **editor_plugin_description**
> InlineResponse20028 editor_plugin_description(policy_id, family_id, plugin_id)

Get plugin details

Gets the details of the plugin associated with the scan or policy.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.EditorApi(api_client)
    policy_id = 56 # int | The ID of the policy to look up.
family_id = 56 # int | The ID of the family to lookup within the policy.
plugin_id = 56 # int | The ID of the plugin to lookup within the family.

    try:
        # Get plugin details
        api_response = api_instance.editor_plugin_description(policy_id, family_id, plugin_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling EditorApi->editor_plugin_description: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **policy_id** | **int**| The ID of the policy to look up. | 
 **family_id** | **int**| The ID of the family to lookup within the policy. | 
 **plugin_id** | **int**| The ID of the plugin to lookup within the family. | 

### Return type

[**InlineResponse20028**](vm__InlineResponse20028.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the plugin output. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **editor_template_details**
> InlineResponse20027 editor_template_details(type, template_uuid)

Get template details

Gets details for the specified template.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.EditorApi(api_client)
    type = 'type_example' # str | The type of template to retrieve (scan or policy).
template_uuid = 'template_uuid_example' # str | The UUID for the template.

    try:
        # Get template details
        api_response = api_instance.editor_template_details(type, template_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling EditorApi->editor_template_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| The type of template to retrieve (scan or policy). | 
 **template_uuid** | **str**| The UUID for the template. | 

### Return type

[**InlineResponse20027**](vm__InlineResponse20027.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the template details. Note that the fields can vary for different template types. |  -  |
**403** | Returned if you do not have permission to open the template. |  -  |
**404** | Returned if Tenable.io cannot find the specified template. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

