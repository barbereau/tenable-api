# InlineObject14

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **str** | The name of the file to import as provided by the response from [file: upload](ref:file-upload). | 
**folder_id** | **int** | The ID of the destination folder. If not specified, the default folder will be used. | [optional] 
**password** | **str** | The password for the file to import (required for nessus.db). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


