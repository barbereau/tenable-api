# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20048(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'attributes': 'list[InlineResponse20048Attributes]',
        'family_name': 'str',
        'name': 'str',
        'id': 'int'
    }

    attribute_map = {
        'attributes': 'attributes',
        'family_name': 'family_name',
        'name': 'name',
        'id': 'id'
    }

    def __init__(self, attributes=None, family_name=None, name=None, id=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20048 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._attributes = None
        self._family_name = None
        self._name = None
        self._id = None
        self.discriminator = None

        if attributes is not None:
            self.attributes = attributes
        if family_name is not None:
            self.family_name = family_name
        if name is not None:
            self.name = name
        if id is not None:
            self.id = id

    @property
    def attributes(self):
        """Gets the attributes of this InlineResponse20048.  # noqa: E501

        The plugin attributes.  # noqa: E501

        :return: The attributes of this InlineResponse20048.  # noqa: E501
        :rtype: list[InlineResponse20048Attributes]
        """
        return self._attributes

    @attributes.setter
    def attributes(self, attributes):
        """Sets the attributes of this InlineResponse20048.

        The plugin attributes.  # noqa: E501

        :param attributes: The attributes of this InlineResponse20048.  # noqa: E501
        :type attributes: list[InlineResponse20048Attributes]
        """

        self._attributes = attributes

    @property
    def family_name(self):
        """Gets the family_name of this InlineResponse20048.  # noqa: E501

        The name of the plugin family.  # noqa: E501

        :return: The family_name of this InlineResponse20048.  # noqa: E501
        :rtype: str
        """
        return self._family_name

    @family_name.setter
    def family_name(self, family_name):
        """Sets the family_name of this InlineResponse20048.

        The name of the plugin family.  # noqa: E501

        :param family_name: The family_name of this InlineResponse20048.  # noqa: E501
        :type family_name: str
        """

        self._family_name = family_name

    @property
    def name(self):
        """Gets the name of this InlineResponse20048.  # noqa: E501

        The name of the plugin.  # noqa: E501

        :return: The name of this InlineResponse20048.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse20048.

        The name of the plugin.  # noqa: E501

        :param name: The name of this InlineResponse20048.  # noqa: E501
        :type name: str
        """

        self._name = name

    @property
    def id(self):
        """Gets the id of this InlineResponse20048.  # noqa: E501

        The ID of the plugin.  # noqa: E501

        :return: The id of this InlineResponse20048.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20048.

        The ID of the plugin.  # noqa: E501

        :param id: The id of this InlineResponse20048.  # noqa: E501
        :type id: int
        """

        self._id = id

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20048):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20048):
            return True

        return self.to_dict() != other.to_dict()
