# InlineResponse20036

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | The status of the export request. Possible values include:  - QUEUED—Tenable.io has queued the export request until it completes other requests currently in process.  - PROCESSING—Tenable.io has started processing the export request.  - FINISHED—Tenable.io has completed processing the export request. The list of chunks is complete.  - CANCELLED—An administrator has cancelled the export request.  - ERROR—Tenable.io encountered an error while processing the export request. Tenable recommends that you retry the request. If the status persists on retry, contact Support. | [optional] 
**chunks_available** | **list[int]** | A comma-separated list of completed chunks available for download. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


