# InlineResponse200PaginationSort

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the field on which Tenable.io sorted the results. | [optional] 
**order** | **str** | The order in which Tenable.io sorted the results. Possible values are ascending (&#x60;asc&#x60;) or descending (&#x60;desc&#x60;). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


