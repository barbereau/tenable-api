# ImportVulnerabilitiesNetworkInterfaces

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ipv4** | **list[str]** | A list of IPv4 address that the scan identified as associated with the network interface. | [optional] 
**ipv6** | **list[str]** | A list of IPv6 addresses that the scan identified as associated with the network interface. | [optional] 
**mac_address** | **str** | The MAC address of the network interface. | [optional] 
**netbios_name** | **str** | The NETBIOS name of the network interface. | [optional] 
**fqdn** | **str** | The fully-qualified domain name (FQDN) of the network interface. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


