# InlineResponse20032

A chunk of vulnerabilities information.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset** | [**list[InlineResponse20032Asset]**](vm__InlineResponse20032Asset.md) | Information about the asset where the scan detected the vulnerability. | [optional] 
**output** | **str** | The text output of the Nessus scanner. | [optional] 
**plugin** | [**list[InlineResponse20032Plugin]**](vm__InlineResponse20032Plugin.md) | Information about the plugin that detected the vulnerability. | [optional] 
**port** | [**list[InlineResponse20032Port]**](vm__InlineResponse20032Port.md) | Information about the port the scanner used to connect to the asset. | [optional] 
**recast_reason** | **str** | The text that appears in the Comment field of the recast rule in the Tenable.io user interface. | [optional] 
**recast_rule_uuid** | **str** | The UUID of the recast rule that applies to the plugin. | [optional] 
**scan** | [**list[InlineResponse20032Scan]**](vm__InlineResponse20032Scan.md) | Information about the latest scan that detected the vulnerability. | [optional] 
**severity** | **str** | The severity of the vulnerability as defined using the Common Vulnerability Scoring System (CVSS) base score. Possible values include &#x60;info&#x60; (CVSS score of 0), &#x60;low&#x60; (CVSS score between 0.1 and 3.9), &#x60;medium&#x60; (CVSS score between 4.0 and 6.9), &#x60;high&#x60; (CVSS score between 7.0 and 9.9), and &#x60;critical&#x60; (CVSS score of 10.0). | [optional] 
**severity_id** | **int** | The code for the severity assigned when a user recast the risk associated with the vulnerability. Possible values include:   - 0—The vulnerability has a CVSS score of 0, which corresponds to the \&quot;info\&quot; severity level.  - 1—The vulnerability has a CVSS score between 0.1 and 3.9, which corresponds to the \&quot;low\&quot; severity level.  - 2—The vulnerability has a CVSS score between 4.0 and 6.9, which corresponds to the \&quot;medium\&quot; severity level.  - 3—The vulnerability has a CVSS score between 7.0 and 9.9, which corresponds to the \&quot;high\&quot; severity level.  - 4—The vulnerability has a CVSS score of 10.0, which corresponds to the \&quot;critical\&quot; severity level. | [optional] 
**severity_default_id** | **int** | The code for the severity originally assigned to a vulnerability before a user recast the risk associated with the vulnerability. Possible values are the same as for the &#x60;severity_id&#x60; attribute. | [optional] 
**severity_modification_type** | **str** | The type of modification a user made to the vulnerability&#39;s severity. Possible values include:   - none—No modification has been made.  - recasted—A user in the Tenable.io user interface has recast the risk associated with the vulnerability.   - accepted—A user in the Tenable.io user interface has accepted the risk associated with the vulnerability.   For more information about recast and accept rules, see the &lt;i&gt;Tenable.io Vulnerability Management User Guide&lt;/i&gt;. | [optional] 
**first_found** | **str** | The ISO date when a scan first detected the vulnerability on the asset. | [optional] 
**last_fixed** | **str** | The ISO date when a scan no longer detects the previously detected vulnerability on the asset. | [optional] 
**last_found** | **str** | The ISO date when a scan last detected the vulnerability on the asset. | [optional] 
**state** | **str** | The state of the vulnerability as determined by the Tenable.io state service. Possible values include:   - open—The vulnerability is currently present on an asset.   - reopened—The vulnerability was previously marked as fixed on an asset, but has been detected again by a new scan.   - fixed—The vulnerability was present on an asset, but is no longer detected. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


