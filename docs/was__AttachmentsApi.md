# tenableapi.was.AttachmentsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_v2_attachments_download**](was__AttachmentsApi.md#was_v2_attachments_download) | **GET** /was/v2/attachments/{attachment_id} | Download attachment


# **was_v2_attachments_download**
> str was_v2_attachments_download(attachment_id)

Download attachment

Returns the specified attachment file for a vulnerability detected by a Tenable.io Web Application Scanning scan. Attachments provide additional details for a detected vulnerability.  **Note:** The `transfer-encoding` header value of the response stream is set to `chunked`. <p>Requires BASIC [16] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.AttachmentsApi(api_client)
    attachment_id = 'attachment_id_example' # str | The UUID of the attachment to download. To determine the UUID of an attachment, use either the [GET /was/v2/vulnerabilities](ref:was-v2-vulns-list) or [GET /was/v2/scans/{scan_id}/vulnerabilities](ref:was-v2-scans-details-vulns) endpoint.

    try:
        # Download attachment
        api_response = api_instance.was_v2_attachments_download(attachment_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AttachmentsApi->was_v2_attachments_download: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attachment_id** | [**str**](.md)| The UUID of the attachment to download. To determine the UUID of an attachment, use either the [GET /was/v2/vulnerabilities](ref:was-v2-vulns-list) or [GET /was/v2/scans/{scan_id}/vulnerabilities](ref:was-v2-scans-details-vulns) endpoint. | 

### Return type

**str**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, image/png, application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the specified attachment file as a chunked transfer-encoded stream. |  -  |
**400** | Returned if your request specifies an invalid attachment ID. |  -  |
**404** | Returned if Tenable.io Web Application Scanning cannot find the specified attachment file. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in Tenable.io Web Application Scanning. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

