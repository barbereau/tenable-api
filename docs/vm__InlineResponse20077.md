# InlineResponse20077

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**missed_targets** | **list[str]** | The list of targets that did not match a route in any scanner group. | [optional] 
**total_missed_targets** | **int** | The total count of missed targets, before being truncated by the optional &#x60;limit&#x60; parameter. | [optional] 
**matched_resource_uuids** | **list[str]** | A list of UUIDs for scanner groups where configured scan routes matched at least one of the specified targets. | [optional] 
**total_matched_resource_uuids** | **int** | The count of matched resource UUIDs, before being truncated by the optional &#x60;matched_resource_limit&#x60; parameter. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


