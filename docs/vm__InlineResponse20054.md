# InlineResponse20054

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** | The UUID of the Tenable-provided template used to create this policy. | [optional] 
**audits** | **object** |  | [optional] 
**credentials** | **object** |  | [optional] 
**plugins** | **object** |  | [optional] 
**scap** | **object** |  | [optional] 
**settings** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


