# CredentialsSettings

The configuration settings for the credential. The parameters of this object vary depending on the credential type. For more information, see [Determine Settings for a Credential Type](doc:determine-settings-for-credential-type).  **Note:** This form displays limited parameters that support a Windows type of credential that uses password authentication.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain** | **str** | The Windows domain to which the username belongs. | [optional] 
**username** | **str** | The username on the target system. | [optional] 
**auth_method** | **str** | The name for the authentication method. This value corresponds to the credentials[].types[].configuration[].options[].id attribute in the response message for the [GET /credentials/types](ref:credentials-list-credential-types) endpoint. | [optional] 
**password** | **str** | The user password on the target system. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


