# RepositoryListResponse

A list of Tenable.io Container Security repositories with pagination information.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pagination** | [**Pagination**](container__Pagination.md) |  | [optional] 
**items** | [**list[RepositoryDetails]**](container__RepositoryDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


