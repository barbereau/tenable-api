# InlineResponse2005Params

The connector parameters:   * For AWS connectors, the parameters include the `access_key`, `secret_key`, associated accounts, and cloudtrails.   * For keyless AWS connectors, the parameters include associated AWS accounts (sub-accounts) and cloudtrails.   * For Azure connectors, the parameters include the `application_ID`, `tenant_ID`, `client_secret` key, and an optional list of subscription IDs. If you don't provide subscription IDs, Tenable.io automatically discovers them.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_key** | **str** | For AWS connectors, the AWS access key. &lt;b&gt;Note: &lt;/b&gt;The &#x60;access_key&#x60; is not included in the keyless AWS connector parameters. | [optional] 
**trails** | [**list[InlineResponse2005ParamsTrails]**](platform__InlineResponse2005ParamsTrails.md) | For AWS connectors, a list of AWS cloudtrails associated with the connector. | [optional] 
**sub_accounts** | [**list[InlineResponse2005ParamsSubAccounts]**](platform__InlineResponse2005ParamsSubAccounts.md) | For AWS connectors, a list of AWS accounts associated with the connector. | [optional] 
**status** | [**list[InlineResponse2005ParamsStatus]**](platform__InlineResponse2005ParamsStatus.md) | For Azure connectors, a list of import status records. | [optional] 
**application_id** | **str** | For Azure connectors, Azure application ID. | [optional] 
**tenant_id** | **str** | For Azure connectors, Azure tenant ID. | [optional] 
**subscription_id** | **str** | For Azure connectors, Azure subscription ID. If you do not provide subscription IDs, Tenable.io automatically discovers them. | [optional] 
**service** | **str** | The service targeted by the connector. Values include: * aws   * aws_keyless   * azure * gcp | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


