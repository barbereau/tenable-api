# InlineResponse20055

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creation_date** | **int** | The creation date for the scanner group in Unix time. | [optional] 
**last_modification_date** | **int** | The last modification date for the scanner group in Unix time. | [optional] 
**owner_id** | **int** | The unique ID of the owner of the scanner group. | [optional] 
**owner** | **str** | The username of the owner of the scanner group. | [optional] 
**default_permissions** | **int** | The access permissions for the Default group. | [optional] 
**user_permissions** | **int** | The sharing permissions for the scanner group. | [optional] 
**shared** | **int** | The shared status of the scanner-group. | [optional] 
**scan_count** | **int** | The number of scans currently tasked to the scanner group. | [optional] 
**scanner_count** | **str** | The number of scanners associated with this scanner group. | [optional] 
**uuid** | **str** | The UUID of the scanner group. | [optional] 
**token** | **str** | The unique token for a scanner group. | [optional] 
**flag** | **str** | The flag indicating what type of scanner group. | [optional] 
**type** | **str** | The type of scanner group. This is set to \&quot;load_balancing\&quot; by default. | [optional] 
**name** | **str** | The name of the scanner group. | [optional] 
**network_name** | **str** | The name of the network object associated with the scanner group. For more information about network objects, see [Manage Networks](doc:manage-networks-tio). | [optional] 
**id** | **int** | The unique ID of the scanner group. | [optional] 
**scanner_id** | **int** | The unique scanner ID of the scanner group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


