# AssessmentSettings

The settings for the Remote File Inclusion (RFI) testing. By default, Tenable.io Web Application Scanning uses a safe file hosted by Tenable for RFI testing. If the scanner cannot reach the internet, you can use an internally hosted file for more accurate RFI testing.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rfi_remote_url** | **str** | The URL of the file on a remote host to use for tests. | [optional] [default to 'http://rfi.nessus.org/rfi.txt']
**dictionary** | **str** | The type of dictionary to use when testing common and backup files. Possible values are &#x60;full&#x60; or &#x60;limited&#x60;. | [optional] [default to 'limited']
**fingerprinting** | **bool** | Indicates whether fingerprinted should be enabled (&#x60;true&#x60;) or disabled (&#x60;false&#x60;). | [optional] [default to True]
**enable** | **bool** | Indicates whether an assessment is enabled (&#x60;true&#x60;) or disabled (&#x60;false&#x60;). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


