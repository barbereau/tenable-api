# PluginSettings

The settings for enabling or disabling individual plugins and/or plugin families to customize your scan.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate_limiter** | [**PluginSettingsRateLimiter**](was__PluginSettingsRateLimiter.md) |  | [optional] 
**mode** | **str** | Indicates whether specific plugins and/or plugin families are enabled (&#x60;enable&#x60;) or disabled (&#x60;disable&#x60;) for the scan. | [optional] 
**ids** | **list[int]** | A list of plugin IDs. | [optional] 
**names** | **list[str]** | A list of plugin names. | [optional] 
**families** | **list[str]** | A list of plugin families. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


