# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class ScansCredentialsAddHost(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'windows': 'list[ScansCredentialsAddHostWindows]'
    }

    attribute_map = {
        'windows': 'Windows'
    }

    def __init__(self, windows=None, local_vars_configuration=None):  # noqa: E501
        """ScansCredentialsAddHost - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._windows = None
        self.discriminator = None

        if windows is not None:
            self.windows = windows

    @property
    def windows(self):
        """Gets the windows of this ScansCredentialsAddHost.  # noqa: E501

        The name of this parameter corresponds to the display name that uniquely identifies the credentials type (in this case, `Windows` for a Windows credential). This value corresponds to the following response message attributes:  - `credentials[].data[].types[].name` in the [GET /editor/type/templates/{template_uuid}](/reference#editor-template-details) response message  - `credentials[].types[].id` in the [GET /credentials/types](/reference#credentials-list-credential-types) response message`  # noqa: E501

        :return: The windows of this ScansCredentialsAddHost.  # noqa: E501
        :rtype: list[ScansCredentialsAddHostWindows]
        """
        return self._windows

    @windows.setter
    def windows(self, windows):
        """Sets the windows of this ScansCredentialsAddHost.

        The name of this parameter corresponds to the display name that uniquely identifies the credentials type (in this case, `Windows` for a Windows credential). This value corresponds to the following response message attributes:  - `credentials[].data[].types[].name` in the [GET /editor/type/templates/{template_uuid}](/reference#editor-template-details) response message  - `credentials[].types[].id` in the [GET /credentials/types](/reference#credentials-list-credential-types) response message`  # noqa: E501

        :param windows: The windows of this ScansCredentialsAddHost.  # noqa: E501
        :type windows: list[ScansCredentialsAddHostWindows]
        """

        self._windows = windows

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ScansCredentialsAddHost):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, ScansCredentialsAddHost):
            return True

        return self.to_dict() != other.to_dict()
