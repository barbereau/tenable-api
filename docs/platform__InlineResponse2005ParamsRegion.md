# InlineResponse2005ParamsRegion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The AWS region code, for example, &#x60;us-east-1&#x60;. The value of &#x60;All&#x60; indicates that the cloudtrail is associated with all AWS available regions. | [optional] 
**friendly_name** | **str** | The AWS region name, for example, &#x60;US East (N. Virginia)&#x60;. The value of &#x60;All&#x60; indicates that the cloudtrail is associated with all AWS available regions. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


