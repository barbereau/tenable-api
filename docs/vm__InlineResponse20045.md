# InlineResponse20045

A list of scanners and scanner groups.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scanners** | [**list[InlineResponse20045Scanners]**](vm__InlineResponse20045Scanners.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


