# ScannerGroupsGroupIdScannersLicenseApps

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The Tenable products licensed on the scanner. | [optional] 
**consec** | [**ScannerGroupsGroupIdScannersLicenseAppsConsec**](vm__ScannerGroupsGroupIdScannersLicenseAppsConsec.md) |  | [optional] 
**was** | [**ScannerGroupsGroupIdScannersLicenseAppsWas**](vm__ScannerGroupsGroupIdScannersLicenseAppsWas.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


