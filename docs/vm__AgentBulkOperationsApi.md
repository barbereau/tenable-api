# tenableapi.vm.AgentBulkOperationsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bulk_add_agents**](vm__AgentBulkOperationsApi.md#bulk_add_agents) | **POST** /scanners/{scanner_id}/agent-groups/{group_id}/agents/_bulk/add | Add agents to group
[**bulk_remove_agents**](vm__AgentBulkOperationsApi.md#bulk_remove_agents) | **POST** /scanners/{scanner_id}/agent-groups/{group_id}/agents/_bulk/remove | Remove agents from group
[**bulk_task_agent_group_status**](vm__AgentBulkOperationsApi.md#bulk_task_agent_group_status) | **GET** /scanners/{scanner_id}/agent-groups/{group_id}/agents/_bulk/{task_uuid} | Check agent group operation status
[**bulk_task_agent_status**](vm__AgentBulkOperationsApi.md#bulk_task_agent_status) | **GET** /scanners/{scanner_id}/agents/_bulk/{task_uuid} | Check agent operation status
[**bulk_unlink_agents**](vm__AgentBulkOperationsApi.md#bulk_unlink_agents) | **POST** /scanners/{scanner_id}/agents/_bulk/unlink | Unlink agents


# **bulk_add_agents**
> InlineResponse20012 bulk_add_agents(scanner_id, group_id, inline_object9)

Add agents to group

Creates a bulk operation task to add agents to a group.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentBulkOperationsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner for the agent group.
group_id = 'group_id_example' # str | The ID or UUID of the agent group.
inline_object9 = tenableapi.vm.InlineObject9() # InlineObject9 | 

    try:
        # Add agents to group
        api_response = api_instance.bulk_add_agents(scanner_id, group_id, inline_object9)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentBulkOperationsApi->bulk_add_agents: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner for the agent group. | 
 **group_id** | **str**| The ID or UUID of the agent group. | 
 **inline_object9** | [**InlineObject9**](vm__InlineObject9.md)|  | 

### Return type

[**InlineResponse20012**](vm__InlineResponse20012.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates the bulk operation task. |  -  |
**400** | Returned if your request message contains an invalid parameter. |  -  |
**403** | Returned if you do not have permission to create a bulk operation task. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to create the bulk operation task. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bulk_remove_agents**
> InlineResponse20012 bulk_remove_agents(scanner_id, group_id, inline_object10)

Remove agents from group

Creates a bulk operation task to remove agents from a group.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentBulkOperationsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner for the agent group.
group_id = 56 # int | The ID or UUID of the agent group.
inline_object10 = tenableapi.vm.InlineObject10() # InlineObject10 | 

    try:
        # Remove agents from group
        api_response = api_instance.bulk_remove_agents(scanner_id, group_id, inline_object10)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentBulkOperationsApi->bulk_remove_agents: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner for the agent group. | 
 **group_id** | **int**| The ID or UUID of the agent group. | 
 **inline_object10** | [**InlineObject10**](vm__InlineObject10.md)|  | 

### Return type

[**InlineResponse20012**](vm__InlineResponse20012.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates the bulk operation task. |  -  |
**400** | Returned if your request message contains an invalid parameter. |  -  |
**403** | Returned if you do not have permission to create a bulk operation task. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to create the bulk operation task. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bulk_task_agent_group_status**
> InlineResponse20012 bulk_task_agent_group_status(scanner_id, group_id, task_uuid)

Check agent group operation status

Check the status of a bulk operation on an agent group.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentBulkOperationsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner for the agent group.
group_id = 56 # int | The ID or UUID of the agent group.
task_uuid = 'task_uuid_example' # str | The UUID of the task

    try:
        # Check agent group operation status
        api_response = api_instance.bulk_task_agent_group_status(scanner_id, group_id, task_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentBulkOperationsApi->bulk_task_agent_group_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner for the agent group. | 
 **group_id** | **int**| The ID or UUID of the agent group. | 
 **task_uuid** | **str**| The UUID of the task | 

### Return type

[**InlineResponse20012**](vm__InlineResponse20012.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the bulk operation task status information. |  -  |
**403** | Returned if you do not have permission to view the bulk operation task. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bulk_task_agent_status**
> InlineResponse20012 bulk_task_agent_status(scanner_id, task_uuid)

Check agent operation status

Check the status of a bulk operation on agents.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentBulkOperationsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner for the agent group.
task_uuid = 'task_uuid_example' # str | The UUID of the task

    try:
        # Check agent operation status
        api_response = api_instance.bulk_task_agent_status(scanner_id, task_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentBulkOperationsApi->bulk_task_agent_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner for the agent group. | 
 **task_uuid** | **str**| The UUID of the task | 

### Return type

[**InlineResponse20012**](vm__InlineResponse20012.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the bulk operation task status information. |  -  |
**403** | Returned if you do not have permission to view the bulk operation task. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bulk_unlink_agents**
> InlineResponse20012 bulk_unlink_agents(scanner_id, inline_object11)

Unlink agents

Creates a bulk operation task to unlink agents. For more information on unlinked agent data, see [Unlink an Agent](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/UnlinkAnAgent.htm).<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentBulkOperationsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner to remove the agents from.
inline_object11 = tenableapi.vm.InlineObject11() # InlineObject11 | 

    try:
        # Unlink agents
        api_response = api_instance.bulk_unlink_agents(scanner_id, inline_object11)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentBulkOperationsApi->bulk_unlink_agents: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner to remove the agents from. | 
 **inline_object11** | [**InlineObject11**](vm__InlineObject11.md)|  | 

### Return type

[**InlineResponse20012**](vm__InlineResponse20012.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates the bulk operation task. |  -  |
**400** | Returned if your request message contains an invalid parameter. |  -  |
**403** | Returned if you do not have permission create a bulk operation task. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to create the bulk operation task. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

