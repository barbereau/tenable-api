# ErrorResponse

Tenable.io Web Application scanning error response.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reasons** | [**list[ErrorResponseReasons]**](was__ErrorResponseReasons.md) | A list of reasons for the Tenable.io Web Application Scanning error. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


