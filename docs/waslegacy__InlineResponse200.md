# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The unique ID of the asset. | [optional] 
**bios_uuid** | **str** | BIOS UUID of the asset. | [optional] 
**ipv4** | **list[str]** | A list of ipv4 addresses for the asset. | [optional] 
**ipv6** | **list[str]** | A list of ipv6 addresses for the asset. | [optional] 
**hostname** | **list[str]** | A list of hostnames for the asset. | [optional] 
**fqdn** | **list[str]** | A list of FQDNs for the asset. | [optional] 
**ssh_fingerprint** | **str** | The SSH fingerprint for the asset. | [optional] 
**mac_address** | **list[str]** | A list of MAC addresses for the asset. | [optional] 
**netbios_name** | **str** | The NetBIOS name for the asset. | [optional] 
**operating_system** | **str** | The operating system installed on the asset. | [optional] 
**system_type** | **str** | The system architecture (x86) of the asset. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


