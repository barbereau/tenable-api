# ReportInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **str** | The report format version. | [optional] [readonly] 
**created_at** | **datetime** | An ISO timestamp indicating the date and time when the report was generated, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] [readonly] 
**config** | [**ReportConfig**](was__ReportConfig.md) |  | [optional] 
**template** | [**ReportScanTemplate**](was__ReportScanTemplate.md) |  | [optional] 
**user_templates** | [**ReportUserScanTemplate**](was__ReportUserScanTemplate.md) |  | [optional] 
**scan** | [**ReportScan**](Reportwas__Scan.md) |  | [optional] 
**findings** | **object** |  | [optional] 
**notes** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


