# InlineResponse409Conflicts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scanner_uuid** | **str** | The UUID of the scanner involved in the conflict. | [optional] 
**scanner_groups** | [**list[InlineResponse409ScannerGroups]**](vm__InlineResponse409ScannerGroups.md) | A list of scanner groups involved in the conflict. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


