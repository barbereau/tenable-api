# InlineResponse20013

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**private** | **int** |  | [optional] 
**no_target** | **str** |  | [optional] 
**template_uuid** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**owner** | **str** |  | [optional] 
**shared** | **int** |  | [optional] 
**user_permissions** | **int** |  | [optional] 
**last_modification_date** | **int** |  | [optional] 
**creation_date** | **int** |  | [optional] 
**owner_id** | **int** |  | [optional] 
**id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


