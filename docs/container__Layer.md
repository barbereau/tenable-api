# Layer

Detailed information for an image layer.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | **float** | The layer size in kilobytes. | [optional] 
**digest** | **str** | A content-addressable layer identifier. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


