# InlineResponse20085

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**list[InlineResponse20085Values]**](vm__InlineResponse20085Values.md) | An array of tag value objects. | [optional] 
**pagination** | [**InlineResponse20084Pagination**](vm__InlineResponse20084Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


