# InlineResponse20018Pagination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | The total number of objects matching your search criteria. Must be in the int32 format. | [optional] 
**limit** | **int** | Maximum number of objects requested (or service imposed limit if not in request). Must be in the int32 format. | [optional] 
**offset** | **int** | Offset from request (or zero). Must be in the int32 format. | [optional] 
**sort** | [**list[Sort]**](vm__Sort.md) | An array of objects representing the fields you specified as sort fields in the request message, which Tenable.io uses to sort the returned data. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


