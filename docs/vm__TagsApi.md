# tenableapi.vm.TagsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tags_assign_asset_tags**](vm__TagsApi.md#tags_assign_asset_tags) | **POST** /tags/assets/assignments | Add or remove asset tags
[**tags_create_tag_category**](vm__TagsApi.md#tags_create_tag_category) | **POST** /tags/categories | Create tag category
[**tags_create_tag_value**](vm__TagsApi.md#tags_create_tag_value) | **POST** /tags/values | Create tag value
[**tags_delete_tag_category**](vm__TagsApi.md#tags_delete_tag_category) | **DELETE** /tags/categories/{category_uuid} | Delete tag category
[**tags_delete_tag_value**](vm__TagsApi.md#tags_delete_tag_value) | **DELETE** /tags/values/{value_uuid} | Delete tag value
[**tags_delete_tag_values_bulk**](vm__TagsApi.md#tags_delete_tag_values_bulk) | **POST** /tags/values/delete-requests | Bulk delete tag values
[**tags_edit_tag_category**](vm__TagsApi.md#tags_edit_tag_category) | **PUT** /tags/categories/{category_uuid} | Update tag category
[**tags_list_asset_filters**](vm__TagsApi.md#tags_list_asset_filters) | **GET** /tags/assets/filters | List asset tag filters
[**tags_list_asset_tags**](vm__TagsApi.md#tags_list_asset_tags) | **GET** /tags/assets/{asset_uuid}/assignments | List tags for an asset
[**tags_list_tag_categories**](vm__TagsApi.md#tags_list_tag_categories) | **GET** /tags/categories | List tag categories
[**tags_list_tag_values**](vm__TagsApi.md#tags_list_tag_values) | **GET** /tags/values | List tag values
[**tags_tag_category_details**](vm__TagsApi.md#tags_tag_category_details) | **GET** /tags/categories/{category_uuid} | Get category details
[**tags_tag_value_details**](vm__TagsApi.md#tags_tag_value_details) | **GET** /tags/values/{value_uuid} | Get tag value details
[**tags_update_tag_value**](vm__TagsApi.md#tags_update_tag_value) | **PUT** /tags/values/{value_uuid} | Update tag value


# **tags_assign_asset_tags**
> InlineResponse2021 tags_assign_asset_tags(inline_object53)

Add or remove asset tags

Adds or removes tags to/from assets.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    inline_object53 = tenableapi.vm.InlineObject53() # InlineObject53 | 

    try:
        # Add or remove asset tags
        api_response = api_instance.tags_assign_asset_tags(inline_object53)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_assign_asset_tags: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object53** | [**InlineObject53**](vm__InlineObject53.md)|  | 

### Return type

[**InlineResponse2021**](vm__InlineResponse2021.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returns the UUID of the asynchronous asset update job. |  -  |
**400** | Returned if Tenable.io cannot find the specified assets. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_create_tag_category**
> InlineResponse20084Categories tags_create_tag_category(inline_object48)

Create tag category

Creates a new tag category.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    inline_object48 = tenableapi.vm.InlineObject48() # InlineObject48 | 

    try:
        # Create tag category
        api_response = api_instance.tags_create_tag_category(inline_object48)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_create_tag_category: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object48** | [**InlineObject48**](vm__InlineObject48.md)|  | 

### Return type

[**InlineResponse20084Categories**](vm__InlineResponse20084Categories.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates a category. |  -  |
**400** | Returned if Tenable.io encounters any of the following error conditions:  - &#x60;name&#x60;—The &#x60;name&#x60; value in the request message exceeds 127 characters.  - &#x60;description&#x60;—The description value in the request message exceeds 3,000 characters.  - &#x60;max_entries&#x60;—Your request exceeds the maximum number of categories (100 per container).  - &#x60;duplicate&#x60;—A category with the name you specified already exists. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_create_tag_value**
> InlineResponse20086 tags_create_tag_value(inline_object50)

Create tag value

Creates a tag value. The tag category can be specified by UUID or name. If Tenable.io cannot find a category you specify by name, the system creates a new category with the specified name. To automatically apply the tag to assets, specify the rules using the `filters` property.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    inline_object50 = tenableapi.vm.InlineObject50() # InlineObject50 | 

    try:
        # Create tag value
        api_response = api_instance.tags_create_tag_value(inline_object50)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_create_tag_value: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object50** | [**InlineObject50**](vm__InlineObject50.md)|  | 

### Return type

[**InlineResponse20086**](vm__InlineResponse20086.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates a value. |  -  |
**400** | Returned if Tenable.io encounters any of the following error conditions:  - the combination of category and value you specified already exists (&#x60;duplicate&#x60;)  - the category you specified does not exist (&#x60;not_found&#x60;)  - your request exceeded a tag limit for your organization, which can be either the maximum of 100 categories or the maximum 100,000 tags for each category, or as configured for your organization  - your request body is greater than 1 MB  - you attempted to specify more than 3,000 rules for an individual tag (&#x60;Filter expression cannot have more than 3,000 conditions&#x60;)  - your request specified an invalid filter for the dynamic tag rule, for example, &#x60;ipv 4&#x60; (&#x60;The following filter types are not valid: {string}&#x60;). |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_delete_tag_category**
> object tags_delete_tag_category(category_uuid)

Delete tag category

Deletes the specified category and any associated tag values. Deleting an asset tag category automatically deletes all tag values associated with that category and removes the tags from any assets where the tags were assigned.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    category_uuid = 'category_uuid_example' # str | The UUID of the category to delete. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio).

    try:
        # Delete tag category
        api_response = api_instance.tags_delete_tag_category(category_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_delete_tag_category: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_uuid** | **str**| The UUID of the category to delete. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio). | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Returned if Tenable.io successfully deletes the specified tag category. |  -  |
**404** | Returned if Tenable.io cannot find the specified tag category. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_delete_tag_value**
> object tags_delete_tag_value(value_uuid)

Delete tag value

Deletes the specified tag value. If you delete an asset tag, Tenable.io also removes that tag from any assets where the tag was assigned.  **Note:** If you delete all asset tags associated with a category, Tenable.io retains the category. You must [delete](ref:tags-delete-tag-category) the category separately.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    value_uuid = 'value_uuid_example' # str | The UUID of the tag value you want to delete.  **Note:** A tag UUID is technically assigned to the tag value only (the second half of the category:value pair), but the API commands use this value to represent the whole `category:value` pair. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio).

    try:
        # Delete tag value
        api_response = api_instance.tags_delete_tag_value(value_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_delete_tag_value: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value_uuid** | **str**| The UUID of the tag value you want to delete.  **Note:** A tag UUID is technically assigned to the tag value only (the second half of the category:value pair), but the API commands use this value to represent the whole &#x60;category:value&#x60; pair. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio). | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Returned if Tenable.io successfully deletes the specified tag value. |  -  |
**404** | Returned if Tenable.io cannot find the specified tag value. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_delete_tag_values_bulk**
> object tags_delete_tag_values_bulk(inline_object52)

Bulk delete tag values

Deletes tag values in bulk. If you delete an asset tag, Tenable.io also removes that tag from any assets where the tag was assigned. If you delete all asset tags associated with a category, Tenable.io retains the category. You must [delete](ref:tags-delete-tag-category) the category separately.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    inline_object52 = tenableapi.vm.InlineObject52() # InlineObject52 | 

    try:
        # Bulk delete tag values
        api_response = api_instance.tags_delete_tag_values_bulk(inline_object52)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_delete_tag_values_bulk: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object52** | [**InlineObject52**](vm__InlineObject52.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deletes the specified tag values. |  -  |
**400** | Returned if you specify invalid UUIDs. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_edit_tag_category**
> InlineResponse20084Categories tags_edit_tag_category(category_uuid, inline_object49)

Update tag category

Updates the specified tag category.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    category_uuid = 'category_uuid_example' # str | The UUID of the category. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio).
inline_object49 = tenableapi.vm.InlineObject49() # InlineObject49 | 

    try:
        # Update tag category
        api_response = api_instance.tags_edit_tag_category(category_uuid, inline_object49)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_edit_tag_category: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_uuid** | **str**| The UUID of the category. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio). | 
 **inline_object49** | [**InlineObject49**](vm__InlineObject49.md)|  | 

### Return type

[**InlineResponse20084Categories**](vm__InlineResponse20084Categories.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully updates the tag category. |  -  |
**400** | Returned if Tenable.io encounters any of the following error conditions:  - &#x60;name&#x60;—The &#x60;name&#x60; value in the request message exceeds 127 characters.  - &#x60;description&#x60;—The &#x60;description&#x60; value in the request message exceeds 3,000 characters.  - Your request message does not specify a value for the tag category name. |  -  |
**404** | Returned if Tenable.io cannot find the specified tag category. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_list_asset_filters**
> list[InlineResponse20088] tags_list_asset_filters()

List asset tag filters

Returns a list of filters that you can use to create the rules for applying dynamic tags. Includes the field or tag names to match, the operators that you can use with the filter, and the rules for matching the values ('control' field), for example, a list of valid values.  For definitions of asset attribute filters you might use in tag rules, see [Asset Attribute Definitions](doc:common-asset-attributes#asset-attribute-definitions). <p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    
    try:
        # List asset tag filters
        api_response = api_instance.tags_list_asset_filters()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_list_asset_filters: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse20088]**](vm__InlineResponse20088.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of filters. |  -  |
**400** | Returned if Tenable.io cannot find the specified assets. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_list_asset_tags**
> InlineResponse20087 tags_list_asset_tags(asset_uuid)

List tags for an asset

Returns a list of assigned tags for an asset specified by UUID.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    asset_uuid = 'asset_uuid_example' # str | The UUID of the asset.

    try:
        # List tags for an asset
        api_response = api_instance.tags_list_asset_tags(asset_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_list_asset_tags: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_uuid** | [**str**](.md)| The UUID of the asset. | 

### Return type

[**InlineResponse20087**](vm__InlineResponse20087.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of tags assigned to the specified asset. If no tags are assigned to the asset, Tenable.io returns an empty list. Also, the service does not validate the asset UUID. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_list_tag_categories**
> InlineResponse20084 tags_list_tag_categories(f=f, ft=ft, limit=limit, offset=offset, sort=sort)

List tag categories

Returns a list of tag categories.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    f = 'f_example' # str | A filter condition in the `field:operator:value` format, for example, `f=name:match:location`. Filter conditions can include:   * name:eq:&lt;name>   * name:match:<partial\\_name>   * description:eq:&lt;description>   * description:match:<partial\\_description>   * updated\\_at:date-eq:<timestamp\\_as\\_int>   * updated\\_at:date-gt:<timestamp\\_as\\_int>   * updated\\_at:date-lt:<timestamp\\_as\\_int>   * created\\_at:date-eq:<timestamp\\_as\\_int>   * created\\_at:date-gt:<timestamp\\_as\\_int>   * created\\_at:date-lt:<timestamp\\_as\\_int>   * updated\\_by:eq:<user\\_uuid> (optional)
ft = 'ft_example' # str | If multiple `f` parameters are present, specifies whether Tenable.io applies `AND` or `OR` to conditions. Supported values are `and` and `or`. If you omit this parameter when using multiple `f` parameters, Tenable.io applies `AND` by default. (optional)
limit = 56 # int | Maximum number of records requested (or service imposed limit if not in request). Must be in the int32 format. (optional)
offset = 56 # int | The number of records to skip in the returned result set. Must be in the int32 format. (optional)
sort = 'sort_example' # str | The fields to sort on and the sort order, for example, `sort=updated_at:desc,name`. Default sort order is `asc`. If you specify multiple fields, fields must be separated by commas. (optional)

    try:
        # List tag categories
        api_response = api_instance.tags_list_tag_categories(f=f, ft=ft, limit=limit, offset=offset, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_list_tag_categories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **f** | **str**| A filter condition in the &#x60;field:operator:value&#x60; format, for example, &#x60;f&#x3D;name:match:location&#x60;. Filter conditions can include:   * name:eq:&amp;lt;name&gt;   * name:match:&lt;partial\\_name&gt;   * description:eq:&amp;lt;description&gt;   * description:match:&lt;partial\\_description&gt;   * updated\\_at:date-eq:&lt;timestamp\\_as\\_int&gt;   * updated\\_at:date-gt:&lt;timestamp\\_as\\_int&gt;   * updated\\_at:date-lt:&lt;timestamp\\_as\\_int&gt;   * created\\_at:date-eq:&lt;timestamp\\_as\\_int&gt;   * created\\_at:date-gt:&lt;timestamp\\_as\\_int&gt;   * created\\_at:date-lt:&lt;timestamp\\_as\\_int&gt;   * updated\\_by:eq:&lt;user\\_uuid&gt; | [optional] 
 **ft** | **str**| If multiple &#x60;f&#x60; parameters are present, specifies whether Tenable.io applies &#x60;AND&#x60; or &#x60;OR&#x60; to conditions. Supported values are &#x60;and&#x60; and &#x60;or&#x60;. If you omit this parameter when using multiple &#x60;f&#x60; parameters, Tenable.io applies &#x60;AND&#x60; by default. | [optional] 
 **limit** | **int**| Maximum number of records requested (or service imposed limit if not in request). Must be in the int32 format. | [optional] 
 **offset** | **int**| The number of records to skip in the returned result set. Must be in the int32 format. | [optional] 
 **sort** | **str**| The fields to sort on and the sort order, for example, &#x60;sort&#x3D;updated_at:desc,name&#x60;. Default sort order is &#x60;asc&#x60;. If you specify multiple fields, fields must be separated by commas. | [optional] 

### Return type

[**InlineResponse20084**](vm__InlineResponse20084.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of tag categories with pagination information. |  -  |
**400** | Returned if your request specifies invalid or malformed query parameters. Tenable.io can encounter the following error conditions:  - &#x60;invalidvalue&#x60;—The query parameter format is incorrect, for example, uses an invalid operator as in this example: &#x60;f&#x3D;name:invalid_operator:some_value&#x60;. - &#x60;unknownproperty&#x60;—The query parameter format is correct, but it references a field that does not exist, for example &#x60;sort&#x3D;non_existing_property:desc&#x60;. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_list_tag_values**
> InlineResponse20085 tags_list_tag_values(f=f, ft=ft, wf=wf, w=w, limit=limit, offset=offset, sort=sort)

List tag values

Returns a list of tag values. **Note:** The list can also include the tag categories that do not have any associated values.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    f = 'f_example' # str | A filter condition in the `field:operator:value` format, for example, `f=value:match:rhel`. Filters should match `field:op:value` format. Filter conditions can include:   * value:eq:&lt;value>   * value:match:&lt;value>   * category\\_name:match:<partial\\_value>   * category\\_name:eq:<category\\_name>   * category\\_name:match:<partial\\_category\\_name>   * description:eq:&lt;description>   * description:match:<partial\\_description>   * updated\\_at:date-eq:<timestamp\\_as\\_int>   * updated\\_at:date-gt:<timestamp\\_as\\_int>   * updated\\_at:date-lt:<timestamp\\_as\\_int>   * updated\\_by:eq:<user\\_uuid> (optional)
ft = 'ft_example' # str | If multiple `f` parameters are present, specifies whether Tenable.io applies `AND` or `OR` to conditions. Supported values are `and` and `or`. If you omit this parameter when using multiple `f` parameters, Tenable.io applies `AND` by default. (optional)
wf = 'wf_example' # str | A comma-separated list of fields to include in the wildcard search. Provides the same functionality as a `match` condition in the `f` in parameter. For example, `f=value:match:Chi` returns the same results as `wf=value&w=Chi`. Wildcard fields include:   * category\\_name   * value   * description   Use the `w` parameter to specify the search value. (optional)
w = 'w_example' # str | A single search value for the wildcard fields specified in the `wf` parameter. (optional)
limit = 56 # int | Maximum number of records requested (or service imposed limit if not in request). Must be in the int32 format. (optional)
offset = 56 # int | The number of records to skip in the returned result set. Must be in the int32 format. (optional)
sort = 'sort_example' # str | The fields to sort on, for example, `sort=updated_at:desc,value`. Default sort order is `asc`. If you specify multiple fields, fields must be separated by commas. (optional)

    try:
        # List tag values
        api_response = api_instance.tags_list_tag_values(f=f, ft=ft, wf=wf, w=w, limit=limit, offset=offset, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_list_tag_values: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **f** | **str**| A filter condition in the &#x60;field:operator:value&#x60; format, for example, &#x60;f&#x3D;value:match:rhel&#x60;. Filters should match &#x60;field:op:value&#x60; format. Filter conditions can include:   * value:eq:&amp;lt;value&gt;   * value:match:&amp;lt;value&gt;   * category\\_name:match:&lt;partial\\_value&gt;   * category\\_name:eq:&lt;category\\_name&gt;   * category\\_name:match:&lt;partial\\_category\\_name&gt;   * description:eq:&amp;lt;description&gt;   * description:match:&lt;partial\\_description&gt;   * updated\\_at:date-eq:&lt;timestamp\\_as\\_int&gt;   * updated\\_at:date-gt:&lt;timestamp\\_as\\_int&gt;   * updated\\_at:date-lt:&lt;timestamp\\_as\\_int&gt;   * updated\\_by:eq:&lt;user\\_uuid&gt; | [optional] 
 **ft** | **str**| If multiple &#x60;f&#x60; parameters are present, specifies whether Tenable.io applies &#x60;AND&#x60; or &#x60;OR&#x60; to conditions. Supported values are &#x60;and&#x60; and &#x60;or&#x60;. If you omit this parameter when using multiple &#x60;f&#x60; parameters, Tenable.io applies &#x60;AND&#x60; by default. | [optional] 
 **wf** | **str**| A comma-separated list of fields to include in the wildcard search. Provides the same functionality as a &#x60;match&#x60; condition in the &#x60;f&#x60; in parameter. For example, &#x60;f&#x3D;value:match:Chi&#x60; returns the same results as &#x60;wf&#x3D;value&amp;w&#x3D;Chi&#x60;. Wildcard fields include:   * category\\_name   * value   * description   Use the &#x60;w&#x60; parameter to specify the search value. | [optional] 
 **w** | **str**| A single search value for the wildcard fields specified in the &#x60;wf&#x60; parameter. | [optional] 
 **limit** | **int**| Maximum number of records requested (or service imposed limit if not in request). Must be in the int32 format. | [optional] 
 **offset** | **int**| The number of records to skip in the returned result set. Must be in the int32 format. | [optional] 
 **sort** | **str**| The fields to sort on, for example, &#x60;sort&#x3D;updated_at:desc,value&#x60;. Default sort order is &#x60;asc&#x60;. If you specify multiple fields, fields must be separated by commas. | [optional] 

### Return type

[**InlineResponse20085**](vm__InlineResponse20085.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of tags with pagination information. |  -  |
**400** | Returned if your request specifies invalid or malformed query parameters. Tenable.io can encounter the following error conditions:  - &#x60;invalidvalue&#x60;—The query parameter format is incorrect, for example, uses an invalid operator as in this example: &#x60;f&#x3D;name:invalid_operator:some_value&#x60;. - &#x60;unknownproperty&#x60;—The query parameter format is correct, but it references a field that does not exist, for example &#x60;sort&#x3D;non_existing_property:desc&#x60;. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_tag_category_details**
> InlineResponse20084Categories tags_tag_category_details(category_uuid)

Get category details

Returns the details for the specified category.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    category_uuid = 'category_uuid_example' # str | The UUID of the tag category to return details for. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio).

    try:
        # Get category details
        api_response = api_instance.tags_tag_category_details(category_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_tag_category_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_uuid** | **str**| The UUID of the tag category to return details for. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio). | 

### Return type

[**InlineResponse20084Categories**](vm__InlineResponse20084Categories.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the category details. |  -  |
**404** | Returned if Tenable.io cannot find the specified tag category. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_tag_value_details**
> InlineResponse20086 tags_tag_value_details(value_uuid)

Get tag value details

Returns the details for specified tag value.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    value_uuid = 'value_uuid_example' # str | The UUID of the tag value. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio).

    try:
        # Get tag value details
        api_response = api_instance.tags_tag_value_details(value_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_tag_value_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value_uuid** | **str**| The UUID of the tag value. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio). | 

### Return type

[**InlineResponse20086**](vm__InlineResponse20086.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the details for tag value. |  -  |
**404** | Returned if Tenable.io cannot find the specified tag value. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tags_update_tag_value**
> InlineResponse20086 tags_update_tag_value(value_uuid, inline_object51)

Update tag value

Updates the specified tag value.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.TagsApi(api_client)
    value_uuid = 'value_uuid_example' # str | The UUID of the value you want to update. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio).
inline_object51 = tenableapi.vm.InlineObject51() # InlineObject51 | 

    try:
        # Update tag value
        api_response = api_instance.tags_update_tag_value(value_uuid, inline_object51)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TagsApi->tags_update_tag_value: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value_uuid** | **str**| The UUID of the value you want to update. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio). | 
 **inline_object51** | [**InlineObject51**](vm__InlineObject51.md)|  | 

### Return type

[**InlineResponse20086**](vm__InlineResponse20086.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully updates the tag value. |  -  |
**400** | Returned if Tenable.io encounters any of the following error conditions:  - the combination of category and value you specified already exists (&#x60;duplicate&#x60;)  - the category you specified does not exist (&#x60;not_found&#x60;)  - your request exceeded a tag limit for your organization, which can be either the maximum of 100 categories or the maximum 100,000 tags for each category, or as configured for your organization  - your request body is greater than 1 MB  - you attempted to specify more than 3,000 rules for an individual tag (&#x60;Filter expression cannot have more than 3,000 conditions&#x60;)  - your request specified an invalid filter for the dynamic tag rule, for example, &#x60;ipv 4&#x60; (&#x60;The following filter types are not valid: {string}&#x60;). |  -  |
**404** | Returned if Tenable.io cannot find the specified tag value. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

