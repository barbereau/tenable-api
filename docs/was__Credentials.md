# Credentials

The credentials the scanner uses to run an authenticated scan against the web application. For more information, see the managed credentials category in [Manage Credentials](doc:manage-credentials).
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credential_ids** | **list[str]** | A list of credentials UUIDs. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


