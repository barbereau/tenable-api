# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20094Assets(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'id': 'str',
        'has_agent': 'bool',
        'last_seen': 'str',
        'last_scan_target': 'str',
        'sources': 'list[InlineResponse20037Sources]',
        'acr_score': 'int',
        'acr_drivers': 'list[InlineResponse20013AcrDrivers]',
        'exposure_score': 'int',
        'scan_frequency': 'list[InlineResponse20013ScanFrequency]',
        'ipv4': 'list[str]',
        'ipv6': 'list[str]',
        'fqdn': 'list[str]',
        'netbios_name': 'list[str]',
        'operating_system': 'list[str]',
        'agent_name': 'list[str]',
        'aws_ec2_name': 'list[str]',
        'mac_address': 'list[str]',
        'bigfix_asset_id': 'list[str]'
    }

    attribute_map = {
        'id': 'id',
        'has_agent': 'has_agent',
        'last_seen': 'last_seen',
        'last_scan_target': 'last_scan_target',
        'sources': 'sources',
        'acr_score': 'acr_score',
        'acr_drivers': 'acr_drivers',
        'exposure_score': 'exposure_score',
        'scan_frequency': 'scan_frequency',
        'ipv4': 'ipv4',
        'ipv6': 'ipv6',
        'fqdn': 'fqdn',
        'netbios_name': 'netbios_name',
        'operating_system': 'operating_system',
        'agent_name': 'agent_name',
        'aws_ec2_name': 'aws_ec2_name',
        'mac_address': 'mac_address',
        'bigfix_asset_id': 'bigfix_asset_id'
    }

    def __init__(self, id=None, has_agent=None, last_seen=None, last_scan_target=None, sources=None, acr_score=None, acr_drivers=None, exposure_score=None, scan_frequency=None, ipv4=None, ipv6=None, fqdn=None, netbios_name=None, operating_system=None, agent_name=None, aws_ec2_name=None, mac_address=None, bigfix_asset_id=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20094Assets - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._id = None
        self._has_agent = None
        self._last_seen = None
        self._last_scan_target = None
        self._sources = None
        self._acr_score = None
        self._acr_drivers = None
        self._exposure_score = None
        self._scan_frequency = None
        self._ipv4 = None
        self._ipv6 = None
        self._fqdn = None
        self._netbios_name = None
        self._operating_system = None
        self._agent_name = None
        self._aws_ec2_name = None
        self._mac_address = None
        self._bigfix_asset_id = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if has_agent is not None:
            self.has_agent = has_agent
        if last_seen is not None:
            self.last_seen = last_seen
        if last_scan_target is not None:
            self.last_scan_target = last_scan_target
        if sources is not None:
            self.sources = sources
        if acr_score is not None:
            self.acr_score = acr_score
        if acr_drivers is not None:
            self.acr_drivers = acr_drivers
        if exposure_score is not None:
            self.exposure_score = exposure_score
        if scan_frequency is not None:
            self.scan_frequency = scan_frequency
        if ipv4 is not None:
            self.ipv4 = ipv4
        if ipv6 is not None:
            self.ipv6 = ipv6
        if fqdn is not None:
            self.fqdn = fqdn
        if netbios_name is not None:
            self.netbios_name = netbios_name
        if operating_system is not None:
            self.operating_system = operating_system
        if agent_name is not None:
            self.agent_name = agent_name
        if aws_ec2_name is not None:
            self.aws_ec2_name = aws_ec2_name
        if mac_address is not None:
            self.mac_address = mac_address
        if bigfix_asset_id is not None:
            self.bigfix_asset_id = bigfix_asset_id

    @property
    def id(self):
        """Gets the id of this InlineResponse20094Assets.  # noqa: E501

        The UUID of the asset.  # noqa: E501

        :return: The id of this InlineResponse20094Assets.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20094Assets.

        The UUID of the asset.  # noqa: E501

        :param id: The id of this InlineResponse20094Assets.  # noqa: E501
        :type id: str
        """

        self._id = id

    @property
    def has_agent(self):
        """Gets the has_agent of this InlineResponse20094Assets.  # noqa: E501

        A value specifying whether a Nessus agent scan detected the asset (`true`).  # noqa: E501

        :return: The has_agent of this InlineResponse20094Assets.  # noqa: E501
        :rtype: bool
        """
        return self._has_agent

    @has_agent.setter
    def has_agent(self, has_agent):
        """Sets the has_agent of this InlineResponse20094Assets.

        A value specifying whether a Nessus agent scan detected the asset (`true`).  # noqa: E501

        :param has_agent: The has_agent of this InlineResponse20094Assets.  # noqa: E501
        :type has_agent: bool
        """

        self._has_agent = has_agent

    @property
    def last_seen(self):
        """Gets the last_seen of this InlineResponse20094Assets.  # noqa: E501

        The ISO timestamp of the scan that most recently detected the asset.  # noqa: E501

        :return: The last_seen of this InlineResponse20094Assets.  # noqa: E501
        :rtype: str
        """
        return self._last_seen

    @last_seen.setter
    def last_seen(self, last_seen):
        """Sets the last_seen of this InlineResponse20094Assets.

        The ISO timestamp of the scan that most recently detected the asset.  # noqa: E501

        :param last_seen: The last_seen of this InlineResponse20094Assets.  # noqa: E501
        :type last_seen: str
        """

        self._last_seen = last_seen

    @property
    def last_scan_target(self):
        """Gets the last_scan_target of this InlineResponse20094Assets.  # noqa: E501

        The IPv4 address, IPv6 address, or FQDN that the scanner last used to evaluate the asset.  # noqa: E501

        :return: The last_scan_target of this InlineResponse20094Assets.  # noqa: E501
        :rtype: str
        """
        return self._last_scan_target

    @last_scan_target.setter
    def last_scan_target(self, last_scan_target):
        """Sets the last_scan_target of this InlineResponse20094Assets.

        The IPv4 address, IPv6 address, or FQDN that the scanner last used to evaluate the asset.  # noqa: E501

        :param last_scan_target: The last_scan_target of this InlineResponse20094Assets.  # noqa: E501
        :type last_scan_target: str
        """

        self._last_scan_target = last_scan_target

    @property
    def sources(self):
        """Gets the sources of this InlineResponse20094Assets.  # noqa: E501

        A list of sources for the asset record.  # noqa: E501

        :return: The sources of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[InlineResponse20037Sources]
        """
        return self._sources

    @sources.setter
    def sources(self, sources):
        """Sets the sources of this InlineResponse20094Assets.

        A list of sources for the asset record.  # noqa: E501

        :param sources: The sources of this InlineResponse20094Assets.  # noqa: E501
        :type sources: list[InlineResponse20037Sources]
        """

        self._sources = sources

    @property
    def acr_score(self):
        """Gets the acr_score of this InlineResponse20094Assets.  # noqa: E501

        The Asset Criticality Rating (ACR) for the asset. Tenable assigns an ACR to each asset on your network to represent the asset's relative risk as an integer from 1 to 10. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license.  # noqa: E501

        :return: The acr_score of this InlineResponse20094Assets.  # noqa: E501
        :rtype: int
        """
        return self._acr_score

    @acr_score.setter
    def acr_score(self, acr_score):
        """Sets the acr_score of this InlineResponse20094Assets.

        The Asset Criticality Rating (ACR) for the asset. Tenable assigns an ACR to each asset on your network to represent the asset's relative risk as an integer from 1 to 10. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license.  # noqa: E501

        :param acr_score: The acr_score of this InlineResponse20094Assets.  # noqa: E501
        :type acr_score: int
        """

        self._acr_score = acr_score

    @property
    def acr_drivers(self):
        """Gets the acr_drivers of this InlineResponse20094Assets.  # noqa: E501

        The key drivers that Tenable uses to calculate an asset's Tenable-provided ACR. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license.  # noqa: E501

        :return: The acr_drivers of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[InlineResponse20013AcrDrivers]
        """
        return self._acr_drivers

    @acr_drivers.setter
    def acr_drivers(self, acr_drivers):
        """Sets the acr_drivers of this InlineResponse20094Assets.

        The key drivers that Tenable uses to calculate an asset's Tenable-provided ACR. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license.  # noqa: E501

        :param acr_drivers: The acr_drivers of this InlineResponse20094Assets.  # noqa: E501
        :type acr_drivers: list[InlineResponse20013AcrDrivers]
        """

        self._acr_drivers = acr_drivers

    @property
    def exposure_score(self):
        """Gets the exposure_score of this InlineResponse20094Assets.  # noqa: E501

        The Asset Exposure Score (AES) for the asset. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license.  # noqa: E501

        :return: The exposure_score of this InlineResponse20094Assets.  # noqa: E501
        :rtype: int
        """
        return self._exposure_score

    @exposure_score.setter
    def exposure_score(self, exposure_score):
        """Sets the exposure_score of this InlineResponse20094Assets.

        The Asset Exposure Score (AES) for the asset. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license.  # noqa: E501

        :param exposure_score: The exposure_score of this InlineResponse20094Assets.  # noqa: E501
        :type exposure_score: int
        """

        self._exposure_score = exposure_score

    @property
    def scan_frequency(self):
        """Gets the scan_frequency of this InlineResponse20094Assets.  # noqa: E501

        Information about how often scans ran against the asset during specified intervals. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license.  # noqa: E501

        :return: The scan_frequency of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[InlineResponse20013ScanFrequency]
        """
        return self._scan_frequency

    @scan_frequency.setter
    def scan_frequency(self, scan_frequency):
        """Sets the scan_frequency of this InlineResponse20094Assets.

        Information about how often scans ran against the asset during specified intervals. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license.  # noqa: E501

        :param scan_frequency: The scan_frequency of this InlineResponse20094Assets.  # noqa: E501
        :type scan_frequency: list[InlineResponse20013ScanFrequency]
        """

        self._scan_frequency = scan_frequency

    @property
    def ipv4(self):
        """Gets the ipv4 of this InlineResponse20094Assets.  # noqa: E501

        A list of ipv4 addresses for the asset.  # noqa: E501

        :return: The ipv4 of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[str]
        """
        return self._ipv4

    @ipv4.setter
    def ipv4(self, ipv4):
        """Sets the ipv4 of this InlineResponse20094Assets.

        A list of ipv4 addresses for the asset.  # noqa: E501

        :param ipv4: The ipv4 of this InlineResponse20094Assets.  # noqa: E501
        :type ipv4: list[str]
        """

        self._ipv4 = ipv4

    @property
    def ipv6(self):
        """Gets the ipv6 of this InlineResponse20094Assets.  # noqa: E501

        A list of ipv6 addresses for the asset.  # noqa: E501

        :return: The ipv6 of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[str]
        """
        return self._ipv6

    @ipv6.setter
    def ipv6(self, ipv6):
        """Sets the ipv6 of this InlineResponse20094Assets.

        A list of ipv6 addresses for the asset.  # noqa: E501

        :param ipv6: The ipv6 of this InlineResponse20094Assets.  # noqa: E501
        :type ipv6: list[str]
        """

        self._ipv6 = ipv6

    @property
    def fqdn(self):
        """Gets the fqdn of this InlineResponse20094Assets.  # noqa: E501

        A list of fully-qualified domain names (FQDNs) for the asset.  # noqa: E501

        :return: The fqdn of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[str]
        """
        return self._fqdn

    @fqdn.setter
    def fqdn(self, fqdn):
        """Sets the fqdn of this InlineResponse20094Assets.

        A list of fully-qualified domain names (FQDNs) for the asset.  # noqa: E501

        :param fqdn: The fqdn of this InlineResponse20094Assets.  # noqa: E501
        :type fqdn: list[str]
        """

        self._fqdn = fqdn

    @property
    def netbios_name(self):
        """Gets the netbios_name of this InlineResponse20094Assets.  # noqa: E501

        The NetBIOS name for the asset.  # noqa: E501

        :return: The netbios_name of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[str]
        """
        return self._netbios_name

    @netbios_name.setter
    def netbios_name(self, netbios_name):
        """Sets the netbios_name of this InlineResponse20094Assets.

        The NetBIOS name for the asset.  # noqa: E501

        :param netbios_name: The netbios_name of this InlineResponse20094Assets.  # noqa: E501
        :type netbios_name: list[str]
        """

        self._netbios_name = netbios_name

    @property
    def operating_system(self):
        """Gets the operating_system of this InlineResponse20094Assets.  # noqa: E501

        The operating system installed on the asset.  # noqa: E501

        :return: The operating_system of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[str]
        """
        return self._operating_system

    @operating_system.setter
    def operating_system(self, operating_system):
        """Sets the operating_system of this InlineResponse20094Assets.

        The operating system installed on the asset.  # noqa: E501

        :param operating_system: The operating_system of this InlineResponse20094Assets.  # noqa: E501
        :type operating_system: list[str]
        """

        self._operating_system = operating_system

    @property
    def agent_name(self):
        """Gets the agent_name of this InlineResponse20094Assets.  # noqa: E501

        The names of any Nessus agents that scanned and identified the asset.  # noqa: E501

        :return: The agent_name of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[str]
        """
        return self._agent_name

    @agent_name.setter
    def agent_name(self, agent_name):
        """Sets the agent_name of this InlineResponse20094Assets.

        The names of any Nessus agents that scanned and identified the asset.  # noqa: E501

        :param agent_name: The agent_name of this InlineResponse20094Assets.  # noqa: E501
        :type agent_name: list[str]
        """

        self._agent_name = agent_name

    @property
    def aws_ec2_name(self):
        """Gets the aws_ec2_name of this InlineResponse20094Assets.  # noqa: E501

        The name of the virtual machine instance in AWS EC2.  # noqa: E501

        :return: The aws_ec2_name of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[str]
        """
        return self._aws_ec2_name

    @aws_ec2_name.setter
    def aws_ec2_name(self, aws_ec2_name):
        """Sets the aws_ec2_name of this InlineResponse20094Assets.

        The name of the virtual machine instance in AWS EC2.  # noqa: E501

        :param aws_ec2_name: The aws_ec2_name of this InlineResponse20094Assets.  # noqa: E501
        :type aws_ec2_name: list[str]
        """

        self._aws_ec2_name = aws_ec2_name

    @property
    def mac_address(self):
        """Gets the mac_address of this InlineResponse20094Assets.  # noqa: E501

        A list of MAC addresses for the asset.  # noqa: E501

        :return: The mac_address of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[str]
        """
        return self._mac_address

    @mac_address.setter
    def mac_address(self, mac_address):
        """Sets the mac_address of this InlineResponse20094Assets.

        A list of MAC addresses for the asset.  # noqa: E501

        :param mac_address: The mac_address of this InlineResponse20094Assets.  # noqa: E501
        :type mac_address: list[str]
        """

        self._mac_address = mac_address

    @property
    def bigfix_asset_id(self):
        """Gets the bigfix_asset_id of this InlineResponse20094Assets.  # noqa: E501

        The unique identifier of the asset in IBM BigFix. For more information, see the IBM BigFix documentation.  # noqa: E501

        :return: The bigfix_asset_id of this InlineResponse20094Assets.  # noqa: E501
        :rtype: list[str]
        """
        return self._bigfix_asset_id

    @bigfix_asset_id.setter
    def bigfix_asset_id(self, bigfix_asset_id):
        """Sets the bigfix_asset_id of this InlineResponse20094Assets.

        The unique identifier of the asset in IBM BigFix. For more information, see the IBM BigFix documentation.  # noqa: E501

        :param bigfix_asset_id: The bigfix_asset_id of this InlineResponse20094Assets.  # noqa: E501
        :type bigfix_asset_id: list[str]
        """

        self._bigfix_asset_id = bigfix_asset_id

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20094Assets):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20094Assets):
            return True

        return self.to_dict() != other.to_dict()
