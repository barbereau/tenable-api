from __future__ import absolute_import

# flake8: noqa

# import apis into api package
from tenableapi.waslegacy.api.assets_api import AssetsApi
from tenableapi.waslegacy.api.editor_api import EditorApi
from tenableapi.waslegacy.api.folders_api import FoldersApi
from tenableapi.waslegacy.api.policies_api import PoliciesApi
from tenableapi.waslegacy.api.scanner_groups_api import ScannerGroupsApi
from tenableapi.waslegacy.api.scanners_api import ScannersApi
from tenableapi.waslegacy.api.scans_api import ScansApi
