# ConfigInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the scan configuration. | 
**owner_id** | **str** | The UUID of the owner of the scan configuration. | 
**target** | **str** | The URL of the target web application. | 
**template_id** | **str** | The UUID of the Tenable-provided configuration template. | 
**user_template_id** | **str** | The UUID of the user-defined configuration template from which this configuration was derived. | [optional] 
**scanner_id** | **int** | The ID of the scanner (if the &#x60;type&#x60; is set to &#x60;managed_webapp&#x60;), or scanner group (if the type is &#x60;pool&#x60; or &#x60;local&#x60;) that performs the scan. | [optional] 
**schedule** | [**ScanSchedule**](was__ScanSchedule.md) |  | [optional] 
**settings** | [**ConfigSettings**](was__ConfigSettings.md) |  | [optional] 
**notifications** | [**Notifications**](was__Notifications.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


