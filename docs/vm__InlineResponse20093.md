# InlineResponse20093

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plugin_output** | **str** | The plugin&#39;s output about the vulnerability. May be an empty string. | [optional] 
**states** | [**list[WorkbenchesVulnerabilitiesPluginIdOutputsStates]**](vm__WorkbenchesVulnerabilitiesPluginIdOutputsStates.md) | Vulnerability state items. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


