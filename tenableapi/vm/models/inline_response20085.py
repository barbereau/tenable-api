# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20085(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'values': 'list[InlineResponse20085Values]',
        'pagination': 'InlineResponse20084Pagination'
    }

    attribute_map = {
        'values': 'values',
        'pagination': 'pagination'
    }

    def __init__(self, values=None, pagination=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20085 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._values = None
        self._pagination = None
        self.discriminator = None

        if values is not None:
            self.values = values
        if pagination is not None:
            self.pagination = pagination

    @property
    def values(self):
        """Gets the values of this InlineResponse20085.  # noqa: E501

        An array of tag value objects.  # noqa: E501

        :return: The values of this InlineResponse20085.  # noqa: E501
        :rtype: list[InlineResponse20085Values]
        """
        return self._values

    @values.setter
    def values(self, values):
        """Sets the values of this InlineResponse20085.

        An array of tag value objects.  # noqa: E501

        :param values: The values of this InlineResponse20085.  # noqa: E501
        :type values: list[InlineResponse20085Values]
        """

        self._values = values

    @property
    def pagination(self):
        """Gets the pagination of this InlineResponse20085.  # noqa: E501


        :return: The pagination of this InlineResponse20085.  # noqa: E501
        :rtype: InlineResponse20084Pagination
        """
        return self._pagination

    @pagination.setter
    def pagination(self, pagination):
        """Sets the pagination of this InlineResponse20085.


        :param pagination: The pagination of this InlineResponse20085.  # noqa: E501
        :type pagination: InlineResponse20084Pagination
        """

        self._pagination = pagination

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20085):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20085):
            return True

        return self.to_dict() != other.to_dict()
