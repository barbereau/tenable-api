# tenableapi.vm.AssetsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**assets_asset_info**](vm__AssetsApi.md#assets_asset_info) | **GET** /assets/{asset_uuid} | Get asset details
[**assets_bulk_delete**](vm__AssetsApi.md#assets_bulk_delete) | **POST** /api/v2/assets/bulk-jobs/delete | Bulk delete assets
[**assets_bulk_move**](vm__AssetsApi.md#assets_bulk_move) | **POST** /api/v2/assets/bulk-jobs/move-to-network | Move assets
[**assets_bulk_update_acr**](vm__AssetsApi.md#assets_bulk_update_acr) | **POST** /api/v2/assets/bulk-jobs/acr | Update ACR
[**assets_import**](vm__AssetsApi.md#assets_import) | **POST** /import/assets | Import assets
[**assets_import_job_info**](vm__AssetsApi.md#assets_import_job_info) | **GET** /import/asset-jobs/{asset_import_job_uuid} | Get import job information
[**assets_list_assets**](vm__AssetsApi.md#assets_list_assets) | **GET** /assets | List assets
[**assets_list_import_jobs**](vm__AssetsApi.md#assets_list_import_jobs) | **GET** /import/asset-jobs | List asset import jobs


# **assets_asset_info**
> InlineResponse20014 assets_asset_info(asset_uuid)

Get asset details

Returns details of the specified asset.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AssetsApi(api_client)
    asset_uuid = 'asset_uuid_example' # str | The UUID of the asset.

    try:
        # Get asset details
        api_response = api_instance.assets_asset_info(asset_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->assets_asset_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_uuid** | **str**| The UUID of the asset. | 

### Return type

[**InlineResponse20014**](vm__InlineResponse20014.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns details of the specified asset. |  -  |
**403** | Returned if you do not have permission to view information about an asset. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **assets_bulk_delete**
> InlineResponse202 assets_bulk_delete(inline_object13)

Bulk delete assets

Deletes the specified assets. This request creates an asynchronous delete job in Tenable.io.  For information about the assets bulk delete workflow and payload examples, see [Bulk Asset Operations](doc:bulk-asset-operations).<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AssetsApi(api_client)
    inline_object13 = tenableapi.vm.InlineObject13() # InlineObject13 | 

    try:
        # Bulk delete assets
        api_response = api_instance.assets_bulk_delete(inline_object13)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->assets_bulk_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object13** | [**InlineObject13**](vm__InlineObject13.md)|  | 

### Return type

[**InlineResponse202**](vm__InlineResponse202.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returns the number of deleted assets. |  -  |
**400** | Returned if you specify an invalid asset query, for example, using a malformed IPv4 address. |  -  |
**403** | Returned if you do not have permission to delete assets in bulk. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**503** | Returned if Tenable.io is unavailable or not ready to process the request. Wait a moment and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **assets_bulk_move**
> InlineResponse202 assets_bulk_move(inline_object12)

Move assets

Moves assets from the specified network to another network. You can use this endpoint to move assets from the default network to a user-defined network, from a user-defined network to the default network, and from one user-defined network to another user-defined network. This request creates an asynchronous job in Tenable.io.  For information about the assets move workflow and payload examples, see [Bulk Asset Operations](doc:bulk-asset-operations).<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AssetsApi(api_client)
    inline_object12 = tenableapi.vm.InlineObject12() # InlineObject12 | 

    try:
        # Move assets
        api_response = api_instance.assets_bulk_move(inline_object12)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->assets_bulk_move: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object12** | [**InlineObject12**](vm__InlineObject12.md)|  | 

### Return type

[**InlineResponse202**](vm__InlineResponse202.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returns the number of moved assets. |  -  |
**400** | Returned if Tenable.io cannot find the specified assets. |  -  |
**403** | Returned if you do not have permission to move assets. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **assets_bulk_update_acr**
> assets_bulk_update_acr(inline_object)

Update ACR

Overwrites the Tenable-provided Asset Criticality Rating (ACR) for the specified assets. Tenable assigns an ACR to each asset on your network to represent the asset's relative risk as an integer from 1 to 10. For more information about ACR, see [Lumin metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  You must have a Lumin license to update the ACR for assets in your organization.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AssetsApi(api_client)
    inline_object = [tenableapi.vm.InlineObject()] # list[InlineObject] | 

    try:
        # Update ACR
        api_instance.assets_bulk_update_acr(inline_object)
    except ApiException as e:
        print("Exception when calling AssetsApi->assets_bulk_update_acr: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object** | [**list[InlineObject]**](vm__InlineObject.md)|  | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returned if Tenable.io successfully queues the update request. |  -  |
**404** | Returned if your request is improperly formatted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **assets_import**
> InlineResponse20015 assets_import(inline_object14)

Import assets

Imports asset data in JSON format.  The request size cannot exceed 5 MB. For example, if the average asset record you want to import is about 2 KB, you can import approximately 2,500 assets in a single request.  **Note:** This endpoint does not support the network_id attribute in asset objects for import. Tenable.io automatically assigns imported assets to the default network object. For more information about network objects, see [Manage Networks](doc:manage-networks-tio).<p>Requires SCAN OPERATOR [24] user permissions and CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AssetsApi(api_client)
    inline_object14 = tenableapi.vm.InlineObject14() # InlineObject14 | 

    try:
        # Import assets
        api_response = api_instance.assets_import(inline_object14)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->assets_import: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object14** | [**InlineObject14**](vm__InlineObject14.md)|  | 

### Return type

[**InlineResponse20015**](vm__InlineResponse20015.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the import job UUID. |  -  |
**400** | Returned if you submitted a bad request. |  -  |
**403** | Returned if you do not have permission to import assets. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **assets_import_job_info**
> InlineResponse20016 assets_import_job_info(asset_import_job_uuid)

Get import job information

Gets information about the specified import job.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AssetsApi(api_client)
    asset_import_job_uuid = 'asset_import_job_uuid_example' # str | The UUID of the asset import job.

    try:
        # Get import job information
        api_response = api_instance.assets_import_job_info(asset_import_job_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->assets_import_job_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_import_job_uuid** | **str**| The UUID of the asset import job. | 

### Return type

[**InlineResponse20016**](vm__InlineResponse20016.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns information about the specified import job. |  -  |
**403** | Returned if you do not have permission to list asset import jobs. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **assets_list_assets**
> InlineResponse20013 assets_list_assets()

List assets

Lists up to 5,000 assets.  **Note:** You can use the [POST /assets/export](ref:exports-assets-request-export) endpoint to export data for all assets. For more information about exporting assets, see [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AssetsApi(api_client)
    
    try:
        # List assets
        api_response = api_instance.assets_list_assets()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->assets_list_assets: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20013**](vm__InlineResponse20013.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of assets. |  -  |
**403** | Returned if you do not have permission to list assets. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **assets_list_import_jobs**
> list[InlineResponse20016] assets_list_import_jobs()

List asset import jobs

Lists asset import jobs.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AssetsApi(api_client)
    
    try:
        # List asset import jobs
        api_response = api_instance.assets_list_import_jobs()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->assets_list_import_jobs: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse20016]**](vm__InlineResponse20016.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of asset import jobs. |  -  |
**403** | Returned if you do not have permission to list asset import jobs. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

