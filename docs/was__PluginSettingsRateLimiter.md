# PluginSettingsRateLimiter

Optional parameters for limiting the rate at which HTTP requests are sent to the target web application.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requests_per_second** | **int** | The maximum number of HTTP requests per second. | [optional] [default to 25]
**autothrottle** | **bool** | Indicates whether the scanner slows down the scan when network congestion is detected (&#x60;true&#x60;). The default value of this parameter is &#x60;true&#x60;. | [optional] [default to True]
**timeout_threshold** | **int** | The number of consecutive timeouts before Tenable.io Web Application Scanning aborts the scan. | [optional] [default to 100]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


