# InputErrorResponseFields

A reason for the Tenable.io Web Application Scanning input error.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **str** | The JSON path of the invalid field. | [optional] 
**message_type** | **str** | The type of input violation. Common input errors include:  - &#x60;VALUE_MUST_BE_SPECIFIED&#x60;—Returned if a required body parameter is missing.  - &#x60;VALUE_NOT_POSITIVE&#x60;—Returned if your input contains a negative integer when it should be positive.  - &#x60;VALUE_IS_NEGATIVE&#x60;—Returned if your input contains a positive integer when it should be negative.  - &#x60;VALUE_CANT_BE_CHANGED&#x60;—Returned if you attempted to change a value that cannot be changed.  - &#x60;VALUE_MUST_BE_GREATER&#x60;—Returned if your input contains a value that is smaller than the minimum required lower bound.  - &#x60;FIELD_CANT_BE_ADDED&#x60;—Returned if your input contains a field that can&#39;t be added.  - &#x60;INVALID_TYPE&#x60;—Returned if your input contains an invalid type (ex. integer or string).  - &#x60;INVALID_FORMAT&#x60;—Returned if your input contains an invalid format. | [optional] 
**value** | **object** | Contains an error message describing the cause of the input validation error. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


