# tenableapi.vm.AgentsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**agent_group_list_agents**](vm__AgentsApi.md#agent_group_list_agents) | **GET** /scanners/{scanner_id}/agent-groups/{agent_group_id}/agents | List agents for agent group
[**agents_delete**](vm__AgentsApi.md#agents_delete) | **DELETE** /scanners/{scanner_id}/agents/{agent_id} | Unlink agent
[**agents_get**](vm__AgentsApi.md#agents_get) | **GET** /scanners/{scanner_id}/agents/{agent_id} | Get agent details
[**agents_list**](vm__AgentsApi.md#agents_list) | **GET** /scanners/{scanner_id}/agents | List agents for scanner


# **agent_group_list_agents**
> InlineResponse20011 agent_group_list_agents(scanner_id, agent_group_id, offset=offset, limit=limit, sort=sort, f=f, ft=ft, w=w, wf=wf)

List agents for agent group

Returns a list of agents for the specified agent group.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner to query for agents. You can find the ID by using the [GET /scanners](ref:scanners-list) endpoint.
agent_group_id = 56 # int | The ID of the agent group to query for agents. You can find the ID by using the [GET /scanners/{scanner_id}/agent-groups](ref:agent-groups-list) endpoint.
offset = 56 # int | The starting record to retrieve. If you omit this parameter, Tenable.io uses the default value of 0. (optional)
limit = 56 # int | The number of records to retrieve. If you omit this parameter, Tenable.io uses a default of 50 records. The minimum supported limit is 1, and the maximum supported limit is 5,000. (optional)
sort = 'sort_example' # str | The sort order of the returned records. Sort can only be applied to the sortable\\_fields specified by the filter capabilities. There may be no more than max\\_sort\\_fields number of columns used in the sort, as specified by the filter capabilities. Sort is applied, in order, in the following format: `:\\[asc|desc\\],:\\[asc|desc\\]`. For example, `sort=field1:asc,field2:desc` would first sort by field1, ascending, then sort by field2, descending. (optional)
f = 'f_example' # str | Apply a filter in the format `::`. For example, `field1:match:sometext` would match any records where the value of field1 contains `sometext`. You can use multiple query filters. For a list of supported filters, use the [GET /filters/scans/agents](ref:filters-agents-filters) endpoint. (optional)
ft = 'ft_example' # str | Filter type. If the filter type is `and`, the record is only returned if all filters match. If the filter type is `or`, the record is returned if any of the filters match. (optional)
w = 'w_example' # str | Wildcard filter text. Wildcard search is a mechanism where multiple fields of a record are filtered against one specific filter string. If any one of the wildcard\\_fields' values matches against the filter string, then the record matches the wildcard filter. For a record to be returned, it must pass the wildcard filter (if there is one) AND the set of standard filters. For example, if `w=wild&f=field1:match:one&f=field2:match:two&ft=or`, the record would match if the value of any supported wildcard\\_fields contained `wild`, AND either field1's value contained `one` or field2's value contained `two`. (optional)
wf = 'wf_example' # str | A comma-delimited subset of wildcard\\_fields to search when applying the wildcard filter. For example, `field1,field2`. If `w` is provided, but `wf` is not, then all wildcard\\_fields' values are searched against the wildcard filter text. (optional)

    try:
        # List agents for agent group
        api_response = api_instance.agent_group_list_agents(scanner_id, agent_group_id, offset=offset, limit=limit, sort=sort, f=f, ft=ft, w=w, wf=wf)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentsApi->agent_group_list_agents: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner to query for agents. You can find the ID by using the [GET /scanners](ref:scanners-list) endpoint. | 
 **agent_group_id** | **int**| The ID of the agent group to query for agents. You can find the ID by using the [GET /scanners/{scanner_id}/agent-groups](ref:agent-groups-list) endpoint. | 
 **offset** | **int**| The starting record to retrieve. If you omit this parameter, Tenable.io uses the default value of 0. | [optional] 
 **limit** | **int**| The number of records to retrieve. If you omit this parameter, Tenable.io uses a default of 50 records. The minimum supported limit is 1, and the maximum supported limit is 5,000. | [optional] 
 **sort** | **str**| The sort order of the returned records. Sort can only be applied to the sortable\\_fields specified by the filter capabilities. There may be no more than max\\_sort\\_fields number of columns used in the sort, as specified by the filter capabilities. Sort is applied, in order, in the following format: &#x60;:\\[asc|desc\\],:\\[asc|desc\\]&#x60;. For example, &#x60;sort&#x3D;field1:asc,field2:desc&#x60; would first sort by field1, ascending, then sort by field2, descending. | [optional] 
 **f** | **str**| Apply a filter in the format &#x60;::&#x60;. For example, &#x60;field1:match:sometext&#x60; would match any records where the value of field1 contains &#x60;sometext&#x60;. You can use multiple query filters. For a list of supported filters, use the [GET /filters/scans/agents](ref:filters-agents-filters) endpoint. | [optional] 
 **ft** | **str**| Filter type. If the filter type is &#x60;and&#x60;, the record is only returned if all filters match. If the filter type is &#x60;or&#x60;, the record is returned if any of the filters match. | [optional] 
 **w** | **str**| Wildcard filter text. Wildcard search is a mechanism where multiple fields of a record are filtered against one specific filter string. If any one of the wildcard\\_fields&#39; values matches against the filter string, then the record matches the wildcard filter. For a record to be returned, it must pass the wildcard filter (if there is one) AND the set of standard filters. For example, if &#x60;w&#x3D;wild&amp;f&#x3D;field1:match:one&amp;f&#x3D;field2:match:two&amp;ft&#x3D;or&#x60;, the record would match if the value of any supported wildcard\\_fields contained &#x60;wild&#x60;, AND either field1&#39;s value contained &#x60;one&#x60; or field2&#39;s value contained &#x60;two&#x60;. | [optional] 
 **wf** | **str**| A comma-delimited subset of wildcard\\_fields to search when applying the wildcard filter. For example, &#x60;field1,field2&#x60;. If &#x60;w&#x60; is provided, but &#x60;wf&#x60; is not, then all wildcard\\_fields&#39; values are searched against the wildcard filter text. | [optional] 

### Return type

[**InlineResponse20011**](vm__InlineResponse20011.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of agents for an agent group. |  -  |
**400** | Returned if you specify invalid query parameters, for example: - invalid filter field name - invalid filter operator - invalid filter value - invalid wildcard filter field name - invalid filter type - invalid sort parameter |  -  |
**403** | Returned if you do not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agents_delete**
> object agents_delete(scanner_id, agent_id)

Unlink agent

Unlinks an agent. For more information on unlinked agent data, see [Unlink an Agent](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/UnlinkAnAgent.htm).<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.
agent_id = 56 # int | The ID of the agent to unlink.

    try:
        # Unlink agent
        api_response = api_instance.agents_delete(scanner_id, agent_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentsApi->agents_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 
 **agent_id** | **int**| The ID of the agent to unlink. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully unlinked the agent. |  -  |
**404** | Returned if Tenable.io cannot find the specified agent. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to unlink the agent. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agents_get**
> InlineResponse20011Agents agents_get(scanner_id, agent_id)

Get agent details

Returns the specified agent details for the specified scanner.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner to query for agents.
agent_id = 56 # int | The ID of the agent to query.

    try:
        # Get agent details
        api_response = api_instance.agents_get(scanner_id, agent_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentsApi->agents_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner to query for agents. | 
 **agent_id** | **int**| The ID of the agent to query. | 

### Return type

[**InlineResponse20011Agents**](vm__InlineResponse20011Agents.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the agent details. |  -  |
**403** | Returned if you do not have permission to view the agent. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agents_list**
> InlineResponse20011 agents_list(scanner_id, offset=offset, limit=limit, sort=sort, f=f, ft=ft, w=w, wf=wf)

List agents for scanner

Returns a list of agents for the specified scanner.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner to query for agents.
offset = 56 # int | The starting record to retrieve. If you omit this parameter, Tenable.io uses the default value of 0. (optional)
limit = 56 # int | The number of records to retrieve. If you omit this parameter, Tenable.io uses a default of 50 records. The minimum supported limit is 1, and the maximum supported limit is 5,000. (optional)
sort = 'sort_example' # str | The sort order of the returned records. Sort can only be applied to the sortable\\_fields specified by the filter capabilities. There may be no more than max\\_sort\\_fields number of columns used in the sort, as specified by the filter capabilities. Sort is applied, in order, in the following format: `:\\[asc|desc\\],:\\[asc|desc\\]`. For example, `sort=field1:asc,field2:desc` would first sort by field1, ascending, then sort by field2, descending. (optional)
f = 'f_example' # str | Apply a filter in the format `::`. For example, `field1:match:sometext` would match any records where the value of field1 contains `sometext`. You can use multiple query filters. For a list of supported filters, use the [GET /filters/scans/agents](ref:filters-agents-filters) endpoint. (optional)
ft = 'ft_example' # str | Filter type. If the filter type is `and`, the record is only returned if all filters match. If the filter type is `or`, the record is returned if any of the filters match. (optional)
w = 'w_example' # str | Wildcard filter text. Wildcard search is a mechanism where multiple fields of a record are filtered against one specific filter string. If any one of the wildcard\\_fields' values matches against the filter string, then the record matches the wildcard filter. For a record to be returned, it must pass the wildcard filter (if there is one) AND the set of standard filters. For example, if `w=wild&f=field1:match:one&f=field2:match:two&ft=or`, the record would match if the value of any supported wildcard\\_fields contained `wild`, AND either field1's value contained `one` or field2's value contained `two`. (optional)
wf = 'wf_example' # str | A comma-delimited subset of wildcard\\_fields to search when applying the wildcard filter. For example, `field1,field2`. If `w` is provided, but `wf` is not, then all wildcard\\_fields' values are searched against the wildcard filter text. (optional)

    try:
        # List agents for scanner
        api_response = api_instance.agents_list(scanner_id, offset=offset, limit=limit, sort=sort, f=f, ft=ft, w=w, wf=wf)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentsApi->agents_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner to query for agents. | 
 **offset** | **int**| The starting record to retrieve. If you omit this parameter, Tenable.io uses the default value of 0. | [optional] 
 **limit** | **int**| The number of records to retrieve. If you omit this parameter, Tenable.io uses a default of 50 records. The minimum supported limit is 1, and the maximum supported limit is 5,000. | [optional] 
 **sort** | **str**| The sort order of the returned records. Sort can only be applied to the sortable\\_fields specified by the filter capabilities. There may be no more than max\\_sort\\_fields number of columns used in the sort, as specified by the filter capabilities. Sort is applied, in order, in the following format: &#x60;:\\[asc|desc\\],:\\[asc|desc\\]&#x60;. For example, &#x60;sort&#x3D;field1:asc,field2:desc&#x60; would first sort by field1, ascending, then sort by field2, descending. | [optional] 
 **f** | **str**| Apply a filter in the format &#x60;::&#x60;. For example, &#x60;field1:match:sometext&#x60; would match any records where the value of field1 contains &#x60;sometext&#x60;. You can use multiple query filters. For a list of supported filters, use the [GET /filters/scans/agents](ref:filters-agents-filters) endpoint. | [optional] 
 **ft** | **str**| Filter type. If the filter type is &#x60;and&#x60;, the record is only returned if all filters match. If the filter type is &#x60;or&#x60;, the record is returned if any of the filters match. | [optional] 
 **w** | **str**| Wildcard filter text. Wildcard search is a mechanism where multiple fields of a record are filtered against one specific filter string. If any one of the wildcard\\_fields&#39; values matches against the filter string, then the record matches the wildcard filter. For a record to be returned, it must pass the wildcard filter (if there is one) AND the set of standard filters. For example, if &#x60;w&#x3D;wild&amp;f&#x3D;field1:match:one&amp;f&#x3D;field2:match:two&amp;ft&#x3D;or&#x60;, the record would match if the value of any supported wildcard\\_fields contained &#x60;wild&#x60;, AND either field1&#39;s value contained &#x60;one&#x60; or field2&#39;s value contained &#x60;two&#x60;. | [optional] 
 **wf** | **str**| A comma-delimited subset of wildcard\\_fields to search when applying the wildcard filter. For example, &#x60;field1,field2&#x60;. If &#x60;w&#x60; is provided, but &#x60;wf&#x60; is not, then all wildcard\\_fields&#39; values are searched against the wildcard filter text. | [optional] 

### Return type

[**InlineResponse20011**](vm__InlineResponse20011.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of agents. |  -  |
**403** | Returned if you do not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

