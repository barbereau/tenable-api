# RepositoryDetails

Details of a Tenable.io Container Security repository.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the repository. | 
**description** | **str** | The description of the repository. | [optional] 
**images_count** | **int** | The total number of images in the repository. | 
**labels_count** | **int** | The number of unique image name/tag combinations in the repository. | 
**vulnerabilities_count** | **int** | The total number of discovered vulnerabilities for the images in the repository. | 
**malware_count** | **int** | The total number of known malware exploits for the images in the repository. | 
**pull_count** | **int** | The number of times the image was pulled from the repository. | 
**push_count** | **int** | The number of times the image was uploaded to the repository | 
**total_bytes** | **int** | The total size in bytes of the images in the repository. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


