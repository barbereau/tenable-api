# InlineResponse2006

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**is_agent** | **bool** |  | [optional] 
**settings** | **object** |  | [optional] 
**credentials** | **object** |  | [optional] 
**plugins** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


