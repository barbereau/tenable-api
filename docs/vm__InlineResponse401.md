# InlineResponse401

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_code** | **str** | The HTTP status code. | [optional] 
**error** | **int** | The HTTP status text. | [optional] 
**message** | **str** | A brief message describing the error Tenable.io encountered. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


