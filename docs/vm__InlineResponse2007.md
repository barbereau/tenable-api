# InlineResponse2007

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rules** | [**list[InlineResponse2007Rules]**](vm__InlineResponse2007Rules.md) | An array specifying values to use when constructing an asset rule for the [POST /access-groups](ref:io-v2-access-groups-create) and [PUT /access-groups/{id}](ref:io-v2-access-groups-edit) methods. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


