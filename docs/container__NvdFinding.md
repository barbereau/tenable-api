# NvdFinding

The details for the discovered vulnerability, including description, external references, and remediation information.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cve** | **str** | The Common Vulnerabilities and Exposures (CVE) ID for vulnerability. | [optional] 
**description** | **str** | The extended description of the vulnerability. | [optional] 
**published_date** | **str** | An ISO timestamp indicating the date when the vulnerability definition was published, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**modified_date** | **str** | An ISO timestamp indicating the date when the vulnerability definition was updated, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**cvss_score** | **str** | The CVSSv2 base score (intrinsic and fundamental characteristics of a vulnerability that are constant over time and user environments). | [optional] 
**access_vector** | **str** | The CVSSv2 Access Vector (AV) metric for the vulnerability indicating how the vulnerability can be exploited. Possible values include:  - Local  - Adjacent Network  - Network | [optional] 
**access_complexity** | **str** | The CVSSv2 Access Complexity (AC) metric for the vulnerability. Possible values include:  - High  - Medium  - Low | [optional] 
**auth** | **str** | The CVSSv2 Authentication (Au) metric for the vulnerability. The metric describes the number of times that an attacker must authenticate to a target to exploit it. Possible values include:  - None required  - Single  - Multiple | [optional] 
**availability_impact** | **str** | The CVSSv2 availability impact metric for the vulnerability. The metric describes the impact on the availability of the target system. Possible values include:  - None  - Partial  - Complete | [optional] 
**confidentiality_impact** | **str** | The CVSSv2 confidentiality impact metric for the vulnerability. The metric describes the impact on the confidentiality of data processed by the system. Possible values include:  - None  - Partial  - Complete | [optional] 
**integrity_impact** | **str** | The CVSSv2 integrity impact metric for the vulnerability. The metric describes the impact on the integrity of the exploited system. Possible values include:  - None  - Partial  - Complete | [optional] 
**cwe** | **str** | The Common Weakness Enumeration (CWE) ID for vulnerability. | [optional] 
**cpe** | **list[str]** | The systems the vulnerability affects identified by Common Platform Enumeration (CPE). | [optional] 
**remediation** | **str** | Remediation information for the vulnerability. | [optional] 
**references** | **list[str]** | Additional references to third-party information about the vulnerability. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


