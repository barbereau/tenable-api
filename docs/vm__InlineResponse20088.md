# InlineResponse20088

A filter definition. Includes the field to be matched, the operators that you can use with the filter, and the rules for matching the values (`control` field), for example, a list of valid values.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the asset attribute or tag. | [optional] 
**readable_name** | **str** | The asset attribute name displayed in the Tenable.io user interface. | [optional] 
**control** | [**TagsAssetsFiltersControl**](vm__TagsAssetsFiltersControl.md) |  | [optional] 
**operators** | **list[str]** | The comparison operators that can be used for the filter. To find supported operators, use the [GET /tags/assets/filters](ref:tags-list-asset-filters) endpoint. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


