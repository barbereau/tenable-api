# InlineResponse20094Assets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The UUID of the asset. | [optional] 
**has_agent** | **bool** | A value specifying whether a Nessus agent scan detected the asset (&#x60;true&#x60;). | [optional] 
**last_seen** | **str** | The ISO timestamp of the scan that most recently detected the asset. | [optional] 
**last_scan_target** | **str** | The IPv4 address, IPv6 address, or FQDN that the scanner last used to evaluate the asset. | [optional] 
**sources** | [**list[InlineResponse20037Sources]**](vm__InlineResponse20037Sources.md) | A list of sources for the asset record. | [optional] 
**acr_score** | **int** | The Asset Criticality Rating (ACR) for the asset. Tenable assigns an ACR to each asset on your network to represent the asset&#39;s relative risk as an integer from 1 to 10. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license. | [optional] 
**acr_drivers** | [**list[InlineResponse20013AcrDrivers]**](vm__InlineResponse20013AcrDrivers.md) | The key drivers that Tenable uses to calculate an asset&#39;s Tenable-provided ACR. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license. | [optional] 
**exposure_score** | **int** | The Asset Exposure Score (AES) for the asset. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license. | [optional] 
**scan_frequency** | [**list[InlineResponse20013ScanFrequency]**](vm__InlineResponse20013ScanFrequency.md) | Information about how often scans ran against the asset during specified intervals. For more information, see [Lumin Metrics](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/LuminMetrics.htm) in the *Tenable.io Vulnerability Management User Guide*.  This attribute is only present if you have a Lumin license. | [optional] 
**ipv4** | **list[str]** | A list of ipv4 addresses for the asset. | [optional] 
**ipv6** | **list[str]** | A list of ipv6 addresses for the asset. | [optional] 
**fqdn** | **list[str]** | A list of fully-qualified domain names (FQDNs) for the asset. | [optional] 
**netbios_name** | **list[str]** | The NetBIOS name for the asset. | [optional] 
**operating_system** | **list[str]** | The operating system installed on the asset. | [optional] 
**agent_name** | **list[str]** | The names of any Nessus agents that scanned and identified the asset. | [optional] 
**aws_ec2_name** | **list[str]** | The name of the virtual machine instance in AWS EC2. | [optional] 
**mac_address** | **list[str]** | A list of MAC addresses for the asset. | [optional] 
**bigfix_asset_id** | **list[str]** | The unique identifier of the asset in IBM BigFix. For more information, see the IBM BigFix documentation. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


