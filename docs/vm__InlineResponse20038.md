# InlineResponse20038

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileuploaded** | **str** | The name of the uploaded file. If the file with the same name already exists, Tenable.io appends an underscore with a number. For example, &#x60;scan-targets_1.txt&#x60;. Use this attribute value when referencing the file for subsequent requests. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


