# InlineResponse20062Scans

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legacy** | **bool** | A value indicating whether the scan results were created before a change in storage method. If &#x60;true&#x60;, Tenable.io stores the results in the old storage method. If &#x60;false&#x60;, Tenable.io stores the results in the new storage method. | [optional] 
**permissions** | **int** | The requesting user&#39;s permissions for the scan. | [optional] 
**type** | **str** | The type of scan. | [optional] 
**read** | **bool** | A value indicating whether the user account associated with the request message has viewed the scan in the Tenable.io user interface. If &#x60;1&#x60;, the user account has viewed the scan results. | [optional] 
**last_modification_date** | **int** | For newly-created scans, the date on which the scan configuration was created. For scans that have been launched at least once, this attribute does not represent the date on which the scan configuration was last modified. Instead, it represents the date on which the scan was last launched, in Unix time format. Tenable.io updates this attribute each time the scan launches. | [optional] 
**creation_date** | **int** | For newly-created scans, the date on which the scan configuration was originally created. For scans that have been launched at least once, this attribute does not represent the date on which the scan configuration was originally created. Instead, it represents the date on which the scan was first launched, in Unix time format. | [optional] 
**status** | **str** | The status of the scan. For a list of possible values, see [Scan Status](doc:scan-status-tio). | [optional] 
**uuid** | **str** | The UUID of the scan. | [optional] 
**shared** | **bool** | If &#x60;true&#x60;, the scan is shared with users other than the scan owner. The level of sharing is specified in the &#x60;acls&#x60; attribute of the scan details. | [optional] 
**user_permissions** | **int** | The sharing permissions for the scan. | [optional] 
**owner** | **str** | The owner of the scan. | [optional] 
**schedule_uuid** | **str** | The UUID for a specific instance in the scan schedule. | [optional] 
**timezone** | **str** | The timezone of the scheduled start time for the scan. | [optional] 
**rrules** | **str** | The interval at which the scan repeats. The interval is formatted as a string of three values delimited by semi-colons. These values are: the frequency (FREQ&#x3D;ONETIME or DAILY or WEEKLY or MONTHLY or YEARLY), the interval (INTERVAL&#x3D;1 or 2 or 3 ... x), and the days of the week (BYDAY&#x3D;SU,MO,TU,WE,TH,FR,SA). For a scan that runs every three weeks on Monday Wednesday and Friday, the string would be &#x60;FREQ&#x3D;WEEKLY;INTERVAL&#x3D;3;BYDAY&#x3D;MO,WE,FR&#x60;. If the scan is not scheduled to recur, this attribute is &#x60;null&#x60;. For more information, see [rrules Format](example-assessment-scan-recurring#rrules-format).  **Note:** To set the &#x60;rrules&#x60; parameter for an agent scan, the request must also include the following body parameters:&lt;ul&gt;&lt;li&gt;The &#x60;uuid&#x60; parameter must specify an agent scan template. For more information, see [Tenable-Provided Agent Templates](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Scans/AgentTemplates.htm) and the [GET /editor/scan/templates](ref:editor-list-templates) endpoint.&lt;/li&gt;&lt;li&gt;The &#x60;agent_group_id&#x60; parameter must specify an agent group. For more information, see [Agent Groups](ref:agent-groups).&lt;/li&gt;&lt;/ul&gt;For an example request body for an agent scan, see [Example Agent Scan: Recurring](doc:example-agent-scan-recurring). | [optional] 
**starttime** | **str** | For one-time scans, the starting time and date for the scan. For recurrent scans, the first date on which the scan schedule is active and the time that recurring scans launch based on the &#x60;rrules&#x60; attribute.  This attribute has the following format: &#x60;YYYYMMDDTHHMMSS&#x60;. | [optional] 
**enabled** | **bool** | Indicates whether the scan schedule is active (&#x60;true&#x60;) or inactive (&#x60;false&#x60;). | [optional] 
**control** | **bool** | If &#x60;true&#x60;, the scan has a schedule and can be launched. | [optional] 
**wizard_uuid** | **str** | The UUID of the Tenable-provided template used to create either the scan or the user-defined template (policy) on which the scan configuration is based. | [optional] 
**policy_id** | **int** | The unique ID of the user-defined template (policy) on which the scan configuration is based. | [optional] 
**name** | **str** | The name of the scan. | [optional] 
**id** | **int** | The unique ID of the scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


