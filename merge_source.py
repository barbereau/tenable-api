#!/usr/bin/env python
import argparse
import sys, os, glob, shutil, copy
import pathlib
from pprint import pprint as pp

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="description", epilog="epilog")
    parser.add_argument("--source", type=str, help="source folder to use")
    parser.add_argument("--destination", type=str, help="destination folder to use")
    parser.add_argument("--key", type=str, help="keyword to use")
    args = parser.parse_args()
    print("%s,%s" % (args.source, args.destination))
    docs_source="%s/docs/"%args.source
    docs_destination="%s/docs/"%args.destination
    try:
        pathlib.Path(docs_destination).mkdir( parents=True, exist_ok=True)
    except:
        raise
        pass
    current_path=os.getcwd()
    os.chdir(docs_source)

    md_files_source = glob.glob("*.md")
    md_files_destination = {}
    for file in md_files_source:
        md_files_destination[file]=args.key+"_"+file

    # getback
    os.chdir(current_path)
    for file in md_files_source:
        # shutil.copyfile(docs_source+file, docs_destination+md_files_destination[file])
        tranform_file=open(docs_source+file,"r")
        target_file=open(docs_destination+md_files_destination[file], "w")
        for l in tranform_file.readlines():
            l_orig=copy.copy(l)
            for k in md_files_source:
                l=l.replace(k, md_files_destination[k])
            #if l!=l_orig:
            #    print(file)
            target_file.write(l)
        #md_files_destination[file]=args.key+"_"+file
        print("Move %s %s" % (docs_source+file, docs_destination+md_files_destination[file]))
        tranform_file.close()
        target_file.close()
#    pp(md_files_destination)
    # do the main README
    tranform_file=open(args.source+"README.md","r")
    target_file=open(args.destination+'/'+args.key+"README.md", "w")
    for l in tranform_file.readlines():
        l_orig=copy.copy(l)
        for k in md_files_source:
            l=l.replace(k, md_files_destination[k])
        #if l!=l_orig:
        #    print(file)
        target_file.write(l)
    tranform_file.close()
    target_file.close()
