# Pagination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | The total number of records matching your search criteria. Must be in the int32 format. | [optional] 
**limit** | **int** | Maximum number of records requested (or service imposed limit if not in request). Must be in the int32 format. | [optional] 
**offset** | **int** | The number of skipped records in the returned result set. Must be in the int32 format. | [optional] 
**sort** | [**list[PaginationSort]**](container__PaginationSort.md) | An array of objects representing the fields you specified as sort parameters in the request. This attribute is only present if your request message specifies sort parameters. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


