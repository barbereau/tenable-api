# InlineResponse2008

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_unlink** | [**InlineResponse2008AutoUnlink**](vm__InlineResponse2008AutoUnlink.md) |  | [optional] 
**software_update** | **bool** | If true, software updates are enabled for agents pursuant to any agent exclusions that are in effect. If false, software updates are disabled for all agents, even if no agent exclusions are in effect. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


