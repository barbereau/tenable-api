# TagsValuesFiltersAssetAnd

A rule for applying the tag to assets by matching asset properties or other tags. Includes a field or tag name, an operator, and a value.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **str** | The asset attribute name or tag to match. | [optional] 
**operator** | **str** | The operator to apply to the matched value, for example, equals, does not equal, or contains. To find supported operators, use the [GET /tags/assets/filters](ref:tags-list-asset-filters) endpoint. | [optional] 
**value** | **str** | The asset attribute value or tag to match. You can specify multiple values separated by commas, for example, \&quot;172.204.81.57,172.82.157.177,172.156.65.8,172.207.124.176,172.106.217.225\&quot;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


