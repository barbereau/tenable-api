# InlineObject29

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acls** | [**list[InlineResponse20046]**](vm__InlineResponse20046.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


