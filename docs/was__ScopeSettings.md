# ScopeSettings

The URLs and file types that you want to include or exclude from your scan.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**option** | **str** | Specifies how the scanner handles the URLs it finds during the application crawl. Options include:  - all—Crawl all detected URLs.  - paths—Limit crawling to specified URLs and child paths.  - urls—Limit crawling to specified URLs. | [optional] 
**urls** | **list[str]** | A list of URLs to include in scan. | [optional] [default to []]
**exclude_file_extensions** | **list[str]** | A list of file extensions to exclude from scan. | [optional] [default to []]
**exclude_path_patterns** | **list[str]** | A list of URL path regex patterns to exclude from scan. | [optional] [default to []]
**dom_depth_limit** | **int** | The maximum depth of HTML DOM elements to crawl. | [optional] [default to 5]
**directory_depth_limit** | **int** | The maximum number of sub-directories to crawl. | [optional] [default to 10]
**page_limit** | **int** | The maximum number of unique URLs to crawl. | [optional] [default to 10000]
**crawl_script** | **object** | Selenium crawl script for auditing specific parts of web application. | [optional] 
**decompose_paths** | **bool** | Indicates whether or not the scanner crawls additional URL paths (&#x60;true&#x60;). For example, if http://example.com/dir1/dir2/dir3/index.html is detected during a scan, then http://example.com/dir1/dir2/dir3/, http://example.com/dir1/dir2/, and http://example.com/dir1/ will be added to the queue. | [optional] [default to False]
**exclude_binaries** | **bool** | Indicates whether or not to exclude binaries from the assessment. | [optional] 
**auto_redundant_paths** | **int** | Indicates the number of redundant paths to be assessed. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


