# InlineResponse200TwoFactor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sms_phone** | **str** | The phone number used for two-factor authentication. The phone number begins with the &#x60;+&#x60; character and the country code. This field is required when sms_enabled is set to &#x60;true&#x60;. | [optional] 
**sms_enabled** | **int** | Indicates whether two-factor authentication is enabled (&#x60;1&#x60;) or disabled (&#x60;0&#x60;). | [optional] 
**email_enabled** | **int** | Indicates whether backup notification for two-factor authentication is enabled (&#x60;1&#x60;) or disabled (&#x60;0&#x60;). If enabled, Tenable.io sends the two-factor verification code via e-mail, as well as via the default SMS message. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


