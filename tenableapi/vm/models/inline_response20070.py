# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20070(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'file': 'str',
        'temp_token': 'str'
    }

    attribute_map = {
        'file': 'file',
        'temp_token': 'temp_token'
    }

    def __init__(self, file=None, temp_token=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20070 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._file = None
        self._temp_token = None
        self.discriminator = None

        if file is not None:
            self.file = file
        if temp_token is not None:
            self.temp_token = temp_token

    @property
    def file(self):
        """Gets the file of this InlineResponse20070.  # noqa: E501


        :return: The file of this InlineResponse20070.  # noqa: E501
        :rtype: str
        """
        return self._file

    @file.setter
    def file(self, file):
        """Sets the file of this InlineResponse20070.


        :param file: The file of this InlineResponse20070.  # noqa: E501
        :type file: str
        """

        self._file = file

    @property
    def temp_token(self):
        """Gets the temp_token of this InlineResponse20070.  # noqa: E501


        :return: The temp_token of this InlineResponse20070.  # noqa: E501
        :rtype: str
        """
        return self._temp_token

    @temp_token.setter
    def temp_token(self, temp_token):
        """Sets the temp_token of this InlineResponse20070.


        :param temp_token: The temp_token of this InlineResponse20070.  # noqa: E501
        :type temp_token: str
        """

        self._temp_token = temp_token

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20070):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20070):
            return True

        return self.to_dict() != other.to_dict()
