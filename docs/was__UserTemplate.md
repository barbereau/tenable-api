# UserTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_template_id** | **str** | The UUID of the user-defined template resource. | [readonly] 
**container_id** | **str** | The UUID of your organization&#39;s Tenable.io instance. | 
**data** | [**UserTemplateResponse**](was__UserTemplateResponse.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


