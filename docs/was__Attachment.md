# Attachment

The report attachments for the vulnerability.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attachment_id** | **str** | The UUID of the attachment file. | [readonly] 
**created_at** | **datetime** | An ISO timestamp indicating the date and time when a scan generated the attachment file, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [readonly] 
**attachment_name** | **str** | The name of the attachment file. | [readonly] 
**md5** | **str** | The MD5 hash of the attachment file. | [readonly] 
**file_type** | **str** | The MIME type of the attachment file. | [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


