# InlineResponse20046

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owner** | **int** | The unique ID of the owner of the object. | [optional] 
**type** | **str** | The type of permission (default, user, group). | [optional] 
**permissions** | **int** | The permission value to grant access as described in [Permissions](doc:permissions). | [optional] 
**id** | **int** | The unique ID of the user or group. | [optional] 
**name** | **str** | The name of the user or group. | [optional] 
**display_name** | **str** | The display-friendly name of the user or group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


