# InlineResponse20024

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileuploaded** | **str** | The name of the uploaded file. If the file with the same name already exists, Tenable.io appends an underscore with a number, for example ssh_private_key_1.txt. Use this attribute value when referencing the file for subsequent requests. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


