# ErrorResponse500

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_code** | **int** | The HTTP status code of the error. | [optional] 
**error** | **str** | The standard HTTP error name. | [optional] 
**message** | **str** | A brief message about the cause of the error. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


