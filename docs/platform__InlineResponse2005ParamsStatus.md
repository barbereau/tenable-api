# InlineResponse2005ParamsStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_event_seen** | **str** | An ISO timestamp indicating the last time the connector found new or changed records and successfully imported them. | [optional] 
**release_timestamp** | **str** | An ISO timestamp indicating the last time the connector successfully completed an import (regardless of whether it found any changes). | [optional] 
**message** | **str** | The extended import status message. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


