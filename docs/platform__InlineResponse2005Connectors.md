# InlineResponse2005Connectors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The type of the connector. Types include:  - aws  - aws_keyless  - azure  - gcp | [optional] 
**human_type** | **str** | The human-readable connector type. | [optional] 
**data_type** | **str** | The data type imported by the connector. For Azure and AWS connectors, the value is always &#x60;assets&#x60;. | [optional] 
**name** | **str** | The name of the connector. The name must be unique within a Tenable.io instance. | [optional] 
**network_uuid** | **str** | The UUID of the [network](doc:manage-networks-tio) associated with the connector. | [optional] 
**status** | **str** | The import status of the connector. Status values can include:  - Completed—Tenable.io successfully used the connector to import assets (no imports scheduled)  - Scheduled—Imports using the connector are scheduled for future dates  - Saved—Tenable.io saved the connector configuration, but did not import assets at this time (no imports scheduled)  - Error—Tenable.io failed to import assets using the connector | [optional] 
**status_message** | **str** | Extended description of the connector status. For information about connector error codes, see [Connectors](doc:io-connectors). | [optional] 
**schedule** | **object** |  | [optional] 
**date_created** | **str** | An ISO timestamp indicating the date and time on which the connector was created, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**date_modified** | **str** | An ISO timestamp indicating the date and time on which the connector was last modified or new records were imported, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**id** | **str** | The UUID of the connector. | [optional] 
**container_uuid** | **str** | The UUID of the Tenable.io instance. | [optional] 
**expired** | **bool** | Indicates whether the Vulnerability Management license for the Tenable.io instance associated with the connector is expired. | [optional] 
**incremental_mode** | **bool** | Indicates whether a connector has completed the initial full import successfully. If the value is &#x60;true&#x60;, then the connector is in incremental mode where it imports assets based on on the service provider event stream instead of enumerating all assets in the account every single time. | [optional] 
**last_sync_time** | **str** | An ISO timestamp indicating the date and time of the last successful import, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**params** | [**InlineResponse2005Params**](platform__InlineResponse2005Params.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


