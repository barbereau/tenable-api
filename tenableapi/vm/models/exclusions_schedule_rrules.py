# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class ExclusionsScheduleRrules(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'freq': 'str',
        'interval': 'int',
        'byweekday': 'str',
        'bymonthday': 'int'
    }

    attribute_map = {
        'freq': 'freq',
        'interval': 'interval',
        'byweekday': 'byweekday',
        'bymonthday': 'bymonthday'
    }

    def __init__(self, freq=None, interval=None, byweekday=None, bymonthday=None, local_vars_configuration=None):  # noqa: E501
        """ExclusionsScheduleRrules - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._freq = None
        self._interval = None
        self._byweekday = None
        self._bymonthday = None
        self.discriminator = None

        if freq is not None:
            self.freq = freq
        if interval is not None:
            self.interval = interval
        if byweekday is not None:
            self.byweekday = byweekday
        if bymonthday is not None:
            self.bymonthday = bymonthday

    @property
    def freq(self):
        """Gets the freq of this ExclusionsScheduleRrules.  # noqa: E501

        The frequency of the rule (ONETIME, DAILY, WEEKLY, MONTHLY, YEARLY).  # noqa: E501

        :return: The freq of this ExclusionsScheduleRrules.  # noqa: E501
        :rtype: str
        """
        return self._freq

    @freq.setter
    def freq(self, freq):
        """Sets the freq of this ExclusionsScheduleRrules.

        The frequency of the rule (ONETIME, DAILY, WEEKLY, MONTHLY, YEARLY).  # noqa: E501

        :param freq: The freq of this ExclusionsScheduleRrules.  # noqa: E501
        :type freq: str
        """

        self._freq = freq

    @property
    def interval(self):
        """Gets the interval of this ExclusionsScheduleRrules.  # noqa: E501

        The interval of the rule.  # noqa: E501

        :return: The interval of this ExclusionsScheduleRrules.  # noqa: E501
        :rtype: int
        """
        return self._interval

    @interval.setter
    def interval(self, interval):
        """Sets the interval of this ExclusionsScheduleRrules.

        The interval of the rule.  # noqa: E501

        :param interval: The interval of this ExclusionsScheduleRrules.  # noqa: E501
        :type interval: int
        """

        self._interval = interval

    @property
    def byweekday(self):
        """Gets the byweekday of this ExclusionsScheduleRrules.  # noqa: E501

        A comma-separated string of days to repeat a WEEKLY freq rule on (SU,MO,TU,WE,TH,FR, or SA).  # noqa: E501

        :return: The byweekday of this ExclusionsScheduleRrules.  # noqa: E501
        :rtype: str
        """
        return self._byweekday

    @byweekday.setter
    def byweekday(self, byweekday):
        """Sets the byweekday of this ExclusionsScheduleRrules.

        A comma-separated string of days to repeat a WEEKLY freq rule on (SU,MO,TU,WE,TH,FR, or SA).  # noqa: E501

        :param byweekday: The byweekday of this ExclusionsScheduleRrules.  # noqa: E501
        :type byweekday: str
        """

        self._byweekday = byweekday

    @property
    def bymonthday(self):
        """Gets the bymonthday of this ExclusionsScheduleRrules.  # noqa: E501

        The day of the month to repeat a MONTHLY freq rule on.  # noqa: E501

        :return: The bymonthday of this ExclusionsScheduleRrules.  # noqa: E501
        :rtype: int
        """
        return self._bymonthday

    @bymonthday.setter
    def bymonthday(self, bymonthday):
        """Sets the bymonthday of this ExclusionsScheduleRrules.

        The day of the month to repeat a MONTHLY freq rule on.  # noqa: E501

        :param bymonthday: The bymonthday of this ExclusionsScheduleRrules.  # noqa: E501
        :type bymonthday: int
        """

        self._bymonthday = bymonthday

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ExclusionsScheduleRrules):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, ExclusionsScheduleRrules):
            return True

        return self.to_dict() != other.to_dict()
