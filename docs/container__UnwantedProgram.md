# UnwantedProgram

The unwanted program details.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**infected_file** | **str** | The path of the program file. | [optional] 
**file_type_descriptor** | **str** | The file type of the program file, for example, &#x60;ELF32&#x60;. | [optional] 
**md5** | **str** | The MD5 signature of the program file. | [optional] 
**sha256** | **str** | The SHA256 signature of the program file. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


