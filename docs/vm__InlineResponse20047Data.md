# InlineResponse20047Data

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plugin_details** | [**list[InlineResponse20047DataPluginDetails]**](vm__InlineResponse20047DataPluginDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


