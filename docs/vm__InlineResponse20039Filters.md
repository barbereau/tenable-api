# InlineResponse20039Filters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The field name to be used in request query strings when applying the filter. | [optional] 
**readable_name** | **str** | The filter&#39;s display label. | [optional] 
**control** | [**InlineResponse20039Control**](vm__InlineResponse20039Control.md) |  | [optional] 
**operators** | **list[str]** | Strings which represent the comparison operations which can be used for the filter. | [optional] 
**group_name** | **str** | Always set to &#x60;NULL&#x60;. Legacy attribute associated with deprecated functionality. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


