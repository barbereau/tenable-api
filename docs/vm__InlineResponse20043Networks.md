# InlineResponse20043Networks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owner_uuid** | **str** | The UUID of the owner of the network object. The owner of the network object does not have any additional permissions above administrator permissions. | [optional] 
**created** | **int** | The date (in Unix milliseconds) when the network object was created. | [optional] 
**modified** | **int** | The date (in Unix milliseconds) when the network object was last updated. | [optional] 
**uuid** | **str** | The UUID of the network object. | [optional] 
**name** | **str** | The name of the network object. | [optional] 
**description** | **str** | The description of the network object. | [optional] 
**is_default** | **bool** | Indicates whether the network object is the default network object. The default network object is a system-generated object that contains any scanners and scanner groups not yet assigned to a custom network object. You cannot update or delete the default network object. | [optional] 
**created_by** | **str** | The UUID of the user who created the network object. | [optional] 
**modified_by** | **str** | The UUID of the user who last updated the network object. | [optional] 
**deleted** | **int** | The date (in Unix time) when the network object was deleted. This attribute is present for deleted network objects only. To view deleted network objects, use the &#x60;includeDeleted&#x60; parameter on the [GET /networks](ref:networks-list) endpoint. | [optional] 
**deleted_by** | **str** | The UUID of the user who deleted the network object. This attribute is present for deleted network objects only. To view deleted network objects, use the &#x60;includeDeleted&#x60; parameter on the [GET /networks](ref:networks-list) endpoint. | [optional] 
**assets_ttl_days** | **int** | The number of days before assets age out. Assets are permanently deleted if they are not seen on a scan within the specified number of days.&lt;ul&gt;&lt;li&gt;Minimum value: 90&lt;/li&gt;&lt;li&gt;Maximum value: 365&lt;/li&gt;&lt;/ul&gt; Note: If you enable this option, Tenable.io immediately deletes assets in the specified network that have not been seen for the specified number of days. All asset records and associated vulnerabilities are deleted and cannot be recovered. The deleted assets no longer count towards [your license](https://docs.tenable.com/earlyaccess/tenableio/vulnerabilitymanagement/Content/GettingStarted/Licenses.htm) | [optional] 
**created_in_seconds** | **int** | The date (in Unix seconds) when the network object was created. | [optional] 
**modified_in_seconds** | **int** | The date (in Unix seconds) when the network object was last updated. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


