# InlineResponse20016

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**folders** | [**list[InlineResponse2007]**](waslegacy__InlineResponse2007.md) |  | [optional] 
**scans** | [**list[InlineResponse20016Scans]**](waslegacy__InlineResponse20016Scans.md) |  | [optional] 
**timestamp** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


