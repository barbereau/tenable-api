# tenableapi.platform.ConnectorsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**connectors_connector_details**](platform__ConnectorsApi.md#connectors_connector_details) | **GET** /settings/connectors/{connector_id} | Get connector details
[**connectors_create_connector**](platform__ConnectorsApi.md#connectors_create_connector) | **POST** /settings/connectors | Create connector
[**connectors_delete_connector**](platform__ConnectorsApi.md#connectors_delete_connector) | **DELETE** /settings/connectors/{connector_id} | Delete connector
[**connectors_get_aws_cloudtrails**](platform__ConnectorsApi.md#connectors_get_aws_cloudtrails) | **POST** /settings/connectors/aws/cloudtrails | List AWS cloudtrails
[**connectors_import_assets_connector**](platform__ConnectorsApi.md#connectors_import_assets_connector) | **POST** /settings/connectors/{connector_id}/import | Import data
[**connectors_list_connectors**](platform__ConnectorsApi.md#connectors_list_connectors) | **GET** /settings/connectors | List connectors
[**connectors_update_connector**](platform__ConnectorsApi.md#connectors_update_connector) | **PUT** /settings/connectors/{connector_id} | Update connector


# **connectors_connector_details**
> InlineResponse2005Connectors connectors_connector_details(connector_id)

Get connector details

Returns the details for the specified connector.<p>For information about connector error codes, see [Connectors](doc:io-connectors).</p><p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.ConnectorsApi(api_client)
    connector_id = 'connector_id_example' # str | The UUID of the connector to return details for.

    try:
        # Get connector details
        api_response = api_instance.connectors_connector_details(connector_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConnectorsApi->connectors_connector_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connector_id** | **str**| The UUID of the connector to return details for. | 

### Return type

[**InlineResponse2005Connectors**](platform__InlineResponse2005Connectors.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the connector details. |  -  |
**404** | Returned if Tenable.io cannot not find the specified connector. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connectors_create_connector**
> InlineResponse2005Connectors connectors_create_connector(inline_object10)

Create connector

Creates a connector.<ul>**Note:** The workflow for creating AWS connectors is as follows:<li>First, [get the available AWS cloudtrails for the account](#connectors-get-aws-cloudtrails)</li><li>Then use the cloudtrail(s) as a required input parameter to create the AWS connector.</li></ul></br><p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.ConnectorsApi(api_client)
    inline_object10 = tenableapi.platform.InlineObject10() # InlineObject10 | 

    try:
        # Create connector
        api_response = api_instance.connectors_create_connector(inline_object10)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConnectorsApi->connectors_create_connector: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object10** | [**InlineObject10**](platform__InlineObject10.md)|  | 

### Return type

[**InlineResponse2005Connectors**](platform__InlineResponse2005Connectors.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates a connector. |  -  |
**400** | Returned if you specify invalid input parameters. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connectors_delete_connector**
> object connectors_delete_connector(connector_id)

Delete connector

Deletes the specified connector.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.ConnectorsApi(api_client)
    connector_id = 'connector_id_example' # str | The UUID of the connector to delete.

    try:
        # Delete connector
        api_response = api_instance.connectors_delete_connector(connector_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConnectorsApi->connectors_delete_connector: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connector_id** | **str**| The UUID of the connector to delete. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Returned if Tenable.io successfully deleted the specified connector. |  -  |
**404** | Returned if Tenable.io cannot find the specified connector. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connectors_get_aws_cloudtrails**
> InlineResponse2006 connectors_get_aws_cloudtrails(inline_object12)

List AWS cloudtrails

Returns a list of available AWS cloudtrails. You can then use the cloudtrails to [create an AWS connector](ref:connectors-create-connector).<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.ConnectorsApi(api_client)
    inline_object12 = tenableapi.platform.InlineObject12() # InlineObject12 | 

    try:
        # List AWS cloudtrails
        api_response = api_instance.connectors_get_aws_cloudtrails(inline_object12)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConnectorsApi->connectors_get_aws_cloudtrails: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object12** | [**InlineObject12**](platform__InlineObject12.md)|  | 

### Return type

[**InlineResponse2006**](platform__InlineResponse2006.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully retrieves the list of cloudtrails. |  -  |
**400** | Returned if you specify invalid input parameters. |  -  |
**403** | Returned if you specify invalid AWS credentials or account ID. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connectors_import_assets_connector**
> InlineResponse2007 connectors_import_assets_connector(connector_id)

Import data

Imports data using a connector. This creates an asynchronous import job in Tenable.io. You can check the import status by examining the `status_message` property in [connector details](ref:connectors-connector-details).<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.ConnectorsApi(api_client)
    connector_id = 'connector_id_example' # str | The UUID of the connector for which to import the data.

    try:
        # Import data
        api_response = api_instance.connectors_import_assets_connector(connector_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConnectorsApi->connectors_import_assets_connector: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connector_id** | **str**| The UUID of the connector for which to import the data. | 

### Return type

[**InlineResponse2007**](platform__InlineResponse2007.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully schedules the connector for import. |  -  |
**404** | Returned if Tenable.io cannot find the connector you specified. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connectors_list_connectors**
> InlineResponse2005 connectors_list_connectors(limit=limit, offset=offset, sort=sort)

List connectors

Returns a list of connectors.<p>For information about connector error codes, see [Connectors](doc:io-connectors).</p><p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.ConnectorsApi(api_client)
    limit = 56 # int | Maximum number of records requested (or service imposed limit if not in request). Must be in the int32 format. Default is 1000. (optional)
offset = 56 # int | The number of records to skip in the returned result set. Must be in the int32 format. Default is 0. (optional)
sort = 'sort_example' # str | The fields to sort on, for example, `sort=date_created:desc`. If you specify multiple fields, fields must be separated by commas. Sortable fields include:   * date_created   * name (optional)

    try:
        # List connectors
        api_response = api_instance.connectors_list_connectors(limit=limit, offset=offset, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConnectorsApi->connectors_list_connectors: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Maximum number of records requested (or service imposed limit if not in request). Must be in the int32 format. Default is 1000. | [optional] 
 **offset** | **int**| The number of records to skip in the returned result set. Must be in the int32 format. Default is 0. | [optional] 
 **sort** | **str**| The fields to sort on, for example, &#x60;sort&#x3D;date_created:desc&#x60;. If you specify multiple fields, fields must be separated by commas. Sortable fields include:   * date_created   * name | [optional] 

### Return type

[**InlineResponse2005**](platform__InlineResponse2005.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of connectors with pagination information. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connectors_update_connector**
> InlineResponse2005Connectors connectors_update_connector(connector_id, inline_object11)

Update connector

Updates the specified connector. You can change the connector name, associated service accounts, and schedule. You cannot change the connector type for an existing connector.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.ConnectorsApi(api_client)
    connector_id = 'connector_id_example' # str | The UUID of the connector to update.
inline_object11 = tenableapi.platform.InlineObject11() # InlineObject11 | 

    try:
        # Update connector
        api_response = api_instance.connectors_update_connector(connector_id, inline_object11)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConnectorsApi->connectors_update_connector: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connector_id** | **str**| The UUID of the connector to update. | 
 **inline_object11** | [**InlineObject11**](platform__InlineObject11.md)|  | 

### Return type

[**InlineResponse2005Connectors**](platform__InlineResponse2005Connectors.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully updates a connector. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

