# InlineResponse20064Comphosts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalchecksconsidered** | **int** | The total number of checks considered on the host. | [optional] 
**numchecksconsidered** | **int** | The number of checks considered on the host. | [optional] 
**scanprogresstotal** | **int** | The total scan progress for the host. | [optional] 
**scanprogresscurrent** | **int** | The current scan progress for the host. | [optional] 
**host_index** | **str** | The index for the host. | [optional] 
**score** | **int** | The overall score for the host. | [optional] 
**severitycount** | **object** |  | [optional] 
**progress** | **str** | The scan progress of the host. | [optional] 
**critical** | **int** | The percentage of critical findings on the host. | [optional] 
**high** | **int** | The percentage of high findings on the host. | [optional] 
**medium** | **int** | The percentage of medium findings on the host. | [optional] 
**low** | **int** | The percentage of low findings on the host. | [optional] 
**info** | **int** | The percentage of info findings on the host. | [optional] 
**host_id** | **int** | The unique ID of the host. | [optional] 
**hostname** | **str** | The name of the host. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


