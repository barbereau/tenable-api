# InlineObject17

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filedata** | **file** | The file to upload. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


