# InlineResponse20028

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plugindescription** | [**InlineResponse20028Plugindescription**](vm__InlineResponse20028Plugindescription.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


