# InlineObject46

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | **str** | The file format to use (&#x60;Nessus&#x60;, &#x60;HTML&#x60;, &#x60;PDF&#x60;, &#x60;CSV&#x60;, or &#x60;DB&#x60;). For scans that are older than 60 days, only &#x60;Nessus&#x60; and &#x60;CSV&#x60; are supported. | 
**password** | **str** | The password used to encrypt database exports. This parameter is required when exporting as &#x60;DB&#x60;. | [optional] 
**chapters** | **str** | The chapters to include in the export. This parameter accepts a semi-colon delimited string comprised of some combination of the following options: vuln_hosts_summary, vuln_by_host, compliance_exec, remediations, vuln_by_plugin, compliance).  **Note:** This parameter is required if the file format is &#x60;PDF&#x60; or &#x60;HTML&#x60;. | [optional] 
**filter_0_filter** | **str** | The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. If you specify the name of the filter, you must specify the operator as the &#x60;filter.0.quality&#x60; parameter and the value as the &#x60;filter.0.value&#x60; parameter. To use multiple filters, increment the &#x60;&lt;INDEX&gt;&#x60; portion of &#x60;filter.&lt;INDEX&gt;.filter&#x60;, for example, &#x60;filter.1.filter&#x60;. For more information about using this parameter, see [Scan Export Filters](doc:scan-export-filters-tio).  **Note:** Filters are not supported when exporting scan results that are older than 60 days. | [optional] 
**filter_0_quality** | **str** | The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. To use multiple filters, increment the &#x60;&lt;INDEX&gt;&#x60; portion of &#x60;filter.&lt;INDEX&gt;.quality&#x60;, for example, &#x60;filter.1.quality&#x60;. For more information about using this parameter, see [Scan Export Filters](doc:scan-export-filters-tio). | [optional] 
**filter_0_value** | **str** | The value of the filter to apply to the exported scan report. You can find valid values for the filter in the &#x60;control&#x60; attribute of the objects returned by the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. To use multiple filters, increment the &#x60;&lt;INDEX&gt;&#x60; portion of &#x60;filter.&lt;INDEX&gt;.value&#x60;, for example, &#x60;filter.1.value&#x60;. For more information about using this parameter, see [Scan Export Filters](doc:scan-export-filters-tio). | [optional] 
**filter_search_type** | **str** | For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about using this parameter, see [Scan Export Filters](doc:scan-export-filters-tio). | [optional] 
**asset_id** | **str** | The ID of the asset scanned. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


