# InlineResponse20080Pagination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | The total number of records matching your search criteria. | [optional] 
**limit** | **int** | Maximum number of records requested (or service-imposed limit if not in request). | 
**offset** | **int** | The starting record to retrieve. If this parameter is not supplied, the default value is &#x60;0&#x60;. | 
**sort** | [**list[InlineResponse200PaginationSort]**](vm__InlineResponse200Paginationvm__Sort.md) | The fields you specified as sort fields in the request, which Tenable.io uses to sort the returned data. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


