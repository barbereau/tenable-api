# InlineResponse20023Options

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The display name of the option in the user interface. | [optional] 
**id** | **str** | The system name for the option. | [optional] 
**inputs** | [**list[InlineResponse20023Inputs]**](vm__InlineResponse20023Inputs.md) | The additional inputs that are required if the user selects this option in the user interface. If the inputs parameter is empty (&#x60;\\[\\]&#x60;), selecting the option does not require additional user input. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


