# tenable-api

**Note:** Tenable will provide advance notice and migration guidance prior to deprecating Web Application Scanning API v1. For most up-to-date announcements, subscribe to Tenables [RSS feed](https://feeds.feedburner.com/TenableDeveloperHub) or check the [API Changelog](https://developer.tenable.com/changelog).

This Python package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.0.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.PythonLegacyClientCodegen
- OpenAPI resources are available at: https://developer.tenable.com/openapi/


## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

If the python package is hosted on a repository, you can install directly using:

```sh
pip install git+https://github.com/barbich/python-tenable.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/barbich/python-tenable.git`)

Then import the package:
```python
import tenableapi.was
```

## Documentation for API Endpoints

All URIs are relative to *https://cloud.tenable.com* except for the Downloads API

- [Vulnerability Management API](docs/vm_README.md)
- [Container Security API - legacy/v1](docs/containerlegacy_README.md)
- [Container Security API - v2](docs/container_README.md)
- [Tenable Platforn API](docs/platform_README.md)
- [MSSP API](docs/mssp_README.md)
- [Downloads API](docs/downloads_README.md)
- [Web Application Security API - legacy/v1](docs/waslegacy_README.md)
- [Web Application Security API - v2](docs/was_README.md)


## Documentation For Authorization

You will need a working Tenable API key for using the above calls. The form of the key is "accessKey=ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890;secretKey=ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"


## cloud

- **Type**: API key
- **API key parameter name**: X-ApiKeys
- **Location**: HTTP header


## Author
