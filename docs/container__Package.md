# Package

A software packages affected by the vulnerability.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the package. | [optional] 
**version** | **str** | The version of the package. | [optional] 
**type** | **str** | The operating system or distribution associated with the package, for example, &#x60;linux&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


