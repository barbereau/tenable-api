# LogoObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** | The UUID of the logo. | [optional] 
**container_uuid** | **str** | The UUID of the container that contains the logo. | [optional] 
**name** | **str** | The user-defined name of the logo. | [optional] 
**filename** | **str** | The filename of the logo. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


