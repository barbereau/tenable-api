# InlineResponse20064Info

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owner** | **str** | The owner of the scan. | [optional] 
**name** | **str** | The name of the scan. | [optional] 
**no_target** | **bool** | Indicates whether the scan based on this policy can specify targets. | [optional] 
**folder_id** | **int** | The unique ID of the destination folder for the scan. | [optional] 
**control** | **bool** | If &#x60;true&#x60;, the scan has a schedule and can be launched. | [optional] 
**user_permissions** | **int** | The sharing permissions for the scan. | [optional] 
**schedule_uuid** | **str** | The UUID for a specific instance in the scan schedule. | [optional] 
**edit_allowed** | **bool** | If &#x60;true&#x60;, the requesting user can edit this scan configuration. | [optional] 
**scanner_name** | **str** | The name of the scanner configured to run the scan. | [optional] 
**policy** | **str** | The name of the scan template associated with the scan. | [optional] 
**shared** | **bool** | If &#x60;true&#x60;, the scan is shared with users other than the owner. The level of sharing is specified in the &#x60;acls&#x60; attribute of the scan details. | [optional] 
**object_id** | **int** |  | [optional] 
**tag_targets** | **list[str]** | The list of asset tag identifiers the scan uses to determine which assets it evaluates. For more information about tag-based scans, see [Manage Tag-Based Scans](doc:manage-tag-based-scans-tio). | [optional] 
**acls** | [**list[ScansSettingsAcls]**](vm__ScansSettingsAcls.md) | An array of objects that control sharing permissions for the scan. | [optional] 
**hostcount** | **int** | The total number of assets scanned for vulnerabilities. | [optional] 
**uuid** | **str** | The UUID of the scan. | [optional] 
**status** | **str** | The status of the scan. For a list of possible values, see [Scan Status](doc:scan-status-tio). | [optional] 
**scan_type** | **str** | The type of scan: &#x60;ps&#x60; (a scan performed over the network by a cloud scanner), &#x60;remote&#x60; (a  scan performed over the network by a local scanner), &#x60;agent&#x60; (a scan on a local host that a Nessus agent performs directly), or &#x60;null&#x60; (the scan has never been launched, or the scan is imported). | [optional] 
**targets** | **str** | A comma-delimited list of IPv4 addresses that are configured as targets for the scan. | [optional] 
**alt_targets_used** | **bool** | If &#x60;true&#x60;, Tenable.io did not not launched with a target list. This parameter is &#x60;true&#x60; for agent scans. | [optional] 
**pci_can_upload** | **bool** | If &#x60;true&#x60;, you can submit the results of the scan for PCI ASV review. For more information, see [PCI ASV](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/PCI_ASV/Welcome.htm) in the Tenable.io Vulnerability Management User Guide. | [optional] 
**scan_start** | **int** | The Unix timestamp when the scan run started. | [optional] 
**timestamp** | **int** | The Unix timestamp when the scan run finished. | [optional] 
**is_archived** | **bool** | Indicates whether the scan results are older than 60 days (&#x60;true&#x60;). If this attribute is &#x60;true&#x60;, the response message for this endpoint excludes the &#x60;hosts&#x60;, &#x60;vulnerabilities&#x60;, &#x60;comphosts&#x60;, &#x60;compliance&#x60;, and &#x60;filters&#x60; objects. For complete scan results older than 60 days, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead. | [optional] 
**scan_end** | **int** | The Unix timestamp when the scan run finished. | [optional] 
**haskb** | **bool** | Indicates whether a scan has a Knowledge Base (KB) associated with it. A KB is an ASCII text file containing a log of information relevant to the scan performed and results found. | [optional] 
**hasaudittrail** | **bool** | Indicates whether the scan is configured to create an audit trail. | [optional] 
**scanner_start** | **str** | The scan&#39;s start time, if the scan is imported. | [optional] 
**scanner_end** | **str** | The scan&#39;s end time, if the scan is imported. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


