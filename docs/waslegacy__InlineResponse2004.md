# InlineResponse2004

Templates are used to create scans or policies with predefined parameters.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unsupported** | **bool** | If &#x60;true&#x60;, the template is not supported. | [optional] 
**cloud_only** | **bool** | If &#x60;true&#x60;, the template is only available on the cloud. | [optional] 
**desc** | **str** | The description of the template. | [optional] 
**subscription_only** | **bool** | If &#x60;true&#x60;, the template is only available for subscribers. | [optional] 
**is_was** | **bool** | If &#x60;true&#x60;, the template can be used for Web Application Scanning only. | [optional] 
**title** | **str** | The long name of the template. | [optional] 
**is_agent** | **bool** | If &#x60;true&#x60;, the template can only be used for agent scans. | [optional] 
**uuid** | **str** | The UUID for the template. Use this value to specify the template when creating scans and policies. | [optional] 
**manager_only** | **bool** | If &#x60;true&#x60;, can only be used by manager. | [optional] 
**name** | **str** | The short name of the template. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


