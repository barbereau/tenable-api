# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from tenableapi.vm.api_client import ApiClient
from tenableapi.vm.exceptions import (  # noqa: F401
    ApiTypeError,
    ApiValueError
)


class FoldersApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def folders_create(self, inline_object24, **kwargs):  # noqa: E501
        """Create folder  # noqa: E501

        Creates a new custom folder for the current user.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.folders_create(inline_object24, async_req=True)
        >>> result = thread.get()

        :param inline_object24: (required)
        :type inline_object24: InlineObject24
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: InlineResponse20042
        """
        kwargs['_return_http_data_only'] = True
        return self.folders_create_with_http_info(inline_object24, **kwargs)  # noqa: E501

    def folders_create_with_http_info(self, inline_object24, **kwargs):  # noqa: E501
        """Create folder  # noqa: E501

        Creates a new custom folder for the current user.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.folders_create_with_http_info(inline_object24, async_req=True)
        >>> result = thread.get()

        :param inline_object24: (required)
        :type inline_object24: InlineObject24
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(InlineResponse20042, status_code(int), headers(HTTPHeaderDict))
        """

        local_var_params = locals()

        all_params = [
            'inline_object24'
        ]
        all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth'
            ]
        )

        for key, val in six.iteritems(local_var_params['kwargs']):
            if key not in all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method folders_create" % key
                )
            local_var_params[key] = val
        del local_var_params['kwargs']
        # verify the required parameter 'inline_object24' is set
        if self.api_client.client_side_validation and ('inline_object24' not in local_var_params or  # noqa: E501
                                                        local_var_params['inline_object24'] is None):  # noqa: E501
            raise ApiValueError("Missing the required parameter `inline_object24` when calling `folders_create`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'inline_object24' in local_var_params:
            body_params = local_var_params['inline_object24']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json', 'text/html'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['cloud']  # noqa: E501
        
        response_types_map = {
            200: "InlineResponse20042",
            400: None,
            403: None,
            429: None,
            500: None,
        }

        return self.api_client.call_api(
            '/folders', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_types_map=response_types_map,
            auth_settings=auth_settings,
            async_req=local_var_params.get('async_req'),
            _return_http_data_only=local_var_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=local_var_params.get('_preload_content', True),
            _request_timeout=local_var_params.get('_request_timeout'),
            collection_formats=collection_formats,
            _request_auth=local_var_params.get('_request_auth'))

    def folders_delete(self, folder_id, **kwargs):  # noqa: E501
        """Delete folder  # noqa: E501

        Deletes a folder. You cannot delete:  - Tenable-provided folders (`main` or `trash`)  - a custom folder that belongs to another user (even if you use an administrator account)  If you delete a folder that contains scans, Tenable.io automatically moves those scans to the trash folder. <p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.folders_delete(folder_id, async_req=True)
        >>> result = thread.get()

        :param folder_id: The ID of the custom folder to delete. Use the [GET /folders](/reference#folders-list) endpoint to determine the ID of the custom folder you want to delete. (required)
        :type folder_id: int
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: object
        """
        kwargs['_return_http_data_only'] = True
        return self.folders_delete_with_http_info(folder_id, **kwargs)  # noqa: E501

    def folders_delete_with_http_info(self, folder_id, **kwargs):  # noqa: E501
        """Delete folder  # noqa: E501

        Deletes a folder. You cannot delete:  - Tenable-provided folders (`main` or `trash`)  - a custom folder that belongs to another user (even if you use an administrator account)  If you delete a folder that contains scans, Tenable.io automatically moves those scans to the trash folder. <p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.folders_delete_with_http_info(folder_id, async_req=True)
        >>> result = thread.get()

        :param folder_id: The ID of the custom folder to delete. Use the [GET /folders](/reference#folders-list) endpoint to determine the ID of the custom folder you want to delete. (required)
        :type folder_id: int
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(object, status_code(int), headers(HTTPHeaderDict))
        """

        local_var_params = locals()

        all_params = [
            'folder_id'
        ]
        all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth'
            ]
        )

        for key, val in six.iteritems(local_var_params['kwargs']):
            if key not in all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method folders_delete" % key
                )
            local_var_params[key] = val
        del local_var_params['kwargs']
        # verify the required parameter 'folder_id' is set
        if self.api_client.client_side_validation and ('folder_id' not in local_var_params or  # noqa: E501
                                                        local_var_params['folder_id'] is None):  # noqa: E501
            raise ApiValueError("Missing the required parameter `folder_id` when calling `folders_delete`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'folder_id' in local_var_params:
            path_params['folder_id'] = local_var_params['folder_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json', 'text/html'])  # noqa: E501

        # Authentication setting
        auth_settings = ['cloud']  # noqa: E501
        
        response_types_map = {
            200: "object",
            403: None,
            404: None,
            429: None,
            500: None,
        }

        return self.api_client.call_api(
            '/folders/{folder_id}', 'DELETE',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_types_map=response_types_map,
            auth_settings=auth_settings,
            async_req=local_var_params.get('async_req'),
            _return_http_data_only=local_var_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=local_var_params.get('_preload_content', True),
            _request_timeout=local_var_params.get('_request_timeout'),
            collection_formats=collection_formats,
            _request_auth=local_var_params.get('_request_auth'))

    def folders_edit(self, folder_id, inline_object25, **kwargs):  # noqa: E501
        """Rename folder  # noqa: E501

        Renames a folder for the current user. You cannot rename:  - system-provided scan folders  - a custom folder that belongs to another user (even if your account has administrator privileges)<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.folders_edit(folder_id, inline_object25, async_req=True)
        >>> result = thread.get()

        :param folder_id: The ID of the folder to edit. (required)
        :type folder_id: int
        :param inline_object25: (required)
        :type inline_object25: InlineObject25
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: object
        """
        kwargs['_return_http_data_only'] = True
        return self.folders_edit_with_http_info(folder_id, inline_object25, **kwargs)  # noqa: E501

    def folders_edit_with_http_info(self, folder_id, inline_object25, **kwargs):  # noqa: E501
        """Rename folder  # noqa: E501

        Renames a folder for the current user. You cannot rename:  - system-provided scan folders  - a custom folder that belongs to another user (even if your account has administrator privileges)<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.folders_edit_with_http_info(folder_id, inline_object25, async_req=True)
        >>> result = thread.get()

        :param folder_id: The ID of the folder to edit. (required)
        :type folder_id: int
        :param inline_object25: (required)
        :type inline_object25: InlineObject25
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(object, status_code(int), headers(HTTPHeaderDict))
        """

        local_var_params = locals()

        all_params = [
            'folder_id',
            'inline_object25'
        ]
        all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth'
            ]
        )

        for key, val in six.iteritems(local_var_params['kwargs']):
            if key not in all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method folders_edit" % key
                )
            local_var_params[key] = val
        del local_var_params['kwargs']
        # verify the required parameter 'folder_id' is set
        if self.api_client.client_side_validation and ('folder_id' not in local_var_params or  # noqa: E501
                                                        local_var_params['folder_id'] is None):  # noqa: E501
            raise ApiValueError("Missing the required parameter `folder_id` when calling `folders_edit`")  # noqa: E501
        # verify the required parameter 'inline_object25' is set
        if self.api_client.client_side_validation and ('inline_object25' not in local_var_params or  # noqa: E501
                                                        local_var_params['inline_object25'] is None):  # noqa: E501
            raise ApiValueError("Missing the required parameter `inline_object25` when calling `folders_edit`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'folder_id' in local_var_params:
            path_params['folder_id'] = local_var_params['folder_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'inline_object25' in local_var_params:
            body_params = local_var_params['inline_object25']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json', 'text/html'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['cloud']  # noqa: E501
        
        response_types_map = {
            200: "object",
            403: None,
            404: None,
            429: None,
            500: None,
        }

        return self.api_client.call_api(
            '/folders/{folder_id}', 'PUT',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_types_map=response_types_map,
            auth_settings=auth_settings,
            async_req=local_var_params.get('async_req'),
            _return_http_data_only=local_var_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=local_var_params.get('_preload_content', True),
            _request_timeout=local_var_params.get('_request_timeout'),
            collection_formats=collection_formats,
            _request_auth=local_var_params.get('_request_auth'))

    def folders_list(self, **kwargs):  # noqa: E501
        """List folders  # noqa: E501

        Lists the Tenable-provided folders, as well as the current user's custom folders.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.folders_list(async_req=True)
        >>> result = thread.get()

        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: list[InlineResponse20041]
        """
        kwargs['_return_http_data_only'] = True
        return self.folders_list_with_http_info(**kwargs)  # noqa: E501

    def folders_list_with_http_info(self, **kwargs):  # noqa: E501
        """List folders  # noqa: E501

        Lists the Tenable-provided folders, as well as the current user's custom folders.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.folders_list_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(list[InlineResponse20041], status_code(int), headers(HTTPHeaderDict))
        """

        local_var_params = locals()

        all_params = [
        ]
        all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth'
            ]
        )

        for key, val in six.iteritems(local_var_params['kwargs']):
            if key not in all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method folders_list" % key
                )
            local_var_params[key] = val
        del local_var_params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json', 'text/html'])  # noqa: E501

        # Authentication setting
        auth_settings = ['cloud']  # noqa: E501
        
        response_types_map = {
            200: "list[InlineResponse20041]",
            403: None,
            429: None,
        }

        return self.api_client.call_api(
            '/folders', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_types_map=response_types_map,
            auth_settings=auth_settings,
            async_req=local_var_params.get('async_req'),
            _return_http_data_only=local_var_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=local_var_params.get('_preload_content', True),
            _request_timeout=local_var_params.get('_request_timeout'),
            collection_formats=collection_formats,
            _request_auth=local_var_params.get('_request_auth'))
