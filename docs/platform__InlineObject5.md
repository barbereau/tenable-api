# InlineObject5

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sms_phone** | **str** | The phone number where Tenable.io sends the one-time verification code. Must begin with the &#x60;+&#x60; character and the country code. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


