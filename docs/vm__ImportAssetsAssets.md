# ImportAssetsAssets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mac_address** | **list[str]** | A list of MAC addresses for the asset. | [optional] 
**netbios_name** | **str** | The NetBIOS name for the asset. | [optional] 
**fqdn** | **list[str]** | A list of FQDNs for the asset. | [optional] 
**ip_address** | **list[str]** | A list of IPv4 addresses for the asset. Tenable.io supports this legacy field for backwards compatibility, but for new requests, this field should be replaced by the ipv4 field. | [optional] 
**ipv4** | **list[str]** | A list of IPv4 addresses for the asset. | [optional] 
**ipv6** | **list[str]** | A list of IPv6 addresses for the asset. | [optional] 
**hostname** | **list[str]** | A list of hostnames for the asset. | [optional] 
**operating_system** | **list[str]** | The operating systems that scans have associated with the asset record. | [optional] 
**ssh_fingerprint** | **str** | The SSH key fingerprints that scans have associated with the asset record. | [optional] 
**bios_uuid** | **str** | The BIOS UUID of the asset. | [optional] 
**manufacturer_tpm_id** | **str** | The manufacturer&#39;s unique identifier of the Trusted Platform Module (TPM) associated with the asset. | [optional] 
**mcafee_epo_guid** | **str** | The unique identifier of the asset in McAfee ePolicy Orchestrator (ePO). For more information, see the McAfee documentation. | [optional] 
**mcafee_epo_agent_guid** | **str** | The unique identifier of the McAfee ePO agent that identified the asset. For more information, see the McAfee documentation. | [optional] 
**symantec_ep_hardware_key** | **str** | The hardware key for the asset in Symantec Endpoint Protection. | [optional] 
**qualys_asset_id** | **str** | The Asset ID of the asset in Qualys. For more information, see the Qualys documentation. | [optional] 
**qualys_host_id** | **str** | The Host ID of the asset in Qualys. For more information, see the Qualys documentation. | [optional] 
**servicenow_sys_id** | **str** | The unique record identifier of the asset in ServiceNow. For more information, see the ServiceNow documentation. | [optional] 
**gcp_project_id** | **str** | The customized name of the project to which the virtual machine instance belongs in Google Cloud Platform (GCP). For more information see \&quot;Creating and Managing Projects\&quot; in the GCP documentation. | [optional] 
**gcp_zone** | **str** | The zone where the virtual machine instance runs in GCP. For more information, see \&quot;Regions and Zones\&quot; in the GCP documentation. | [optional] 
**gcp_instance_id** | **str** | The unique identifier of the virtual machine instance in GCP. | [optional] 
**azure_vm_id** | **str** | The unique identifier of the Microsoft Azure virtual machine instance. For more information, see \&quot;Accessing and Using Azure VM Unique ID\&quot; in the Microsoft Azure documentation. | [optional] 
**azure_resource_id** | **str** | The unique identifier of the resource in the Azure Resource Manager. For more information, see the Azure Resource Manager Documentation. | [optional] 
**aws_availability_zone** | **str** | The availability zone where Amazon Web Services hosts the virtual machine instance, for example, &#x60;us-east-1a&#x60;. Availability zones are subdivisions of AWS regions. For more information, see \&quot;Regions and Availability Zones\&quot; in the AWS documentation. | [optional] 
**aws_ec2_instance_id** | **str** | The unique identifier of the Linux instance in Amazon EC2. For more information, see the Amazon Elastic Compute Cloud Documentation. | [optional] 
**aws_ec2_instance_ami_id** | **str** | The unique identifier of the Linux AMI image in Amazon Elastic Compute Cloud (Amazon EC2). For more information, see the Amazon Elastic Compute Cloud Documentation. | [optional] 
**aws_ec2_instance_group_name** | **str** | The virtual machine instance&#39;s group in AWS. | [optional] 
**aws_ec2_instance_state_name** | **str** | The state of the virtual machine instance in AWS at the time of the scan. For more information on instance states, see the AWS documentation. | [optional] 
**aws_ec2_instance_type** | **str** | The type of instance in AWS EC2. | [optional] 
**aws_ec2_name** | **str** | The name of the virtual machine instance in AWS EC2. | [optional] 
**aws_ec2_product_code** | **str** | The product code associated with the AMI used to launch the virtual machine instance in AWS EC2. | [optional] 
**aws_owner_id** | **str** | The canonical user identifier for the AWS account associated with the asset. For more information, see \&quot;AWS Account Identifiers\&quot; in the AWS documentation. | [optional] 
**aws_region** | **str** | The region where AWS hosts the virtual machine instance, for example, &#x60;us-east-1&#x60;. For more information, see \&quot;Regions and Availability Zones\&quot; in the AWS documentation. | [optional] 
**aws_subnet_id** | **str** | The unique identifier of the AWS subnet where the virtual machine instance was running at the time of the scan. | [optional] 
**aws_vpc_id** | **str** | The unique identifier of the public cloud that hosts the AWS virtual machine instance. For more information, see the Amazon Virtual Private Cloud User Guide. | [optional] 
**installed_software** | **list[str]** | A list of Common Platform Enumeration (CPE) values that represent software applications a scan identified as present on an asset. The strings in this array must be valid CPE 2.2 values. For more information, see the \&quot;Component Syntax\&quot; section of the [CPE Specification, Version 2.2](https://cpe.mitre.org/files/cpe-specification_2.2.pdf).  **Note:** If no scan detects an application within 30 days of the scan that originally detected the application, Tenable.io considers the detection of that application expired. As a result, the next time a scan evaluates the asset, Tenable.io removes the expired application from the installed_software attribute. This activity is logged as a &#x60;remove&#x60; type of &#x60;attribute_change&#x60; update in the asset activity log. | [optional] 
**bigfix_asset_id** | **list[str]** | The unique identifiers of the asset in IBM BigFix. For more information, see the IBM BigFix documentation. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


