# InlineResponse20010

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The unique ID of the policy. | [optional] 
**template_uuid** | **str** | The UUID for the template the policy uses. | [optional] 
**name** | **str** | The name of the policy. | [optional] 
**description** | **str** | The description of the policy. | [optional] 
**owner_id** | **str** | The unique ID of the owner of the policy. | [optional] 
**owner** | **str** | The username for the owner of the policy. | [optional] 
**shared** | **int** | The shared status of the policy. | [optional] 
**user_permissions** | **int** | The sharing permissions for the policy. | [optional] 
**creation_date** | **int** | The creation date of the policy in Unix time. | [optional] 
**last_modification_date** | **int** | The last modification date for the policy in Unix time. | [optional] 
**visibility** | **int** | The visibility of the target (private or shared). | [optional] 
**no_target** | **bool** | If &#x60;true&#x60;, the policy does not use targets. | [optional] 
**timeout_abort_threshold** | **int** | The number of consecutive timeouts before the scan aborts (minimum 100). | [optional] 
**was_browser_cluster_ignore_images** | **str** | Specifies whether images on web pages should be crawled or ignored by the virtual browser instance embedded into the scanner. Possible values are &#x60;yes&#x60; or &#x60;no&#x60;. | [optional] 
**was_browser_cluster_job_timeout** | **int** | The time that the scanner waits for a response from a browser, unless otherwise specified within a plugin. If you are scanning over a slow connection, you may wish to set this to a higher number of seconds. | [optional] 
**was_browser_cluster_screen_height** | **int** | The screen height, in pixels, of the virtual browser instance embedded into the scanner. | [optional] 
**was_browser_cluster_screen_width** | **int** | The screen width, in pixels, of the virtual browser instance embedded into the scanner. | [optional] 
**was_chrome_script_command_wait** | **int** | When running Selenium scripts for authentication and crawling, the number of milliseconds to wait after processing a command before passing to the next one. | [optional] 
**was_chrome_script_finish_wait** | **int** | When running Selenium scripts for authentication and crawling, the number of milliseconds to wait once all commands are processed for rendering new contents. | [optional] 
**was_chrome_script_page_load_wait** | **int** | When running Selenium scripts for authentication and crawling, the number of milliseconds to let the browser render the page. | [optional] 
**was_http_request_concurrency** | **int** | The maximum number of established HTTP sessions for a single host. | [optional] 
**was_http_request_headers** | **str** | A list of custom headers injected into each HTTP request. | [optional] 
**was_http_request_redirect_limit** | **int** | The number of redirects the scan follows before it stops trying to crawl the page. | [optional] 
**was_http_request_timeout** | **int** | The time that the scanner waits for a response from a host, unless otherwise specified within a plugin. If you are scanning over a slow connection, you may wish to set this to a higher number of seconds. | [optional] 
**was_http_response_max_size** | **int** | The maximum load size of a page in order to be audited. If the scanner crawls a URL and the response exceeds the limit, then it is not audited and no vulnerability assessment is performed. | [optional] 
**was_http_user_agent** | **str** | The user-agent header used by the scanner when sending an HTTP request. For example, &#x60;Nessus WAS/%v&#x60; where %v is the version of the scan engine. | [optional] 
**was_plugins_autothrottle** | **str** | The plugins autothrottle setting (yes or no). | [optional] 
**was_plugins_rate_limiter_requests_per_second** | **int** | The maximum number of HTTP requests for the entire scan for a single host. | [optional] 
**was_plugins_selenium_crawl_script** | **str** | The name of the file containing Selenium scripts that Web Application Scanning uploads and uses to crawl during the scan. For more information, see &lt;a href&#x3D;\&quot;https://docs.tenable.com/tenableio/webapplicationscanning/Content/WebApplicationScanning/WebAppAuthentication.htm\&quot;&gt;Configure Selenium Authentication&lt;/a&gt; in the Tenable.io Web Application Scanning User Guide. | [optional] 
**was_scope_directory_depth_limit** | **int** | The maximum number of sub-directories the scanner crawls. For example, http://www.tenable.com/products/tenable-io has two sub-directories. | [optional] 
**was_scope_dom_depth_limit** | **int** | The maximum depth of HTML nested elements the scanner crawls. | [optional] 
**was_scope_exclude_file_extensions** | **str** | A list of file types excluded from the scan. Possible values are: css, js, png, jpeg, gif, pdf, csv. | [optional] 
**was_scope_exclude_path_patterns** | **str** | A regex specifying URLs excluded from the scan. | [optional] 
**was_scope_option** | **str** | Specifies how the scanner handles URLs found during the application crawl. Possible values are:  - all—Crawl all URLs detected.  - urls—Limit crawling to specified URLs.  - paths—Limit crawling to specified URLs and child paths. | [optional] 
**was_scope_page_limit** | **int** | The maximum number of URLs the scanner attempts to crawl and therefore audit. | [optional] 
**was_scope_urls** | **str** | The list of URLs that are scanned. | [optional] 
**was_timeout** | **str** | The maximum duration the scan runs before it stops automatically. This value uses the format, HH:MM:SS. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


