# InlineResponse20031

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | The status of the export request. Possible values include:  - QUEUED—Tenable.io has queued the export request until it completes other requests currently in process.  - PROCESSING—Tenable.io has started processing the export request.  - FINISHED—Tenable.io has completed processing the export request. The list of chunks is complete.  - CANCELLED—An administrator has cancelled the export request.  - ERROR—Tenable.io encountered an error while processing the export request. Tenable recommends that you retry the request. If the status persists on retry, contact Support. | [optional] 
**chunks_available** | **list[int]** | A list of completed chunks available for download. | [optional] 
**chunks_failed** | **list[int]** | A list of chunks for which the export process failed. If a chunk fails processing, submit the export request again. If the chunk continues to fail, contact Support. | [optional] 
**chunks_cancelled** | **list[int]** | A list of chunks for which the export process was cancelled. If a chunk fails processing, Tenable.io automatically cancels all subsequent chunks queued for export in the same request.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


