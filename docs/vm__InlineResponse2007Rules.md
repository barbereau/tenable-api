# InlineResponse2007Rules

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operators** | **list[str]** | The operator that specifies how Tenable.io matches the terms value to asset data. Corresponds to the operator component of the rules parameter. Possible operators include:   *eq—Tenable.io matches the rule to assets based on an exact match of the specified term. Note: Tenable.io interprets the operator as &#x60;equals&#x60; for ipv4 rules that specify a single IP address, but interprets the operator as &#x60;contains&#x60; for ipv4 rules that specify an IP range or CIDR range.   * match—Tenable.io matches the rule to assets based a partial match of the specified term.   * starts—Tenable.io matches the rule to assets that start with the specified term.   * ends—Tenable.io matches the rule to assets that end with the specified term. For a complete list of operators by rule type, use the [GET /v2/access-groups/filters](ref:io-v2-access-groups-list-filters) endpoint. | [optional] 
**name** | **str** | The name of the filter parameter. Corresponds to the asset rule type. | [optional] 
**readable_name** | **str** | The name of the field as it appears in the Tenable.io user interface. | [optional] 
**placeholder** | **str** | An example value. | [optional] 
**control** | [**list[InlineResponse2007Control]**](vm__InlineResponse2007Control.md) | Indicates how the field appears in the Tenable.io user interface. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


