# InlineResponse20072

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pagination** | [**InlineResponse20018Pagination**](vm__InlineResponse20018Pagination.md) |  | [optional] 
**history** | [**list[InlineResponse20072History]**](vm__InlineResponse20072History.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


