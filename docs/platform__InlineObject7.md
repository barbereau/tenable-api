# InlineObject7

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_enabled** | **bool** | Specifies whether backup notification for two-factor authentication is enabled. If enabled, Tenable.io sends the two-factor verification code via e-mail, as well as via the default SMS message. | 
**sms_enabled** | **bool** | Specifies whether two-factor authentication is enabled. If enabled, Tenable.io sends the verification code via an SMS message. This parameter must be enabled to enable two-factor verification for the user. | 
**sms_phone** | **str** | The phone number to use for two-factor authentication. Must begin with the &#x60;+&#x60; character and the country code. This field is required when sms\\_enabled is set to &#x60;true&#x60;. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


