# VulnsExportFiltersVprScore

Returns vulnerabilities with the specified Vulnerability Priority Rating (VPR) score or scores. You can combine properties in this object to specify VPR ranges. For example, to export vulnerabilities greater than or equal to 9.0 but lesser than or equal to 9.9, the object would contain a `gte` property of 9.0 and an `lte` property of 9.9.   For more information about VPR, see <a href=\"https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/RiskMetrics.htm\" target=\"_blank\">Severity vs. VPR</a> in the <i>Tenable.io Vulnerability Management User Guide</i>.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eq** | **list[float]** | Returns vulnerabilities with a VPR equal to the specified score or scores. This property cannot be combined with the following range operators: &#x60;lt&#x60;, &#x60;gt&#x60;, &#x60;lte&#x60;, or &#x60;gte&#x60;. | [optional] 
**neq** | **list[float]** | Returns vulnerabilities with a VPR not equal to the specified score or scores. This property can be combined with the &#x60;eq&#x60; property. | [optional] 
**gt** | **float** | Returns vulnerabilities with a VPR greater than the specified score. This property cannot be combined with the &#x60;eq&#x60; property. | [optional] 
**gte** | **float** | Returns vulnerabilities with a VPR greater than or equal to the specified score. This property cannot be combined with the &#x60;eq&#x60; property. | [optional] 
**lt** | **float** | Returns vulnerabilities with a VPR lesser than the specified score. This property cannot be combined with the &#x60;eq&#x60; property. | [optional] 
**lte** | **float** | Returns vulnerabilities with a VPR lesser than or equal to the specified score. This property cannot be combined with the &#x60;eq&#x60; property. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


