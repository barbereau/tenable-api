# InlineResponse20039Sort

The sorting parameters supported for data returned by the endpoint.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_sort_fields** | **int** | Maximum number of fields that may be specified for sorting. If this is parameter is not present, any number of fields may be used. | [optional] 
**sortable_fields** | **list[str]** | Fields by which the returned list of records may be sorted. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


