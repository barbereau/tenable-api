# UserTemplateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**template_id** | **str** | The UUID of the Tenable-provided template on which the user-defined template is based. | [readonly] 
**name** | **str** | The name of the user-defined template. | 
**description** | **str** | The description for the user-defined template. | [optional] 
**owner_id** | **str** | The UUID of the owner of the user-defined template. | 
**settings** | **object** | The restricted settings for the user-defined template. This is a free-form object as each template can define different parameters for a configuration. | [readonly] 
**default_permissions** | [**PermissionLevel**](was__PermissionLevel.md) |  | 
**results_visibility** | [**ResultsVisibility**](was__ResultsVisibility.md) |  | 
**permissions** | [**list[Permissions]**](was__Permissions.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


