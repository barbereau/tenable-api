# InlineObject51

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** | The new tag value. | [optional] 
**description** | **str** | The new tag value description. | [optional] 
**filters** | [**TagsValuesValueUuidFilters**](vm__TagsValuesValueUuidFilters.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


