# ScansCredentialsAddHost

The name of this parameter corresponds to the display name that uniquely identifies the credentials category (in this case, `Host` for credentials from the Host category). This value corresponds to the following response message attributes:  - `credentials[].data[].name` in the [GET /editor/type/templates/{template_uuid}](/reference#editor-template-details) response message  - `credentials[].id` in the [GET /credentials/types](/reference#credentials-list-credential-types) response message
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**windows** | [**list[ScansCredentialsAddHostWindows]**](vm__ScansCredentialsAddHostWindows.md) | The name of this parameter corresponds to the display name that uniquely identifies the credentials type (in this case, &#x60;Windows&#x60; for a Windows credential). This value corresponds to the following response message attributes:  - &#x60;credentials[].data[].types[].name&#x60; in the [GET /editor/type/templates/{template_uuid}](/reference#editor-template-details) response message  - &#x60;credentials[].types[].id&#x60; in the [GET /credentials/types](/reference#credentials-list-credential-types) response message&#x60; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


