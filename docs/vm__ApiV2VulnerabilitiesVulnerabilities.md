# ApiV2VulnerabilitiesVulnerabilities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tenable_plugin_id** | **str** | The ID of the Nessus plugin that identified the vulnerability. This parameter is required if the vulnerability object does not specify a cve value. | [optional] 
**cve** | **str** | The Common Vulnerability and Exposure (CVE) ID for the vulnerability. This parameter is required if the vulnerability object does not specify a &#x60;tenable_plugin_id&#x60; value. | [optional] 
**port** | **int** | The port the scanner used to communicate with the asset. | [optional] 
**protocol** | **str** | The protocol the scanner used to communicate with the asset. | [optional] 
**authenticated** | **bool** | A value specifying whether the scan that identified the vulnerability was an authenticated (credentialed) scan. | [optional] 
**last_found** | **int** | The date (in Unix time) when a scan last identified the vulnerability on the asset. | [optional] 
**output** | **str** | (Required) The text output of the scanner, up to 2,000 maximum characters. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


