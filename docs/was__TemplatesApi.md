# tenableapi.was.TemplatesApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_v2_templates_details**](was__TemplatesApi.md#was_v2_templates_details) | **GET** /was/v2/templates/{template_id} | Get Tenable-provided template details
[**was_v2_templates_list**](was__TemplatesApi.md#was_v2_templates_list) | **GET** /was/v2/templates | List Tenable-provided templates
[**was_v2_user_templates_delete**](was__TemplatesApi.md#was_v2_user_templates_delete) | **DELETE** /was/v2/user-templates/{user_template_id} | Delete user-defined template
[**was_v2_user_templates_details**](was__TemplatesApi.md#was_v2_user_templates_details) | **GET** /was/v2/user-templates/{user_template_id} | Get user-defined template details
[**was_v2_user_templates_list**](was__TemplatesApi.md#was_v2_user_templates_list) | **GET** /was/v2/user-templates | List user-defined templates
[**was_v2_user_templates_update**](was__TemplatesApi.md#was_v2_user_templates_update) | **PUT** /was/v2/user-templates/{user_template_id} | Update user-defined template


# **was_v2_templates_details**
> ConfigTemplate was_v2_templates_details(template_id)

Get Tenable-provided template details

Returns the details for a Tenable-provided template. Tenable-provided templates can be used to define scan configurations.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.TemplatesApi(api_client)
    template_id = 'template_id_example' # str | The UUID of the Tenable-provided template resource.

    try:
        # Get Tenable-provided template details
        api_response = api_instance.was_v2_templates_details(template_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TemplatesApi->was_v2_templates_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template_id** | [**str**](.md)| The UUID of the Tenable-provided template resource. | 

### Return type

[**ConfigTemplate**](was__ConfigTemplate.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the details of the specified Tenable-provided template. |  -  |
**400** | Returned if your request specifies an invalid template UUID. |  -  |
**404** | Returned if Tenable.io cannot find the specified template. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_templates_list**
> InlineResponse2005 was_v2_templates_list(order_by=order_by, ordering=ordering, page=page, size=size)

List Tenable-provided templates

Returns a paginated list of Tenable-provided templates that are available to be used for scan configurations.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.TemplatesApi(api_client)
    order_by = 'created_at' # str | The field used to order the query results. (optional)
ordering = 'asc' # str | The sort order applied when sorting by the `order_by` parameter. Values include:  - `asc`  - `desc`  If your request omits the `ordering` query parameter, this value defaults to `asc`. (optional) (default to 'asc')
page = 0 # int | The starting record to retrieve. Use in combination with the `size` parameter to paginate results. The default value is `0`. (optional) (default to 0)
size = 10 # int | The page size of the query results. Use in combination with the `page` parameter to paginate results. The default value is `10`. (optional) (default to 10)

    try:
        # List Tenable-provided templates
        api_response = api_instance.was_v2_templates_list(order_by=order_by, ordering=ordering, page=page, size=size)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TemplatesApi->was_v2_templates_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_by** | **str**| The field used to order the query results. | [optional] 
 **ordering** | **str**| The sort order applied when sorting by the &#x60;order_by&#x60; parameter. Values include:  - &#x60;asc&#x60;  - &#x60;desc&#x60;  If your request omits the &#x60;ordering&#x60; query parameter, this value defaults to &#x60;asc&#x60;. | [optional] [default to &#39;asc&#39;]
 **page** | **int**| The starting record to retrieve. Use in combination with the &#x60;size&#x60; parameter to paginate results. The default value is &#x60;0&#x60;. | [optional] [default to 0]
 **size** | **int**| The page size of the query results. Use in combination with the &#x60;page&#x60; parameter to paginate results. The default value is &#x60;10&#x60;. | [optional] [default to 10]

### Return type

[**InlineResponse2005**](was__InlineResponse2005.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of Tenable-provided templates. |  -  |
**400** | Returned if your request specifies invalid parameters. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_user_templates_delete**
> was_v2_user_templates_delete(user_template_id)

Delete user-defined template

Deletes the specified user-defined template. You cannot delete a user-defined template if scan configurations currently use the template. You must delete any scan configuration using the template prior to deleting the template. You can delete scan configurations with the [DELETE /was/v2/configs/{config_id}](ref:was-v2-config-delete) endpoint. <p>Requires STANDARD [32] user permissions and CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.TemplatesApi(api_client)
    user_template_id = 'user_template_id_example' # str | The UUID of the user-defined template.

    try:
        # Delete user-defined template
        api_instance.was_v2_user_templates_delete(user_template_id)
    except ApiException as e:
        print("Exception when calling TemplatesApi->was_v2_user_templates_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_template_id** | [**str**](.md)| The UUID of the user-defined template. | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returned if Tenable.io Web Application Scanning successfully created the deletion job for the user-defined template. |  -  |
**400** | Returned if your request specifies an invalid UUID for a user-defined template. |  -  |
**404** | Returned if Tenable.io Web Application Scanning cannot find the specified user-defined template. |  -  |
**409** | Returned if you attempt to delete a user-defined template still in use by a scan configuration. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_user_templates_details**
> UserTemplate was_v2_user_templates_details(user_template_id)

Get user-defined template details

Returns details for a user-defined template. User-defined templates can be used to define scan configurations.<p>Requires SCAN OPERATOR [24] user permissions and CAN USE [16] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.TemplatesApi(api_client)
    user_template_id = 'user_template_id_example' # str | The UUID of the user-defined template.

    try:
        # Get user-defined template details
        api_response = api_instance.was_v2_user_templates_details(user_template_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TemplatesApi->was_v2_user_templates_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_template_id** | [**str**](.md)| The UUID of the user-defined template. | 

### Return type

[**UserTemplate**](was__UserTemplate.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns details for the specified user-defined template. |  -  |
**400** | Returned if your request specifies an invalid UUID for a user-defined template. |  -  |
**404** | Returned if Tenable.io Web Application Scanning cannot find the specified user-defined template. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_user_templates_list**
> InlineResponse2006 was_v2_user_templates_list(order_by=order_by, ordering=ordering, page=page, size=size)

List user-defined templates

Returns a paginated list of user-defined templates that are available to be used for scan configurations.<p>Requires SCAN OPERATOR [24] user permissions and CAN USE [16] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.TemplatesApi(api_client)
    order_by = 'created_at' # str | The field used to order the query results. (optional)
ordering = 'asc' # str | The sort order applied when sorting by the `order_by` parameter. Values include:  - `asc`  - `desc`  If your request omits the `ordering` query parameter, this value defaults to `asc`. (optional) (default to 'asc')
page = 0 # int | The starting record to retrieve. Use in combination with the `size` parameter to paginate results. The default value is `0`. (optional) (default to 0)
size = 10 # int | The page size of the query results. Use in combination with the `page` parameter to paginate results. The default value is `10`. (optional) (default to 10)

    try:
        # List user-defined templates
        api_response = api_instance.was_v2_user_templates_list(order_by=order_by, ordering=ordering, page=page, size=size)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TemplatesApi->was_v2_user_templates_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_by** | **str**| The field used to order the query results. | [optional] 
 **ordering** | **str**| The sort order applied when sorting by the &#x60;order_by&#x60; parameter. Values include:  - &#x60;asc&#x60;  - &#x60;desc&#x60;  If your request omits the &#x60;ordering&#x60; query parameter, this value defaults to &#x60;asc&#x60;. | [optional] [default to &#39;asc&#39;]
 **page** | **int**| The starting record to retrieve. Use in combination with the &#x60;size&#x60; parameter to paginate results. The default value is &#x60;0&#x60;. | [optional] [default to 0]
 **size** | **int**| The page size of the query results. Use in combination with the &#x60;page&#x60; parameter to paginate results. The default value is &#x60;10&#x60;. | [optional] [default to 10]

### Return type

[**InlineResponse2006**](was__InlineResponse2006.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of user-defined templates. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_user_templates_update**
> UserTemplate was_v2_user_templates_update(user_template_id, user_template_input)

Update user-defined template

Updates the specified user-defined template.<p>Requires STANDARD [32] user permissions and CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.TemplatesApi(api_client)
    user_template_id = 'user_template_id_example' # str | The UUID of the user-defined template.
user_template_input = tenableapi.was.UserTemplateInput() # UserTemplateInput | Specifies the updates you want to make to a user-defined template.

    try:
        # Update user-defined template
        api_response = api_instance.was_v2_user_templates_update(user_template_id, user_template_input)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling TemplatesApi->was_v2_user_templates_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_template_id** | [**str**](.md)| The UUID of the user-defined template. | 
 **user_template_input** | [**UserTemplateInput**](was__UserTemplateInput.md)| Specifies the updates you want to make to a user-defined template. | 

### Return type

[**UserTemplate**](was__UserTemplate.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returned if Tenable.io Web Application Scanning successfully updated the user-defined template. |  -  |
**400** | Returned if your request specifies invalid parameters. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

