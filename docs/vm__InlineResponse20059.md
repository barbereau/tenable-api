# InlineResponse20059

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scanner_id** | **str** | The linking key, that is, the alpha-numeric sequence of characters you use to link a scanner to Tenable.io. For more information about linking a scanner to Tenable.io, see the [Tenable.io Vulnerability Management Guide](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/LinkaSensor.htm). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


