# Permissions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**permissions_id** | **str** | The UUID of the permissions object. | 
**entity** | **str** | The type of permissions object: &#x60;user&#x60; (permissions for an individual user), or &#x60;group&#x60; (permissions for a user group). | 
**entity_id** | **str** | The UUID of the user or group granted the specified permissions. | 
**level** | [**PermissionLevel**](was__PermissionLevel.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


