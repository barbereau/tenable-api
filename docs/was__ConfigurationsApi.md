# tenableapi.was.ConfigurationsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_v2_config_create**](was__ConfigurationsApi.md#was_v2_config_create) | **POST** /was/v2/configs | Create scan configuration
[**was_v2_config_delete**](was__ConfigurationsApi.md#was_v2_config_delete) | **DELETE** /was/v2/configs/{config_id} | Delete scan configuration
[**was_v2_config_details**](was__ConfigurationsApi.md#was_v2_config_details) | **GET** /was/v2/configs/{config_id} | Get scan configuration details
[**was_v2_config_list**](was__ConfigurationsApi.md#was_v2_config_list) | **GET** /was/v2/configs | List configurations
[**was_v2_config_upsert**](was__ConfigurationsApi.md#was_v2_config_upsert) | **PUT** /was/v2/configs/{config_id} | Upsert scan configuration


# **was_v2_config_create**
> ScanConfig was_v2_config_create(config_input)

Create scan configuration

Creates a scan configuration. <p>Requires SCAN MANAGER [40] user permissions and CAN CONFIGURE [64] scan permissions. Alternatively, SCAN OPERATOR [24] user permissions and CAN USE [16] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ConfigurationsApi(api_client)
    config_input = tenableapi.was.ConfigInput() # ConfigInput | 

    try:
        # Create scan configuration
        api_response = api_instance.was_v2_config_create(config_input)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConfigurationsApi->was_v2_config_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **config_input** | [**ConfigInput**](was__ConfigInput.md)|  | 

### Return type

[**ScanConfig**](was__ScanConfig.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returned if Tenable.io successfully creates a scan configuration. |  -  |
**400** | Returned if your request specifies invalid scan configuration parameters. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_config_delete**
> was_v2_config_delete(config_id)

Delete scan configuration

Deletes the specified scan configuration and all associated scan history and vulnerabilities. You cannot delete a scan configuration if a scan based on that configuration is currently running.<p>Requires SCAN MANAGER [40] user permissions and CAN CONFIGURE [64] scan permissions. Alternatively, SCAN OPERATOR [24] user permissions and CAN USE [16] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ConfigurationsApi(api_client)
    config_id = 'config_id_example' # str | The UUID of the scan configuration to delete.

    try:
        # Delete scan configuration
        api_instance.was_v2_config_delete(config_id)
    except ApiException as e:
        print("Exception when calling ConfigurationsApi->was_v2_config_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **config_id** | [**str**](.md)| The UUID of the scan configuration to delete. | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returned if Tenable.io Web Application Scanning successfully created the deletion job. |  -  |
**400** | Returned if your request specifies an invalid scan configuration ID. |  -  |
**404** | Returned if Tenable.io Web Application Scanning cannot find the specified configuration. |  -  |
**409** | Returned if you attempt to delete a configuration in use by a running scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_config_details**
> ScanConfig was_v2_config_details(config_id)

Get scan configuration details

Returns details for the specified scan configuration.<p>Requires SCAN OPERATOR [24] user permissions and CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ConfigurationsApi(api_client)
    config_id = 'config_id_example' # str | The UUID of a scan configuration.

    try:
        # Get scan configuration details
        api_response = api_instance.was_v2_config_details(config_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConfigurationsApi->was_v2_config_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **config_id** | [**str**](.md)| The UUID of a scan configuration. | 

### Return type

[**ScanConfig**](was__ScanConfig.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns details for the specified scan configuration. |  -  |
**400** | Returned if your request specifies an invalid scan configuration ID. |  -  |
**404** | Returned if Tenable.io Web Application Scanning cannot find the specified configuration. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_config_list**
> InlineResponse200 was_v2_config_list(status=status, order_by=order_by, ordering=ordering, page=page, size=size)

List configurations

Returns a list of web application scan configurations. If a scan has been run using the configuration, the list also contains information about the last scan that was run. <p>Requires BASIC [16] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ConfigurationsApi(api_client)
    status = [tenableapi.was.ScanStatus()] # list[ScanStatus] | The scan status. (optional)
order_by = 'created_at' # str | The field used to order the query results. (optional)
ordering = 'asc' # str | The sort order applied when sorting by the `order_by` parameter. Values include:  - `asc`  - `desc`  If your request omits the `ordering` query parameter, this value defaults to `asc`. (optional) (default to 'asc')
page = 0 # int | The starting record to retrieve. Use in combination with the `size` parameter to paginate results. The default value is `0`. (optional) (default to 0)
size = 10 # int | The page size of the query results. Use in combination with the `page` parameter to paginate results. The default value is `10`. (optional) (default to 10)

    try:
        # List configurations
        api_response = api_instance.was_v2_config_list(status=status, order_by=order_by, ordering=ordering, page=page, size=size)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConfigurationsApi->was_v2_config_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | [**list[ScanStatus]**](was__ScanStatus.md)| The scan status. | [optional] 
 **order_by** | **str**| The field used to order the query results. | [optional] 
 **ordering** | **str**| The sort order applied when sorting by the &#x60;order_by&#x60; parameter. Values include:  - &#x60;asc&#x60;  - &#x60;desc&#x60;  If your request omits the &#x60;ordering&#x60; query parameter, this value defaults to &#x60;asc&#x60;. | [optional] [default to &#39;asc&#39;]
 **page** | **int**| The starting record to retrieve. Use in combination with the &#x60;size&#x60; parameter to paginate results. The default value is &#x60;0&#x60;. | [optional] [default to 0]
 **size** | **int**| The page size of the query results. Use in combination with the &#x60;page&#x60; parameter to paginate results. The default value is &#x60;10&#x60;. | [optional] [default to 10]

### Return type

[**InlineResponse200**](was__InlineResponse200.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of web application scan configurations. If a scan has been run using the configuration, the list also contains information about the last scan that was run. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_config_upsert**
> ScanConfig was_v2_config_upsert(config_id, config_input)

Upsert scan configuration

Updates an existing scan configuration or creates a new scan configuration.  **Note:** Although this endpoint can be used to create a scan configuration, Tenable recommends the [POST /was/v2/configs](ref:was-v2-config-create) endpoint instead for the creation of scan configurations. To create a scan configuration with this endpoint, you first need to generate a UUID. Tenable recommends the `uuidgen` tool available in most Linux distributions. The `--time` option can be passed to the `uuidgen` tool to ensure that the UUID is unique.<p>Requires SCAN MANAGER [40] user permissions and CAN CONFIGURE [64] scan permissions. Alternatively, SCAN OPERATOR [24] user permissions and CAN USE [16] policy permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.ConfigurationsApi(api_client)
    config_id = 'config_id_example' # str | If updating an existing scan configuration, the UUID of the scan configuration you want to update. If creating a new scan configuration, a new UUID generated with a tool like `uuidgen`.
config_input = tenableapi.was.ConfigInput() # ConfigInput | 

    try:
        # Upsert scan configuration
        api_response = api_instance.was_v2_config_upsert(config_id, config_input)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConfigurationsApi->was_v2_config_upsert: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **config_id** | [**str**](.md)| If updating an existing scan configuration, the UUID of the scan configuration you want to update. If creating a new scan configuration, a new UUID generated with a tool like &#x60;uuidgen&#x60;. | 
 **config_input** | [**ConfigInput**](was__ConfigInput.md)|  | 

### Return type

[**ScanConfig**](was__ScanConfig.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returned if Tenable.io successfully updates an existing scan configuration or creates a new scan configuration. |  -  |
**400** | Returned if your request specifies invalid scan configuration parameters. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

