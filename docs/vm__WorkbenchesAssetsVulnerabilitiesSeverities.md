# WorkbenchesAssetsVulnerabilitiesSeverities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | The number of vulnerabilities with the specified severity. | [optional] 
**level** | **int** | The code for the severity. Possible values include:   - 0—The vulnerability has a CVSS score of 0, which corresponds to the \&quot;info\&quot; severity level.  - 1—The vulnerability has a CVSS score between 0.1 and 3.9, which corresponds to the \&quot;low\&quot; severity level.  - 2—The vulnerability has a CVSS score between 4.0 and 6.9, which corresponds to the \&quot;medium\&quot; severity level.  - 3—The vulnerability has a CVSS score between 7.0 and 9.9, which corresponds to the \&quot;high\&quot; severity level.  - 4—The vulnerability has a CVSS score of 10.0, which corresponds to the \&quot;critical\&quot; severity level. | [optional] 
**name** | **str** | The severity of the vulnerability as defined using the Common Vulnerability Scoring System (CVSS) base score. Possible values include &#x60;info&#x60; (CVSS score of 0), &#x60;low&#x60; (CVSS score between 0.1 and 3.9), &#x60;medium&#x60; (CVSS score between 4.0 and 6.9), &#x60;high&#x60; (CVSS score between 7.0 and 9.9), and &#x60;critical&#x60; (CVSS score of 10.0). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


