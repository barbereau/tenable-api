# ScanNote

A note describing error conditions the scanner encountered for a given scan.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scan_note_id** | **str** | The UUID of the note. | [optional] [readonly] 
**scan_id** | **str** | The UUID of the scan. | [optional] [readonly] 
**created_at** | **datetime** | An ISO timestamp indicating the date and time when the scan was created, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] [readonly] 
**severity** | **str** | The severity rating. For example: &#x60;info&#x60;, &#x60;low&#x60;, &#x60;medium&#x60;, &#x60;high&#x60;, &#x60;critical&#x60;. | [optional] 
**title** | **str** | The title of the note. | [optional] [readonly] 
**message** | **str** | The specific message of the note. | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


