# InlineObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host** | **str** |  | 
**port** | **int** |  | 
**username** | **str** |  | 
**password** | **str** |  | 
**provider** | **str** |  | 
**active** | **bool** |  | [optional] 
**ssl** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


