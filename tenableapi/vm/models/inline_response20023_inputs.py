# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20023Inputs(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'type': 'str',
        'name': 'str',
        'required': 'bool',
        'placeholder': 'str',
        'regex': 'str',
        'hint': 'str',
        'callback': 'str',
        'default_row_count': 'int',
        'hide_values': 'bool',
        'id': 'str'
    }

    attribute_map = {
        'type': 'type',
        'name': 'name',
        'required': 'required',
        'placeholder': 'placeholder',
        'regex': 'regex',
        'hint': 'hint',
        'callback': 'callback',
        'default_row_count': 'default-row-count',
        'hide_values': 'hide-values',
        'id': 'id'
    }

    def __init__(self, type=None, name=None, required=None, placeholder=None, regex=None, hint=None, callback=None, default_row_count=None, hide_values=None, id=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20023Inputs - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._type = None
        self._name = None
        self._required = None
        self._placeholder = None
        self._regex = None
        self._hint = None
        self._callback = None
        self._default_row_count = None
        self._hide_values = None
        self._id = None
        self.discriminator = None

        if type is not None:
            self.type = type
        if name is not None:
            self.name = name
        if required is not None:
            self.required = required
        if placeholder is not None:
            self.placeholder = placeholder
        if regex is not None:
            self.regex = regex
        if hint is not None:
            self.hint = hint
        if callback is not None:
            self.callback = callback
        if default_row_count is not None:
            self.default_row_count = default_row_count
        if hide_values is not None:
            self.hide_values = hide_values
        if id is not None:
            self.id = id

    @property
    def type(self):
        """Gets the type of this InlineResponse20023Inputs.  # noqa: E501

        The type of input prompt in the user interface. Possible values include:  - password—Prompts for input via text box.  - text—Prompts for input via text box.  - select—Prompts for input via selectable options.  - file—Prompts user to upload file of input data.  - toggle—Prompts user to select one of two mutually-exclusive options in toggle format.  - checkbox—Prompts user to select options via checkboxes. Checkboxes can represent enabling a single option or can allow users to select from multiple, mutually-exclusive options.  - key-value— Prompts for text entry of a key-value pair via two text boxes.  # noqa: E501

        :return: The type of this InlineResponse20023Inputs.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this InlineResponse20023Inputs.

        The type of input prompt in the user interface. Possible values include:  - password—Prompts for input via text box.  - text—Prompts for input via text box.  - select—Prompts for input via selectable options.  - file—Prompts user to upload file of input data.  - toggle—Prompts user to select one of two mutually-exclusive options in toggle format.  - checkbox—Prompts user to select options via checkboxes. Checkboxes can represent enabling a single option or can allow users to select from multiple, mutually-exclusive options.  - key-value— Prompts for text entry of a key-value pair via two text boxes.  # noqa: E501

        :param type: The type of this InlineResponse20023Inputs.  # noqa: E501
        :type type: str
        """

        self._type = type

    @property
    def name(self):
        """Gets the name of this InlineResponse20023Inputs.  # noqa: E501

        The display name of the option in the user interface.  # noqa: E501

        :return: The name of this InlineResponse20023Inputs.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse20023Inputs.

        The display name of the option in the user interface.  # noqa: E501

        :param name: The name of this InlineResponse20023Inputs.  # noqa: E501
        :type name: str
        """

        self._name = name

    @property
    def required(self):
        """Gets the required of this InlineResponse20023Inputs.  # noqa: E501

        A value specifying whether the input is required (`true`) or optional (`false`).  # noqa: E501

        :return: The required of this InlineResponse20023Inputs.  # noqa: E501
        :rtype: bool
        """
        return self._required

    @required.setter
    def required(self, required):
        """Sets the required of this InlineResponse20023Inputs.

        A value specifying whether the input is required (`true`) or optional (`false`).  # noqa: E501

        :param required: The required of this InlineResponse20023Inputs.  # noqa: E501
        :type required: bool
        """

        self._required = required

    @property
    def placeholder(self):
        """Gets the placeholder of this InlineResponse20023Inputs.  # noqa: E501

        An example of the input value. This value appears as example text in the user interface.    This attribute is only present for credential parameters that require text input in the interface.     In cases where the input type is `key-value`, this attribute can be an array of strings.  # noqa: E501

        :return: The placeholder of this InlineResponse20023Inputs.  # noqa: E501
        :rtype: str
        """
        return self._placeholder

    @placeholder.setter
    def placeholder(self, placeholder):
        """Sets the placeholder of this InlineResponse20023Inputs.

        An example of the input value. This value appears as example text in the user interface.    This attribute is only present for credential parameters that require text input in the interface.     In cases where the input type is `key-value`, this attribute can be an array of strings.  # noqa: E501

        :param placeholder: The placeholder of this InlineResponse20023Inputs.  # noqa: E501
        :type placeholder: str
        """

        self._placeholder = placeholder

    @property
    def regex(self):
        """Gets the regex of this InlineResponse20023Inputs.  # noqa: E501

        A regular expression defining the valid input for the parameter in the user interface.  # noqa: E501

        :return: The regex of this InlineResponse20023Inputs.  # noqa: E501
        :rtype: str
        """
        return self._regex

    @regex.setter
    def regex(self, regex):
        """Sets the regex of this InlineResponse20023Inputs.

        A regular expression defining the valid input for the parameter in the user interface.  # noqa: E501

        :param regex: The regex of this InlineResponse20023Inputs.  # noqa: E501
        :type regex: str
        """

        self._regex = regex

    @property
    def hint(self):
        """Gets the hint of this InlineResponse20023Inputs.  # noqa: E501

        Helpful information about the input required, for example, \"PEM formatted certificate\". Hints appear in the user interface, but can contain information that is relevant to API requests.  # noqa: E501

        :return: The hint of this InlineResponse20023Inputs.  # noqa: E501
        :rtype: str
        """
        return self._hint

    @hint.setter
    def hint(self, hint):
        """Sets the hint of this InlineResponse20023Inputs.

        Helpful information about the input required, for example, \"PEM formatted certificate\". Hints appear in the user interface, but can contain information that is relevant to API requests.  # noqa: E501

        :param hint: The hint of this InlineResponse20023Inputs.  # noqa: E501
        :type hint: str
        """

        self._hint = hint

    @property
    def callback(self):
        """Gets the callback of this InlineResponse20023Inputs.  # noqa: E501

        Not supported as a parameter in managed credentials.  # noqa: E501

        :return: The callback of this InlineResponse20023Inputs.  # noqa: E501
        :rtype: str
        """
        return self._callback

    @callback.setter
    def callback(self, callback):
        """Sets the callback of this InlineResponse20023Inputs.

        Not supported as a parameter in managed credentials.  # noqa: E501

        :param callback: The callback of this InlineResponse20023Inputs.  # noqa: E501
        :type callback: str
        """

        self._callback = callback

    @property
    def default_row_count(self):
        """Gets the default_row_count of this InlineResponse20023Inputs.  # noqa: E501

        The number of text box rows that appear by default when the input type is `key-value`.  # noqa: E501

        :return: The default_row_count of this InlineResponse20023Inputs.  # noqa: E501
        :rtype: int
        """
        return self._default_row_count

    @default_row_count.setter
    def default_row_count(self, default_row_count):
        """Sets the default_row_count of this InlineResponse20023Inputs.

        The number of text box rows that appear by default when the input type is `key-value`.  # noqa: E501

        :param default_row_count: The default_row_count of this InlineResponse20023Inputs.  # noqa: E501
        :type default_row_count: int
        """

        self._default_row_count = default_row_count

    @property
    def hide_values(self):
        """Gets the hide_values of this InlineResponse20023Inputs.  # noqa: E501

        A value specifying whether the user interface hides the value by default when the input type is `key-value`. If `true`, dots appear instead of characters as you type the value in the user interface.  # noqa: E501

        :return: The hide_values of this InlineResponse20023Inputs.  # noqa: E501
        :rtype: bool
        """
        return self._hide_values

    @hide_values.setter
    def hide_values(self, hide_values):
        """Sets the hide_values of this InlineResponse20023Inputs.

        A value specifying whether the user interface hides the value by default when the input type is `key-value`. If `true`, dots appear instead of characters as you type the value in the user interface.  # noqa: E501

        :param hide_values: The hide_values of this InlineResponse20023Inputs.  # noqa: E501
        :type hide_values: bool
        """

        self._hide_values = hide_values

    @property
    def id(self):
        """Gets the id of this InlineResponse20023Inputs.  # noqa: E501

        The system name for the input. Use this value as the input name in request messages when configuring credentials.  # noqa: E501

        :return: The id of this InlineResponse20023Inputs.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20023Inputs.

        The system name for the input. Use this value as the input name in request messages when configuring credentials.  # noqa: E501

        :param id: The id of this InlineResponse20023Inputs.  # noqa: E501
        :type id: str
        """

        self._id = id

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20023Inputs):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20023Inputs):
            return True

        return self.to_dict() != other.to_dict()
