# InlineObject40

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the managed credentials. This name must be unique within your Tenable.io instance. | 
**settings** | [**ScansScanIdCredentialsCredentialsIdUpgradeSettings**](vm__ScansScanIdCredentialsCredentialsIdUpgradeSettings.md) |  | 
**type** | **str** | The system name that uniquely identifies the credentials type, for example, &#x60;Windows&#x60;. | 
**category** | **str** | The system name that uniquely identifies the credentials category, for example &#x60;Host&#x60;. | [optional] 
**ad_hoc** | **bool** | A value specifying whether the credentials are scan-specific (&#x60;true&#x60;) or managed (&#x60;false&#x60;). You must use &#x60;false&#x60; for this request body attribute to convert scan-specific to managed credentials. | 
**permissions** | [**list[ScansScanIdCredentialsCredentialsIdUpgradePermissions]**](vm__ScansScanIdCredentialsCredentialsIdUpgradePermissions.md) | A list of user permissions for the managed credentials. If a request message omits this parameter, Tenable.io automatically creates a &#x60;permissions&#x60; object for the user account that submits the request. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


