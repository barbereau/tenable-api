# InlineResponse20064

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | [**InlineResponse20064Info**](vm__InlineResponse20064Info.md) |  | [optional] 
**comphosts** | [**list[InlineResponse20064Comphosts]**](vm__InlineResponse20064Comphosts.md) | A list of the hosts targeted by the scan for the specified run. If the scan results are older than 60 days (that is, if the &#x60;info.archived&#x60; attribute for the scan is &#x60;true&#x60;), this array does not appear in the response message. | [optional] 
**hosts** | [**list[InlineResponse20064Comphosts]**](vm__InlineResponse20064Comphosts.md) | A list of the hosts targeted by the scan for the specified run. If the scan results are older than 60 days (that is, if the &#x60;info.archived&#x60; attribute for the scan is &#x60;true&#x60;), this array does not appear in the response message. | [optional] 
**notes** | [**list[InlineResponse20064Notes]**](vm__InlineResponse20064Notes.md) |  | [optional] 
**remediations** | **object** |  | [optional] 
**vulnerabilities** | [**list[InlineResponse20064Vulnerabilities]**](vm__InlineResponse20064Vulnerabilities.md) | A list of vulnerabilities that the scan identified on the target hosts. If the scan results are older than 60 days (that is, if the &#x60;info.archived&#x60; attribute for the scan is &#x60;true&#x60;), this array does not appear in the response message. | [optional] 
**filters** | [**list[InlineResponse20064Filters]**](vm__InlineResponse20064Filters.md) | A list of filters. If the scan results are older than 60 days (that is, if the &#x60;info.archived&#x60; attribute for the scan is &#x60;true&#x60;), this array does not appear in the response message. | [optional] 
**history** | [**list[InlineResponse20064History]**](vm__InlineResponse20064History.md) | A list of details about each time the scan has run. | [optional] 
**compliance** | [**list[InlineResponse20064Vulnerabilities]**](vm__InlineResponse20064Vulnerabilities.md) | A list of compliance checks performed during the run of the scan. If the scan results are older than 60 days (that is, if the &#x60;info.archived&#x60; attribute for the scan is &#x60;true&#x60;), this array does not appear in the response message. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


