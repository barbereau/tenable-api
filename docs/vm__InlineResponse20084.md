# InlineResponse20084

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categories** | [**list[InlineResponse20084Categories]**](vm__InlineResponse20084Categories.md) | A collection of category objects. | [optional] 
**pagination** | [**InlineResponse20084Pagination**](vm__InlineResponse20084Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


