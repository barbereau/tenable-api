# InputErrorResponse

Tenable.io Web Application scanning invalid input error response.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** | The Tenable.io Web Application Scanning error code. The code &#x60;INPUT_FORM_VIOLATION&#x60; is returned if your request body contains invalid input. | 
**fields** | [**list[InputErrorResponseFields]**](was__InputErrorResponseFields.md) | A list of fields that contain invalid input along with a message about the cause of the input error. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


