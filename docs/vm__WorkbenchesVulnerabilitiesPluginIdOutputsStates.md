# WorkbenchesVulnerabilitiesPluginIdOutputsStates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The current state of the reported plugin (Active, Fixed, New, etc.) | [optional] 
**results** | [**list[WorkbenchesVulnerabilitiesPluginIdOutputsResults]**](vm__WorkbenchesVulnerabilitiesPluginIdOutputsResults.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


