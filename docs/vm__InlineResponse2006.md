# InlineResponse2006

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wildcard_fields** | **list[str]** | The fields you can use as a wildcard (&#x60;wf&#x60; parameter) value in the [GET /v2/access-groups](ref:io-v2-access-groups-list) endpoint. | [optional] 
**filters** | [**list[InlineResponse2002Filters]**](vm__InlineResponse2002Filters.md) | The filters and operators for each field you can use when constructing filter (&#x60;f&#x60; parameter) values in the [GET /v2/access-groups](ref:io-v2-access-groups-list) endpoint. | [optional] 
**sort** | [**list[InlineResponse2006Sort]**](vm__InlineResponse2006vm__Sort.md) | The fields you can use when constructing &#x60;sort&#x60; parameter values for the [GET /v2/access-groups](ref:io-v2-access-groups-list) endpoint. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


