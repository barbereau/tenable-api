# InlineResponse20092Discovery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seen_first** | **str** | The Unix timestamp of the scan that first detected the vulnerability on an asset. | [optional] 
**seen_last** | **str** | The Unix timestamp of the scan that most recently detected the vulnerability on an asset. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


