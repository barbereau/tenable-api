# InlineObject1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the access group you want to modify. | [optional] 
**all_assets** | **bool** | Specifies whether the access group you want to modify is the All Assets group or a user-defined group:   * If you want to refine membership in the All Assets access group (the only change you can make to the All Assets group), this parameter must be &#x60;true&#x60;. Tenable.io ignores any rules parameters in your request, but overwrrites existing principals parameters with those in the request based on the &#x60;all_users&#x60; and principals parameters in the request.   * If you want to modify a user-defined access group, this parameter must be &#x60;false&#x60;. Tenable.io overwrites the existing rules parameters with the rules parameters you specify in this request, and overwrites existing principals parameters based on the &#x60;all_users&#x60; and principals parameters in the request. | [optional] 
**all_users** | **bool** | Specifies whether assets in the access group can be viewed by all or only some users in your organization:   * If &#x60;true&#x60;, all users in your organization have Can View access to the assets defined in the rules parameter. Tenable.io ignores any principal parameters in your request.   * If &#x60;false&#x60;, only specified users have Can View access to the assets defined in the rules parameter. You define which users or user groups have access in the principals parameter of the request.      If you omit this parameter, Tenable.io sets the parameter to &#x60;false&#x60; by default. | [optional] 
**rules** | [**list[AccessGroupsRules]**](vm__AccessGroupsRules.md) | An array of asset rules. Tenable.io uses these rules to assign assets to the access group. You can specify a maximum of 1,000 rules for an individual access group. If you specify multiple rules for an access group, Tenable.io assigns an asset to the access group if the asset matches any of the rules. You can only add rules to access groups if the &#x60;all_assets&#x60; parameter is set to &#x60;false&#x60;. | [optional] 
**principals** | [**list[AccessGroupsPrincipals]**](vm__AccessGroupsPrincipals.md) | An array of principals. Each principal represents a user or user group assigned to the access group. You cannot add an access group as a principal to another access group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


