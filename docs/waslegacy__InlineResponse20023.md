# InlineResponse20023

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pagination** | [**InlineResponse20023Pagination**](waslegacy__InlineResponse20023Pagination.md) |  | [optional] 
**history** | [**list[InlineResponse20023History]**](waslegacy__InlineResponse20023History.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


