# InlineObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logo** | **file** | The logo to upload. The logo must be in PNG format and no larger than 246x52 pixels. | 
**name** | **str** | An identifiable, user-defined name for the logo. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


