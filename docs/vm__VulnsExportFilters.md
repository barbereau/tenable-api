# VulnsExportFilters

Specifies filters for exported vulnerabilities.  **Note:** By default, vulnerability exports will only include vulnerabilities found or fixed within the last 30 days if no time-based filters (`last_fixed`, `last_found`, or `first_found`) are submitted with the request.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cidr_range** | **str** | Restricts search for vulnerabilities to assets assigned an IP address within the specified CIDR range. For example, 0.0.0.0/0 restricts the search to 0.0.0.1 and 255.255.255.254. | [optional] 
**first_found** | **int** | Returns vulnerabilities that were first found between the specified date (in Unix time) and now. | [optional] 
**last_found** | **int** | Returns vulnerabilities that were last found between the specified date (in Unix time) and now. | [optional] 
**last_fixed** | **int** | Returns vulnerabilities that were fixed between the specified date (in Unix time) and now. | [optional] 
**plugin_family** | **list[str]** | A list of plugin families for which you want to filter the vulnerabilities returned in the vulnerability export. This filter is case-sensitive. If your request omits this parameter, the export includes all vulnerabilities, regardless of plugin family. For a list of supported plugin family values, use the [GET /plugins/families](ref:io-plugins-families-list) endpoint. | [optional] 
**plugin_id** | **list[int]** | A list of plugin IDs for which you want to filter the vulnerabilities returned in the vulnerability export. If your request omits this parameter, the export includes all vulnerabilities, regardless of plugin ID. | [optional] 
**network_id** | **str** | The ID of the network object associated with scanners that detected the vulnerabilities you want to export. The default network ID is &#x60;00000000-0000-0000-0000-000000000000&#x60;. To determine the ID of a custom network, use the [GET /networks](ref:networks-list) endpoint. For more information about network objects, see [Manage Networks](doc:manage-networks-tio). | [optional] 
**severity** | **list[str]** | The severity of the vulnerabilities to include in the export. Defaults to all severity levels. The severity of a vulnerability is defined using the Common Vulnerability Scoring System (CVSS) base score. Supported array values are:  - info—The vulnerability has a CVSS score of 0.  - low—The vulnerability has a CVSS score between 0.1 and 3.9.  - medium—The vulnerability has a CVSS score between 4.0 and 6.9.  - high—The vulnerability has a CVSS score between 7.0 and 9.9.  - critical—The vulnerability has a CVSS score of 10.0. | [optional] 
**state** | **list[str]** | The state of the vulnerabilities you want the export to include. Supported, case-insensitive values are:  - open—The vulnerability is currently present on a host.  - reopened—The vulnerability was previously marked as fixed on a host, but has returned.  - fixed—The vulnerability was present on a host, but is no longer detected.  This parameter is required if your request includes &#x60;first_found&#x60;, &#x60;last_found&#x60;, or &#x60;last_fixed&#x60; parameters. If your request omits this parameter, the export includes default states &#x60;open&#x60; and &#x60;reopened&#x60; only. | [optional] 
**tag_category** | **list[str]** | Returns vulnerabilities on assets with the specified asset tags. The filter is defined as \&quot;tag\&quot;, a period (\&quot;.\&quot;), and the tag category name. The value of the filter is an array of tag values. For more information about tags, see the &lt;a href&#x3D;\&quot;https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/TagFormatAndApplication.htm\&quot; target&#x3D;\&quot;_blank\&quot;&gt;Tenable.io Vulnerability Management User Guide&lt;/a&gt;. | [optional] 
**vpr_score** | [**VulnsExportFiltersVprScore**](vm__VulnsExportFiltersVprScore.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


