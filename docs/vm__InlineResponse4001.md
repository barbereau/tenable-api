# InlineResponse4001

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** | The HTTP error code. | [optional] 
**message** | **str** | Text describing the error condition Tenable.io encountered. Possible values include:   - Cannot cancel a completed job  - Export UUID is invalid | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


