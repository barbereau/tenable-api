# ScannerGroupsGroupIdScannersLicenseAppsWas

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Indicates that the scanner is licensed to perform Tenable.io Web Application Scanning scans. | [optional] 
**mode** | **str** | Indicates whether the product license is an evaluation license (&#x60;eval&#x60;) or standard license (&#x60;standard&#x60;). | [optional] 
**expiration_date** | **int** | The Unix timestamp when the license expires. | [optional] 
**activation_code** | **int** | The activation code you used to enable the license. This value is present for standard licenses only. | [optional] 
**web_assets** | **int** | The number of web assets which your license authorizes you to scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


