# ScansPlugins

A list of plugins to add to the scan. Use the [GET /plugins/families](ref:io-plugins-families-list) endpoint to get a list of plugin families to choose from. Then, use the [GET /plugins/families/{id}](ref:io-plugins-family-details) endpoint to find plugins within the family to add to the scan.  **Note:** This form displays limited parameters. The example below illustrates how to add two plugins within the `Web Servers` plugin family to a scan.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**web_servers** | [**ScansPluginsWebServers**](vm__ScansPluginsWebServers.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


