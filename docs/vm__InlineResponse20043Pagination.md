# InlineResponse20043Pagination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | The total number of objects matching your search criteria. | [optional] 
**limit** | **int** | Maximum number of objects requested (or service imposed limit if not in request). | [optional] 
**offset** | **int** | Offset from request (or zero). | [optional] 
**sort** | [**list[Sort]**](vm__Sort.md) | An array of the fields you specified as sort fields in the request, which Tenable.io uses to sort the returned data. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


