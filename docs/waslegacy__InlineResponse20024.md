# InlineResponse20024

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owner_id** | **int** |  | [optional] 
**schedule_uuid** | **str** |  | [optional] 
**scan_start** | **int** |  | [optional] 
**scan_end** | **int** |  | [optional] 
**owner_uuid** | **str** |  | [optional] 
**owner** | **str** |  | [optional] 
**targets** | **str** |  | [optional] 
**object_id** | **int** |  | [optional] 
**uuid** | **str** |  | [optional] 
**scan_type** | **str** |  | [optional] 
**name** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


