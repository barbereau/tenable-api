# ConfigMetadataUserTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_template_id** | **str** | The UUID of the user-defined template. | [optional] 
**name** | **str** | The name of the user-defined template. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


