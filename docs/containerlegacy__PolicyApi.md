# tenableapi.containerv1.PolicyApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**container_security_policy_policy_compliance_by_id**](containerlegacy__PolicyApi.md#container_security_policy_policy_compliance_by_id) | **GET** /container-security/api/v1/policycompliance | Get compliance status by ID
[**container_security_policy_policy_compliance_by_name**](containerlegacy__PolicyApi.md#container_security_policy_policy_compliance_by_name) | **GET** /container-security/api/v1/compliancebyname | Get compliance status by name


# **container_security_policy_policy_compliance_by_id**
> object container_security_policy_policy_compliance_by_id(image_id)

Get compliance status by ID

**Deprecated!** Tenable.io Container Security API v1 is deprecated. Checks the compliance of an image that you specify by ID against your policies.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.PolicyApi(api_client)
    image_id = 56 # int | The ID of the image that you want to check for policy compliance.

    try:
        # Get compliance status by ID
        api_response = api_instance.container_security_policy_policy_compliance_by_id(image_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PolicyApi->container_security_policy_policy_compliance_by_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image_id** | **int**| The ID of the image that you want to check for policy compliance. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns an array of compliance results. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_policy_policy_compliance_by_name**
> object container_security_policy_policy_compliance_by_name(image, repo=repo, tag=tag)

Get compliance status by name

**Deprecated!** Tenable.io Container Security API v1 is deprecated. Checks the compliance of an image that you specify by name against your policies.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.PolicyApi(api_client)
    image = 'image_example' # str | The name of the image for which you want the job status.
repo = 'repo_example' # str | The name of the repository that hosts the image. By default, this value is library. (optional)
tag = 'tag_example' # str | The tag for the image that you want to check for policy compliance. (optional)

    try:
        # Get compliance status by name
        api_response = api_instance.container_security_policy_policy_compliance_by_name(image, repo=repo, tag=tag)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PolicyApi->container_security_policy_policy_compliance_by_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image** | **str**| The name of the image for which you want the job status. | 
 **repo** | **str**| The name of the repository that hosts the image. By default, this value is library. | [optional] 
 **tag** | **str**| The tag for the image that you want to check for policy compliance. | [optional] 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns an array compliance results. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

