# coding: utf-8

"""
    Container Security v1

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from tenableapi.containerv1.api_client import ApiClient
from tenableapi.containerv1.exceptions import (  # noqa: F401
    ApiTypeError,
    ApiValueError
)


class ContainersApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def container_security_containers_image_inventory(self, image_id, **kwargs):  # noqa: E501
        """Get image inventory  # noqa: E501

        **Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images](ref:container-security-v2-list-images) endpoint instead. Returns an inventory of an image by ID.<p>Requires BASIC [16] user permissions. See [Permissions](docs/permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.container_security_containers_image_inventory(image_id, async_req=True)
        >>> result = thread.get()

        :param image_id: The ID of the image that you want to inventory. (required)
        :type image_id: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: InlineResponse200
        """
        kwargs['_return_http_data_only'] = True
        return self.container_security_containers_image_inventory_with_http_info(image_id, **kwargs)  # noqa: E501

    def container_security_containers_image_inventory_with_http_info(self, image_id, **kwargs):  # noqa: E501
        """Get image inventory  # noqa: E501

        **Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images](ref:container-security-v2-list-images) endpoint instead. Returns an inventory of an image by ID.<p>Requires BASIC [16] user permissions. See [Permissions](docs/permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.container_security_containers_image_inventory_with_http_info(image_id, async_req=True)
        >>> result = thread.get()

        :param image_id: The ID of the image that you want to inventory. (required)
        :type image_id: str
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(InlineResponse200, status_code(int), headers(HTTPHeaderDict))
        """

        local_var_params = locals()

        all_params = [
            'image_id'
        ]
        all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth'
            ]
        )

        for key, val in six.iteritems(local_var_params['kwargs']):
            if key not in all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method container_security_containers_image_inventory" % key
                )
            local_var_params[key] = val
        del local_var_params['kwargs']
        # verify the required parameter 'image_id' is set
        if self.api_client.client_side_validation and ('image_id' not in local_var_params or  # noqa: E501
                                                        local_var_params['image_id'] is None):  # noqa: E501
            raise ApiValueError("Missing the required parameter `image_id` when calling `container_security_containers_image_inventory`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'image_id' in local_var_params:
            path_params['imageID'] = local_var_params['image_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json', 'text/html'])  # noqa: E501

        # Authentication setting
        auth_settings = ['cloud']  # noqa: E501
        
        response_types_map = {
            200: "InlineResponse200",
            401: None,
            429: None,
        }

        return self.api_client.call_api(
            '/container-security/api/v1/container/{imageID}/status', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_types_map=response_types_map,
            auth_settings=auth_settings,
            async_req=local_var_params.get('async_req'),
            _return_http_data_only=local_var_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=local_var_params.get('_preload_content', True),
            _request_timeout=local_var_params.get('_request_timeout'),
            collection_formats=collection_formats,
            _request_auth=local_var_params.get('_request_auth'))

    def container_security_containers_list_containers(self, **kwargs):  # noqa: E501
        """List containers  # noqa: E501

        **Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images](ref:container-security-v2-list-images) endpoint instead. Lists all containers.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.container_security_containers_list_containers(async_req=True)
        >>> result = thread.get()

        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: object
        """
        kwargs['_return_http_data_only'] = True
        return self.container_security_containers_list_containers_with_http_info(**kwargs)  # noqa: E501

    def container_security_containers_list_containers_with_http_info(self, **kwargs):  # noqa: E501
        """List containers  # noqa: E501

        **Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images](ref:container-security-v2-list-images) endpoint instead. Lists all containers.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.container_security_containers_list_containers_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(object, status_code(int), headers(HTTPHeaderDict))
        """

        local_var_params = locals()

        all_params = [
        ]
        all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth'
            ]
        )

        for key, val in six.iteritems(local_var_params['kwargs']):
            if key not in all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method container_security_containers_list_containers" % key
                )
            local_var_params[key] = val
        del local_var_params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json', 'text/html'])  # noqa: E501

        # Authentication setting
        auth_settings = ['cloud']  # noqa: E501
        
        response_types_map = {
            200: "object",
            401: None,
            429: None,
        }

        return self.api_client.call_api(
            '/container-security/api/v1/container/list', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_types_map=response_types_map,
            auth_settings=auth_settings,
            async_req=local_var_params.get('async_req'),
            _return_http_data_only=local_var_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=local_var_params.get('_preload_content', True),
            _request_timeout=local_var_params.get('_request_timeout'),
            collection_formats=collection_formats,
            _request_auth=local_var_params.get('_request_auth'))
