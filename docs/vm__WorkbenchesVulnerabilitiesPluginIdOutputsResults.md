# WorkbenchesVulnerabilitiesPluginIdOutputsResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application_protocol** | **str** | The application protocol where this vulnerability was found. | [optional] 
**port** | **int** | The port number where this vulnerability was found. | [optional] 
**transport_protocol** | **str** | The transportation protocol (TCP or UDP) where this vulnerability was found. | [optional] 
**assets** | [**list[WorkbenchesVulnerabilitiesPluginIdOutputsAssets]**](vm__WorkbenchesVulnerabilitiesPluginIdOutputsAssets.md) | A list of assets where this output was found. | [optional] 
**severity** | **int** | Integer [0-4] indicating how severe the vulnerability is, where 0 is info only. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


