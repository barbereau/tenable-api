# ScansPluginsWebServers

A list of plugin families containing individual plugins to add to the scan. Use the [GET /plugins/families](ref:io-plugins-families-list) endpoint to get a list of plugin families.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**individual** | [**ScansPluginsWebServersIndividual**](vm__ScansPluginsWebServersIndividual.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


