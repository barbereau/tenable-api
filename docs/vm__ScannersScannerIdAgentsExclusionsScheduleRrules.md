# ScannersScannerIdAgentsExclusionsScheduleRrules

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**freq** | **str** | The frequency of the rule (ONETIME, DAILY, WEEKLY, MONTHLY, YEARLY). | 
**interval** | **int** | The interval of the rule. | [optional] 
**byweekday** | **str** | A comma separated string of days to repeat a WEEKLY freq rule on (SU,MO,TU,WE,TH,FR, or SA). | [optional] 
**bymonthday** | **int** | The day of the month to repeat a MONTHLY freq rule on. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


