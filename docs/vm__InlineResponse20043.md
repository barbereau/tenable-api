# InlineResponse20043

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**networks** | [**list[InlineResponse20043Networks]**](vm__InlineResponse20043Networks.md) |  | [optional] 
**pagination** | [**list[InlineResponse20043Pagination]**](vm__InlineResponse20043Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


