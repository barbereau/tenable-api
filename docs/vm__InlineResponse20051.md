# InlineResponse20051

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The unique ID of the policy. | [optional] 
**template_uuid** | **str** | The UUID for the Tenable-provided template used to create the policy. | [optional] 
**name** | **str** | The name of the policy. | [optional] 
**description** | **str** | The description of the policy. | [optional] 
**owner_id** | **str** | The unique ID of the owner of the policy. | [optional] 
**owner** | **str** | The username for the owner of the policy. | [optional] 
**shared** | **int** | The shared status of the policy (&#x60;1&#x60; if shared with users other than owner, &#x60;0&#x60; if not shared). | [optional] 
**user_permissions** | **int** | The sharing permissions for the policy. | [optional] 
**creation_date** | **int** | The creation date of the policy in Unix time format. | [optional] 
**last_modification_date** | **int** | The last modification date for the policy in Unix time format. | [optional] 
**visibility** | **int** | The visibility of the target (&#x60;private&#x60; or &#x60;shared&#x60;). | [optional] 
**no_target** | **bool** | If &#x60;true&#x60;, the policy configuration does not include targets. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


