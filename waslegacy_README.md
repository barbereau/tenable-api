# tenable-api
No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

This Python package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.0.0
- Package version: 1.0.1
- Build package: org.openapitools.codegen.languages.PythonLegacyClientCodegen

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

If the python package is hosted on a repository, you can install directly using:

```sh
pip install git+https://github.com/barbich/python-tenable.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/barbich/python-tenable.git`)

Then import the package:
```python
import tenableapi.waslegacy
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import tenableapi.waslegacy
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function

import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'


# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.AssetsApi(api_client)
    asset_uuid = 'asset_uuid_example' # str | The UUID of the asset.

    try:
        # Get asset information
        api_response = api_instance.was_assets_asset_info(asset_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AssetsApi->was_assets_asset_info: %s\n" % e)
    
```

## Documentation for API Endpoints

All URIs are relative to *https://cloud.tenable.com*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AssetsApi* | [**was_assets_asset_info**](docs/waslegacy__AssetsApi.md#was_assets_asset_info) | **GET** /assets/{asset_uuid} | Get asset information
*AssetsApi* | [**was_assets_import**](docs/waslegacy__AssetsApi.md#was_assets_import) | **POST** /import/assets | Import asset list
*AssetsApi* | [**was_assets_import_job_info**](docs/waslegacy__AssetsApi.md#was_assets_import_job_info) | **GET** /import/asset-jobs/{asset_import_job_uuid} | Get import job information
*AssetsApi* | [**was_assets_list_assets**](docs/waslegacy__AssetsApi.md#was_assets_list_assets) | **GET** /assets | List assets
*AssetsApi* | [**was_assets_list_import_jobs**](docs/waslegacy__AssetsApi.md#was_assets_list_import_jobs) | **GET** /import/asset-jobs | List asset import jobs
*EditorApi* | [**was_editor_details**](docs/waslegacy__EditorApi.md#was_editor_details) | **GET** /editor/{type}/{id} | Get configuration details
*EditorApi* | [**was_editor_list**](docs/waslegacy__EditorApi.md#was_editor_list) | **GET** /editor/{type}/templates | List templates
*EditorApi* | [**was_editor_plugin_description**](docs/waslegacy__EditorApi.md#was_editor_plugin_description) | **GET** /editor/policy/{policy_id}/families/{family_id}/plugins/{plugin_id} | Get plugin details
*EditorApi* | [**was_editor_template_details**](docs/waslegacy__EditorApi.md#was_editor_template_details) | **GET** /editor/{type}/templates/{template_uuid} | Get template details
*FoldersApi* | [**was_folders_create**](docs/waslegacy__FoldersApi.md#was_folders_create) | **POST** /folders | Create new folder
*FoldersApi* | [**was_folders_delete**](docs/waslegacy__FoldersApi.md#was_folders_delete) | **DELETE** /folders/{folder_id} | Delete folder
*FoldersApi* | [**was_folders_edit**](docs/waslegacy__FoldersApi.md#was_folders_edit) | **PUT** /folders/{folder_id} | Rename folder
*FoldersApi* | [**was_folders_list**](docs/waslegacy__FoldersApi.md#was_folders_list) | **GET** /folders | List scan folders
*PoliciesApi* | [**was_policies_configure**](docs/waslegacy__PoliciesApi.md#was_policies_configure) | **PUT** /policies/{policy_id} | Update policy
*PoliciesApi* | [**was_policies_copy**](docs/waslegacy__PoliciesApi.md#was_policies_copy) | **POST** /policies/{policy_id}/copy | Copy policy
*PoliciesApi* | [**was_policies_create**](docs/waslegacy__PoliciesApi.md#was_policies_create) | **POST** /policies | Create policy
*PoliciesApi* | [**was_policies_delete**](docs/waslegacy__PoliciesApi.md#was_policies_delete) | **DELETE** /policies/{policy_id} | Delete policy
*PoliciesApi* | [**was_policies_details**](docs/waslegacy__PoliciesApi.md#was_policies_details) | **GET** /policies/{policy_id} | List policy details
*PoliciesApi* | [**was_policies_export**](docs/waslegacy__PoliciesApi.md#was_policies_export) | **GET** /policies/{policy_id}/export | Export policy
*PoliciesApi* | [**was_policies_import**](docs/waslegacy__PoliciesApi.md#was_policies_import) | **POST** /policies/import | Import policy
*PoliciesApi* | [**was_policies_list**](docs/waslegacy__PoliciesApi.md#was_policies_list) | **GET** /policies | List policies
*ScannerGroupsApi* | [**was_scanner_groups_add_scanner**](docs/waslegacy__ScannerGroupsApi.md#was_scanner_groups_add_scanner) | **POST** /scanner-groups/{group_id}/scanners/{scanner_id} | Add scanner to scanner group
*ScannerGroupsApi* | [**was_scanner_groups_create**](docs/waslegacy__ScannerGroupsApi.md#was_scanner_groups_create) | **POST** /scanner-groups | Create scanner group
*ScannerGroupsApi* | [**was_scanner_groups_delete**](docs/waslegacy__ScannerGroupsApi.md#was_scanner_groups_delete) | **DELETE** /scanner-groups/{group_id} | Delete a scanner group
*ScannerGroupsApi* | [**was_scanner_groups_delete_scanner**](docs/waslegacy__ScannerGroupsApi.md#was_scanner_groups_delete_scanner) | **DELETE** /scanner-groups/{group_id}/scanners/{scanner_id} | Remove scanner from scanner group
*ScannerGroupsApi* | [**was_scanner_groups_details**](docs/waslegacy__ScannerGroupsApi.md#was_scanner_groups_details) | **GET** /scanner-groups/{group_id} | List scanner group details
*ScannerGroupsApi* | [**was_scanner_groups_edit**](docs/waslegacy__ScannerGroupsApi.md#was_scanner_groups_edit) | **PUT** /scanner-groups/{group_id} | Update scanner group
*ScannerGroupsApi* | [**was_scanner_groups_list**](docs/waslegacy__ScannerGroupsApi.md#was_scanner_groups_list) | **GET** /scanner-groups | List scanner groups
*ScannerGroupsApi* | [**was_scanner_groups_list_scanners**](docs/waslegacy__ScannerGroupsApi.md#was_scanner_groups_list_scanners) | **GET** /scanner-groups/{group_id}/scanners | List scanners within scanner group
*ScannersApi* | [**was_scanners_delete**](docs/waslegacy__ScannersApi.md#was_scanners_delete) | **DELETE** /scanners/{scanner_id} | Delete scanner
*ScannersApi* | [**was_scanners_details**](docs/waslegacy__ScannersApi.md#was_scanners_details) | **GET** /scanners/{scanner_id} | Get scanner details
*ScannersApi* | [**was_scanners_get_scanner_key**](docs/waslegacy__ScannersApi.md#was_scanners_get_scanner_key) | **GET** /scanners/{scanner_id}/key | Get scanner key
*ScannersApi* | [**was_scanners_get_scans**](docs/waslegacy__ScannersApi.md#was_scanners_get_scans) | **GET** /scanners/{scanner_id}/scans | List running scans
*ScannersApi* | [**was_scanners_list**](docs/waslegacy__ScannersApi.md#was_scanners_list) | **GET** /scanners | List scanners
*ScannersApi* | [**was_scanners_toggle_link_state**](docs/waslegacy__ScannersApi.md#was_scanners_toggle_link_state) | **PUT** /scanners/{scanner_id}/link | Toggle scanner link state
*ScansApi* | [**was_scans_attachments**](docs/waslegacy__ScansApi.md#was_scans_attachments) | **GET** /scans/{scan_id}/attachments/{attachment_id} | Get scan attachment file
*ScansApi* | [**was_scans_configure**](docs/waslegacy__ScansApi.md#was_scans_configure) | **PUT** /scans/{scan_id} | Update scan
*ScansApi* | [**was_scans_copy**](docs/waslegacy__ScansApi.md#was_scans_copy) | **POST** /scans/{scan_id}/copy | Copy scan
*ScansApi* | [**was_scans_create**](docs/waslegacy__ScansApi.md#was_scans_create) | **POST** /scans | Create scan
*ScansApi* | [**was_scans_delete**](docs/waslegacy__ScansApi.md#was_scans_delete) | **DELETE** /scans/{scan_id} | Delete scan
*ScansApi* | [**was_scans_delete_history**](docs/waslegacy__ScansApi.md#was_scans_delete_history) | **DELETE** /scans/{scan_id}/history/{history_uuid} | Delete scan history
*ScansApi* | [**was_scans_details**](docs/waslegacy__ScansApi.md#was_scans_details) | **GET** /scans/{scan_uuid} | Get scan details
*ScansApi* | [**was_scans_export_download**](docs/waslegacy__ScansApi.md#was_scans_export_download) | **GET** /scans/{scan_id}/export/{file_uuid}/download | Download exported scan
*ScansApi* | [**was_scans_export_request**](docs/waslegacy__ScansApi.md#was_scans_export_request) | **POST** /scans/{scan_id}/export | Export scan
*ScansApi* | [**was_scans_export_status**](docs/waslegacy__ScansApi.md#was_scans_export_status) | **GET** /scans/{scan_id}/export/{file_uuid}/status | Check scan export status
*ScansApi* | [**was_scans_history**](docs/waslegacy__ScansApi.md#was_scans_history) | **GET** /scans/{scan_id}/history | Get scan history
*ScansApi* | [**was_scans_history_details**](docs/waslegacy__ScansApi.md#was_scans_history_details) | **GET** /scans/{scan_id}/history/{history_uuid} | Get scan history details
*ScansApi* | [**was_scans_host_details**](docs/waslegacy__ScansApi.md#was_scans_host_details) | **GET** /was-query/scans/{scan_uuid}/hosts/{host_id} | Return host details
*ScansApi* | [**was_scans_import**](docs/waslegacy__ScansApi.md#was_scans_import) | **POST** /scans/import | Import scan
*ScansApi* | [**was_scans_launch**](docs/waslegacy__ScansApi.md#was_scans_launch) | **POST** /scans/{scan_id}/launch | Launch scan
*ScansApi* | [**was_scans_list**](docs/waslegacy__ScansApi.md#was_scans_list) | **GET** /scans | List scans
*ScansApi* | [**was_scans_plugin_output**](docs/waslegacy__ScansApi.md#was_scans_plugin_output) | **GET** /was-query/scans/{scan_uuid}/hosts/{host_id}/plugins/{plugin_id} | Get plugin output
*ScansApi* | [**was_scans_progress**](docs/waslegacy__ScansApi.md#was_scans_progress) | **GET** /was-query/scans/{scan_uuid}/progress | Check scan status
*ScansApi* | [**was_scans_read_status**](docs/waslegacy__ScansApi.md#was_scans_read_status) | **PUT** /scans/{scan_id}/status | Update scan status
*ScansApi* | [**was_scans_schedule**](docs/waslegacy__ScansApi.md#was_scans_schedule) | **PUT** /scans/{scan_id}/schedule | Enable schedule
*ScansApi* | [**was_scans_stop**](docs/waslegacy__ScansApi.md#was_scans_stop) | **POST** /scans/{scan_id}/stop | Stop scan
*ScansApi* | [**was_scans_timezones**](docs/waslegacy__ScansApi.md#was_scans_timezones) | **GET** /scans/timezones | Get timezones


## Documentation For Models

 - [InlineObject](docs/waslegacy__InlineObject.md)
 - [InlineObject1](docs/waslegacy__InlineObject1.md)
 - [InlineObject10](docs/waslegacy__InlineObject10.md)
 - [InlineObject11](docs/waslegacy__InlineObject11.md)
 - [InlineObject12](docs/waslegacy__InlineObject12.md)
 - [InlineObject13](docs/waslegacy__InlineObject13.md)
 - [InlineObject14](docs/waslegacy__InlineObject14.md)
 - [InlineObject15](docs/waslegacy__InlineObject15.md)
 - [InlineObject2](docs/waslegacy__InlineObject2.md)
 - [InlineObject3](docs/waslegacy__InlineObject3.md)
 - [InlineObject4](docs/waslegacy__InlineObject4.md)
 - [InlineObject5](docs/waslegacy__InlineObject5.md)
 - [InlineObject6](docs/waslegacy__InlineObject6.md)
 - [InlineObject7](docs/waslegacy__InlineObject7.md)
 - [InlineObject8](docs/waslegacy__InlineObject8.md)
 - [InlineObject9](docs/waslegacy__InlineObject9.md)
 - [InlineResponse200](docs/waslegacy__InlineResponse200.md)
 - [InlineResponse2001](docs/waslegacy__InlineResponse2001.md)
 - [InlineResponse20010](docs/waslegacy__InlineResponse20010.md)
 - [InlineResponse20011](docs/waslegacy__InlineResponse20011.md)
 - [InlineResponse20012](docs/waslegacy__InlineResponse20012.md)
 - [InlineResponse20013](docs/waslegacy__InlineResponse20013.md)
 - [InlineResponse20014](docs/waslegacy__InlineResponse20014.md)
 - [InlineResponse20015](docs/waslegacy__InlineResponse20015.md)
 - [InlineResponse20016](docs/waslegacy__InlineResponse20016.md)
 - [InlineResponse20016Scans](docs/waslegacy__InlineResponse20016Scans.md)
 - [InlineResponse20017](docs/waslegacy__InlineResponse20017.md)
 - [InlineResponse20017Control](docs/waslegacy__InlineResponse20017Control.md)
 - [InlineResponse20017Filters](docs/waslegacy__InlineResponse20017Filters.md)
 - [InlineResponse20017History](docs/waslegacy__InlineResponse20017History.md)
 - [InlineResponse20017Hosts](docs/waslegacy__InlineResponse20017Hosts.md)
 - [InlineResponse20017Notes](docs/waslegacy__InlineResponse20017Notes.md)
 - [InlineResponse20017Vulnerabilities](docs/waslegacy__InlineResponse20017Vulnerabilities.md)
 - [InlineResponse20018](docs/waslegacy__InlineResponse20018.md)
 - [InlineResponse20019](docs/waslegacy__InlineResponse20019.md)
 - [InlineResponse2002](docs/waslegacy__InlineResponse2002.md)
 - [InlineResponse20020](docs/waslegacy__InlineResponse20020.md)
 - [InlineResponse20021](docs/waslegacy__InlineResponse20021.md)
 - [InlineResponse20022](docs/waslegacy__InlineResponse20022.md)
 - [InlineResponse20023](docs/waslegacy__InlineResponse20023.md)
 - [InlineResponse20023History](docs/waslegacy__InlineResponse20023History.md)
 - [InlineResponse20023Pagination](docs/waslegacy__InlineResponse20023Pagination.md)
 - [InlineResponse20023PaginationSort](docs/waslegacy__InlineResponse20023PaginationSort.md)
 - [InlineResponse20023Targets](docs/waslegacy__InlineResponse20023Targets.md)
 - [InlineResponse20024](docs/waslegacy__InlineResponse20024.md)
 - [InlineResponse20025](docs/waslegacy__InlineResponse20025.md)
 - [InlineResponse20025Vulnerabilities](docs/waslegacy__InlineResponse20025Vulnerabilities.md)
 - [InlineResponse20026](docs/waslegacy__InlineResponse20026.md)
 - [InlineResponse20026Output](docs/waslegacy__InlineResponse20026Output.md)
 - [InlineResponse20027](docs/waslegacy__InlineResponse20027.md)
 - [InlineResponse2003](docs/waslegacy__InlineResponse2003.md)
 - [InlineResponse2004](docs/waslegacy__InlineResponse2004.md)
 - [InlineResponse2005](docs/waslegacy__InlineResponse2005.md)
 - [InlineResponse2006](docs/waslegacy__InlineResponse2006.md)
 - [InlineResponse2007](docs/waslegacy__InlineResponse2007.md)
 - [InlineResponse2008](docs/waslegacy__InlineResponse2008.md)
 - [InlineResponse2009](docs/waslegacy__InlineResponse2009.md)
 - [ScansScanIdSettings](docs/waslegacy__ScansScanIdSettings.md)
 - [ScansSettings](docs/waslegacy__ScansSettings.md)


## Documentation For Authorization


## cloud

- **Type**: API key
- **API key parameter name**: X-ApiKeys
- **Location**: HTTP header


## Author




