# InlineResponse20017Vulnerabilities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | The number of vulnerabilities found. | [optional] 
**plugin_name** | **str** | The name of the vulnerability plugin. | [optional] 
**vuln_index** | **int** | The index of the vulnerability plugin. | [optional] 
**severity** | **int** | The severity rating of the plugin. | [optional] 
**plugin_id** | **int** | The unique ID of the vulnerability plugin. | [optional] 
**severity_index** | **int** | The severity index order of the plugin. | [optional] 
**plugin_family** | **str** | The parent family of the vulnerability plugin. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


