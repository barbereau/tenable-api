# InlineObject43

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read** | **bool** | If &#x60;true&#x60;, the scan has been read. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


