# InlineResponse20025

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | **object** |  | [optional] 
**vulnerabilities** | [**list[InlineResponse20025Vulnerabilities]**](waslegacy__InlineResponse20025Vulnerabilities.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


