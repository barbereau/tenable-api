# ScanStatus

The scan status. Values include:  - ready—Scan is ready to be started. pending—Start requested but not yet running.  - running—Scan is in progress.  - stopping—Stop requested but not yet stopped.  - canceled—Stopped by a user request.  - completed—Completed successfully.  - aborted—Terminated before completion for reason other than a user-requested stop.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


