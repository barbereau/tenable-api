# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20053(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'private': 'int',
        'no_target': 'str',
        'template_uuid': 'str',
        'description': 'str',
        'name': 'str',
        'owner': 'str',
        'shared': 'int',
        'user_permissions': 'int',
        'last_modification_date': 'int',
        'creation_date': 'int',
        'owner_id': 'int',
        'id': 'int'
    }

    attribute_map = {
        'private': 'private',
        'no_target': 'no_target',
        'template_uuid': 'template_uuid',
        'description': 'description',
        'name': 'name',
        'owner': 'owner',
        'shared': 'shared',
        'user_permissions': 'user_permissions',
        'last_modification_date': 'last_modification_date',
        'creation_date': 'creation_date',
        'owner_id': 'owner_id',
        'id': 'id'
    }

    def __init__(self, private=None, no_target=None, template_uuid=None, description=None, name=None, owner=None, shared=None, user_permissions=None, last_modification_date=None, creation_date=None, owner_id=None, id=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20053 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._private = None
        self._no_target = None
        self._template_uuid = None
        self._description = None
        self._name = None
        self._owner = None
        self._shared = None
        self._user_permissions = None
        self._last_modification_date = None
        self._creation_date = None
        self._owner_id = None
        self._id = None
        self.discriminator = None

        if private is not None:
            self.private = private
        if no_target is not None:
            self.no_target = no_target
        if template_uuid is not None:
            self.template_uuid = template_uuid
        if description is not None:
            self.description = description
        if name is not None:
            self.name = name
        if owner is not None:
            self.owner = owner
        if shared is not None:
            self.shared = shared
        if user_permissions is not None:
            self.user_permissions = user_permissions
        if last_modification_date is not None:
            self.last_modification_date = last_modification_date
        if creation_date is not None:
            self.creation_date = creation_date
        if owner_id is not None:
            self.owner_id = owner_id
        if id is not None:
            self.id = id

    @property
    def private(self):
        """Gets the private of this InlineResponse20053.  # noqa: E501


        :return: The private of this InlineResponse20053.  # noqa: E501
        :rtype: int
        """
        return self._private

    @private.setter
    def private(self, private):
        """Sets the private of this InlineResponse20053.


        :param private: The private of this InlineResponse20053.  # noqa: E501
        :type private: int
        """

        self._private = private

    @property
    def no_target(self):
        """Gets the no_target of this InlineResponse20053.  # noqa: E501


        :return: The no_target of this InlineResponse20053.  # noqa: E501
        :rtype: str
        """
        return self._no_target

    @no_target.setter
    def no_target(self, no_target):
        """Sets the no_target of this InlineResponse20053.


        :param no_target: The no_target of this InlineResponse20053.  # noqa: E501
        :type no_target: str
        """

        self._no_target = no_target

    @property
    def template_uuid(self):
        """Gets the template_uuid of this InlineResponse20053.  # noqa: E501


        :return: The template_uuid of this InlineResponse20053.  # noqa: E501
        :rtype: str
        """
        return self._template_uuid

    @template_uuid.setter
    def template_uuid(self, template_uuid):
        """Sets the template_uuid of this InlineResponse20053.


        :param template_uuid: The template_uuid of this InlineResponse20053.  # noqa: E501
        :type template_uuid: str
        """

        self._template_uuid = template_uuid

    @property
    def description(self):
        """Gets the description of this InlineResponse20053.  # noqa: E501


        :return: The description of this InlineResponse20053.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this InlineResponse20053.


        :param description: The description of this InlineResponse20053.  # noqa: E501
        :type description: str
        """

        self._description = description

    @property
    def name(self):
        """Gets the name of this InlineResponse20053.  # noqa: E501


        :return: The name of this InlineResponse20053.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse20053.


        :param name: The name of this InlineResponse20053.  # noqa: E501
        :type name: str
        """

        self._name = name

    @property
    def owner(self):
        """Gets the owner of this InlineResponse20053.  # noqa: E501


        :return: The owner of this InlineResponse20053.  # noqa: E501
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this InlineResponse20053.


        :param owner: The owner of this InlineResponse20053.  # noqa: E501
        :type owner: str
        """

        self._owner = owner

    @property
    def shared(self):
        """Gets the shared of this InlineResponse20053.  # noqa: E501


        :return: The shared of this InlineResponse20053.  # noqa: E501
        :rtype: int
        """
        return self._shared

    @shared.setter
    def shared(self, shared):
        """Sets the shared of this InlineResponse20053.


        :param shared: The shared of this InlineResponse20053.  # noqa: E501
        :type shared: int
        """

        self._shared = shared

    @property
    def user_permissions(self):
        """Gets the user_permissions of this InlineResponse20053.  # noqa: E501


        :return: The user_permissions of this InlineResponse20053.  # noqa: E501
        :rtype: int
        """
        return self._user_permissions

    @user_permissions.setter
    def user_permissions(self, user_permissions):
        """Sets the user_permissions of this InlineResponse20053.


        :param user_permissions: The user_permissions of this InlineResponse20053.  # noqa: E501
        :type user_permissions: int
        """

        self._user_permissions = user_permissions

    @property
    def last_modification_date(self):
        """Gets the last_modification_date of this InlineResponse20053.  # noqa: E501


        :return: The last_modification_date of this InlineResponse20053.  # noqa: E501
        :rtype: int
        """
        return self._last_modification_date

    @last_modification_date.setter
    def last_modification_date(self, last_modification_date):
        """Sets the last_modification_date of this InlineResponse20053.


        :param last_modification_date: The last_modification_date of this InlineResponse20053.  # noqa: E501
        :type last_modification_date: int
        """

        self._last_modification_date = last_modification_date

    @property
    def creation_date(self):
        """Gets the creation_date of this InlineResponse20053.  # noqa: E501


        :return: The creation_date of this InlineResponse20053.  # noqa: E501
        :rtype: int
        """
        return self._creation_date

    @creation_date.setter
    def creation_date(self, creation_date):
        """Sets the creation_date of this InlineResponse20053.


        :param creation_date: The creation_date of this InlineResponse20053.  # noqa: E501
        :type creation_date: int
        """

        self._creation_date = creation_date

    @property
    def owner_id(self):
        """Gets the owner_id of this InlineResponse20053.  # noqa: E501


        :return: The owner_id of this InlineResponse20053.  # noqa: E501
        :rtype: int
        """
        return self._owner_id

    @owner_id.setter
    def owner_id(self, owner_id):
        """Sets the owner_id of this InlineResponse20053.


        :param owner_id: The owner_id of this InlineResponse20053.  # noqa: E501
        :type owner_id: int
        """

        self._owner_id = owner_id

    @property
    def id(self):
        """Gets the id of this InlineResponse20053.  # noqa: E501


        :return: The id of this InlineResponse20053.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20053.


        :param id: The id of this InlineResponse20053.  # noqa: E501
        :type id: int
        """

        self._id = id

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20053):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20053):
            return True

        return self.to_dict() != other.to_dict()
