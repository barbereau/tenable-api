# InlineResponse20011Agents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The unique ID of the agent. | [optional] 
**uuid** | **str** | The UUID of the agent. Note: This value corresponds to the ID of the asset where the agent is installed. You can use this attribute to match agent data to asset data. | [optional] 
**name** | **str** | The name of the agent. | [optional] 
**platform** | **str** | The platform of the agent. | [optional] 
**distro** | **str** | The agent software distribution. | [optional] 
**ip** | **str** | The IP address of the agent. | [optional] 
**last_scanned** | **int** | The Unix timestamp when the agent last scanned the asset. | [optional] 
**plugin_feed_id** | **str** | The currently loaded plugin set of the agent (null if the agent has no plugin set loaded). | [optional] 
**core_build** | **str** | Build number for the agent. | [optional] 
**core_version** | **str** | Build version for the agent. | [optional] 
**linked_on** | **int** | The Unix timestamp when the link from Tenable.io to the agent was established. | [optional] 
**last_connect** | **int** | The Unix timestamp when the agent last communicated with Tenable.io. | [optional] 
**status** | **str** | \&quot;on\&quot;, \&quot;off\&quot;, or \&quot;init\&quot;.  \&quot;on\&quot; means that the agent has connected recently, and is therefore likely ready to scan.  \&quot;off\&quot; means that the agent has not been seen recently and should be considered offline.  \&quot;init\&quot; means that the agent is online, but it is still processing plugin updates and is not ready to scan. | [optional] 
**groups** | [**list[InlineResponse20011Groups]**](vm__InlineResponse20011Groups.md) | Array of groups to which the agent belongs. Groups are returned in the form {\&quot;name\&quot;: \&quot;group name\&quot;, \&quot;id\&quot;: \&quot;group id\&quot;}. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


