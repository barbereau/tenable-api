# InlineResponse20047DataCvssVector

The raw CVSSv2 metrics for the vulnerability. For more information, see CVSSv2 documentation.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_complexity** | **str** | The CVSSv2 Access Complexity (AC) metric for the vulnerability the plugin covers. Possible values include:&lt;br /&gt;• H—High&lt;br /&gt;• M—Medium&lt;br /&gt;• L—Low | [optional] 
**access_vector** | **str** | The CVSSv2 Access Vector (AV) metric for the vulnerability the plugin covers. Possible values include: &lt;br /&gt;• L—Local&lt;br /&gt;• A—Adjacent Network&lt;br /&gt;• N—Network | [optional] 
**authentication** | **str** | The CVSSv2 Authentication (Au) metric for the vulnerability the plugin covers. Possible values include: &lt;br /&gt;• N—None&lt;br /&gt;• S—Single&lt;br /&gt;• M—Multiple | [optional] 
**availability_impact** | **str** | The CVSSv2 availability impact metric for the vulnerability the plugin covers. Possible values include: &lt;br /&gt;• N—None&lt;br /&gt;• P—Partial&lt;br /&gt;• C—Complete | [optional] 
**confidentiality_impact** | **str** | The CVSSv2 confidentiality impact metric for the vulnerability the plugin covers. Possible values include: &lt;br /&gt;• N—None&lt;br /&gt;• P—Partial&lt;br /&gt;• C—Complete | [optional] 
**integrity_impact** | **str** | The CVSSv2 integrity impact metric for the vulnerability the plugin covers. Possible values include: &lt;br /&gt;• N—None&lt;br /&gt;• P—Partial&lt;br /&gt;• C—Complete | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


