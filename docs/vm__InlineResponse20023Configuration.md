# InlineResponse20023Configuration

The configuration settings for the credential. The parameters of this object vary depending on the credential type. For a list of possible configuration parameters, use the GET /credentials/types endpoint.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The parameter input type. This attribute reflects how the user interface prompts for parameter input. Possible values include:   - password—Prompts for input via text box.  - text—Prompts for input via text box.  - select—Prompts for input via selectable options.  - file—Prompts user to upload file of input data.  - toggle—Prompts user to toggle an option on or off.  - checkbox—Prompts user to select options via checkboxes. Checkboxes can represent enabling a single option or can allow users to select from multiple, mutually-exclusive options. | [optional] 
**name** | **str** | The display name for the credential configuration in the user interface. | [optional] 
**required** | **bool** | A value specifying whether the configuration parameter is required (&#x60;true&#x60;) or optional (&#x60;false&#x60;). If this attribute is absent, the parameter is optional. | [optional] 
**id** | **str** | The system name for the credential parameter. Use this value as the parameter name in request messages configuring credentials. | [optional] 
**placeholder** | **str** | An example of the parameter value. This value appears as example text in the user interface.    This attribute is only present for credential parameters that require text input in the user interface. | [optional] 
**options** | [**list[InlineResponse20023Options]**](vm__InlineResponse20023Options.md) | The supported options for the credential parameter. | [optional] 
**default** | **str** | The option that appears as selected by default in the user interface. | [optional] 
**alt_ids** | **str** | Not supported as a parameter in managed credentials. | [optional] 
**preferences** | **list[str]** | Not supported as a parameter in managed credentials. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


