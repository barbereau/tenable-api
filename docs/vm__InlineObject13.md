# InlineObject13

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**query** | [**ApiV2AssetsBulkJobsDeleteQuery**](vm__ApiV2AssetsBulkJobsDeleteQuery.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


