# InlineResponse20017Filters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The short name of the filter. | [optional] 
**readable_name** | **str** | The long name of the filter. | [optional] 
**operators** | **list[str]** | The comparison options for the filter. | [optional] 
**control** | [**InlineResponse20017Control**](waslegacy__InlineResponse20017Control.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


