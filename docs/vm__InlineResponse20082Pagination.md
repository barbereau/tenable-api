# InlineResponse20082Pagination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** | The starting record to retrieve. Because this endpoint does not support an &#x60;offset&#x60; query parameter, this value is always &#x60;0&#x60;. | 
**total** | **int** | The total number of records matching your search criteria. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


