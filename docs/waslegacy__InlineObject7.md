# InlineObject7

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**link** | **int** | Pass 1 enable the link. Pass 0 to disable. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


