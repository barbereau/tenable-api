# InlineObject37

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**link** | **int** | Pass &#x60;1&#x60; enable the link. Pass &#x60;0&#x60; to disable. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


