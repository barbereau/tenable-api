# ScansCredentialsAddHostWindows

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain** | **str** | The Windows domain to which the username belongs. | [optional] 
**username** | **str** | The username on the target system. | [optional] 
**auth_method** | **str** | The name for the authentication method. This value corresponds to the credentials[].types[].configuration[].options[].id attribute in the response message for the [GET /credentials/types](/reference#credentials-list-credential-types) endpoint. | [optional] 
**password** | **str** | The user password on the target system. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


