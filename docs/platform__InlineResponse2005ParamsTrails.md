# InlineResponse2005ParamsTrails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**arn** | **str** | Amazon Resource Name (ARN) of the cloudtrail. | [optional] 
**name** | **str** | The name of the cloudtrail. | [optional] 
**region** | [**InlineResponse2005ParamsRegion**](platform__InlineResponse2005ParamsRegion.md) |  | [optional] 
**availability** | **str** | Indicates whether a cloudtrail is available to be used by a connector (logging is turned on in AWS, or it has at least one EventSelector with IncludeManagementEvents). Values include:  - &#x60;success&#x60;—The cloudtrail is available.  - &#x60;error&#x60;—The cloudtrail is not available. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


