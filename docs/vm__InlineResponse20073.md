# InlineResponse20073

A history object containing data about a run of the specified scan.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owner_id** | **int** | The unique ID of the owner of the scan. | [optional] 
**schedule_uuid** | **str** | The UUID for a specific instance in the scan schedule. | [optional] 
**status** | **str** | The terminal status of the scan run. For possible values, see [Scan Status](doc:scan-status-tio). | [optional] 
**is_archived** | **bool** | Indicates whether the scan results are older than 60 days (&#x60;true&#x60;). If this parameter is &#x60;true&#x60;, Tenable.io returns limited data for the scan run. For complete scan results that are older than 60 days, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead. | [optional] 
**scan_start** | **int** | The Unix timestamp when the scan run started. | [optional] 
**owner_uuid** | **int** | The UUID of the owner of the scan when the scan run occurred. | [optional] 
**owner** | **str** | The username of the owner of the scan when the scan run occurred. | [optional] 
**targets** | **str** | The hosts that the scan targeted. | [optional] 
**object_id** | **int** |  | [optional] 
**uuid** | **str** | The UUID of the historical data. | [optional] 
**scan_end** | **int** | The Unix timestamp when the scan run finished. | [optional] 
**scan_type** | **str** | The type of scan: &#x60;ps&#x60; (a scan performed over the network by a cloud scanner), &#x60;remote&#x60; (a  scan performed over the network by a local scanner), &#x60;agent&#x60; (a scan on a local host that a Nessus agent performs directly), or &#x60;null&#x60; (the scan has never been launched, or the scan is imported). | [optional] 
**name** | **str** | The name of the scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


