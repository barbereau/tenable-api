# InlineObject12

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**region** | [**list[InlineResponse2005ParamsRegion]**](platform__InlineResponse2005ParamsRegion.md) | A complete list of available AWS regions as shown in the following example. | [optional] 
**credentials** | [**SettingsConnectorsAwsCloudtrailsCredentials**](platform__SettingsConnectorsAwsCloudtrailsCredentials.md) |  | [optional] 
**account_id** | **str** | For keyless AWS connectors, the AWS account ID. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


