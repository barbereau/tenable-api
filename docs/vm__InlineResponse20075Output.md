# InlineResponse20075Output

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ports** | **object** |  | [optional] 
**has_attachment** | **int** | If the value is 1, the plugin output contains files that may be exported. | [optional] 
**custom_description** | **str** | A custom description of the plugin. | [optional] 
**plugin_output** | **str** | The text of the plugin output. | [optional] 
**hosts** | **str** | Other hosts with the same output. | [optional] 
**severity** | **int** | The severity the output. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


