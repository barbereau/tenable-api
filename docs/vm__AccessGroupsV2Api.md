# tenableapi.vm.AccessGroupsV2Api

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**io_v2_access_groups_create**](vm__AccessGroupsV2Api.md#io_v2_access_groups_create) | **POST** /v2/access-groups | Create access group
[**io_v2_access_groups_delete**](vm__AccessGroupsV2Api.md#io_v2_access_groups_delete) | **DELETE** /v2/access-groups/{id} | Delete access group
[**io_v2_access_groups_details**](vm__AccessGroupsV2Api.md#io_v2_access_groups_details) | **GET** /v2/access-groups/{id} | Get access group details
[**io_v2_access_groups_edit**](vm__AccessGroupsV2Api.md#io_v2_access_groups_edit) | **PUT** /v2/access-groups/{id} | Update access group
[**io_v2_access_groups_list**](vm__AccessGroupsV2Api.md#io_v2_access_groups_list) | **GET** /v2/access-groups | List access groups
[**io_v2_access_groups_list_filters**](vm__AccessGroupsV2Api.md#io_v2_access_groups_list_filters) | **GET** /v2/access-groups/filters | List access group filters
[**io_v2_access_groups_list_rule_filters**](vm__AccessGroupsV2Api.md#io_v2_access_groups_list_rule_filters) | **GET** /v2/access-groups/rules/filters | List asset rule filters


# **io_v2_access_groups_create**
> InlineResponse2004AccessGroups io_v2_access_groups_create(inline_object2)

Create access group

Creates an access group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AccessGroupsV2Api(api_client)
    inline_object2 = tenableapi.vm.InlineObject2() # InlineObject2 | 

    try:
        # Create access group
        api_response = api_instance.io_v2_access_groups_create(inline_object2)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessGroupsV2Api->io_v2_access_groups_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object2** | [**InlineObject2**](vm__InlineObject2.md)|  | 

### Return type

[**InlineResponse2004AccessGroups**](vm__InlineResponse2004AccessGroups.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates an access group. |  -  |
**400** | Returned if Tenable.io encountered any of the following error conditions:  - max_entries—your request exceeds the maximum number of 5,000 access groups.  - duplicate—an access group with the name you specified already exists.  - protected—you attempted to set the all_assets parameter  to &#x60;true&#x60;, and you cannot create the system-provided access group, All Assets. |  -  |
**403** | Returned if you do not have permission to create access groups. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_v2_access_groups_delete**
> io_v2_access_groups_delete(id)

Delete access group

Deletes an access group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AccessGroupsV2Api(api_client)
    id = 'id_example' # str | The UUID for the access group you want to delete.

    try:
        # Delete access group
        api_instance.io_v2_access_groups_delete(id)
    except ApiException as e:
        print("Exception when calling AccessGroupsV2Api->io_v2_access_groups_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The UUID for the access group you want to delete. | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deleted the access group you specified. |  -  |
**403** | Returned if you do not have sufficient permissions to delete an access group. |  -  |
**404** | Returned if Tenable.io could not find the access group you specified. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_v2_access_groups_details**
> InlineResponse2005 io_v2_access_groups_details(id)

Get access group details

Returns details for a specific access group.<p>Requires BASIC [16] user permissions to view details for an access group to which you are assigned; however, details do not include principals information. Requires ADMINISTRATOR [64] to view details for any access group in your organization; in this case, details include principals information. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AccessGroupsV2Api(api_client)
    id = 'id_example' # str | The UUID of the access group where you want to view details.

    try:
        # Get access group details
        api_response = api_instance.io_v2_access_groups_details(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessGroupsV2Api->io_v2_access_groups_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The UUID of the access group where you want to view details. | 

### Return type

[**InlineResponse2005**](vm__InlineResponse2005.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the access group details. |  -  |
**403** | Returned if you do not have permissions to view the access group details. |  -  |
**404** | Returned if Tenable.io could not find the access group you specified. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_v2_access_groups_edit**
> InlineResponse2005 io_v2_access_groups_edit(id, inline_object3)

Update access group

Modifies an access group. This method overwrites the existing data.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AccessGroupsV2Api(api_client)
    id = 'id_example' # str | The UUID for the access group you want to modify.
inline_object3 = tenableapi.vm.InlineObject3() # InlineObject3 | 

    try:
        # Update access group
        api_response = api_instance.io_v2_access_groups_edit(id, inline_object3)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessGroupsV2Api->io_v2_access_groups_edit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The UUID for the access group you want to modify. | 
 **inline_object3** | [**InlineObject3**](vm__InlineObject3.md)|  | 

### Return type

[**InlineResponse2005**](vm__InlineResponse2005.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io has either modified the existing access group successfully or created a new access group because it could not find an existing access group with the specified UUID. |  -  |
**400** | Returned if Tenable.io encountered any of the following error conditions:  - incomplete—the body of your request did not include the required fields.  - duplicate—an access group with the name you specified already exists.  - protected—you attempted to update an access group where the &#x60;all_assets&#x60; parameter is set to &#x60;true&#x60;, and you cannot update the system-provided &#x60;All Assets&#x60; access group. |  -  |
**403** | Returned if you do not have sufficient permissions to modify access groups. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_v2_access_groups_list**
> InlineResponse2004 io_v2_access_groups_list(f=f, ft=ft, w=w, wf=wf, limit=limit, offset=offset, sort=sort)

List access groups

Lists access groups without associated rules.<p>Requires BASIC [16] user permissions to list access groups to which you are assigned. Requires ADMINISTRATOR [64] permissions to list all access groups for your organization. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AccessGroupsV2Api(api_client)
    f = 'f_example' # str | A filter condition in the following format: `field:operator:value`. For a list of possible fields and operators, use the [GET /v2/access-groups/filters](ref:io-v2-access-groups-list-filters) endpoint. You can specify multiple `f` parameters, separated by ampersand (&) characters. If you specify multiple `f` parameters, use the `ft` parameter to specify how Tenable.io applies the multiple filter conditions. (optional)
ft = 'ft_example' # str | If multiple \\`f\\` parameters are present, specifies whether Tenable.io applies \\`AND\\` or \\`OR\\` to conditions. Supported values are `and` and `or`. If you omit this parameter when using multiple `f` parameters, Tenable.io applies `AND` by default. (optional)
w = 'w_example' # str | The search value that Tenable.io applies across the wildcard fields. Wildcard fields are specified in the `wf` parameter. (optional)
wf = 'wf_example' # str | A comma-separated list of fields where Tenable.io applies the search value specified in the `w` parameter. For a list of supported wildcard fields, use the [GET /v2/access-groups/filters](ref:io-v2-access-groups-list-filters) endpoint. (optional)
limit = 56 # int | Maximum number of records requested (or service imposed limit if not in request). (optional)
offset = 56 # int | Offset from request (or zero). (optional)
sort = 'sort_example' # str | The field or fields on which Tenable.io sorts the results (descending or ascending) using the following format: `field:desc`. If you specify multiple fields, fields must be separated by commas. For a list of supported sort fields, use the [GET /v2/access-groups/filters](ref:io-v2-access-groups-list-filters) endpoint. (optional)

    try:
        # List access groups
        api_response = api_instance.io_v2_access_groups_list(f=f, ft=ft, w=w, wf=wf, limit=limit, offset=offset, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessGroupsV2Api->io_v2_access_groups_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **f** | **str**| A filter condition in the following format: &#x60;field:operator:value&#x60;. For a list of possible fields and operators, use the [GET /v2/access-groups/filters](ref:io-v2-access-groups-list-filters) endpoint. You can specify multiple &#x60;f&#x60; parameters, separated by ampersand (&amp;) characters. If you specify multiple &#x60;f&#x60; parameters, use the &#x60;ft&#x60; parameter to specify how Tenable.io applies the multiple filter conditions. | [optional] 
 **ft** | **str**| If multiple \\&#x60;f\\&#x60; parameters are present, specifies whether Tenable.io applies \\&#x60;AND\\&#x60; or \\&#x60;OR\\&#x60; to conditions. Supported values are &#x60;and&#x60; and &#x60;or&#x60;. If you omit this parameter when using multiple &#x60;f&#x60; parameters, Tenable.io applies &#x60;AND&#x60; by default. | [optional] 
 **w** | **str**| The search value that Tenable.io applies across the wildcard fields. Wildcard fields are specified in the &#x60;wf&#x60; parameter. | [optional] 
 **wf** | **str**| A comma-separated list of fields where Tenable.io applies the search value specified in the &#x60;w&#x60; parameter. For a list of supported wildcard fields, use the [GET /v2/access-groups/filters](ref:io-v2-access-groups-list-filters) endpoint. | [optional] 
 **limit** | **int**| Maximum number of records requested (or service imposed limit if not in request). | [optional] 
 **offset** | **int**| Offset from request (or zero). | [optional] 
 **sort** | **str**| The field or fields on which Tenable.io sorts the results (descending or ascending) using the following format: &#x60;field:desc&#x60;. If you specify multiple fields, fields must be separated by commas. For a list of supported sort fields, use the [GET /v2/access-groups/filters](ref:io-v2-access-groups-list-filters) endpoint. | [optional] 

### Return type

[**InlineResponse2004**](vm__InlineResponse2004.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of access groups that you have permission to view. |  -  |
**403** | Returned if you do not have permission to view access groups. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_v2_access_groups_list_filters**
> InlineResponse2006 io_v2_access_groups_list_filters()

List access group filters

Lists available filters for access groups.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AccessGroupsV2Api(api_client)
    
    try:
        # List access group filters
        api_response = api_instance.io_v2_access_groups_list_filters()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessGroupsV2Api->io_v2_access_groups_list_filters: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2006**](vm__InlineResponse2006.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of available filters for access groups. The response includes supported filter operators for each filter. Filter operators can include:  - eq—filters on exact matches of the specified value  - match—filters on partial matches of the specified value  - date-lt —filters on dates earlier than the specified date  - date-gt—filters on dates later than the specified date  - date-eq—filters on dates equal to the specified date.  The sample below does not represent a complete list of supported filters. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_v2_access_groups_list_rule_filters**
> InlineResponse2007 io_v2_access_groups_list_rule_filters()

List asset rule filters

Lists available filters for asset rules.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AccessGroupsV2Api(api_client)
    
    try:
        # List asset rule filters
        api_response = api_instance.io_v2_access_groups_list_rule_filters()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccessGroupsV2Api->io_v2_access_groups_list_rule_filters: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2007**](vm__InlineResponse2007.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of filters. The sample below does not represent a complete list of supported filters. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

