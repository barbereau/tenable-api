# InlineResponse2005

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connectors** | [**list[InlineResponse2005Connectors]**](platform__InlineResponse2005Connectors.md) |  | [optional] 
**pagination** | [**InlineResponse2005Pagination**](platform__InlineResponse2005Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


