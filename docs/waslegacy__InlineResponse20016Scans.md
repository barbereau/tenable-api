# InlineResponse20016Scans

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The unique ID of the scan. | [optional] 
**uuid** | **str** | The UUID for the scan. | [optional] 
**name** | **str** | The name of the scan. | [optional] 
**type** | **str** | The type of scan (local, remote, webapp, or agent). WAS scans will always have the type set to webapp. | [optional] 
**owner** | **str** | The owner of the scan. | [optional] 
**enabled** | **bool** | If &#x60;true&#x60;, the schedule for the scan is enabled. | [optional] 
**read** | **bool** | If &#x60;true&#x60;, the scan has been read. | [optional] 
**status** | **str** | The status of the scan (completed, aborted, imported, pending, running, resuming, canceling, canceled, pausing, paused, stopping, stopped). | [optional] 
**shared** | **bool** | If &#x60;true&#x60;, the scan is shared. | [optional] 
**user_permissions** | **int** | The sharing permissions for the scan. | [optional] 
**creation_date** | **int** | The creation date for the scan in Unix time. | [optional] 
**last_modification_date** | **int** | The last modification date for the scan in Unix time. | [optional] 
**control** | **bool** | If &#x60;true&#x60;, the scan has a schedule and can be launched. | [optional] 
**starttime** | **str** | The scheduled start time for the scan. | [optional] 
**timezone** | **str** | The timezone for the scan. | [optional] 
**rrules** | **str** | The rules for repeating the scan. | [optional] 
**schedule_uuid** | **str** | The schedule_uuid of the scan that should be returned. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


