# HttpSettings

The HTTP request settings for the scan.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_max_size** | **int** | The maximum load size for an audited page. | [optional] [default to 500000]
**request_redirect_limit** | **int** | The number of redirects the scan follows before it stops trying to crawl the page. | [optional] [default to 1]
**user_agent** | **str** | The user-agent header the scanner includes in HTTP requests to the web application. | [optional] [default to 'Nessus WAS/%v']
**custom_user_agent** | **bool** | Indicates whether the scanner includes the &#x60;user-agent&#x60; header in HTTP requests to the web application (&#x60;true&#x60;). | [optional] 
**request_headers** | **object** | The custom headers the scanner includes in HTTP requests to the web application.  **Note:** If you specify a user-agent header value in this parameter, it overrides the &#x60;user_agent&#x60; parameter value. | [optional] 
**request_concurrency** | **int** | The maximum number of concurrent HTTP connections to the web application. | [optional] [default to 10]
**request_timeout** | **int** | HTTP request timeout (in milliseconds). | [optional] [default to 5000]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


