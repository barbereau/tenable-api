# tenableapi.vm.PluginsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**io_plugins_details**](vm__PluginsApi.md#io_plugins_details) | **GET** /plugins/plugin/{id} | Get plugin details
[**io_plugins_families_list**](vm__PluginsApi.md#io_plugins_families_list) | **GET** /plugins/families | List plugin families
[**io_plugins_family_details**](vm__PluginsApi.md#io_plugins_family_details) | **GET** /plugins/families/{id} | List plugins in family
[**io_plugins_list**](vm__PluginsApi.md#io_plugins_list) | **GET** /plugins/plugin | List plugins


# **io_plugins_details**
> InlineResponse20048 io_plugins_details(id)

Get plugin details

Returns details for a specified plugin.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.PluginsApi(api_client)
    id = 56 # int | The ID of the plugin.

    try:
        # Get plugin details
        api_response = api_instance.io_plugins_details(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->io_plugins_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the plugin. | 

### Return type

[**InlineResponse20048**](vm__InlineResponse20048.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the plugin details. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_plugins_families_list**
> InlineResponse20049 io_plugins_families_list(all=all)

List plugin families

Returns the list of plugin families.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.PluginsApi(api_client)
    all = True # bool | Specifies whether to return all plugin families. If `true`, the plugin families hidden in Tenable.io UI, for example, Port Scanners, are included in the list. (optional)

    try:
        # List plugin families
        api_response = api_instance.io_plugins_families_list(all=all)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->io_plugins_families_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **bool**| Specifies whether to return all plugin families. If &#x60;true&#x60;, the plugin families hidden in Tenable.io UI, for example, Port Scanners, are included in the list. | [optional] 

### Return type

[**InlineResponse20049**](vm__InlineResponse20049.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the list of plugin families. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_plugins_family_details**
> InlineResponse20050 io_plugins_family_details(id)

List plugins in family

Returns the list of plugins in the specified family.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.PluginsApi(api_client)
    id = 56 # int | The ID of the family to lookup.

    try:
        # List plugins in family
        api_response = api_instance.io_plugins_family_details(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->io_plugins_family_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the family to lookup. | 

### Return type

[**InlineResponse20050**](vm__InlineResponse20050.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the list of plugins in a family. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_plugins_list**
> InlineResponse20047 io_plugins_list(last_updated=last_updated, size=size, page=page)

List plugins

Returns a paginated list of Tenable plugins with detailed plugin information. You can filter the list on the value of the `last_updated` date field. The list is sorted by plugin ID.<p>Requires BASIC [16] user permissions. See <a href=\"/docs/permissions\">Permissions</a>.</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.PluginsApi(api_client)
    last_updated = 'last_updated_example' # str | The last updated date to filter on in the `YYYY-MM-DD` format. Tenable.io returns only the plugins that have been updated after the specified date. (optional)
size = 56 # int | The number of records to include in the result set. Default is 1,000. The maximum size is 10,000. (optional)
page = 56 # int | The index of the page to return relative to the specified page size. For example, to return records 10-19 with page size 10, you must specify page 2. If you omit this parameter, Tenable.io applies the default value of 1. (optional)

    try:
        # List plugins
        api_response = api_instance.io_plugins_list(last_updated=last_updated, size=size, page=page)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->io_plugins_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **last_updated** | **str**| The last updated date to filter on in the &#x60;YYYY-MM-DD&#x60; format. Tenable.io returns only the plugins that have been updated after the specified date. | [optional] 
 **size** | **int**| The number of records to include in the result set. Default is 1,000. The maximum size is 10,000. | [optional] 
 **page** | **int**| The index of the page to return relative to the specified page size. For example, to return records 10-19 with page size 10, you must specify page 2. If you omit this parameter, Tenable.io applies the default value of 1. | [optional] 

### Return type

[**InlineResponse20047**](vm__InlineResponse20047.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a paginated list of available plugins. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

