# ReportUserScanTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**template_id** | **str** | The UUID of the Tenable-provided template on which the user-defined template is based. | [optional] 
**name** | **str** | The name of the user-defined template. | [optional] 
**description** | **str** | The description for the user-defined template. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


