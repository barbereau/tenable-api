# InlineObject52

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | **list[str]** | The UUIDs of the tag values you want to delete.  **Note:** A tag UUID is technically assigned to the tag value only (the second half of the category:value pair), but the API commands use this value to represent the whole &#x60;category:value&#x60; pair. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


