# InlineObject34

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**routes** | **list[str]** | A list of zero or more hostnames, hostname wildcards, IP addresses, CIDR addresses, or IP ranges. For more information about supported route formats, see [Supported Scan Routing Target Formats](doc:manage-scan-routing-tio#section-supported-scan-routing-target-formats). | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


