# SigningKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **str** | The name of the file. | [optional] 
**size** | **int** | The size of the file in bytes. | [optional] 
**md5** | **str** | The MD5 hash of the file. | [optional] 
**sha256** | **str** | The SHA256 hash of the file. | [optional] 
**file_url** | **str** | The URL to download the file. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


