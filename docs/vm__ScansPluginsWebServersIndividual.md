# ScansPluginsWebServersIndividual

A list of individual plugins within the specified plugin family.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_11213** | **str** | The status (&#x60;enabled&#x60; or &#x60;disabled&#x60;) of the individual plugin within the plugin family to add to the scan. | [optional] 
**_18261** | **str** | The status (&#x60;enabled&#x60; or &#x60;disabled&#x60;) of the individual plugin within the plugin family to add to the scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


