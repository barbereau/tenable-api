# InlineResponse2001

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_uuid** | **str** | The UUID of the container. | [optional] 
**user_uuid** | **str** | The UUID of the user. | [optional] 
**api_permitted** | **bool** | Indicates whether API access is authorized for the user. | [optional] 
**password_permitted** | **bool** | Indicates whether user name and password login is authorized for the user. | [optional] 
**saml_permitted** | **bool** | Indicates whether SSO with SAML is authorized for the user. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


