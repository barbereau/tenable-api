# InlineResponse20095

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The UUID of the asset. | [optional] 
**severities** | [**list[WorkbenchesAssetsVulnerabilitiesSeverities]**](vm__WorkbenchesAssetsVulnerabilitiesSeverities.md) | A count of vulnerabilities by severity. | [optional] 
**total** | **int** | The total number of vulnerabilities detected on the asset. | [optional] 
**fqdn** | **list[str]** | A list of fully-qualified domain names (FQDNs) for the asset. | [optional] 
**ipv4** | **list[str]** | A list of ipv4 addresses for the asset. | [optional] 
**ipv6** | **list[str]** | A list of ipv6 addresses for the asset. | [optional] 
**last_seen** | **str** | The ISO timestamp of the scan that most recently detected the asset. | [optional] 
**netbios_name** | **list[str]** | The NetBIOS name for the asset. | [optional] 
**agent_name** | **list[str]** | The names of any Nessus agents that scanned and identified the asset. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


