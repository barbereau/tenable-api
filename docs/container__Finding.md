# Finding

The details for the discovered vulnerability and a list of associated software packages.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nvd_finding** | [**NvdFinding**](container__Nvdcontainer__Finding.md) |  | [optional] 
**packages** | **list[object]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


