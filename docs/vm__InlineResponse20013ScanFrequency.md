# InlineResponse20013ScanFrequency

Information about how often scans ran against asset during a specified interval.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interval** | **int** | The number of days over which Tenable searches for scans involving the asset. | [optional] 
**frequency** | **int** | The number of times that a scan ran against the asset during the specified interval. | [optional] 
**licensed** | **bool** | Indicates whether the asset was licensed at the time of the identified scans. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


