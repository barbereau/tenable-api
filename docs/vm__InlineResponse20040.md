# InlineResponse20040

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | [**list[InlineResponse20039Filters]**](vm__InlineResponse20039Filters.md) | A list of filters available for the record type. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


