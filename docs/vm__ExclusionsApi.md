# tenableapi.vm.ExclusionsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**exclusions_create**](vm__ExclusionsApi.md#exclusions_create) | **POST** /exclusions | Create exclusion
[**exclusions_delete**](vm__ExclusionsApi.md#exclusions_delete) | **DELETE** /exclusions/{exclusion_id} | Delete an exclusion
[**exclusions_details**](vm__ExclusionsApi.md#exclusions_details) | **GET** /exclusions/{exclusion_id} | Get exclusion details
[**exclusions_edit**](vm__ExclusionsApi.md#exclusions_edit) | **PUT** /exclusions/{exclusion_id} | Update an exclusion
[**exclusions_import**](vm__ExclusionsApi.md#exclusions_import) | **POST** /exclusions/import | Import exclusion
[**exclusions_list**](vm__ExclusionsApi.md#exclusions_list) | **GET** /exclusions | List exclusions


# **exclusions_create**
> InlineResponse20029 exclusions_create(inline_object18)

Create exclusion

Creates a new exclusion.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExclusionsApi(api_client)
    inline_object18 = tenableapi.vm.InlineObject18() # InlineObject18 | 

    try:
        # Create exclusion
        api_response = api_instance.exclusions_create(inline_object18)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExclusionsApi->exclusions_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object18** | [**InlineObject18**](vm__InlineObject18.md)|  | 

### Return type

[**InlineResponse20029**](vm__InlineResponse20029.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates the exclusion. |  -  |
**400** | Returned if your request message contains invalid parameters. |  -  |
**403** | Returned if you do not have permission to create an exclusion. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to create the exclusion. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exclusions_delete**
> object exclusions_delete(exclusion_id)

Delete an exclusion

Deletes an exclusion.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExclusionsApi(api_client)
    exclusion_id = 56 # int | The ID of the exclusion to delete.

    try:
        # Delete an exclusion
        api_response = api_instance.exclusions_delete(exclusion_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExclusionsApi->exclusions_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exclusion_id** | **int**| The ID of the exclusion to delete. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deletes the exclusion. |  -  |
**403** | Returned if you do not have permission to delete the exclusion. |  -  |
**404** | Returned if Tenable.io cannot find the specified exclusion. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exclusions_details**
> InlineResponse20029 exclusions_details(exclusion_id)

Get exclusion details

Returns exclusion details.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExclusionsApi(api_client)
    exclusion_id = 56 # int | The ID of the exclusion.

    try:
        # Get exclusion details
        api_response = api_instance.exclusions_details(exclusion_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExclusionsApi->exclusions_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exclusion_id** | **int**| The ID of the exclusion. | 

### Return type

[**InlineResponse20029**](vm__InlineResponse20029.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the exclusion details. |  -  |
**403** | Returned if you do not have permission to view the exclusion. |  -  |
**404** | Returned if Tenable.io cannot find the specified exclusion. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exclusions_edit**
> object exclusions_edit(exclusion_id, inline_object20)

Update an exclusion

Updates an exclusion.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExclusionsApi(api_client)
    exclusion_id = 56 # int | The ID of the exclusion to update.
inline_object20 = tenableapi.vm.InlineObject20() # InlineObject20 | 

    try:
        # Update an exclusion
        api_response = api_instance.exclusions_edit(exclusion_id, inline_object20)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExclusionsApi->exclusions_edit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exclusion_id** | **int**| The ID of the exclusion to update. | 
 **inline_object20** | [**InlineObject20**](vm__InlineObject20.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully modifies the exclusion. |  -  |
**403** | Returned if you do not have permission to modify the exclusion. |  -  |
**404** | Returned if Tenable.io cannot find the specified exclusion. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to change the exclusion. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exclusions_import**
> object exclusions_import(inline_object19)

Import exclusion

Import exclusions from an [exclusion import file](doc:import-file-formats) that you have previously uploaded via the [POST /file/upload](ref:file-upload) endpoint.  **Note:** This endpoint does not support the network_id attribute in exclusion objects for import. Tenable.io automatically assigns imported exclusions to the default network object. To assign imported exclusions to a custom network, use the [PUT /exclusions/exclusion_id](ref:exclusions-edit) endpoint after import. For more information about network objects, see [Manage Networks](doc:manage-networks-tio).<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExclusionsApi(api_client)
    inline_object19 = tenableapi.vm.InlineObject19() # InlineObject19 | 

    try:
        # Import exclusion
        api_response = api_instance.exclusions_import(inline_object19)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExclusionsApi->exclusions_import: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object19** | [**InlineObject19**](vm__InlineObject19.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully imports the exclusion file. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to import the exclusion. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **exclusions_list**
> list[InlineResponse20029] exclusions_list()

List exclusions

Lists exclusions for your Tenable.io scans.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ExclusionsApi(api_client)
    
    try:
        # List exclusions
        api_response = api_instance.exclusions_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ExclusionsApi->exclusions_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse20029]**](vm__InlineResponse20029.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the exclusions. |  -  |
**403** | Returned if you do not have permission to view the exclusions. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

