# InlineObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **str** | The login name for the user. A valid username must be in the format, &#x60;name@domain&#x60;, where &#x60;domain&#x60; corresponds to a domain approved for your Tenable.io instance.  **Note:** During initial setup, Tenable configures approved domains for your Tenable.io instance. To add domains to your instance, contact Tenable Support. | 
**password** | **str** | The initial password for the user. Passwords must be at least 12 characters long and contain at least one uppercase letter, one lowercase letter, one number, and one special character symbol. | 
**permissions** | **int** | The user permissions as described in [Permissions](doc:permissions). | 
**name** | **str** | The name of the user (for example, first and last name). | [optional] 
**email** | **str** | The email address of the user. A valid email address must be in the format, &#x60;name@domain&#x60;, where &#x60;domain&#x60; corresponds to a domain approved for your Tenable.io instance.  This email address overrides the email address set in &#x60;username&#x60;. If your request omits this parameter, Tenable.io uses the &#x60;username&#x60; value as the user&#39;s email address.  **Note:** During initial setup, Tenable configures approved domains for your Tenable.io instance. To add domains to your instance, contact Tenable Support. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


