# InlineResponse2005Pagination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | The total number of records matching your search criteria. Must be in the int32 format. | [optional] 
**limit** | **int** | Maximum number of records requested (or service imposed limit if not in request). Must be in the int32 format. | [optional] 
**offset** | **int** | The number of skipped records in the returned result set. Must be in the int32 format. | [optional] 
**sort** | **list[str]** | An array of the fields you specified as sort fields in the request, which Tenable.io uses to sort the returned data. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


