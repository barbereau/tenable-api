# InlineObject11

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the connector. | 
**network_uuid** | **str** | The UUID of the [network](doc:manage-networks-tio) to associate with the connector. You can find the UUID using the [GET /networks](ref:networks-list) endpoint. If you do not specify a network, Tenable.io automatically associates the connector with the default network (UUID &#x60;00000000-0000-0000-0000-000000000000&#x60;). **Note**: Tenable recommends creating a network for each connector type in use to prevent asset records in different cloud environments from overwriting each other. For more information, see [Managing Networks](doc:manage-networks-tio). | [optional] 
**params** | [**SettingsConnectorsParams**](platform__SettingsConnectorsParams.md) |  | 
**schedule** | [**SettingsConnectorsSchedule**](platform__SettingsConnectorsSchedule.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


