# ScannersScannerIdAgentsExclusionsSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** | If true, the exclusion is scheduled. | [optional] 
**starttime** | **str** | The start time of the exclusion formatted as &#x60;YYYY-MM-DD HH:MM:SS&#x60;. | 
**endtime** | **str** | The end time of the exclusion formatted as &#x60;YYYY-MM-DD HH:MM:SS&#x60;. | [optional] 
**timezone** | **str** | The timezone for the exclusion as returned by [scans: timezones](ref:scans-timezones). | 
**rrules** | [**ScannersScannerIdAgentsExclusionsScheduleRrules**](vm__ScannersScannerIdAgentsvm__ExclusionsScheduleRrules.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


