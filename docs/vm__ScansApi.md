# tenableapi.vm.ScansApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**io_scans_check_auto_targets**](vm__ScansApi.md#io_scans_check_auto_targets) | **POST** /scans/check-auto-targets | Test scan routes
[**io_scans_credentials_convert**](vm__ScansApi.md#io_scans_credentials_convert) | **POST** /scans/{scan_id}/credentials/{credentials_id}/upgrade | Convert credentials
[**scans_attachments**](vm__ScansApi.md#scans_attachments) | **GET** /scans/{scan_id}/attachments/{attachment_id} | Get scan attachment file
[**scans_configure**](vm__ScansApi.md#scans_configure) | **PUT** /scans/{scan_id} | Update scan
[**scans_copy**](vm__ScansApi.md#scans_copy) | **POST** /scans/{scan_id}/copy | Copy scan
[**scans_create**](vm__ScansApi.md#scans_create) | **POST** /scans | Create scan
[**scans_delete**](vm__ScansApi.md#scans_delete) | **DELETE** /scans/{scan_id} | Delete scan
[**scans_delete_history**](vm__ScansApi.md#scans_delete_history) | **DELETE** /scans/{scan_id}/history/{history_id} | Delete scan history
[**scans_details**](vm__ScansApi.md#scans_details) | **GET** /scans/{scan_id} | Get scan details
[**scans_export_download**](vm__ScansApi.md#scans_export_download) | **GET** /scans/{scan_id}/export/{file_id}/download | Download exported scan
[**scans_export_request**](vm__ScansApi.md#scans_export_request) | **POST** /scans/{scan_id}/export | Export scan
[**scans_export_status**](vm__ScansApi.md#scans_export_status) | **GET** /scans/{scan_id}/export/{file_id}/status | Check scan export status
[**scans_get_latest_status**](vm__ScansApi.md#scans_get_latest_status) | **GET** /scans/{scan_id}/latest-status | Get latest scan status
[**scans_history**](vm__ScansApi.md#scans_history) | **GET** /scans/{scan_id}/history | Get scan history
[**scans_history_details**](vm__ScansApi.md#scans_history_details) | **GET** /scans/{scan_id}/history/{history_uuid} | Get scan history details
[**scans_host_details**](vm__ScansApi.md#scans_host_details) | **GET** /scans/{scan_uuid}/hosts/{host_id} | Get host details
[**scans_import**](vm__ScansApi.md#scans_import) | **POST** /scans/import | Import uploaded scan
[**scans_launch**](vm__ScansApi.md#scans_launch) | **POST** /scans/{scan_id}/launch | Launch scan
[**scans_list**](vm__ScansApi.md#scans_list) | **GET** /scans | List scans
[**scans_pause**](vm__ScansApi.md#scans_pause) | **POST** /scans/{scan_id}/pause | Pause scan
[**scans_plugin_output**](vm__ScansApi.md#scans_plugin_output) | **GET** /scans/{scan_uuid}/hosts/{host_id}/plugins/{plugin_id} | Get plugin output
[**scans_read_status**](vm__ScansApi.md#scans_read_status) | **PUT** /scans/{scan_id}/status | Update scan status
[**scans_resume**](vm__ScansApi.md#scans_resume) | **POST** /scans/{scan_id}/resume | Resume scan
[**scans_schedule**](vm__ScansApi.md#scans_schedule) | **PUT** /scans/{scan_id}/schedule | Enable schedule
[**scans_stop**](vm__ScansApi.md#scans_stop) | **POST** /scans/{scan_id}/stop | Stop scan
[**scans_timezones**](vm__ScansApi.md#scans_timezones) | **GET** /scans/timezones | Get timezones


# **io_scans_check_auto_targets**
> InlineResponse20077 io_scans_check_auto_targets(inline_object47, limit=limit, matched_resource_limit=matched_resource_limit)

Test scan routes

Evaluates a list of targets and/or tags against the scan route configuration of scanner groups.<p>Requires SCAN OPERATOR [24] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    inline_object47 = tenableapi.vm.InlineObject47() # InlineObject47 | 
limit = 5 # int | Limit the number of missed targets returned in the response. (optional)
matched_resource_limit = 5 # int | Limit the number of matched resource UUIDs returned in the response. (optional)

    try:
        # Test scan routes
        api_response = api_instance.io_scans_check_auto_targets(inline_object47, limit=limit, matched_resource_limit=matched_resource_limit)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->io_scans_check_auto_targets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object47** | [**InlineObject47**](vm__InlineObject47.md)|  | 
 **limit** | **int**| Limit the number of missed targets returned in the response. | [optional] 
 **matched_resource_limit** | **int**| Limit the number of matched resource UUIDs returned in the response. | [optional] 

### Return type

[**InlineResponse20077**](vm__InlineResponse20077.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Return the list of missed targets (if any), and the list of matched scanner groups (if any). |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_scans_credentials_convert**
> InlineResponse20065 io_scans_credentials_convert(scan_id, credentials_id, inline_object40)

Convert credentials

Converts scan-specific credentials to managed credentials. For more information, see [Convert Scan-specific Credentials to Managed Credentials](doc:convert-scan-specific-credentials-to-managed-credentials). <p>Requires SCAN MANAGER [40] user permissions and CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan where the scan-specific credentials are currently stored.
credentials_id = 56 # int | The ID of the scan-specific credentials. For more information about determining this ID, see [Convert Scan-specific Credentials to Managed Credentials](doc:convert-scan-specific-credentials-to-managed-credentials).
inline_object40 = tenableapi.vm.InlineObject40() # InlineObject40 | 

    try:
        # Convert credentials
        api_response = api_instance.io_scans_credentials_convert(scan_id, credentials_id, inline_object40)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->io_scans_credentials_convert: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan where the scan-specific credentials are currently stored. | 
 **credentials_id** | **int**| The ID of the scan-specific credentials. For more information about determining this ID, see [Convert Scan-specific Credentials to Managed Credentials](doc:convert-scan-specific-credentials-to-managed-credentials). | 
 **inline_object40** | [**InlineObject40**](vm__InlineObject40.md)|  | 

### Return type

[**InlineResponse20065**](vm__InlineResponse20065.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully converts the scan-specific credentials to managed credentials. |  -  |
**400** | Returned if Tenable.io encounters invalid JSON in request body. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have permission to convert scan-specific credentials to managed credentials. |  -  |
**404** | Returned if Tenable.io cannot find the scan-specific credentials you want to convert. |  -  |
**409** | Returned if managed credentials with the same name already exist. |  -  |
**415** | Returned if the request payload is in an unsupported format. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_attachments**
> object scans_attachments(scan_id, attachment_id, key)

Get scan attachment file

Gets the requested scan attachment file.<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan containing the attachment. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
attachment_id = 'attachment_id_example' # str | The ID of the scan attachment.
key = 'key_example' # str | The attachment access token.

    try:
        # Get scan attachment file
        api_response = api_instance.scans_attachments(scan_id, attachment_id, key)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_attachments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan containing the attachment. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **attachment_id** | **str**| The ID of the scan attachment. | 
 **key** | **str**| The attachment access token. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the attachment file. |  -  |
**404** | Returned if Tenable.io cannot find the specified attachment file. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_configure**
> InlineResponse20063 scans_configure(scan_id, inline_object39)

Update scan

Updates the scan configuration. For example, you can enable or disable a scan, change the scan name, description, folder, scanner, targets, and schedule parameters. For more information and request body examples, see [Update a Scan](doc:update-scan-tio).<p>Requires SCAN OPERATOR [24] user permissions and CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan you want to update. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
inline_object39 = tenableapi.vm.InlineObject39() # InlineObject39 | 

    try:
        # Update scan
        api_response = api_instance.scans_configure(scan_id, inline_object39)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_configure: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan you want to update. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **inline_object39** | [**InlineObject39**](vm__InlineObject39.md)|  | 

### Return type

[**InlineResponse20063**](vm__InlineResponse20063.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io updates the scan configuration as specified. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encounters an error while attempting to save the configuration. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_copy**
> InlineResponse20069 scans_copy(scan_id, inline_object44)

Copy scan

Copies the specified scan.<p>Requires SCAN OPERATOR [24] user permissions and CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The identifier for the scan you want to copy. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
inline_object44 = tenableapi.vm.InlineObject44() # InlineObject44 | 

    try:
        # Copy scan
        api_response = api_instance.scans_copy(scan_id, inline_object44)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_copy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The identifier for the scan you want to copy. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **inline_object44** | [**InlineObject44**](vm__InlineObject44.md)|  | 

### Return type

[**InlineResponse20069**](vm__InlineResponse20069.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the copied scan object. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encounters an error while attempting to copy. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_create**
> InlineResponse20063 scans_create(inline_object38)

Create scan

Creates a scan configuration. For more information and request body examples, see [Create a Scan](doc:create-scan-tio).   **Note:** Tenable.io limits the number of scans you can create to 10,000 scans. Tenable recommends you re-use scheduled scans instead of creating new scans. An HTTP 403 error is returned if you attempt to create a scan after you have already reached the scan limit of 10,000.  <p>Requires SCAN OPERATOR [24] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    inline_object38 = tenableapi.vm.InlineObject38() # InlineObject38 | 

    try:
        # Create scan
        api_response = api_instance.scans_create(inline_object38)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object38** | [**InlineObject38**](vm__InlineObject38.md)|  | 

### Return type

[**InlineResponse20063**](vm__InlineResponse20063.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io creates the scan configuration successfully. |  -  |
**403** | Returned if you attempt to create a scan after you have already reached the scan limit of 10,000. Tenable recommends you re-use scheduled scans instead of creating new scans. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encounters an error while attempting to save the scan. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_delete**
> object scans_delete(scan_id)

Delete scan

Deletes a scan.  You cannot delete scans with a `running`, `paused`, or `stopping` status. For more information, see [Scan Status](doc:scan-status-tio). <p>Requires SCAN MANAGER [40] user permissions and CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan you want to delete. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.

    try:
        # Delete scan
        api_response = api_instance.scans_delete(scan_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan you want to delete. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deletes the specified scan configuration. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to delete the scan. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_delete_history**
> object scans_delete_history(scan_id, history_id)

Delete scan history

Deletes historical results from a scan.<p>Requires SCAN OPERATOR [24] user permissions and CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
history_id = 56 # int | The unique identifier of the historical data that you want Tenable.io to delete. This identifier corresponds to the `history.id` attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint.

    try:
        # Delete scan history
        api_response = api_instance.scans_delete_history(scan_id, history_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_delete_history: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **history_id** | **int**| The unique identifier of the historical data that you want Tenable.io to delete. This identifier corresponds to the &#x60;history.id&#x60; attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deleted the specified scan results. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan results. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to delete the scan results. |  -  |
**501** | Returned if Tenable.io does not support deleting historical scan results. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_details**
> InlineResponse20064 scans_details(scan_id, history_id=history_id, history_uuid=history_uuid)

Get scan details

Returns scan results for a specific scan. If you submit a request without query parameters, Tenable.io returns results from the latest run of the specified scan. If you submit a request using the query parameters to specify a historical run of the scan, Tenable.io returns the scan results for the specified run.   **Note:** Keep in mind potential [rate limits](doc:rate-limiting) and [concurrency limits](doc:concurrency-limiting) when using this endpoint. To check the status of a scan, use the [GET /scans/{scan_id}/latest-status](ref:scans-get-latest-status) endpoint. Tenable recommends the [GET /scans/{scan_id}/latest-status](ref:scans-get-latest-status) endpoint especially if you are programmatically checking the status of large numbers of scans.   **Note:** This endpoint returns limited data if scan results are older than 60 days. Scans that are older than 60 days are effectively \"archived\". The `info.is_archived` attribute in the response message for this endpoint serves as the indication of archival status. For complete data from archived scan results, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead. <p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan you want to retrieve. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
history_id = 56 # int | The unique identifier of the historical data that you want Tenable.io to return. This identifier corresponds to the `history.id` attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. You can use either this parameter or the `history_uuid` query parameter to specify a scan run. (optional)
history_uuid = 'history_uuid_example' # str | The UUID of the historical data that you want Tenable.io to return. This identifier corresponds to the `history.scan_uuid` attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. You can use either this parameter or the `history_id` query parameter to specify a scan run. (optional)

    try:
        # Get scan details
        api_response = api_instance.scans_details(scan_id, history_id=history_id, history_uuid=history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | [**str**](.md)| The unique identifier for the scan you want to retrieve. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **history_id** | **int**| The unique identifier of the historical data that you want Tenable.io to return. This identifier corresponds to the &#x60;history.id&#x60; attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. You can use either this parameter or the &#x60;history_uuid&#x60; query parameter to specify a scan run. | [optional] 
 **history_uuid** | **str**| The UUID of the historical data that you want Tenable.io to return. This identifier corresponds to the &#x60;history.scan_uuid&#x60; attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. You can use either this parameter or the &#x60;history_id&#x60; query parameter to specify a scan run. | [optional] 

### Return type

[**InlineResponse20064**](vm__InlineResponse20064.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scan details. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_export_download**
> object scans_export_download(scan_id, file_id)

Download exported scan

Download an exported scan.<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the exported scan you want to download. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
file_id = 'file_id_example' # str | The ID of the file to download (Included in response from /scans/{scan\\_id}/export).

    try:
        # Download exported scan
        api_response = api_instance.scans_export_download(scan_id, file_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_export_download: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the exported scan you want to download. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **file_id** | **str**| The ID of the file to download (Included in response from /scans/{scan\\_id}/export). | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the content of the file as an attachment. |  -  |
**404** | Returned if Tenable.io cannot find the specified file. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_export_request**
> InlineResponse20070 scans_export_request(scan_id, inline_object46, history_id=history_id, history_uuid=history_uuid)

Export scan

Export the specified scan. To see the status of the requested export, submit an [export status](ref:scans-export-status) request. On receiving a \"ready\" status from the export-status request, download the export file using the [export download](ref:scans-export-download) method.  **Note:** If you request a scan export in the `nessus` file format, but do not specify filters for the export, Tenable.io truncates the plugins output data in the export file at 5 MB or 5,000,000 characters, and appends `TRUNCATED` (bracketed by three asterisks) at the end of the output in the export file. You can obtain the full plugins output by exporting the scan in any other file format than `nessus`.<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The identifier for the scan you want to export. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
inline_object46 = tenableapi.vm.InlineObject46() # InlineObject46 | 
history_id = 56 # int | The unique identifier of the historical data that you want Tenable.io to export. This identifier corresponds to the `history.id` attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. (optional)
history_uuid = 56 # int | The UUID of the historical data that you want Tenable.io to return. This identifier corresponds to the `history.scan_uuid` attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. (optional)

    try:
        # Export scan
        api_response = api_instance.scans_export_request(scan_id, inline_object46, history_id=history_id, history_uuid=history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_export_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The identifier for the scan you want to export. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **inline_object46** | [**InlineObject46**](vm__InlineObject46.md)|  | 
 **history_id** | **int**| The unique identifier of the historical data that you want Tenable.io to export. This identifier corresponds to the &#x60;history.id&#x60; attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. | [optional] 
 **history_uuid** | **int**| The UUID of the historical data that you want Tenable.io to return. This identifier corresponds to the &#x60;history.scan_uuid&#x60; attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. | [optional] 

### Return type

[**InlineResponse20070**](vm__InlineResponse20070.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io queues the export successfully. |  -  |
**400** | Returned under the following conditions:  - Your request message is missing a required parameter.  - The &#x60;format&#x60; parameter in your request specified an unsupported export format for scan results that are older than 60 days. Only &#x60;Nessus&#x60; and &#x60;CSV&#x60; formats are supported for scan results that are older than 60 days.  - Your request included filter query parameters for scan results that are older than 60 days. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_export_status**
> InlineResponse20071 scans_export_status(scan_id, file_id)

Check scan export status

Check the file status of an exported scan. When an export has been requested, it is necessary to poll this endpoint until a \"ready\" status is returned, at which point the file is complete and can be downloaded using the [export download](ref:scans-export-download) endpoint.<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
file_id = 'file_id_example' # str | The ID of the file to poll (included in response from /scans/{scan\\_id}/export).

    try:
        # Check scan export status
        api_response = api_instance.scans_export_status(scan_id, file_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_export_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **file_id** | **str**| The ID of the file to poll (included in response from /scans/{scan\\_id}/export). | 

### Return type

[**InlineResponse20071**](vm__InlineResponse20071.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the status of the file. A status of &#x60;ready&#x60; indicates the file can be downloaded. |  -  |
**404** | Returned if Tenable.io cannot find the specified file. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_get_latest_status**
> InlineResponse20068 scans_get_latest_status(scan_id)

Get latest scan status

Returns the latest status for a scan. For a list of possible status values, see [Scan Status](doc:scan-status-tio).<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan.  This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.

    try:
        # Get latest scan status
        api_response = api_instance.scans_get_latest_status(scan_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_get_latest_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan.  This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 

### Return type

[**InlineResponse20068**](vm__InlineResponse20068.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully retrieves the scan status. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encounters an error while attempting to retrieve the status. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_history**
> InlineResponse20072 scans_history(scan_id, limit=limit, offset=offset)

Get scan history

Returns a list of objects, each of which represent an individual run of the specified scan.<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
limit = 56 # int | Maximum number of objects requested (or service imposed limit if not in request). The max limit value allowed is 50. Must be in the int32 format. (optional)
offset = 56 # int | Offset from request (or zero). Must be in the int32 format. (optional)

    try:
        # Get scan history
        api_response = api_instance.scans_history(scan_id, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_history: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **limit** | **int**| Maximum number of objects requested (or service imposed limit if not in request). The max limit value allowed is 50. Must be in the int32 format. | [optional] 
 **offset** | **int**| Offset from request (or zero). Must be in the int32 format. | [optional] 

### Return type

[**InlineResponse20072**](vm__InlineResponse20072.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of scan history objects. |  -  |
**404** | Returned if Tenable.io cannot find the specified &#x60;scan_id&#x60;. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_history_details**
> InlineResponse20073 scans_history_details(scan_id, history_uuid)

Get scan history details

Returns the details of a previous run of the specified scan. Scan details include information about when and where the scan ran, as well as the scan results for the target hosts.<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan in your Tenable.io instance. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
history_uuid = 'history_uuid_example' # str | The UUID of the historical scan result to return details about. This identifier corresponds to the `history.scan_uuid` attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint.

    try:
        # Get scan history details
        api_response = api_instance.scans_history_details(scan_id, history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_history_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan in your Tenable.io instance. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **history_uuid** | **str**| The UUID of the historical scan result to return details about. This identifier corresponds to the &#x60;history.scan_uuid&#x60; attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. | 

### Return type

[**InlineResponse20073**](vm__InlineResponse20073.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns details of the historical scan result. |  -  |
**404** | Returned if Tenable.io cannot find the specified &#x60;scan_id&#x60; or &#x60;history_uuid&#x60;. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_host_details**
> InlineResponse20074 scans_host_details(scan_uuid, host_id, history_id=history_id, history_uuid=history_uuid)

Get host details

Returns details for the specified host.   **Note:** This endpoint can only return scan results that are younger than 60 days. For scan results that are older than 60 days, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead.<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_uuid = 'scan_uuid_example' # str | The identifier for the scan. This identifier can be the either the `schedule_uuid` or the numeric `id` attribute for the scan. We recommend that you use `schedule_uuid`.
host_id = 56 # int | The ID of the host to retrieve.
history_id = 56 # int | The unique identifier of the historical data that you want Tenable.io to return. This identifier corresponds to the `history.id` attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint.  (optional)
history_uuid = 56 # int | The UUID of the historical data that you want Tenable.io to return. This identifier corresponds to the `history.scan_uuid` attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. (optional)

    try:
        # Get host details
        api_response = api_instance.scans_host_details(scan_uuid, host_id, history_id=history_id, history_uuid=history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_host_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_uuid** | **str**| The identifier for the scan. This identifier can be the either the &#x60;schedule_uuid&#x60; or the numeric &#x60;id&#x60; attribute for the scan. We recommend that you use &#x60;schedule_uuid&#x60;. | 
 **host_id** | **int**| The ID of the host to retrieve. | 
 **history_id** | **int**| The unique identifier of the historical data that you want Tenable.io to return. This identifier corresponds to the &#x60;history.id&#x60; attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint.  | [optional] 
 **history_uuid** | **int**| The UUID of the historical data that you want Tenable.io to return. This identifier corresponds to the &#x60;history.scan_uuid&#x60; attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. | [optional] 

### Return type

[**InlineResponse20074**](vm__InlineResponse20074.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the host details. |  -  |
**404** | Returned under the following conditions:  - Tenable.io cannot find the specified scan.  - The requested scan results are older than 60 days. Use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_import**
> InlineResponse20069 scans_import(inline_object45, include_aggregate=include_aggregate)

Import uploaded scan

Import an existing scan uploaded using the [Upload File](ref:file-upload) endpoint. Tenable.io supports scan imports up to 4 GB in size.  **Note:** You cannot import results from scans run more than 15 months ago.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    inline_object45 = tenableapi.vm.InlineObject45() # InlineObject45 | 
include_aggregate = 56 # int | Specifies whether to include the imported scan data in the vulnerabilities dashboard views. To include, use `1`. To exclude, use `0`. If you don't specify the include_aggregate parameter, the data does not appear in the dashboard. (optional)

    try:
        # Import uploaded scan
        api_response = api_instance.scans_import(inline_object45, include_aggregate=include_aggregate)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_import: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object45** | [**InlineObject45**](vm__InlineObject45.md)|  | 
 **include_aggregate** | **int**| Specifies whether to include the imported scan data in the vulnerabilities dashboard views. To include, use &#x60;1&#x60;. To exclude, use &#x60;0&#x60;. If you don&#39;t specify the include_aggregate parameter, the data does not appear in the dashboard. | [optional] 

### Return type

[**InlineResponse20069**](vm__InlineResponse20069.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scan object. |  -  |
**409** | Returned if you attempt to import results from scans run more than 15 months ago. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to import the scan. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_launch**
> InlineResponse20066 scans_launch(scan_id, inline_object41)

Launch scan

Launches a scan. For more information, see [Launch a Scan](doc:launch-scan-tio).<p>Requires SCAN OPERATOR [24] user permissions and CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan you want to launch. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
inline_object41 = tenableapi.vm.InlineObject41() # InlineObject41 | 

    try:
        # Launch scan
        api_response = api_instance.scans_launch(scan_id, inline_object41)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_launch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan you want to launch. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **inline_object41** | [**InlineObject41**](vm__InlineObject41.md)|  | 

### Return type

[**InlineResponse20066**](vm__InlineResponse20066.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully launches the scan. |  -  |
**403** | Returned if Tenable.io cannot launch the scan because the scan is disabled. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_list**
> InlineResponse20062 scans_list(folder_id=folder_id, last_modification_date=last_modification_date)

List scans

Returns a list of scans where you have at least CAN VIEW [16] permissions.   **Note:** Keep in mind potential [rate limits](doc:rate-limiting) when using this endpoint. To check the status of your scans, use the [GET /scans/{scan_id}/latest-status](ref:scans-get-latest-status) endpoint. Tenable recommends the [GET /scans/{scan_id}/latest-status](ref:scans-get-latest-status) endpoint especially if you are programmatically checking the status of large numbers of scans.<p>Requires BASIC [16] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    folder_id = 56 # int | The ID of the folder where the scans you want to list are stored. (optional)
last_modification_date = 56 # int | Limit the results to those scans that have run since the specified time. This parameter does not represent the date on which the scan configuration was last modified. Must be in Unix time format. (optional)

    try:
        # List scans
        api_response = api_instance.scans_list(folder_id=folder_id, last_modification_date=last_modification_date)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **folder_id** | **int**| The ID of the folder where the scans you want to list are stored. | [optional] 
 **last_modification_date** | **int**| Limit the results to those scans that have run since the specified time. This parameter does not represent the date on which the scan configuration was last modified. Must be in Unix time format. | [optional] 

### Return type

[**InlineResponse20062**](vm__InlineResponse20062.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scan list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_pause**
> object scans_pause(scan_id)

Pause scan

Pauses a scan. You can only pause scans that have a `running` status.<p>Requires SCAN OPERATOR [24] user permissions and CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan you want to pause. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.

    try:
        # Pause scan
        api_response = api_instance.scans_pause(scan_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_pause: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan you want to pause. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully queues the scan to pause. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**409** | Returned if the specified scan has a status other than &#x60;running&#x60;. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_plugin_output**
> InlineResponse20075 scans_plugin_output(scan_uuid, host_id, plugin_id, history_id=history_id, history_uuid=history_uuid)

Get plugin output

Returns the output for a specified plugin.   **Note:** This endpoint can only return scan results that are younger than 60 days. For scan results that are older than 60 days, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead. Output for an individual plugin is limited to 1,024 KB (1 MB).<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_uuid = 'scan_uuid_example' # str | The identifier for the scan. This identifier can be the either the `schedule_uuid` or the numeric `id` attribute for the scan. We recommend that you use `schedule_uuid`.
host_id = 56 # int | The ID of the host to retrieve.
plugin_id = 56 # int | The ID of the plugin to retrieve.
history_id = 56 # int | The unique identifier of the historical data that you want Tenable.io to return. This identifier corresponds to the `history.id` attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint.  (optional)
history_uuid = 56 # int | The UUID of the historical data that you want Tenable.io to return. This identifier corresponds to the `history.scan_uuid` attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. (optional)

    try:
        # Get plugin output
        api_response = api_instance.scans_plugin_output(scan_uuid, host_id, plugin_id, history_id=history_id, history_uuid=history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_plugin_output: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_uuid** | **str**| The identifier for the scan. This identifier can be the either the &#x60;schedule_uuid&#x60; or the numeric &#x60;id&#x60; attribute for the scan. We recommend that you use &#x60;schedule_uuid&#x60;. | 
 **host_id** | **int**| The ID of the host to retrieve. | 
 **plugin_id** | **int**| The ID of the plugin to retrieve. | 
 **history_id** | **int**| The unique identifier of the historical data that you want Tenable.io to return. This identifier corresponds to the &#x60;history.id&#x60; attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint.  | [optional] 
 **history_uuid** | **int**| The UUID of the historical data that you want Tenable.io to return. This identifier corresponds to the &#x60;history.scan_uuid&#x60; attribute of the response message from the [GET /scans/{scan_id}/history](ref:scans-history) endpoint. | [optional] 

### Return type

[**InlineResponse20075**](vm__InlineResponse20075.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the plugin output. |  -  |
**404** | Returned under the following conditions:  - Tenable.io cannot find the specified scan.  - The requested scan results are older than 60 days. Use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_read_status**
> object scans_read_status(scan_id, inline_object43)

Update scan status

Changes the status of a scan. For a list of possible status values, see [Scan Status](doc:scan-status-tio).<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
inline_object43 = tenableapi.vm.InlineObject43() # InlineObject43 | 

    try:
        # Update scan status
        api_response = api_instance.scans_read_status(scan_id, inline_object43)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_read_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **inline_object43** | [**InlineObject43**](vm__InlineObject43.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io updates the scan status. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_resume**
> object scans_resume(scan_id)

Resume scan

Resumes a scan. You can only resume a scan that has a status of `paused`.<p>Requires SCAN OPERATOR [24] user permissions and CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan you want to resume. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.

    try:
        # Resume scan
        api_response = api_instance.scans_resume(scan_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_resume: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan you want to resume. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully queues the scan to resume. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**409** | Returned if the specified scan has a status other than &#x60;paused&#x60;. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_schedule**
> InlineResponse20067 scans_schedule(scan_id, inline_object42)

Enable schedule

Enables or disables a scan schedule.<p>Requires SCAN OPERATOR [24] user permissions and CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The unique identifier for the scan you want to enable or disable. This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.
inline_object42 = tenableapi.vm.InlineObject42() # InlineObject42 | 

    try:
        # Enable schedule
        api_response = api_instance.scans_schedule(scan_id, inline_object42)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_schedule: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The unique identifier for the scan you want to enable or disable. This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 
 **inline_object42** | [**InlineObject42**](vm__InlineObject42.md)|  | 

### Return type

[**InlineResponse20067**](vm__InlineResponse20067.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully enabled or disabled the scan schedule. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if the scan does not have a schedule to enable. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_stop**
> object scans_stop(scan_id)

Stop scan

Stops a scan.   You can only stop a scan that has a status of `pending`, `running`, or `resuming`. For more information about these statuses, see [Scan Status](doc:scan-status-tio).<p>Requires SCAN OPERATOR [24] user permissions and CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The identifier for the scan you want to stop.  This identifier can be either the `scans.schedule_uuid` or the `scans.id` attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use `scans.schedule_uuid`.

    try:
        # Stop scan
        api_response = api_instance.scans_stop(scan_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_stop: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The identifier for the scan you want to stop.  This identifier can be either the &#x60;scans.schedule_uuid&#x60; or the &#x60;scans.id&#x60; attribute in the response message from the [GET /scans](ref:scans-list) endpoint. Tenable recommends that you use &#x60;scans.schedule_uuid&#x60;. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully queues the scan to stop. |  -  |
**404** | Returned if Tenable.io cannot find the specified scan. |  -  |
**409** | Returned if the scan does not have a &#x60;pending&#x60;,&#x60;running&#x60;, or &#x60;resuming&#x60; status. For more information about these statuses, see [Scan Status](doc:scan-status-tio). |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **scans_timezones**
> list[InlineResponse20076] scans_timezones()

Get timezones

Returns the timezones list for creating a recurring scan. For more information, see [Example Assessment Scan: Recurring](doc:example-assessment-scan-recurring).<p>Requires SCAN OPERATOR [24] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.ScansApi(api_client)
    
    try:
        # Get timezones
        api_response = api_instance.scans_timezones()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->scans_timezones: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse20076]**](vm__InlineResponse20076.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the timezone list. |  -  |
**403** | Returned if you do not have permission to view timezones. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

