# InlineResponse20099

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outputs** | [**list[InlineResponse20093]**](vm__InlineResponse20093.md) | A list of vulnerabilities discovered by the plugin. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


