# InlineResponse20018

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials** | [**list[InlineResponse20018Credentials]**](vm__InlineResponse20018Credentials.md) |  | [optional] 
**pagination** | [**InlineResponse20018Pagination**](vm__InlineResponse20018Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


