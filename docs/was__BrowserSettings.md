# BrowserSettings

The settings for the virtual browser instance used by the scanner.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**screen_width** | **int** | The screen width (in pixels) of the browser embedded into the scanner. | [optional] [default to 1600]
**screen_height** | **int** | The screen height (in pixels) of the browser embedded into the scanner. | [optional] [default to 1200]
**ignore_images** | **bool** | Indicates whether the browser should crawl images on web pages (&#x60;false&#x60;) or ignore those images (&#x60;true&#x60;) . The default value of this parameter is &#x60;true&#x60;. | [optional] [default to True]
**job_timeout** | **int** | The browser timeout (in milliseconds). | [optional] [default to 10000]
**analysis** | **bool** | Indicates whether DOM page analysis should be enabled (&#x60;true&#x60;) or (&#x60;disabled&#x60;) during the scan. | [optional] [default to True]
**pool_size** | **int** | The number of browser instances used for the scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


