# InlineObject13

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**folder_id** | **int** | The ID of the destination folder. | [optional] 
**name** | **str** | The name of the copied scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


