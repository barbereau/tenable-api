# InlineResponse20018LastUsedBy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The ID of the user who last used the credential in a scan. | [optional] 
**display_name** | **str** | The name of the user who last used the credential in a scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


