# InlineResponse20092Vpr

Information about the Vulnerability Priority Rating (VPR) for the vulnerability.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**score** | **float** | The Vulnerability Priority Rating (VPR) for the vulnerability. If a plugin is designed to detect multiple vulnerabilities, the VPR represents the highest value calculated for a vulnerability associated with the plugin. For more information, see &lt;a href&#x3D;\&quot;https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/RiskMetrics.htm\&quot; target&#x3D;\&quot;_blank\&quot;&gt;Severity vs. VPR&lt;/a&gt; in the &lt;i&gt;Tenable.io Vulnerability Management User Guide&lt;/i&gt;. | [optional] 
**drivers** | **object** | The key drivers Tenable uses to calculate a vulnerability&#39;s VPR. For more information, see &lt;[Vulnerability Priority Rating Drivers](doc:vpr-drivers-tio). | [optional] 
**updated** | **str** | The ISO timestamp when Tenable.io last imported the VPR for this vulnerability. Tenable.io imports a VPR value the first time you scan a vulnerability on your network. Then, Tenable.io automatically re-imports new and updated VPR values daily. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


