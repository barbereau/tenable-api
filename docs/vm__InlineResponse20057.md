# InlineResponse20057

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**route** | **str** | A hostname, hostname wildcard, IP address, CIDR address, or IP range. For more information about supported route formats, see [Supported Scan Routing Target Formats](doc:manage-scan-routing-tio#section-supported-scan-routing-target-formats). | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


