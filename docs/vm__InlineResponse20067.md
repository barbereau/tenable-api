# InlineResponse20067

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** | If &#x60;true&#x60;, the schedule for the scan is enabled. | [optional] 
**control** | **bool** | If &#x60;true&#x60;, the scan has a schedule and can be launched. | [optional] 
**rrules** | **str** | The interval at which the scan repeats. The interval is formatted as a string of three values delimited by semi-colons. These values are: the frequency (FREQ&#x3D;ONETIME or DAILY or WEEKLY or MONTHLY or YEARLY), the interval (INTERVAL&#x3D;1 or 2 or 3 ... x), and the days of the week (BYDAY&#x3D;SU,MO,TU,WE,TH,FR,SA). For a scan that runs every three weeks on Monday Wednesday and Friday, the string would be &#x60;FREQ&#x3D;WEEKLY;INTERVAL&#x3D;3;BYDAY&#x3D;MO,WE,FR&#x60;. If the scan is not scheduled to recur, this attribute is &#x60;null&#x60;. For more information, see [rrules Format](example-assessment-scan-recurring#rrules-format).  **Note:** To set the &#x60;rrules&#x60; parameter for an agent scan, the request must also include the following body parameters:&lt;ul&gt;&lt;li&gt;The &#x60;uuid&#x60; parameter must specify an agent scan template. For more information, see [Tenable-Provided Agent Templates](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Scans/AgentTemplates.htm) and the [GET /editor/scan/templates](ref:editor-list-templates) endpoint.&lt;/li&gt;&lt;li&gt;The &#x60;agent_group_id&#x60; parameter must specify an agent group. For more information, see [Agent Groups](ref:agent-groups).&lt;/li&gt;&lt;/ul&gt;For an example request body for an agent scan, see [Example Agent Scan: Recurring](doc:example-agent-scan-recurring). | [optional] 
**starttime** | **str** | For one-time scans, the starting time and date for the scan. For recurrent scans, the first date on which the scan schedule is active and the time that recurring scans launch based on the &#x60;rrules&#x60; parameter.  This attribute has the following format: &#x60;YYYYMMDDTHHMMSS&#x60;. | [optional] 
**timezone** | **str** | The timezone of the scheduled start time for the scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


