# TagsAssetsFiltersControl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**readable_regex** | **str** | Provides a human-readable \&quot;hint\&quot; to guide users creating tag rules in the Tenable.io user interface. | [optional] 
**type** | **str** | The type of UI control that represents the filter in the Tenable.io user interface. | [optional] 
**regex** | **str** | A regular expression that Tenable.io UI uses to validate input. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


