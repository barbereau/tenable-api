# coding: utf-8

"""
    Web Application Scanning v1

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.waslegacy.configuration import Configuration


class InlineResponse20016Scans(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'id': 'int',
        'uuid': 'str',
        'name': 'str',
        'type': 'str',
        'owner': 'str',
        'enabled': 'bool',
        'read': 'bool',
        'status': 'str',
        'shared': 'bool',
        'user_permissions': 'int',
        'creation_date': 'int',
        'last_modification_date': 'int',
        'control': 'bool',
        'starttime': 'str',
        'timezone': 'str',
        'rrules': 'str',
        'schedule_uuid': 'str'
    }

    attribute_map = {
        'id': 'id',
        'uuid': 'uuid',
        'name': 'name',
        'type': 'type',
        'owner': 'owner',
        'enabled': 'enabled',
        'read': 'read',
        'status': 'status',
        'shared': 'shared',
        'user_permissions': 'user_permissions',
        'creation_date': 'creation_date',
        'last_modification_date': 'last_modification_date',
        'control': 'control',
        'starttime': 'starttime',
        'timezone': 'timezone',
        'rrules': 'rrules',
        'schedule_uuid': 'schedule_uuid'
    }

    def __init__(self, id=None, uuid=None, name=None, type=None, owner=None, enabled=None, read=None, status=None, shared=None, user_permissions=None, creation_date=None, last_modification_date=None, control=None, starttime=None, timezone=None, rrules=None, schedule_uuid=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20016Scans - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._id = None
        self._uuid = None
        self._name = None
        self._type = None
        self._owner = None
        self._enabled = None
        self._read = None
        self._status = None
        self._shared = None
        self._user_permissions = None
        self._creation_date = None
        self._last_modification_date = None
        self._control = None
        self._starttime = None
        self._timezone = None
        self._rrules = None
        self._schedule_uuid = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if uuid is not None:
            self.uuid = uuid
        if name is not None:
            self.name = name
        if type is not None:
            self.type = type
        if owner is not None:
            self.owner = owner
        if enabled is not None:
            self.enabled = enabled
        if read is not None:
            self.read = read
        if status is not None:
            self.status = status
        if shared is not None:
            self.shared = shared
        if user_permissions is not None:
            self.user_permissions = user_permissions
        if creation_date is not None:
            self.creation_date = creation_date
        if last_modification_date is not None:
            self.last_modification_date = last_modification_date
        if control is not None:
            self.control = control
        if starttime is not None:
            self.starttime = starttime
        if timezone is not None:
            self.timezone = timezone
        if rrules is not None:
            self.rrules = rrules
        if schedule_uuid is not None:
            self.schedule_uuid = schedule_uuid

    @property
    def id(self):
        """Gets the id of this InlineResponse20016Scans.  # noqa: E501

        The unique ID of the scan.  # noqa: E501

        :return: The id of this InlineResponse20016Scans.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20016Scans.

        The unique ID of the scan.  # noqa: E501

        :param id: The id of this InlineResponse20016Scans.  # noqa: E501
        :type id: int
        """

        self._id = id

    @property
    def uuid(self):
        """Gets the uuid of this InlineResponse20016Scans.  # noqa: E501

        The UUID for the scan.  # noqa: E501

        :return: The uuid of this InlineResponse20016Scans.  # noqa: E501
        :rtype: str
        """
        return self._uuid

    @uuid.setter
    def uuid(self, uuid):
        """Sets the uuid of this InlineResponse20016Scans.

        The UUID for the scan.  # noqa: E501

        :param uuid: The uuid of this InlineResponse20016Scans.  # noqa: E501
        :type uuid: str
        """

        self._uuid = uuid

    @property
    def name(self):
        """Gets the name of this InlineResponse20016Scans.  # noqa: E501

        The name of the scan.  # noqa: E501

        :return: The name of this InlineResponse20016Scans.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse20016Scans.

        The name of the scan.  # noqa: E501

        :param name: The name of this InlineResponse20016Scans.  # noqa: E501
        :type name: str
        """

        self._name = name

    @property
    def type(self):
        """Gets the type of this InlineResponse20016Scans.  # noqa: E501

        The type of scan (local, remote, webapp, or agent). WAS scans will always have the type set to webapp.  # noqa: E501

        :return: The type of this InlineResponse20016Scans.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this InlineResponse20016Scans.

        The type of scan (local, remote, webapp, or agent). WAS scans will always have the type set to webapp.  # noqa: E501

        :param type: The type of this InlineResponse20016Scans.  # noqa: E501
        :type type: str
        """

        self._type = type

    @property
    def owner(self):
        """Gets the owner of this InlineResponse20016Scans.  # noqa: E501

        The owner of the scan.  # noqa: E501

        :return: The owner of this InlineResponse20016Scans.  # noqa: E501
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this InlineResponse20016Scans.

        The owner of the scan.  # noqa: E501

        :param owner: The owner of this InlineResponse20016Scans.  # noqa: E501
        :type owner: str
        """

        self._owner = owner

    @property
    def enabled(self):
        """Gets the enabled of this InlineResponse20016Scans.  # noqa: E501

        If `true`, the schedule for the scan is enabled.  # noqa: E501

        :return: The enabled of this InlineResponse20016Scans.  # noqa: E501
        :rtype: bool
        """
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        """Sets the enabled of this InlineResponse20016Scans.

        If `true`, the schedule for the scan is enabled.  # noqa: E501

        :param enabled: The enabled of this InlineResponse20016Scans.  # noqa: E501
        :type enabled: bool
        """

        self._enabled = enabled

    @property
    def read(self):
        """Gets the read of this InlineResponse20016Scans.  # noqa: E501

        If `true`, the scan has been read.  # noqa: E501

        :return: The read of this InlineResponse20016Scans.  # noqa: E501
        :rtype: bool
        """
        return self._read

    @read.setter
    def read(self, read):
        """Sets the read of this InlineResponse20016Scans.

        If `true`, the scan has been read.  # noqa: E501

        :param read: The read of this InlineResponse20016Scans.  # noqa: E501
        :type read: bool
        """

        self._read = read

    @property
    def status(self):
        """Gets the status of this InlineResponse20016Scans.  # noqa: E501

        The status of the scan (completed, aborted, imported, pending, running, resuming, canceling, canceled, pausing, paused, stopping, stopped).  # noqa: E501

        :return: The status of this InlineResponse20016Scans.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this InlineResponse20016Scans.

        The status of the scan (completed, aborted, imported, pending, running, resuming, canceling, canceled, pausing, paused, stopping, stopped).  # noqa: E501

        :param status: The status of this InlineResponse20016Scans.  # noqa: E501
        :type status: str
        """

        self._status = status

    @property
    def shared(self):
        """Gets the shared of this InlineResponse20016Scans.  # noqa: E501

        If `true`, the scan is shared.  # noqa: E501

        :return: The shared of this InlineResponse20016Scans.  # noqa: E501
        :rtype: bool
        """
        return self._shared

    @shared.setter
    def shared(self, shared):
        """Sets the shared of this InlineResponse20016Scans.

        If `true`, the scan is shared.  # noqa: E501

        :param shared: The shared of this InlineResponse20016Scans.  # noqa: E501
        :type shared: bool
        """

        self._shared = shared

    @property
    def user_permissions(self):
        """Gets the user_permissions of this InlineResponse20016Scans.  # noqa: E501

        The sharing permissions for the scan.  # noqa: E501

        :return: The user_permissions of this InlineResponse20016Scans.  # noqa: E501
        :rtype: int
        """
        return self._user_permissions

    @user_permissions.setter
    def user_permissions(self, user_permissions):
        """Sets the user_permissions of this InlineResponse20016Scans.

        The sharing permissions for the scan.  # noqa: E501

        :param user_permissions: The user_permissions of this InlineResponse20016Scans.  # noqa: E501
        :type user_permissions: int
        """

        self._user_permissions = user_permissions

    @property
    def creation_date(self):
        """Gets the creation_date of this InlineResponse20016Scans.  # noqa: E501

        The creation date for the scan in Unix time.  # noqa: E501

        :return: The creation_date of this InlineResponse20016Scans.  # noqa: E501
        :rtype: int
        """
        return self._creation_date

    @creation_date.setter
    def creation_date(self, creation_date):
        """Sets the creation_date of this InlineResponse20016Scans.

        The creation date for the scan in Unix time.  # noqa: E501

        :param creation_date: The creation_date of this InlineResponse20016Scans.  # noqa: E501
        :type creation_date: int
        """

        self._creation_date = creation_date

    @property
    def last_modification_date(self):
        """Gets the last_modification_date of this InlineResponse20016Scans.  # noqa: E501

        The last modification date for the scan in Unix time.  # noqa: E501

        :return: The last_modification_date of this InlineResponse20016Scans.  # noqa: E501
        :rtype: int
        """
        return self._last_modification_date

    @last_modification_date.setter
    def last_modification_date(self, last_modification_date):
        """Sets the last_modification_date of this InlineResponse20016Scans.

        The last modification date for the scan in Unix time.  # noqa: E501

        :param last_modification_date: The last_modification_date of this InlineResponse20016Scans.  # noqa: E501
        :type last_modification_date: int
        """

        self._last_modification_date = last_modification_date

    @property
    def control(self):
        """Gets the control of this InlineResponse20016Scans.  # noqa: E501

        If `true`, the scan has a schedule and can be launched.  # noqa: E501

        :return: The control of this InlineResponse20016Scans.  # noqa: E501
        :rtype: bool
        """
        return self._control

    @control.setter
    def control(self, control):
        """Sets the control of this InlineResponse20016Scans.

        If `true`, the scan has a schedule and can be launched.  # noqa: E501

        :param control: The control of this InlineResponse20016Scans.  # noqa: E501
        :type control: bool
        """

        self._control = control

    @property
    def starttime(self):
        """Gets the starttime of this InlineResponse20016Scans.  # noqa: E501

        The scheduled start time for the scan.  # noqa: E501

        :return: The starttime of this InlineResponse20016Scans.  # noqa: E501
        :rtype: str
        """
        return self._starttime

    @starttime.setter
    def starttime(self, starttime):
        """Sets the starttime of this InlineResponse20016Scans.

        The scheduled start time for the scan.  # noqa: E501

        :param starttime: The starttime of this InlineResponse20016Scans.  # noqa: E501
        :type starttime: str
        """

        self._starttime = starttime

    @property
    def timezone(self):
        """Gets the timezone of this InlineResponse20016Scans.  # noqa: E501

        The timezone for the scan.  # noqa: E501

        :return: The timezone of this InlineResponse20016Scans.  # noqa: E501
        :rtype: str
        """
        return self._timezone

    @timezone.setter
    def timezone(self, timezone):
        """Sets the timezone of this InlineResponse20016Scans.

        The timezone for the scan.  # noqa: E501

        :param timezone: The timezone of this InlineResponse20016Scans.  # noqa: E501
        :type timezone: str
        """

        self._timezone = timezone

    @property
    def rrules(self):
        """Gets the rrules of this InlineResponse20016Scans.  # noqa: E501

        The rules for repeating the scan.  # noqa: E501

        :return: The rrules of this InlineResponse20016Scans.  # noqa: E501
        :rtype: str
        """
        return self._rrules

    @rrules.setter
    def rrules(self, rrules):
        """Sets the rrules of this InlineResponse20016Scans.

        The rules for repeating the scan.  # noqa: E501

        :param rrules: The rrules of this InlineResponse20016Scans.  # noqa: E501
        :type rrules: str
        """

        self._rrules = rrules

    @property
    def schedule_uuid(self):
        """Gets the schedule_uuid of this InlineResponse20016Scans.  # noqa: E501

        The schedule_uuid of the scan that should be returned.  # noqa: E501

        :return: The schedule_uuid of this InlineResponse20016Scans.  # noqa: E501
        :rtype: str
        """
        return self._schedule_uuid

    @schedule_uuid.setter
    def schedule_uuid(self, schedule_uuid):
        """Sets the schedule_uuid of this InlineResponse20016Scans.

        The schedule_uuid of the scan that should be returned.  # noqa: E501

        :param schedule_uuid: The schedule_uuid of this InlineResponse20016Scans.  # noqa: E501
        :type schedule_uuid: str
        """

        self._schedule_uuid = schedule_uuid

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20016Scans):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20016Scans):
            return True

        return self.to_dict() != other.to_dict()
