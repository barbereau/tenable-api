# coding: utf-8

"""
    Web Application Scanning v2

    The Tenable.io Web Application Scanning v2 API serves as a gateway for you to interact with the Tenable.io Web Application Scanning application and enables you to automate the security management for your web applications. You can safely and accurately scan web applications to provide deep visibility into vulnerabilities and context for prioritizing remediations. Use the Tenable.io Web Application Scanning v2 API endpoints to perform CRUD operations on web application scans, launch web application scans, and obtain scan and asset details.  **Note:** While Tenable that you use Tenable.io Web Application Scanning API v2 for any new development. You can continue to use existing integrations that are based on Tenable.io Web Application Scanning API v1. Tenable will provide advance notice and migration guidance prior to deprecating Web Application Scanning API v1. For most up-to-date announcements, subscribe to our [RSS feed](https://feeds.feedburner.com/TenableDeveloperHub) or check the [API Changelog](https://developer.tenable.com/changelog).  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.was.configuration import Configuration


class PluginSettings(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'rate_limiter': 'PluginSettingsRateLimiter',
        'mode': 'str',
        'ids': 'list[int]',
        'names': 'list[str]',
        'families': 'list[str]'
    }

    attribute_map = {
        'rate_limiter': 'rateLimiter',
        'mode': 'mode',
        'ids': 'ids',
        'names': 'names',
        'families': 'families'
    }

    def __init__(self, rate_limiter=None, mode=None, ids=None, names=None, families=None, local_vars_configuration=None):  # noqa: E501
        """PluginSettings - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._rate_limiter = None
        self._mode = None
        self._ids = None
        self._names = None
        self._families = None
        self.discriminator = None

        if rate_limiter is not None:
            self.rate_limiter = rate_limiter
        if mode is not None:
            self.mode = mode
        if ids is not None:
            self.ids = ids
        if names is not None:
            self.names = names
        if families is not None:
            self.families = families

    @property
    def rate_limiter(self):
        """Gets the rate_limiter of this PluginSettings.  # noqa: E501


        :return: The rate_limiter of this PluginSettings.  # noqa: E501
        :rtype: PluginSettingsRateLimiter
        """
        return self._rate_limiter

    @rate_limiter.setter
    def rate_limiter(self, rate_limiter):
        """Sets the rate_limiter of this PluginSettings.


        :param rate_limiter: The rate_limiter of this PluginSettings.  # noqa: E501
        :type rate_limiter: PluginSettingsRateLimiter
        """

        self._rate_limiter = rate_limiter

    @property
    def mode(self):
        """Gets the mode of this PluginSettings.  # noqa: E501

        Indicates whether specific plugins and/or plugin families are enabled (`enable`) or disabled (`disable`) for the scan.  # noqa: E501

        :return: The mode of this PluginSettings.  # noqa: E501
        :rtype: str
        """
        return self._mode

    @mode.setter
    def mode(self, mode):
        """Sets the mode of this PluginSettings.

        Indicates whether specific plugins and/or plugin families are enabled (`enable`) or disabled (`disable`) for the scan.  # noqa: E501

        :param mode: The mode of this PluginSettings.  # noqa: E501
        :type mode: str
        """

        self._mode = mode

    @property
    def ids(self):
        """Gets the ids of this PluginSettings.  # noqa: E501

        A list of plugin IDs.  # noqa: E501

        :return: The ids of this PluginSettings.  # noqa: E501
        :rtype: list[int]
        """
        return self._ids

    @ids.setter
    def ids(self, ids):
        """Sets the ids of this PluginSettings.

        A list of plugin IDs.  # noqa: E501

        :param ids: The ids of this PluginSettings.  # noqa: E501
        :type ids: list[int]
        """

        self._ids = ids

    @property
    def names(self):
        """Gets the names of this PluginSettings.  # noqa: E501

        A list of plugin names.  # noqa: E501

        :return: The names of this PluginSettings.  # noqa: E501
        :rtype: list[str]
        """
        return self._names

    @names.setter
    def names(self, names):
        """Sets the names of this PluginSettings.

        A list of plugin names.  # noqa: E501

        :param names: The names of this PluginSettings.  # noqa: E501
        :type names: list[str]
        """

        self._names = names

    @property
    def families(self):
        """Gets the families of this PluginSettings.  # noqa: E501

        A list of plugin families.  # noqa: E501

        :return: The families of this PluginSettings.  # noqa: E501
        :rtype: list[str]
        """
        return self._families

    @families.setter
    def families(self, families):
        """Sets the families of this PluginSettings.

        A list of plugin families.  # noqa: E501

        :param families: The families of this PluginSettings.  # noqa: E501
        :type families: list[str]
        """

        self._families = families

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PluginSettings):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, PluginSettings):
            return True

        return self.to_dict() != other.to_dict()
