# InlineResponse20025Control

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The input type (entry or dropdown). | [optional] 
**readable_regest** | **str** | The placeholder for the input. | [optional] 
**regex** | **str** | A regex for checking the value of the input. | [optional] 
**options** | **list[object]** | A list of options if the input is a dropdown. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


