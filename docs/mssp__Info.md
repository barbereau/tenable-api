# Info

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mssp_logo** | **str** | A logo in Base64 format. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


