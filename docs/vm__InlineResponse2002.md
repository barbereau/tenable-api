# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wildcard_fields** | **list[str]** | The fields you can use as a wildcard (&#x60;wf&#x60; parameter) value in the [GET /access-groups](ref:io-v1-access-groups-list) endpoint. | [optional] 
**filters** | [**list[InlineResponse2002Filters]**](vm__InlineResponse2002Filters.md) | The filters and operators for each field you can use when constructing filter (&#x60;f&#x60; parameter) values in the [GET /access-groups](ref:io-v1-access-groups-list) endpoint. | [optional] 
**sort** | [**list[InlineResponse2002Sort]**](vm__InlineResponse2002vm__Sort.md) | The fields you can use when constructing &#x60;sort&#x60; parameter values for the [GET /access-groups](ref:io-v1-access-groups-list) endpoint. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


