# InlineResponse20047DataPluginDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The ID of the plugin. | [optional] 
**name** | **str** | The name of the plugin. | [optional] 
**attributes** | [**list[InlineResponse20047DataAttributes]**](vm__InlineResponse20047DataAttributes.md) | The plugin attributes. For more information, see [Tenable Plugin Attributes](/docs/tenable-plugin-attributes). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


