# ScannerGroupsGroupIdScannersLicense

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The license type. | [optional] 
**ips** | **int** | The number of hosts the scanner is licensed to use. | [optional] 
**agents** | **int** | The number of agents the scanner is licensed to use. | [optional] 
**scanners** | **int** | The number of scanners the scanner is licensed to use. | [optional] 
**apps** | [**ScannerGroupsGroupIdScannersLicenseApps**](vm__ScannerGroupsGroupIdScannersLicenseApps.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


