# Releases

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latest** | **object** |  | [optional] 
**product_name___x_x_x** | [**list[Download]**](downloads__Download.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


