# InlineResponse20025Vulnerabilities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host_id** | **int** | The unique ID of the host where the scan identified the vulnerability. | [optional] 
**hostname** | **str** | The name of the host where the scan identified the vulnerability. | [optional] 
**plugin_id** | **int** | The unique ID of the vulnerability plugin. | [optional] 
**plugin_name** | **str** | The name of the vulnerability plugin. | [optional] 
**plugin_family** | **str** | The parent family of the vulnerability plugin. | [optional] 
**count** | **int** | The number of vulnerabilities found. | [optional] 
**vuln_index** | **int** | The index of the vulnerability plugin. | [optional] 
**severity_index** | **int** | The severity index order of the plugin. | [optional] 
**severity** | **int** | The severity of plugin. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


