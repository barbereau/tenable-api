# InlineResponse20039

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wildcard_fields** | **list[str]** | Array of strings which represent each field which supports \&quot;wildcard\&quot; search. Wildcard search is a mechanism where multiple fields of a record are filtered against one specific filter string. If any one of the supported fields&#39; values matches against the filter string, then the record matches the wildcard filter. Note that for a record to be returned, it must pass the wildcard filter (if there is one) AND the set of standard filters. | [optional] 
**filters** | [**list[InlineResponse20039Filters]**](vm__InlineResponse20039Filters.md) | A list of filters available for the record type. | [optional] 
**sort** | [**InlineResponse20039Sort**](vm__InlineResponse20039vm__Sort.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


