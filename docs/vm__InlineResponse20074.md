# InlineResponse20074

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | **object** |  | [optional] 
**compliance** | [**list[InlineResponse20074Compliance]**](vm__InlineResponse20074Compliance.md) |  | [optional] 
**vulnerabilities** | [**list[InlineResponse20074Vulnerabilities]**](vm__InlineResponse20074Vulnerabilities.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


