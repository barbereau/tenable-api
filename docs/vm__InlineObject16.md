# InlineObject16

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The new name of the managed credential object. This name must be unique within your Tenable.io instance. | [optional] 
**description** | **str** | The new description of the managed credential object. | [optional] 
**ad_hoc** | **bool** | A value specifying if the credential is managed (&#x60;false&#x60;) versus stored in a scan or policy configuration (&#x60;true&#x60;). You can only set this parameter from &#x60;true&#x60; to &#x60;false&#x60;. You cannot set this parameter to &#x60;true&#x60;. If you omit this parameter, the value defaults to &#x60;false&#x60;. | [optional] 
**settings** | [**CredentialsSettings**](vm__CredentialsSettings.md) |  | [optional] 
**permissions** | [**list[CredentialsPermissions]**](vm__CredentialsPermissions.md) | User permissions for the managed credential. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


