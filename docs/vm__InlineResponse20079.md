# InlineResponse20079

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capabilities** | **object** |  | [optional] 
**enterprise** | **bool** |  | [optional] 
**expiration** | **int** |  | [optional] 
**expiration_time** | **int** |  | [optional] 
**idle_timeout** | **int** |  | [optional] 
**license** | **object** |  | [optional] 
**loaded_plugin_set** | **str** |  | [optional] 
**login_banner** | **bool** |  | [optional] 
**nessus_type** | **str** |  | [optional] 
**nessus_ui_version** | **str** |  | [optional] 
**notifications** | **list[str]** |  | [optional] 
**plugin_set** | **str** |  | [optional] 
**scanner_boottime** | **int** |  | [optional] 
**server_version** | **str** |  | [optional] 
**server_uuid** | **str** |  | [optional] 
**update** | [**InlineResponse20079Update**](vm__InlineResponse20079Update.md) |  | [optional] 
**analytics** | **object** |  | [optional] 
**limit_enabled** | **bool** |  | [optional] 
**msp** | **bool** |  | [optional] 
**server_build** | **str** |  | [optional] 
**force_ui_reload** | **bool** |  | [optional] 
**nessus_ui_build** | **str** |  | [optional] 
**container_db_version** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


