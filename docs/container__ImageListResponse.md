# ImageListResponse

A list of images with pagination information.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pagination** | [**Pagination**](container__Pagination.md) |  | [optional] 
**items** | [**list[ImageDetails]**](container__ImageDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


