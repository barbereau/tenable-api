# SettingsConnectorsSchedule

The data import schedule.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**units** | **str** | The units of time for the import interval. Units can include:  - days  - hours  - minutes  - weeks | [optional] 
**value** | **int** | The number of units between import intervals. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


