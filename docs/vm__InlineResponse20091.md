# InlineResponse20091

A list of vulnerabilities.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vulnerabilities** | [**list[InlineResponse20091Vulnerabilities]**](vm__InlineResponse20091Vulnerabilities.md) | A list of discovered vulnerabilities. | [optional] 
**total_vulnerability_count** | **int** | The total number of discovered vulnerabilities. | [optional] 
**total_asset_count** | **int** | The total number of assets. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


