# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20047(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'data': 'InlineResponse20047Data',
        'size': 'int',
        'params': 'InlineResponse20047Params',
        'total_count': 'int'
    }

    attribute_map = {
        'data': 'data',
        'size': 'size',
        'params': 'params',
        'total_count': 'total_count'
    }

    def __init__(self, data=None, size=None, params=None, total_count=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20047 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._data = None
        self._size = None
        self._params = None
        self._total_count = None
        self.discriminator = None

        if data is not None:
            self.data = data
        if size is not None:
            self.size = size
        if params is not None:
            self.params = params
        if total_count is not None:
            self.total_count = total_count

    @property
    def data(self):
        """Gets the data of this InlineResponse20047.  # noqa: E501


        :return: The data of this InlineResponse20047.  # noqa: E501
        :rtype: InlineResponse20047Data
        """
        return self._data

    @data.setter
    def data(self, data):
        """Sets the data of this InlineResponse20047.


        :param data: The data of this InlineResponse20047.  # noqa: E501
        :type data: InlineResponse20047Data
        """

        self._data = data

    @property
    def size(self):
        """Gets the size of this InlineResponse20047.  # noqa: E501

        The number of records in the returned result set.  # noqa: E501

        :return: The size of this InlineResponse20047.  # noqa: E501
        :rtype: int
        """
        return self._size

    @size.setter
    def size(self, size):
        """Sets the size of this InlineResponse20047.

        The number of records in the returned result set.  # noqa: E501

        :param size: The size of this InlineResponse20047.  # noqa: E501
        :type size: int
        """

        self._size = size

    @property
    def params(self):
        """Gets the params of this InlineResponse20047.  # noqa: E501


        :return: The params of this InlineResponse20047.  # noqa: E501
        :rtype: InlineResponse20047Params
        """
        return self._params

    @params.setter
    def params(self, params):
        """Sets the params of this InlineResponse20047.


        :param params: The params of this InlineResponse20047.  # noqa: E501
        :type params: InlineResponse20047Params
        """

        self._params = params

    @property
    def total_count(self):
        """Gets the total_count of this InlineResponse20047.  # noqa: E501

        The total number of available plugin records after Tenable.io applies the last_updated filter.  # noqa: E501

        :return: The total_count of this InlineResponse20047.  # noqa: E501
        :rtype: int
        """
        return self._total_count

    @total_count.setter
    def total_count(self, total_count):
        """Sets the total_count of this InlineResponse20047.

        The total number of available plugin records after Tenable.io applies the last_updated filter.  # noqa: E501

        :param total_count: The total_count of this InlineResponse20047.  # noqa: E501
        :type total_count: int
        """

        self._total_count = total_count

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20047):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20047):
            return True

        return self.to_dict() != other.to_dict()
