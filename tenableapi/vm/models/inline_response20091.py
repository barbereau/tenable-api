# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20091(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'vulnerabilities': 'list[InlineResponse20091Vulnerabilities]',
        'total_vulnerability_count': 'int',
        'total_asset_count': 'int'
    }

    attribute_map = {
        'vulnerabilities': 'vulnerabilities',
        'total_vulnerability_count': 'total_vulnerability_count',
        'total_asset_count': 'total_asset_count'
    }

    def __init__(self, vulnerabilities=None, total_vulnerability_count=None, total_asset_count=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20091 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._vulnerabilities = None
        self._total_vulnerability_count = None
        self._total_asset_count = None
        self.discriminator = None

        if vulnerabilities is not None:
            self.vulnerabilities = vulnerabilities
        if total_vulnerability_count is not None:
            self.total_vulnerability_count = total_vulnerability_count
        if total_asset_count is not None:
            self.total_asset_count = total_asset_count

    @property
    def vulnerabilities(self):
        """Gets the vulnerabilities of this InlineResponse20091.  # noqa: E501

        A list of discovered vulnerabilities.  # noqa: E501

        :return: The vulnerabilities of this InlineResponse20091.  # noqa: E501
        :rtype: list[InlineResponse20091Vulnerabilities]
        """
        return self._vulnerabilities

    @vulnerabilities.setter
    def vulnerabilities(self, vulnerabilities):
        """Sets the vulnerabilities of this InlineResponse20091.

        A list of discovered vulnerabilities.  # noqa: E501

        :param vulnerabilities: The vulnerabilities of this InlineResponse20091.  # noqa: E501
        :type vulnerabilities: list[InlineResponse20091Vulnerabilities]
        """

        self._vulnerabilities = vulnerabilities

    @property
    def total_vulnerability_count(self):
        """Gets the total_vulnerability_count of this InlineResponse20091.  # noqa: E501

        The total number of discovered vulnerabilities.  # noqa: E501

        :return: The total_vulnerability_count of this InlineResponse20091.  # noqa: E501
        :rtype: int
        """
        return self._total_vulnerability_count

    @total_vulnerability_count.setter
    def total_vulnerability_count(self, total_vulnerability_count):
        """Sets the total_vulnerability_count of this InlineResponse20091.

        The total number of discovered vulnerabilities.  # noqa: E501

        :param total_vulnerability_count: The total_vulnerability_count of this InlineResponse20091.  # noqa: E501
        :type total_vulnerability_count: int
        """

        self._total_vulnerability_count = total_vulnerability_count

    @property
    def total_asset_count(self):
        """Gets the total_asset_count of this InlineResponse20091.  # noqa: E501

        The total number of assets.  # noqa: E501

        :return: The total_asset_count of this InlineResponse20091.  # noqa: E501
        :rtype: int
        """
        return self._total_asset_count

    @total_asset_count.setter
    def total_asset_count(self, total_asset_count):
        """Sets the total_asset_count of this InlineResponse20091.

        The total number of assets.  # noqa: E501

        :param total_asset_count: The total_asset_count of this InlineResponse20091.  # noqa: E501
        :type total_asset_count: int
        """

        self._total_asset_count = total_asset_count

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20091):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20091):
            return True

        return self.to_dict() != other.to_dict()
