# InlineResponse20061

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scanner_uuid** | **str** | The UUID of the scanner the scan belongs to. | [optional] 
**name** | **str** | The name of the scan. | [optional] 
**status** | **str** | Scan status. Can be one of the following values: pending, processing, stopping, pausing, paused, resuming, or running. | [optional] 
**id** | **str** | The scan UUID. | [optional] 
**scan_id** | **int** | The ID of the scan. | [optional] 
**user** | **str** | The username of the owner of the scan. | [optional] 
**last_modification_date** | **int** | The last time the scan was modified. | [optional] 
**start_time** | **int** | When the scan was started. | [optional] 
**remote** | **bool** | Indicates whether the scan is running remotely (&#39;true&#39;) or not (&#39;false&#39;). | [optional] 
**network_id** | **str** | The ID of the network object associated with the scanner currently running the scan. The default network ID is &#x60;00000000-0000-0000-0000-000000000000&#x60;. To determine the ID of a custom network, use the [GET /networks](ref:networks-list) endpoint. For more information about network objects, see [Manage Networks](doc:manage-networks-tio). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


