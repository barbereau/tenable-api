# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The unique ID of the job. | [optional] 
**user_uuid** | **str** | The unique ID of the user who started the import job. | [optional] 
**start_time** | **str** | The start time of the job. | [optional] 
**end_time** | **str** | The end time of the job. | [optional] 
**asset_count** | **int** | The total number of assets uploaded for import. | [optional] 
**imported_asset_count** | **int** | The number of assets successfully imported. | [optional] 
**failed_asset_count** | **int** | The number of assets that failed to import. | [optional] 
**status** | **str** | The status of the job. | [optional] 
**error_message** | **str** | The description of why a job failed. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


