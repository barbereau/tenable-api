# InlineResponse20060

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scanner_id** | **int** | The ID of the scanner. | [optional] 
**instance_id** | **str** | Unique instance identifier from Amazon. | [optional] 
**private_ip** | **str** | Private IP address of the AWS instance. | [optional] 
**public_ip** | **str** | Public IP address of the AWS instance. | [optional] 
**state** | **str** | The state of the instance. Can be one of the following values: &#x60;running&#x60;, &#x60;stopped&#x60;, or &#x60;terminated&#x60;. | [optional] 
**zone** | **str** | The availability zone for the instance. Example: &#x60;us-east-1a&#x60;, &#x60;us-east-1b&#x60;, etc. | [optional] 
**type** | **str** | The size of the instance. Example: &#x60;t2.small&#x60;, &#x60;t2.medium&#x60;, etc. | [optional] 
**name** | **str** | The user-defined name of the instance. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


