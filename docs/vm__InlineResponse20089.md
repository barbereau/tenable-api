# InlineResponse20089

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acls** | [**list[InlineResponse20046]**](vm__InlineResponse20046.md) | The Access Control Lists applicable to the group. | [optional] 
**id** | **int** | The unique ID of the group. | [optional] 
**default_group** | **bool** | If true, this group is the default. | [optional] 
**name** | **str** | The name of the group. | [optional] 
**members** | **str** | The members of the group. | [optional] 
**type** | **str** | The group type (user or system). Note that the system group type is deprecated. Tenable recommends that you create only user target groups. | [optional] 
**owner** | **str** | The name of the owner of the group. A user of &#x60;nessus_ms_agent&#x60; indicates it is a system target group. | [optional] 
**owner_id** | **int** | The unique ID of the owner of the group. | [optional] 
**last_modification_date** | **int** | The last modification date for the group in unixtime. | [optional] 
**shared** | **int** | The shared status of the group. | [optional] 
**user_permissions** | **int** | The current user permissions for the group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


