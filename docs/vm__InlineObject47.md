# InlineObject47

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**network_uuid** | **str** | Specify a value as follows:&lt;ul&gt;&lt;li&gt;If your scans involve separate environments with overlapping IP ranges, specify the UUID of the [network](doc:manage-networks-tio) you want to associate with the results of the auto-routed scan. This value must match the network where you have assigned the scanner groups that you configured for scan routing.&lt;/li&gt;&lt;li&gt;Otherwise, specify the default network (00000000-0000-0000-0000-000000000000).&lt;/li&gt;&lt;/ul&gt; | 
**tags** | **list[str]** | A list of asset tags UUIDs. | [optional] 
**text_targets** | **str** | A comma-delimited list of targets to scan. For a full list of supported target formats, see the [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Scans/AboutScanTargets.htm). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


