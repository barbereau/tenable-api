# tenableapi.waslegacy.ScansApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_scans_attachments**](waslegacy__ScansApi.md#was_scans_attachments) | **GET** /scans/{scan_id}/attachments/{attachment_id} | Get scan attachment file
[**was_scans_configure**](waslegacy__ScansApi.md#was_scans_configure) | **PUT** /scans/{scan_id} | Update scan
[**was_scans_copy**](waslegacy__ScansApi.md#was_scans_copy) | **POST** /scans/{scan_id}/copy | Copy scan
[**was_scans_create**](waslegacy__ScansApi.md#was_scans_create) | **POST** /scans | Create scan
[**was_scans_delete**](waslegacy__ScansApi.md#was_scans_delete) | **DELETE** /scans/{scan_id} | Delete scan
[**was_scans_delete_history**](waslegacy__ScansApi.md#was_scans_delete_history) | **DELETE** /scans/{scan_id}/history/{history_uuid} | Delete scan history
[**was_scans_details**](waslegacy__ScansApi.md#was_scans_details) | **GET** /scans/{scan_uuid} | Get scan details
[**was_scans_export_download**](waslegacy__ScansApi.md#was_scans_export_download) | **GET** /scans/{scan_id}/export/{file_uuid}/download | Download exported scan
[**was_scans_export_request**](waslegacy__ScansApi.md#was_scans_export_request) | **POST** /scans/{scan_id}/export | Export scan
[**was_scans_export_status**](waslegacy__ScansApi.md#was_scans_export_status) | **GET** /scans/{scan_id}/export/{file_uuid}/status | Check scan export status
[**was_scans_history**](waslegacy__ScansApi.md#was_scans_history) | **GET** /scans/{scan_id}/history | Get scan history
[**was_scans_history_details**](waslegacy__ScansApi.md#was_scans_history_details) | **GET** /scans/{scan_id}/history/{history_uuid} | Get scan history details
[**was_scans_host_details**](waslegacy__ScansApi.md#was_scans_host_details) | **GET** /was-query/scans/{scan_uuid}/hosts/{host_id} | Return host details
[**was_scans_import**](waslegacy__ScansApi.md#was_scans_import) | **POST** /scans/import | Import scan
[**was_scans_launch**](waslegacy__ScansApi.md#was_scans_launch) | **POST** /scans/{scan_id}/launch | Launch scan
[**was_scans_list**](waslegacy__ScansApi.md#was_scans_list) | **GET** /scans | List scans
[**was_scans_plugin_output**](waslegacy__ScansApi.md#was_scans_plugin_output) | **GET** /was-query/scans/{scan_uuid}/hosts/{host_id}/plugins/{plugin_id} | Get plugin output
[**was_scans_progress**](waslegacy__ScansApi.md#was_scans_progress) | **GET** /was-query/scans/{scan_uuid}/progress | Check scan status
[**was_scans_read_status**](waslegacy__ScansApi.md#was_scans_read_status) | **PUT** /scans/{scan_id}/status | Update scan status
[**was_scans_schedule**](waslegacy__ScansApi.md#was_scans_schedule) | **PUT** /scans/{scan_id}/schedule | Enable schedule
[**was_scans_stop**](waslegacy__ScansApi.md#was_scans_stop) | **POST** /scans/{scan_id}/stop | Stop scan
[**was_scans_timezones**](waslegacy__ScansApi.md#was_scans_timezones) | **GET** /scans/timezones | Get timezones


# **was_scans_attachments**
> object was_scans_attachments(scan_id, attachment_id, key)

Get scan attachment file

Gets the requested scan attachment file.<p>Requires CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan containing the attachment.
attachment_id = 56 # int | The ID of the scan attachment.
key = 'key_example' # str | The attachment access token.

    try:
        # Get scan attachment file
        api_response = api_instance.was_scans_attachments(scan_id, attachment_id, key)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_attachments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan containing the attachment. | 
 **attachment_id** | **int**| The ID of the scan attachment. | 
 **key** | **str**| The attachment access token. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the attachment file. |  -  |
**404** | Returned if the attachment file does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_configure**
> InlineResponse20016Scans was_scans_configure(scan_id, inline_object9)

Update scan

Updates the scan configuration. For example, you can enable or disable a scan, change the scan name, description, folder, scanner, targets, and schedule parameters. <b>Note: </b>You can specify scan targets as text, input file, or target groups.<p>Requires CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan to change.
inline_object9 = tenableapi.waslegacy.InlineObject9() # InlineObject9 | 

    try:
        # Update scan
        api_response = api_instance.was_scans_configure(scan_id, inline_object9)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_configure: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan to change. | 
 **inline_object9** | [**InlineObject9**](waslegacy__InlineObject9.md)|  | 

### Return type

[**InlineResponse20016Scans**](waslegacy__InlineResponse20016Scans.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the configuration was changed. |  -  |
**404** | Returned if the scan does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an error occurred while saving the configuration. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_copy**
> InlineResponse20016Scans was_scans_copy(scan_id, inline_object13)

Copy scan

Copies the given scan.<p>Requires CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan to copy.
inline_object13 = tenableapi.waslegacy.InlineObject13() # InlineObject13 | 

    try:
        # Copy scan
        api_response = api_instance.was_scans_copy(scan_id, inline_object13)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_copy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan to copy. | 
 **inline_object13** | [**InlineObject13**](waslegacy__InlineObject13.md)|  | 

### Return type

[**InlineResponse20016Scans**](waslegacy__InlineResponse20016Scans.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the copied scan object. |  -  |
**404** | Returned if the scan does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an error occurred while copying. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_create**
> InlineResponse20016Scans was_scans_create(inline_object8)

Create scan

Creates a scan.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    inline_object8 = tenableapi.waslegacy.InlineObject8() # InlineObject8 | 

    try:
        # Create scan
        api_response = api_instance.was_scans_create(inline_object8)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object8** | [**InlineObject8**](waslegacy__InlineObject8.md)|  | 

### Return type

[**InlineResponse20016Scans**](waslegacy__InlineResponse20016Scans.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the scan was saved successfully. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an error occurred while saving the scan. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_delete**
> object was_scans_delete(scan_id)

Delete scan

Deletes a scan. **Note:** You cannot delete scans in running, paused, or stopping states.<p>Requires CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan to delete.

    try:
        # Delete scan
        api_response = api_instance.was_scans_delete(scan_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan to delete. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deleted the scan. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io failed to delete the scan. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_delete_history**
> object was_scans_delete_history(scan_id, history_uuid)

Delete scan history

Deletes historical results from a scan.<p>Requires CAN CONFIGURE [64] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan.
history_uuid = 'history_uuid_example' # str | The ID of the results to delete.

    try:
        # Delete scan history
        api_response = api_instance.was_scans_delete_history(scan_id, history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_delete_history: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan. | 
 **history_uuid** | [**str**](.md)| The ID of the results to delete. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deleted the results. |  -  |
**404** | Returned if Tenable.io could not find the results. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io failed to delete the results. |  -  |
**501** | Returned if Tenable.io does not support deleting historical scan results. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_details**
> InlineResponse20017 was_scans_details(scan_uuid, history_id=history_id, history_uuid=history_uuid)

Get scan details

Returns details for the specified scan.<p>Requires CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_uuid = 'scan_uuid_example' # str | The UUID of the scan to retrieve. While scan UUID is preferred, scan ID is supported.
history_id = 56 # int | The ID of the historical data that should be returned. (optional)
history_uuid = 56 # int | The UUID of the historical data that should be returned. (optional)

    try:
        # Get scan details
        api_response = api_instance.was_scans_details(scan_uuid, history_id=history_id, history_uuid=history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_uuid** | **str**| The UUID of the scan to retrieve. While scan UUID is preferred, scan ID is supported. | 
 **history_id** | **int**| The ID of the historical data that should be returned. | [optional] 
 **history_uuid** | **int**| The UUID of the historical data that should be returned. | [optional] 

### Return type

[**InlineResponse20017**](waslegacy__InlineResponse20017.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scan details. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_export_download**
> object was_scans_export_download(scan_id, file_uuid, type=type)

Download exported scan

Download an exported scan.<p>Requires CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan to export.
file_uuid = 'file_uuid_example' # str | The ID of the file to download (Included in response from /scans/{scan\\_id}/export).
type = 'type_example' # str | The value `web-app`. This parameter is required only when using the API with Web Application Scanning. (optional)

    try:
        # Download exported scan
        api_response = api_instance.was_scans_export_download(scan_id, file_uuid, type=type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_export_download: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan to export. | 
 **file_uuid** | **str**| The ID of the file to download (Included in response from /scans/{scan\\_id}/export). | 
 **type** | **str**| The value &#x60;web-app&#x60;. This parameter is required only when using the API with Web Application Scanning. | [optional] 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the content of the file as an attachment. |  -  |
**404** | Returned if the file does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_export_request**
> InlineResponse20021 was_scans_export_request(scan_id, type, inline_object15, history_id=history_id, history_uuid=history_uuid)

Export scan

Export the given scan. To see the status of the requested export, submit a [scan export status](ref:was-scan-export-status] request. On receiving a \"ready\" status from the was-export-status request, download the export file using the [scan export download](ref:was-scans-export-download) method.<p>Requires CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan to export.
type = 'type_example' # str | The value `web-app`. This parameter is required only when using the API with Web Application Scanning.
inline_object15 = tenableapi.waslegacy.InlineObject15() # InlineObject15 | 
history_id = 56 # int | The ID of the historical data that should be exported. (optional)
history_uuid = 56 # int | The UUID of the historical data that should be returned. (optional)

    try:
        # Export scan
        api_response = api_instance.was_scans_export_request(scan_id, type, inline_object15, history_id=history_id, history_uuid=history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_export_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan to export. | 
 **type** | **str**| The value &#x60;web-app&#x60;. This parameter is required only when using the API with Web Application Scanning. | 
 **inline_object15** | [**InlineObject15**](waslegacy__InlineObject15.md)|  | 
 **history_id** | **int**| The ID of the historical data that should be exported. | [optional] 
 **history_uuid** | **int**| The UUID of the historical data that should be returned. | [optional] 

### Return type

[**InlineResponse20021**](waslegacy__InlineResponse20021.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the export was queued successfully. |  -  |
**400** | Returned if a required parameter is missing. |  -  |
**404** | Returned if the scan does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_export_status**
> InlineResponse20022 was_scans_export_status(scan_id, file_uuid, type)

Check scan export status

Check the file status of an exported scan. After you request an export, you must poll this endpoint until a \"ready\" status is returned, at which point the file is complete and can be downloaded using the [>was-export-download](ref:was-scans-export-download) endpoint.<p>Requires CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan to export.
file_uuid = 'file_uuid_example' # str | The ID of the file to poll (Included in response from /was-scans/{scan\\_id}/export).
type = 'type_example' # str | The value `web-app`. This parameter is required only when using the API with Web Application Scanning.

    try:
        # Check scan export status
        api_response = api_instance.was_scans_export_status(scan_id, file_uuid, type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_export_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan to export. | 
 **file_uuid** | **str**| The ID of the file to poll (Included in response from /was-scans/{scan\\_id}/export). | 
 **type** | **str**| The value &#x60;web-app&#x60;. This parameter is required only when using the API with Web Application Scanning. | 

### Return type

[**InlineResponse20022**](waslegacy__InlineResponse20022.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the status of the file. A status of &#x60;ready&#x60; indicates the file can be downloaded. |  -  |
**404** | Returned if the file does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_history**
> InlineResponse20023 was_scans_history(scan_id, limit=limit, offset=offset)

Get scan history

Returns a scan's history records.<p>Requires SCAN OPERATOR [24] user permissions and CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 'scan_id_example' # str | The identifier for the scan. This identifier can be the either the `schedule_uuid` or the numeric `id` attribute for the scan. We recommend that you use `schedule_uuid`.
limit = 56 # int | Maximum number of objects requested (or service imposed limit if not in request). The max limit value allowed is 50. Must be in the int32 format. (optional)
offset = 56 # int | Offset from request (or zero). Must be in the int32 format. (optional)

    try:
        # Get scan history
        api_response = api_instance.was_scans_history(scan_id, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_history: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **str**| The identifier for the scan. This identifier can be the either the &#x60;schedule_uuid&#x60; or the numeric &#x60;id&#x60; attribute for the scan. We recommend that you use &#x60;schedule_uuid&#x60;. | 
 **limit** | **int**| Maximum number of objects requested (or service imposed limit if not in request). The max limit value allowed is 50. Must be in the int32 format. | [optional] 
 **offset** | **int**| Offset from request (or zero). Must be in the int32 format. | [optional] 

### Return type

[**InlineResponse20023**](waslegacy__InlineResponse20023.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a scan&#39;s history records. |  -  |
**404** | Returned if Tenable.io cannot find the specified &#x60;scan_id&#x60;. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_history_details**
> InlineResponse20024 was_scans_history_details(scan_id, history_uuid)

Get scan history details

Returns the details of a previous result of a scan.<p>Requires CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan to retrieve.
history_uuid = 'history_uuid_example' # str | The UUID of the historical scan result to return details about.

    try:
        # Get scan history details
        api_response = api_instance.was_scans_history_details(scan_id, history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_history_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan to retrieve. | 
 **history_uuid** | **str**| The UUID of the historical scan result to return details about. | 

### Return type

[**InlineResponse20024**](waslegacy__InlineResponse20024.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns details of the historical scan result. |  -  |
**404** | Returned if scan_id or history_uuid are not found. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_host_details**
> InlineResponse20025 was_scans_host_details(scan_uuid, host_id, history_id=history_id, history_uuid=history_uuid)

Return host details

Returns details for the specified host.<p>Requires CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_uuid = 'scan_uuid_example' # str | The UUID of the scan to retrieve. While scan UUID is preferred, scan ID is supported.
host_id = 56 # int | The ID of the host to retrieve.
history_id = 56 # int | The ID of the historical data that should be returned. (optional)
history_uuid = 56 # int | The UUID of the historical data that should be returned. (optional)

    try:
        # Return host details
        api_response = api_instance.was_scans_host_details(scan_uuid, host_id, history_id=history_id, history_uuid=history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_host_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_uuid** | **str**| The UUID of the scan to retrieve. While scan UUID is preferred, scan ID is supported. | 
 **host_id** | **int**| The ID of the host to retrieve. | 
 **history_id** | **int**| The ID of the historical data that should be returned. | [optional] 
 **history_uuid** | **int**| The UUID of the historical data that should be returned. | [optional] 

### Return type

[**InlineResponse20025**](waslegacy__InlineResponse20025.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the host details. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_import**
> InlineResponse20016Scans was_scans_import(inline_object14)

Import scan

Import an existing scan uploaded using [file: upload](ref:file-upload-1).<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    inline_object14 = tenableapi.waslegacy.InlineObject14() # InlineObject14 | 

    try:
        # Import scan
        api_response = api_instance.was_scans_import(inline_object14)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_import: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object14** | [**InlineObject14**](waslegacy__InlineObject14.md)|  | 

### Return type

[**InlineResponse20016Scans**](waslegacy__InlineResponse20016Scans.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scan object. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io failed to import the scan. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_launch**
> InlineResponse20018 was_scans_launch(scan_id, inline_object10)

Launch scan

Launches a scan.<p>Requires CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan to launch.
inline_object10 = tenableapi.waslegacy.InlineObject10() # InlineObject10 | 

    try:
        # Launch scan
        api_response = api_instance.was_scans_launch(scan_id, inline_object10)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_launch: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan to launch. | 
 **inline_object10** | [**InlineObject10**](waslegacy__InlineObject10.md)|  | 

### Return type

[**InlineResponse20018**](waslegacy__InlineResponse20018.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the scan was successfully launched. |  -  |
**403** | Returned if the scan is disabled. |  -  |
**404** | Returned if the scan does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_list**
> InlineResponse20016 was_scans_list(folder_id=folder_id, last_modification_date=last_modification_date)

List scans

Returns the scan list. Scans of all types are returned.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    folder_id = 56 # int | The ID of the folder whose scans should be listed. (optional)
last_modification_date = 56 # int | Limit the results to those that have only changed since this time. (optional)

    try:
        # List scans
        api_response = api_instance.was_scans_list(folder_id=folder_id, last_modification_date=last_modification_date)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **folder_id** | **int**| The ID of the folder whose scans should be listed. | [optional] 
 **last_modification_date** | **int**| Limit the results to those that have only changed since this time. | [optional] 

### Return type

[**InlineResponse20016**](waslegacy__InlineResponse20016.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scan list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_plugin_output**
> InlineResponse20026 was_scans_plugin_output(scan_uuid, host_id, plugin_id, history_id=history_id, history_uuid=history_uuid)

Get plugin output

Returns the output for a given plugin.<p>Requires CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_uuid = 'scan_uuid_example' # str | The UUID of the scan to retrieve. While scan UUID is preferred, scan ID is supported.
host_id = 56 # int | The ID of the host to retrieve.
plugin_id = 56 # int | The ID of the plugin to retrieve.
history_id = 56 # int | The ID of the historical data that should be returned. (optional)
history_uuid = 56 # int | The UUID of the historical data that should be returned. (optional)

    try:
        # Get plugin output
        api_response = api_instance.was_scans_plugin_output(scan_uuid, host_id, plugin_id, history_id=history_id, history_uuid=history_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_plugin_output: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_uuid** | **str**| The UUID of the scan to retrieve. While scan UUID is preferred, scan ID is supported. | 
 **host_id** | **int**| The ID of the host to retrieve. | 
 **plugin_id** | **int**| The ID of the plugin to retrieve. | 
 **history_id** | **int**| The ID of the historical data that should be returned. | [optional] 
 **history_uuid** | **int**| The UUID of the historical data that should be returned. | [optional] 

### Return type

[**InlineResponse20026**](waslegacy__InlineResponse20026.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the plugin output. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_progress**
> InlineResponse20020 was_scans_progress(scan_uuid)

Check scan status

Retrieve the status of a web application scan that is currently running. Data returned includes scan statistics and the number of plugins identified so far.<p>Requires CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_uuid = 'scan_uuid_example' # str | The UUID of the scan for which you want to view the progress.

    try:
        # Check scan status
        api_response = api_instance.was_scans_progress(scan_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_progress: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_uuid** | [**str**](.md)| The UUID of the scan for which you want to view the progress. | 

### Return type

[**InlineResponse20020**](waslegacy__InlineResponse20020.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the progress of the scan. |  -  |
**404** | Returned if the scan does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_read_status**
> object was_scans_read_status(scan_id, inline_object12)

Update scan status

Changes the status of a scan.<p>Requires CAN VIEW [16] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan to change.
inline_object12 = tenableapi.waslegacy.InlineObject12() # InlineObject12 | 

    try:
        # Update scan status
        api_response = api_instance.was_scans_read_status(scan_id, inline_object12)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_read_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan to change. | 
 **inline_object12** | [**InlineObject12**](waslegacy__InlineObject12.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io changed the status. |  -  |
**404** | Returned if the scan does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_schedule**
> InlineResponse20019 was_scans_schedule(scan_id, inline_object11)

Enable schedule

Enables or disables a scan schedule.<p>Requires CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan.
inline_object11 = tenableapi.waslegacy.InlineObject11() # InlineObject11 | 

    try:
        # Enable schedule
        api_response = api_instance.was_scans_schedule(scan_id, inline_object11)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_schedule: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan. | 
 **inline_object11** | [**InlineObject11**](waslegacy__InlineObject11.md)|  | 

### Return type

[**InlineResponse20019**](waslegacy__InlineResponse20019.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io enabled or disabled the scan schedule. |  -  |
**404** | Returned if the scan does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if the scan does not have a schedule to enable. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_stop**
> object was_scans_stop(scan_id)

Stop scan

Stops a scan.<p>Requires CAN CONTROL [32] scan permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    scan_id = 56 # int | The ID of the scan to stop.

    try:
        # Stop scan
        api_response = api_instance.was_scans_stop(scan_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_stop: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scan_id** | **int**| The ID of the scan to stop. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully queued the scan to stop. |  -  |
**404** | Returned if the scan does not exist. |  -  |
**409** | Returned if the scan is not active. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scans_timezones**
> list[InlineResponse20027] was_scans_timezones()

Get timezones

Returns the timezones list for creating a scan.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScansApi(api_client)
    
    try:
        # Get timezones
        api_response = api_instance.was_scans_timezones()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScansApi->was_scans_timezones: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse20027]**](waslegacy__InlineResponse20027.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the timezone list. |  -  |
**403** | Returned if the user does not have permission to view timezones. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

