# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class ScannerGroupsGroupIdScannersLicenseApps(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'type': 'str',
        'consec': 'ScannerGroupsGroupIdScannersLicenseAppsConsec',
        'was': 'ScannerGroupsGroupIdScannersLicenseAppsWas'
    }

    attribute_map = {
        'type': 'type',
        'consec': 'consec',
        'was': 'was'
    }

    def __init__(self, type=None, consec=None, was=None, local_vars_configuration=None):  # noqa: E501
        """ScannerGroupsGroupIdScannersLicenseApps - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._type = None
        self._consec = None
        self._was = None
        self.discriminator = None

        if type is not None:
            self.type = type
        if consec is not None:
            self.consec = consec
        if was is not None:
            self.was = was

    @property
    def type(self):
        """Gets the type of this ScannerGroupsGroupIdScannersLicenseApps.  # noqa: E501

        The Tenable products licensed on the scanner.  # noqa: E501

        :return: The type of this ScannerGroupsGroupIdScannersLicenseApps.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this ScannerGroupsGroupIdScannersLicenseApps.

        The Tenable products licensed on the scanner.  # noqa: E501

        :param type: The type of this ScannerGroupsGroupIdScannersLicenseApps.  # noqa: E501
        :type type: str
        """

        self._type = type

    @property
    def consec(self):
        """Gets the consec of this ScannerGroupsGroupIdScannersLicenseApps.  # noqa: E501


        :return: The consec of this ScannerGroupsGroupIdScannersLicenseApps.  # noqa: E501
        :rtype: ScannerGroupsGroupIdScannersLicenseAppsConsec
        """
        return self._consec

    @consec.setter
    def consec(self, consec):
        """Sets the consec of this ScannerGroupsGroupIdScannersLicenseApps.


        :param consec: The consec of this ScannerGroupsGroupIdScannersLicenseApps.  # noqa: E501
        :type consec: ScannerGroupsGroupIdScannersLicenseAppsConsec
        """

        self._consec = consec

    @property
    def was(self):
        """Gets the was of this ScannerGroupsGroupIdScannersLicenseApps.  # noqa: E501


        :return: The was of this ScannerGroupsGroupIdScannersLicenseApps.  # noqa: E501
        :rtype: ScannerGroupsGroupIdScannersLicenseAppsWas
        """
        return self._was

    @was.setter
    def was(self, was):
        """Sets the was of this ScannerGroupsGroupIdScannersLicenseApps.


        :param was: The was of this ScannerGroupsGroupIdScannersLicenseApps.  # noqa: E501
        :type was: ScannerGroupsGroupIdScannersLicenseAppsWas
        """

        self._was = was

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ScannerGroupsGroupIdScannersLicenseApps):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, ScannerGroupsGroupIdScannersLicenseApps):
            return True

        return self.to_dict() != other.to_dict()
