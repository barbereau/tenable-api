# AuditSettings

The settings that allow the scan to audit elements other than the ones detected during crawling.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forms** | **bool** | Indicates whether to scan for form vulnerabilities (&#x60;true&#x60;). | [optional] [default to False]
**cookies** | **bool** | Indicates whether to scan for cookie vulnerabilities (&#x60;true&#x60;). | [optional] [default to False]
**ui_forms** | **bool** | Indicates whether to scan input and button groups associated with JavaScript code (&#x60;true&#x60;). | [optional] [default to False]
**ui_inputs** | **bool** | Indicates whether to scan orphan input elements with associated DOM events (&#x60;true&#x60;). | [optional] [default to False]
**headers** | **bool** | Indicates whether to scan headers for vulnerabilities and insecure configurations (&#x60;true&#x60;); for example, missing X-Frame-Options. | [optional] [default to False]
**links** | **bool** | Indicates whether to scan links and their parameters in vulnerability checks (&#x60;true&#x60;). | [optional] [default to False]
**parameter_names** | **bool** | Indicates whether to perform extensive fuzzing of parameter names (&#x60;true&#x60;). | [optional] [default to False]
**parameter_values** | **bool** | Indicates whether to perform performs extensive fuzzing of parameter values (&#x60;true&#x60;). | [optional] [default to False]
**jsons** | **bool** | Indicates whether to scan JSON request data (&#x60;true&#x60;). | [optional] [default to False]
**xmls** | **bool** | Indicates whether to scan XML request data (&#x60;true&#x60;). | [optional] [default to False]
**cookies_extensively** | **bool** | Indicates whether an assessment is enabled (&#x60;true&#x60;) or disabled (&#x60;false&#x60;). | [optional] 
**with_raw_payloads** | **bool** | Indicates whether parameters should be tested with their raw payloads (&#x60;true&#x60;) or not (&#x60;false&#x60;). | [optional] 
**with_both_http_methods** | **bool** | Indicates if both GET and POST methods should be used during an assessment (&#x60;true&#x60;) or not (&#x60;false&#x60;). | [optional] 
**with_extra_parameter** | **bool** | Indicates whether to test non-existing parameters (&#x60;true&#x60;) or disabled (&#x60;false&#x60;). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


