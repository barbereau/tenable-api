# tenableapi.downloads.DownloadsApi

All URIs are relative to *https://www.tenable.com/downloads/api/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**pages_get**](downloads__DownloadsApi.md#pages_get) | **GET** /pages | List product pages
[**pages_slug_files_file_get**](downloads__DownloadsApi.md#pages_slug_files_file_get) | **GET** /pages/{slug}/files/{file} | Download a file
[**pages_slug_get**](downloads__DownloadsApi.md#pages_slug_get) | **GET** /pages/{slug} | List downloadable files for a product


# **pages_get**
> list[Page] pages_get()

List product pages

Returns a list of product pages.

### Example

* Api Key Authentication (Bearer):
```python
from __future__ import print_function
import time
import tenableapi.downloads
from tenableapi.downloads.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://www.tenable.com/downloads/api/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.downloads.Configuration(
    host = "https://www.tenable.com/downloads/api/v2"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: Bearer
configuration.api_key['Bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.downloads.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.downloads.DownloadsApi(api_client)
    
    try:
        # List product pages
        api_response = api_instance.pages_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DownloadsApi->pages_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Page]**](downloads__Page.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | An array of product pages. |  -  |
**401** | Unauthorized |  -  |
**500** | Internal Server Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pages_slug_files_file_get**
> pages_slug_files_file_get(slug, file)

Download a file

Downloads a requested file.

### Example

* Api Key Authentication (Bearer):
```python
from __future__ import print_function
import time
import tenableapi.downloads
from tenableapi.downloads.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://www.tenable.com/downloads/api/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.downloads.Configuration(
    host = "https://www.tenable.com/downloads/api/v2"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: Bearer
configuration.api_key['Bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.downloads.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.downloads.DownloadsApi(api_client)
    slug = 'slug_example' # str | Product page slug, for example, `nessus`.
file = 'file_example' # str | File name, for example,. `Nessus-latest-x64.msi`.

    try:
        # Download a file
        api_instance.pages_slug_files_file_get(slug, file)
    except ApiException as e:
        print("Exception when calling DownloadsApi->pages_slug_files_file_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **slug** | **str**| Product page slug, for example, &#x60;nessus&#x60;. | 
 **file** | **str**| File name, for example,. &#x60;Nessus-latest-x64.msi&#x60;. | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Requested file |  -  |
**401** | Unauthorized |  -  |
**404** | File or Page Not Found |  -  |
**500** | Internal Server Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pages_slug_get**
> InlineResponse200 pages_slug_get(slug)

List downloadable files for a product

Returns a JSON hash of all download files for a given product page.

### Example

* Api Key Authentication (Bearer):
```python
from __future__ import print_function
import time
import tenableapi.downloads
from tenableapi.downloads.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://www.tenable.com/downloads/api/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.downloads.Configuration(
    host = "https://www.tenable.com/downloads/api/v2"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: Bearer
configuration.api_key['Bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.downloads.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.downloads.DownloadsApi(api_client)
    slug = 'slug_example' # str | Product page slug, for example, `nessus`.

    try:
        # List downloadable files for a product
        api_response = api_instance.pages_slug_get(slug)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DownloadsApi->pages_slug_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **slug** | **str**| Product page slug, for example, &#x60;nessus&#x60;. | 

### Return type

[**InlineResponse200**](downloads__InlineResponse200.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A JSON hash of releases for a given product |  -  |
**401** | Unauthorized |  -  |
**404** | Page Not Found |  -  |
**500** | Internal Server Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

