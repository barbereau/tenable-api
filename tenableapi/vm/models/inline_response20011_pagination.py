# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20011Pagination(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'total': 'int',
        'offset': 'int',
        'limit': 'int',
        'sort': 'list[str]'
    }

    attribute_map = {
        'total': 'total',
        'offset': 'offset',
        'limit': 'limit',
        'sort': 'sort'
    }

    def __init__(self, total=None, offset=None, limit=None, sort=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20011Pagination - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._total = None
        self._offset = None
        self._limit = None
        self._sort = None
        self.discriminator = None

        if total is not None:
            self.total = total
        if offset is not None:
            self.offset = offset
        if limit is not None:
            self.limit = limit
        if sort is not None:
            self.sort = sort

    @property
    def total(self):
        """Gets the total of this InlineResponse20011Pagination.  # noqa: E501

        The total number of records which match any applied filters. This number may be approximate.  # noqa: E501

        :return: The total of this InlineResponse20011Pagination.  # noqa: E501
        :rtype: int
        """
        return self._total

    @total.setter
    def total(self, total):
        """Sets the total of this InlineResponse20011Pagination.

        The total number of records which match any applied filters. This number may be approximate.  # noqa: E501

        :param total: The total of this InlineResponse20011Pagination.  # noqa: E501
        :type total: int
        """

        self._total = total

    @property
    def offset(self):
        """Gets the offset of this InlineResponse20011Pagination.  # noqa: E501

        The index of the first record retrieved.  # noqa: E501

        :return: The offset of this InlineResponse20011Pagination.  # noqa: E501
        :rtype: int
        """
        return self._offset

    @offset.setter
    def offset(self, offset):
        """Sets the offset of this InlineResponse20011Pagination.

        The index of the first record retrieved.  # noqa: E501

        :param offset: The offset of this InlineResponse20011Pagination.  # noqa: E501
        :type offset: int
        """

        self._offset = offset

    @property
    def limit(self):
        """Gets the limit of this InlineResponse20011Pagination.  # noqa: E501

        The number of records returned with this response.  # noqa: E501

        :return: The limit of this InlineResponse20011Pagination.  # noqa: E501
        :rtype: int
        """
        return self._limit

    @limit.setter
    def limit(self, limit):
        """Sets the limit of this InlineResponse20011Pagination.

        The number of records returned with this response.  # noqa: E501

        :param limit: The limit of this InlineResponse20011Pagination.  # noqa: E501
        :type limit: int
        """

        self._limit = limit

    @property
    def sort(self):
        """Gets the sort of this InlineResponse20011Pagination.  # noqa: E501

        The sorting parameters applied to response, in order of application.  # noqa: E501

        :return: The sort of this InlineResponse20011Pagination.  # noqa: E501
        :rtype: list[str]
        """
        return self._sort

    @sort.setter
    def sort(self, sort):
        """Sets the sort of this InlineResponse20011Pagination.

        The sorting parameters applied to response, in order of application.  # noqa: E501

        :param sort: The sort of this InlineResponse20011Pagination.  # noqa: E501
        :type sort: list[str]
        """

        self._sort = sort

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20011Pagination):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20011Pagination):
            return True

        return self.to_dict() != other.to_dict()
