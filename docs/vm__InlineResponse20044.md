# InlineResponse20044

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_assets_not_seen** | **int** | The number of assets in the network not seen since the specified number of days. | [optional] 
**num_assetstotal** | **int** | The total number of assets in the network. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


