# InlineResponse20020

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**crawled_urls** | **int** |  | [optional] 
**queued_urls** | **int** |  | [optional] 
**audited_pages** | **int** |  | [optional] 
**queued_pages** | **int** |  | [optional] 
**request_count** | **int** |  | [optional] 
**response_time** | **str** |  | [optional] 
**assets** | **list[object]** |  | [optional] 
**vulnerabilities** | **list[object]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


