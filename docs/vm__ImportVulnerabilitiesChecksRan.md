# ImportVulnerabilitiesChecksRan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tenable_plugin_id** | **str** | The Tenable plugin ID. | [optional] 
**port** | **int** | The port on the asset where the check ran. | [optional] 
**protocol** | **str** | The protocol used to communicate with the asset while running the check. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


