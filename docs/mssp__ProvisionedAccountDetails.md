# ProvisionedAccountDetails

The contact information details of the provisioned account.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** | The primary contact&#39;s email address. A valid email address must be in the format &#x60;name@domain&#x60;. | 
**first_name** | **str** | The primary contact&#39;s first name. | 
**last_name** | **str** | The primary contact&#39;s last name. | 
**company_name** | **str** | The name of the company for which the primary contact works. | 
**address** | [**Address**](mssp__Address.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


