# ApiV2AssetsBulkJobsDeleteQueryAnd

The query for selecting the assets to delete. Includes an asset attribute, an operator, and a value. To get the list of supported filters, use the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **str** | The name of the asset attribute to match. Asset attributes can include tags, for example, &#x60;tag.city&#x60;. | [optional] 
**operator** | **str** | The operator to apply to the matched value, for example, &#x60;eq&#x60; (equals), &#x60;neq&#x60; (does not equal), or &#x60;contains&#x60;. | [optional] 
**value** | **str** | The asset attribute value to match. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


