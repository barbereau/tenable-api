# InlineResponse200100

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **int** | The ID of the generated file. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


