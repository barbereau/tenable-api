# InlineResponse20087

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tags** | [**list[InlineResponse20087Tags]**](vm__InlineResponse20087Tags.md) | An array of asset assignment objects. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


