# InlineResponse20083Control

Indicates how the parameter appears in the Tenable.io user interface.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The type of UI control which represents the filter. | [optional] 
**regex** | **str** | A regex which can be used by a user interface to validate input. | [optional] 
**readable_regex** | **str** | Provides a human-readable \&quot;hint\&quot; which describes what the filter string should look like. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


