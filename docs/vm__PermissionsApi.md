# tenableapi.vm.PermissionsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**permissions_change**](vm__PermissionsApi.md#permissions_change) | **PUT** /permissions/{object_type}/{object_id} | Update object permissions
[**permissions_list**](vm__PermissionsApi.md#permissions_list) | **GET** /permissions/{object_type}/{object_id} | Get object permissions


# **permissions_change**
> object permissions_change(object_type, object_id, inline_object29)

Update object permissions

Updates the permissions for a Tenable.io object.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.PermissionsApi(api_client)
    object_type = 'scanner' # str | The type of object. (default to 'scanner')
object_id = 56 # int | The unique ID of the object (for example, scanner).
inline_object29 = tenableapi.vm.InlineObject29() # InlineObject29 | 

    try:
        # Update object permissions
        api_response = api_instance.permissions_change(object_type, object_id, inline_object29)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PermissionsApi->permissions_change: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **object_type** | **str**| The type of object. | [default to &#39;scanner&#39;]
 **object_id** | **int**| The unique ID of the object (for example, scanner). | 
 **inline_object29** | [**InlineObject29**](vm__InlineObject29.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully updates the object permissions. |  -  |
**403** | Returned if you do not have permission to edit the object. |  -  |
**404** | Returned if Tenable.io cannot find the specified object. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **permissions_list**
> list[InlineResponse20046] permissions_list(object_type, object_id)

Get object permissions

Returns the object's permissions.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.PermissionsApi(api_client)
    object_type = 'scanner' # str | The type of object. (default to 'scanner')
object_id = 56 # int | The unique ID of the object.

    try:
        # Get object permissions
        api_response = api_instance.permissions_list(object_type, object_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PermissionsApi->permissions_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **object_type** | **str**| The type of object. | [default to &#39;scanner&#39;]
 **object_id** | **int**| The unique ID of the object. | 

### Return type

[**list[InlineResponse20046]**](vm__InlineResponse20046.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the object permissions. |  -  |
**403** | Returned if you do not have permission to view the object. |  -  |
**404** | Returned if Tenable.io cannot find the specified object. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

