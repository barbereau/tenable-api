# ConfigTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**template_id** | **str** | The UUID of the Tenable-provided template. | [readonly] 
**name** | **str** | The name of the Tenable-provided template. | [readonly] 
**description** | **str** | The description of the Tenable-provided template. | [readonly] 
**timeout** | **str** | The maximum duration the scan runs before it stops automatically. The maximum duration you can set for your overall scan max time is 99:59:59 (hours:minutes:seconds). | [readonly] 
**plugin_state** | **str** | Indicates whether the user of the Tenable-provided template has the ability to redefine the settings for the template. Possible values are:   - &#x60;locked&#x60; -- You cannot modify the template. Tenable.io Web Application scanning rejects any request submitted with settings that differ from the defined template settings.  - &#x60;open&#x60; -- You can modify the template without restriction | [readonly] 
**scanner_types** | **str** | Indicates which scanners you can use to run scans based on the Tenable-provided template. Possible values are:   - &#x60;scanner&#x60; -- A single scanner running on premises in the client&#39;s own infrastructure or environment.   - &#x60;container_group&#x60; -- A group of client scanners running on premises.   - &#x60;cloud-group&#x60; -- A group of managed scanners running within Tenable&#39;s cloud infrastructure. | [readonly] 
**settings** | **object** | The restricted settings for the Tenable-provided template. This is a free-form object as each template can define different parameters for a configuration. | [readonly] 
**defaults** | **object** | The default settings for this template. | [readonly] 
**plugins** | [**list[Plugin]**](was__Plugin.md) | An array of plugins available to the template. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


