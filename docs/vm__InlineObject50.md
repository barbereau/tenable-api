# InlineObject50

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_name** | **str** | The name of the tag category to associate with the new value.  Specify the name of a new category if you want to add both a &lt;i&gt;new&lt;/i&gt; category and tag value.  Specify the name of an &lt;i&gt;existing&lt;/i&gt; category if you want to add the tag value to the existing category.  &lt;b&gt;Caution:&lt;/b&gt; This value is case-sensitive. For example, Tenable.io considers \&quot;location\&quot; and \&quot;Location\&quot; to be separate categories.  The category_name can result in the following responses:   - If the category_name you specify exists, and the tag value you specify already exists for that category, Tenable.io returns a 400 response code, instead of adding the tag.   - If the category_name you specify exists, but the tag value you specify does not yet exist for that category, Tenable.io adds the tag value to the existing category.   - If the category_name you specify does not exist, Tenable.io creates a new tag category and adds the new tag value to that category.  This parameter is required if category_uuid is not present in the request message. | [optional] 
**category_uuid** | **str** | The UUID of the tag category to associate with the new value. For more information on determining this value, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio).  Use this parameter only if you want to add the tag value to an existing category. If the UUID you specify does not exist, Tenable.io does not create a new catgory. Instead, it returns a 400 (Bad Request) response code.  This parameter is required if category_name is not present in the request message. | [optional] 
**category_description** | **str** | The description for the new tag category that Tenable.io creates if the category specified by name does not exist. Otherwise, Tenable.io ignores the description. | [optional] 
**value** | **str** | The new tag value.  &lt;b&gt;Caution:&lt;/b&gt; This value is case-sensitive. For example, Tenable.io considers \&quot;headquarters\&quot; and \&quot;Headquarters\&quot; to be separate tag values. | 
**description** | **str** | The new tag value description. | [optional] 
**filters** | [**TagsValuesFilters**](vm__TagsValuesFilters.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


