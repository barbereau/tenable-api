# ScansCredentialsAdd

A credentials object you want to add to the scan. For scan-specific credentials, the parameters of the object vary based on credentials category, credentials type, and type-specific settings. For more information, see [Determine Settings for a Credential Type](doc:determine-settings-for-credential-type). For managed credentials, the object contains a single parameter `id`, which specifies the UUID of the managed credentials you want to add.  **Note:** This form displays limited parameters that support a Windows type of credentials that uses password authentication.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host** | [**ScansCredentialsAddHost**](vm__ScansCredentialsAddHost.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


