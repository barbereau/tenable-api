# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** | The UUID of the user. | [optional] 
**id** | **int** | The unique ID of the user. | [optional] 
**user_name** | **str** | The username for the user. | [optional] 
**username** | **str** | The username for the user. | [optional] 
**email** | **str** | The email address of the user. If this attribute is empty, Tenable.io uses the &#x60;username&#x60; value as the user&#39;s email address. | [optional] 
**name** | **str** | The name of the user (for example, first and last name). | [optional] 
**type** | **str** | The type of user. The only supported type is &#x60;local&#x60;. | [optional] 
**permissions** | **int** | The user permissions as described in [Permissions](doc:permissions). | [optional] 
**last_login_attempt** | **int** | The time (in Unix milliseconds) of the last time the user failed to log in to the Tenable.io user interface. This attribute is only present if the user has attempted unsuccessfully to log in to the Tenable.io user interface. | [optional] 
**login_fail_count** | **int** | The number of failed login attempts since the user last successfully logged in to the Tenable.io user interface.   If this attribute is greater than 5, Tenable.io locks the user account (that is, updates the &#x60;lockout&#x60; attribute for the user to &#x60;1&#x60;.)  You can reset this count to &#x60;0&#x60; by [changing the user&#39;s password](ref:users-password) or [generating the user&#39;s API keys](ref:users-keys). | [optional] 
**login_fail_total** | **int** | The total number of failed login attempts for the user. | [optional] 
**enabled** | **bool** | Specifies whether the user account is enabled (&#x60;true&#x60;) or disabled (&#x60;false&#x60;). | [optional] 
**undeletable** | **bool** | Specifies whether the user account is undeletable (&#x60;true&#x60;) or deletable (&#x60;false&#x60;). | [optional] 
**two_factor** | [**InlineResponse200TwoFactor**](platform__InlineResponse200TwoFactor.md) |  | [optional] 
**lockout** | **int** | Specifies whether the user can log into the Tenable.io user interface (&#x60;0&#x60;) or is locked out (&#x60;1&#x60;).  Tenable.io automatically updates this attribute to &#x60;1&#x60; if the &#x60;login_fail_count&#x60; attribute for the user is greater than 5. To unlock a user account, reset the user&#39;s password using the [PUT /users/{user_id}/chpasswd](ref:users-password) endpoint.  **Note:** A user can be locked out of the user interface but still submit API requests if they are assigned the appropriate authorizations (&#x60;api_permitted&#x60;). To prevent a user from submitting API requests, limit authorizations using the [PUT /users/{user_id}/authorizations](ref:users-update-auths) endpoint, or disable the user entirely using the [PUT /users/{user_id}/enabled](ref:users-enabled) endpoint. | [optional] 
**container_uuid** | **str** | The UUID of the Tenable.io instance to which the user belongs. | [optional] 
**lastlogin** | **int** | The last time (in Unix milliseconds) that the user successfully logged in to the Tenable.io user interface. This attribute is only present if the user has logged in at least once successfully to the Tenable.io user interface. | [optional] 
**uuid_id** | **str** | The UUID for the user. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


