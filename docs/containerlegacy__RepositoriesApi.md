# tenableapi.containerv1.RepositoriesApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**container_security_repositories_list_images**](containerlegacy__RepositoriesApi.md#container_security_repositories_list_images) | **GET** /container-security/api/v1/repositories/{id}/images | List images in repository
[**container_security_repositories_list_repositories**](containerlegacy__RepositoriesApi.md#container_security_repositories_list_repositories) | **GET** /container-security/api/v1/repositories | List repositories


# **container_security_repositories_list_images**
> InlineResponse2004 container_security_repositories_list_images(id, offset=offset, limit=limit)

List images in repository

**Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/images](ref:container-security-v2-list-images) endpoint with the repository filter instead. Returns a list of images inside a specific repository.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.RepositoriesApi(api_client)
    id = 56 # int | The ID of the relevant repository.
offset = 56 # int | The number of items to skip before Tenable.io Container Security starts to collect the result set. (optional)
limit = 56 # int | The maximum number of items to return. (optional)

    try:
        # List images in repository
        api_response = api_instance.container_security_repositories_list_images(id, offset=offset, limit=limit)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling RepositoriesApi->container_security_repositories_list_images: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the relevant repository. | 
 **offset** | **int**| The number of items to skip before Tenable.io Container Security starts to collect the result set. | [optional] 
 **limit** | **int**| The maximum number of items to return. | [optional] 

### Return type

[**InlineResponse2004**](containerlegacy__InlineResponse2004.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns an array of images. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_repositories_list_repositories**
> InlineResponse2004 container_security_repositories_list_repositories(offset=offset, limit=limit)

List repositories

**Deprecated!** Tenable.io Container Security API v1 is deprecated. Use the [GET /container-security/api/v2/repositories](ref:container-security-v2-list-repositories) endpoint instead. Returns a list of repositories.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.RepositoriesApi(api_client)
    offset = 56 # int | The number of items Tenable.io Container Security skips before starting to collect the result set. (optional)
limit = 56 # int | The maximum number of items to return. (optional)

    try:
        # List repositories
        api_response = api_instance.container_security_repositories_list_repositories(offset=offset, limit=limit)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling RepositoriesApi->container_security_repositories_list_repositories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int**| The number of items Tenable.io Container Security skips before starting to collect the result set. | [optional] 
 **limit** | **int**| The maximum number of items to return. | [optional] 

### Return type

[**InlineResponse2004**](containerlegacy__InlineResponse2004.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns an array of repositories. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

