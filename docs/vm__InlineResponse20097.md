# InlineResponse20097

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Event type:  - discovered—Asset created (for example, by a network scan or import).  - seen—Asset observed by a network scan without any changes to its attributes.  - tagging—Tag added to or removed from asset.  - attribute_change—A scan identified new or changed attributes for the asset (for example, new software applications installed on the asset).  - updated—Asset updated either manually by a user or automatically by a new scan. | [optional] 
**timestamp** | **int** | The timestamp of the event. The timestamp is reported in ISO 8601 format in UTC time. | [optional] 
**scan_id** | **str** | The UUID of the scan that logged the event. | [optional] 
**schedule_id** | **str** | The ID of the scheduled scan associated with the event. | [optional] 
**source** | **str** | The entity that logged the event, for example, NESSUS_AGENT, NESSUS_AGENT, PVS, or WAS. | [optional] 
**details** | [**WorkbenchesAssetsAssetUuidActivityDetails**](vm__WorkbenchesAssetsAssetUuidActivityDetails.md) |  | [optional] 
**updates** | [**list[WorkbenchesAssetsAssetUuidActivityUpdates]**](vm__WorkbenchesAssetsAssetUuidActivityUpdates.md) | (attribute_change entries only) A list of updates to the asset&#39;s attributes. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


