# InlineObject53

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | **str** | Specifies whether to add or remove tags. | 
**assets** | **list[str]** | An array of asset UUIDs. For more information on determining values for this array, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio). | 
**tags** | **list[str]** | An array of tag value UUIDs. For more information on determining values for this array, see [Determine Tag Identifiers](doc:determine-tag-identifiers-tio). | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


