# ApiV2AssetsBulkJobsDeleteQuery

The query for selecting the assets to delete. Must include one or more filters. A filter must include an asset attribute, an operator, and a value. To get the list of supported filters, use the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. Sets of multiple filters must be specified inside `and` or `or` arrays.  **Note:** You can also nest conditions, for example, specify a set of `or` sub-conditions for a condition inside the `and` array.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **str** | The name of the asset attribute to match. Asset attributes can include tags, for example, &#x60;tag.city&#x60;. | [optional] 
**operator** | **str** | The operator to apply to the matched value, for example, &#x60;eq&#x60; (equals), &#x60;neq&#x60; (does not equal), or &#x60;contains&#x60;. | [optional] 
**value** | **str** | The asset attribute value to match. | [optional] 
**_and** | [**list[ApiV2AssetsBulkJobsDeleteQueryAnd]**](vm__ApiV2AssetsBulkJobsDeleteQueryAnd.md) | To select assets that match all of multiple conditions, specify the conditions inside the &#x60;and&#x60; array. | [optional] 
**_or** | [**list[ApiV2AssetsBulkJobsDeleteQueryAnd]**](vm__ApiV2AssetsBulkJobsDeleteQueryAnd.md) | To select assets that match any of multiple conditions, specify the conditions inside the &#x60;or&#x60; array. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


