# InlineResponse20023Inputs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The type of input prompt in the user interface. Possible values include:  - password—Prompts for input via text box.  - text—Prompts for input via text box.  - select—Prompts for input via selectable options.  - file—Prompts user to upload file of input data.  - toggle—Prompts user to select one of two mutually-exclusive options in toggle format.  - checkbox—Prompts user to select options via checkboxes. Checkboxes can represent enabling a single option or can allow users to select from multiple, mutually-exclusive options.  - key-value— Prompts for text entry of a key-value pair via two text boxes. | [optional] 
**name** | **str** | The display name of the option in the user interface. | [optional] 
**required** | **bool** | A value specifying whether the input is required (&#x60;true&#x60;) or optional (&#x60;false&#x60;). | [optional] 
**placeholder** | **str** | An example of the input value. This value appears as example text in the user interface.    This attribute is only present for credential parameters that require text input in the interface.     In cases where the input type is &#x60;key-value&#x60;, this attribute can be an array of strings. | [optional] 
**regex** | **str** | A regular expression defining the valid input for the parameter in the user interface. | [optional] 
**hint** | **str** | Helpful information about the input required, for example, \&quot;PEM formatted certificate\&quot;. Hints appear in the user interface, but can contain information that is relevant to API requests. | [optional] 
**callback** | **str** | Not supported as a parameter in managed credentials. | [optional] 
**default_row_count** | **int** | The number of text box rows that appear by default when the input type is &#x60;key-value&#x60;. | [optional] 
**hide_values** | **bool** | A value specifying whether the user interface hides the value by default when the input type is &#x60;key-value&#x60;. If &#x60;true&#x60;, dots appear instead of characters as you type the value in the user interface. | [optional] 
**id** | **str** | The system name for the input. Use this value as the input name in request messages when configuring credentials. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


