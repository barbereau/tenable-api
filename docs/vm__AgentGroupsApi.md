# tenableapi.vm.AgentGroupsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**agent_groups_add_agent**](vm__AgentGroupsApi.md#agent_groups_add_agent) | **PUT** /scanners/{scanner_id}/agent-groups/{group_id}/agents/{agent_id} | Add agent to agent group
[**agent_groups_configure**](vm__AgentGroupsApi.md#agent_groups_configure) | **PUT** /scanners/{scanner_id}/agent-groups/{group_id} | Update name of agent group
[**agent_groups_create**](vm__AgentGroupsApi.md#agent_groups_create) | **POST** /scanners/{scanner_id}/agent-groups | Create agent group on scanner
[**agent_groups_delete**](vm__AgentGroupsApi.md#agent_groups_delete) | **DELETE** /scanners/{scanner_id}/agent-groups/{group_id} | Delete agent group from scanner
[**agent_groups_delete_agent**](vm__AgentGroupsApi.md#agent_groups_delete_agent) | **DELETE** /scanners/{scanner_id}/agent-groups/{group_id}/agents/{agent_id} | Delete agent from agent group
[**agent_groups_details**](vm__AgentGroupsApi.md#agent_groups_details) | **GET** /scanners/{scanner_id}/agent-groups/{group_id} | Get details for agent group
[**agent_groups_list**](vm__AgentGroupsApi.md#agent_groups_list) | **GET** /scanners/{scanner_id}/agent-groups | List agent groups for scanner


# **agent_groups_add_agent**
> object agent_groups_add_agent(scanner_id, group_id, agent_id)

Add agent to agent group

Adds an agent to the agent group. You can use the [GET /scanners/{scanner_id}/agent-groups/{group_id}](ref:agent-groups-details) endpoint to verify that the agent was added to the specified agent group.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentGroupsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.
group_id = 56 # int | The ID of the agent group.
agent_id = 56 # int | The ID of the agent to add.

    try:
        # Add agent to agent group
        api_response = api_instance.agent_groups_add_agent(scanner_id, group_id, agent_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentGroupsApi->agent_groups_add_agent: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 
 **group_id** | **int**| The ID of the agent group. | 
 **agent_id** | **int**| The ID of the agent to add. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the agent was added to the group. |  -  |
**404** | Returned if Tenable.io cannot find the specified agent group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an error occurred while attempting to add the agent. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_groups_configure**
> object agent_groups_configure(scanner_id, group_id, inline_object8)

Update name of agent group

Changes the name of the agent group.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentGroupsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.
group_id = 56 # int | The ID of the agent group to change.
inline_object8 = tenableapi.vm.InlineObject8() # InlineObject8 | 

    try:
        # Update name of agent group
        api_response = api_instance.agent_groups_configure(scanner_id, group_id, inline_object8)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentGroupsApi->agent_groups_configure: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 
 **group_id** | **int**| The ID of the agent group to change. | 
 **inline_object8** | [**InlineObject8**](vm__InlineObject8.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the configuration was changed. |  -  |
**404** | Returned if Tenable.io cannot find the specified agent group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an error occurred while saving the configuration. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_groups_create**
> InlineResponse20010 agent_groups_create(scanner_id, inline_object7)

Create agent group on scanner

Creates an agent group on the scanner.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentGroupsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner to add the agent group to.
inline_object7 = tenableapi.vm.InlineObject7() # InlineObject7 | 

    try:
        # Create agent group on scanner
        api_response = api_instance.agent_groups_create(scanner_id, inline_object7)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentGroupsApi->agent_groups_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner to add the agent group to. | 
 **inline_object7** | [**InlineObject7**](vm__InlineObject7.md)|  | 

### Return type

[**InlineResponse20010**](vm__InlineResponse20010.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the agent group has been created. |  -  |
**400** | Returned if your request message contains an invalid parameter. |  -  |
**403** | Returned if you do not have permission to create an agent group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to add the agent group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_groups_delete**
> object agent_groups_delete(scanner_id, group_id)

Delete agent group from scanner

Deletes an agent group from the scanner.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentGroupsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.
group_id = 56 # int | The ID of the agent group to delete.

    try:
        # Delete agent group from scanner
        api_response = api_instance.agent_groups_delete(scanner_id, group_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentGroupsApi->agent_groups_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 
 **group_id** | **int**| The ID of the agent group to delete. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the agent group has been successfully deleted. |  -  |
**404** | Returned if Tenable.io cannot find the specified agent group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to delete the agent group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_groups_delete_agent**
> object agent_groups_delete_agent(scanner_id, group_id, agent_id)

Delete agent from agent group

Deletes an agent from the agent group.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentGroupsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.
group_id = 56 # int | The ID of the agent group.
agent_id = 56 # int | The ID of the agent to remove.

    try:
        # Delete agent from agent group
        api_response = api_instance.agent_groups_delete_agent(scanner_id, group_id, agent_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentGroupsApi->agent_groups_delete_agent: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 
 **group_id** | **int**| The ID of the agent group. | 
 **agent_id** | **int**| The ID of the agent to remove. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the agent has been successfully removed from the agent group. |  -  |
**404** | Returned if Tenable.io cannot find the specified agent. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to remove the agent from the agent group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_groups_details**
> InlineResponse20010 agent_groups_details(scanner_id, group_id, offset=offset, limit=limit, sort=sort, f=f, ft=ft, w=w, wf=wf)

Get details for agent group

Gets details for the agent group.  Agent records which belong to this group will also be returned. You can apply filtering, sorting, or pagination to the agent records.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentGroupsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner.
group_id = 56 # int | The ID of the agent group to query.
offset = 56 # int | The starting record to retrieve. If this parameter is not supplied, the default value is 0. (optional)
limit = 56 # int | The number of records to retrieve. If this parameter is not supplied, a default of 50 records is used. The minimum supported limit is 1, and the maximum supported limit is 5000. (optional)
sort = 'sort_example' # str | The sort order of the returned records. Sort can only be applied to the sortable\\_fields specified by the filter capabilities. There may be no more than max\\_sort\\_fields number of columns used in the sort, as specified by the filter capabilities. Sort is applied, in order, in the following format: `:\\[asc|desc\\],:\\[asc|desc\\]`. For example, `sort=field1:asc,field2:desc` would first sort by field1, ascending, then sort by field2, descending. (optional)
f = 'f_example' # str | Apply a filter in the format `::`. For example, `field1:match:sometext` would match any records where the value of field1 contains `sometext`. You can use multiple query filters. (optional)
ft = 'ft_example' # str | Filter type. If the filter type is `and`, the record is only returned if all filters match. If the filter type is `or`, the record is returned if any of the filters match. (optional)
w = 'w_example' # str | Wildcard filter text. Wildcard search is a mechanism where multiple fields of a record are filtered against one specific filter string. If any one of the wildcard\\_fields' values matches against the filter string, then the record matches the wildcard filter. For a record to be returned, it must pass the wildcard filter (if there is one) AND the set of standard filters. For example, if `w=wild&f=field1:match:one&f=field2:match:two&ft=or`, the record would match if the value of any supported wildcard\\_fields contained `wild`, AND either field1's value contained `one` or field2's value contained `two`. (optional)
wf = 'wf_example' # str | A comma delimited subset of wildcard\\_fields to search when applying the wildcard filter. For example, `field1,field2`. If `w` is provided, but `wf` is not, then all wildcard\\_fields' values are searched against the wildcard filter text. (optional)

    try:
        # Get details for agent group
        api_response = api_instance.agent_groups_details(scanner_id, group_id, offset=offset, limit=limit, sort=sort, f=f, ft=ft, w=w, wf=wf)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentGroupsApi->agent_groups_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner. | 
 **group_id** | **int**| The ID of the agent group to query. | 
 **offset** | **int**| The starting record to retrieve. If this parameter is not supplied, the default value is 0. | [optional] 
 **limit** | **int**| The number of records to retrieve. If this parameter is not supplied, a default of 50 records is used. The minimum supported limit is 1, and the maximum supported limit is 5000. | [optional] 
 **sort** | **str**| The sort order of the returned records. Sort can only be applied to the sortable\\_fields specified by the filter capabilities. There may be no more than max\\_sort\\_fields number of columns used in the sort, as specified by the filter capabilities. Sort is applied, in order, in the following format: &#x60;:\\[asc|desc\\],:\\[asc|desc\\]&#x60;. For example, &#x60;sort&#x3D;field1:asc,field2:desc&#x60; would first sort by field1, ascending, then sort by field2, descending. | [optional] 
 **f** | **str**| Apply a filter in the format &#x60;::&#x60;. For example, &#x60;field1:match:sometext&#x60; would match any records where the value of field1 contains &#x60;sometext&#x60;. You can use multiple query filters. | [optional] 
 **ft** | **str**| Filter type. If the filter type is &#x60;and&#x60;, the record is only returned if all filters match. If the filter type is &#x60;or&#x60;, the record is returned if any of the filters match. | [optional] 
 **w** | **str**| Wildcard filter text. Wildcard search is a mechanism where multiple fields of a record are filtered against one specific filter string. If any one of the wildcard\\_fields&#39; values matches against the filter string, then the record matches the wildcard filter. For a record to be returned, it must pass the wildcard filter (if there is one) AND the set of standard filters. For example, if &#x60;w&#x3D;wild&amp;f&#x3D;field1:match:one&amp;f&#x3D;field2:match:two&amp;ft&#x3D;or&#x60;, the record would match if the value of any supported wildcard\\_fields contained &#x60;wild&#x60;, AND either field1&#39;s value contained &#x60;one&#x60; or field2&#39;s value contained &#x60;two&#x60;. | [optional] 
 **wf** | **str**| A comma delimited subset of wildcard\\_fields to search when applying the wildcard filter. For example, &#x60;field1,field2&#x60;. If &#x60;w&#x60; is provided, but &#x60;wf&#x60; is not, then all wildcard\\_fields&#39; values are searched against the wildcard filter text. | [optional] 

### Return type

[**InlineResponse20010**](vm__InlineResponse20010.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the agent group details. |  -  |
**403** | Returned if you do not have permission to view the agent group. |  -  |
**404** | Returned if Tenable.io cannot find the specified agent group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **agent_groups_list**
> list[InlineResponse20010] agent_groups_list(scanner_id)

List agent groups for scanner

Lists the agent groups for the scanner.<p>Requires SCAN MANAGER [40] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.AgentGroupsApi(api_client)
    scanner_id = 56 # int | The ID of the scanner to query for agent groups.

    try:
        # List agent groups for scanner
        api_response = api_instance.agent_groups_list(scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentGroupsApi->agent_groups_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scanner_id** | **int**| The ID of the scanner to query for agent groups. | 

### Return type

[**list[InlineResponse20010]**](vm__InlineResponse20010.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the agent groups list. |  -  |
**403** | Returned if you do not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

