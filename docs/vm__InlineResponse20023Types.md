# InlineResponse20023Types

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The system name that uniquely identifies the credential type. | [optional] 
**name** | **str** | The display name for the credential type in the user interface. | [optional] 
**max** | **int** | The maximum number of instances of this credential type that Tenable.io supports for an individual scan or policy. | [optional] 
**configuration** | [**list[InlineResponse20023Configuration]**](vm__InlineResponse20023Configuration.md) | The configuration settings for a credential type. For a definition of object attributes, see [Determine Settings for a Credential Type](docs/determine-settings-for-credential-type). | [optional] 
**expand_settings** | **bool** | A value specifying whether the configuration settings appear expanded by default in the user interface. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


