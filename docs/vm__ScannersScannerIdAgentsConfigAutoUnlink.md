# ScannersScannerIdAgentsConfigAutoUnlink

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** | If true, agent auto-unlink is enabled. Enabling auto-unlink causes it to take effect against all agents retroactively. | [optional] 
**expiration** | **int** | The expiration time for agents, in days. If an agent has not communicated in this number of days, it will be considered &#x60;expired&#x60; and auto-unlinked if auto\\_unlink.enabled is &#x60;true&#x60;. Valid values are 1-365. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


