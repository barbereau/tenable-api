# InlineObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | [**list[InlineResponse200]**](waslegacy__InlineResponse200.md) | An array of asset objects to import. Each asset object requires a value for at least one of the following properties: fqdn, ipv4, netbios\\_name, mac\\_address. Each asset object may also optionally contain additional properties for each property of was-assets. If any asset fails to be imported, an error message is returned indicating the cause of the failure. | 
**source** | **str** | An identifier for the script used to upload the assets. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


