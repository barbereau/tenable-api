# ScanReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**ReportInfo**](was__ReportInfo.md) |  | [optional] 
**created_at** | **datetime** | An ISO timestamp indicating the date and time when a scan generated the attachment file, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


