# ReportScanSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**http** | [**HttpSettings**](was__HttpSettings.md) |  | [optional] 
**audit** | [**AuditSettings**](was__AuditSettings.md) |  | [optional] 
**scope** | [**ScopeSettings**](was__ScopeSettings.md) |  | [optional] 
**plugin** | [**PluginSettings**](was__PluginSettings.md) |  | [optional] 
**target** | **str** | The URL of the target web application. | [optional] 
**browser** | [**BrowserSettings**](was__BrowserSettings.md) |  | [optional] 
**timeout** | **str** | The time (in milliseconds) that the scanner waits for a response from the web application host. | [optional] 
**assessment** | [**AssessmentSettings**](was__AssessmentSettings.md) |  | [optional] 
**credentials** | [**Credentials**](was__Credentials.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


