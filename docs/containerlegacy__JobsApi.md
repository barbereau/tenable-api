# tenableapi.containerv1.JobsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**container_security_jobs_job_status**](containerlegacy__JobsApi.md#container_security_jobs_job_status) | **GET** /container-security/api/v1/jobs/status | Get job status by ID
[**container_security_jobs_job_status_by_image_digest**](containerlegacy__JobsApi.md#container_security_jobs_job_status_by_image_digest) | **GET** /container-security/api/v1/jobs/image_status_digest | Get job status by image digest
[**container_security_jobs_job_status_by_image_id**](containerlegacy__JobsApi.md#container_security_jobs_job_status_by_image_id) | **GET** /container-security/api/v1/jobs/image_status | Get job status by image ID
[**container_security_jobs_list_jobs**](containerlegacy__JobsApi.md#container_security_jobs_list_jobs) | **GET** /container-security/api/v1/jobs/list | List active jobs


# **container_security_jobs_job_status**
> InlineResponse2002 container_security_jobs_job_status(job_id)

Get job status by ID

**Deprecated!** Tenable.io Container Security API v1 is deprecated. To determine the progress of an image analysis, use the [GET /container-security/api/v2/images/{repository}/{image}/{tag}](ref:container-security-v2-get-image-report) endpoint in Tenable.io Container Security API v2. Returns the status of a job that you specify by ID to determine if the job is still queued, in progress, or has completed.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.JobsApi(api_client)
    job_id = 56 # int | The ID of the job for which you want the status.

    try:
        # Get job status by ID
        api_response = api_instance.container_security_jobs_job_status(job_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling JobsApi->container_security_jobs_job_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **int**| The ID of the job for which you want the status. | 

### Return type

[**InlineResponse2002**](containerlegacy__InlineResponse2002.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the status of the job you specified. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_jobs_job_status_by_image_digest**
> InlineResponse2002 container_security_jobs_job_status_by_image_digest(image_digest)

Get job status by image digest

**Deprecated!** Tenable.io Container Security API v1 is deprecated. To determine the progress of an image analysis, use the [GET /container-security/api/v2/images/{repository}/{image}/{tag}](ref:container-security-v2-get-image-report) endpoint in Tenable.io Container Security API v2. Returns the status of a job by specifying an image digest to determine if the job is still queued, in progress, or has completed.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.JobsApi(api_client)
    image_digest = 'image_digest_example' # str | The image digest of the job for which you want the status.

    try:
        # Get job status by image digest
        api_response = api_instance.container_security_jobs_job_status_by_image_digest(image_digest)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling JobsApi->container_security_jobs_job_status_by_image_digest: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image_digest** | **str**| The image digest of the job for which you want the status. | 

### Return type

[**InlineResponse2002**](containerlegacy__InlineResponse2002.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the status of the job you specified. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_jobs_job_status_by_image_id**
> InlineResponse2002 container_security_jobs_job_status_by_image_id(image_id)

Get job status by image ID

**Deprecated!** Tenable.io Container Security API v1 is deprecated. To determine the progress of an image analysis, use the [GET /container-security/api/v2/images/{repository}/{image}/{tag}](ref:container-security-v2-get-image-report) endpoint in Tenable.io Container Security API v2. Returns the status of a job by specifying an image ID to determine if the job is still queued, in progress, or has completed.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.JobsApi(api_client)
    image_id = 'image_id_example' # str | The ID of the image for which you want the status.

    try:
        # Get job status by image ID
        api_response = api_instance.container_security_jobs_job_status_by_image_id(image_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling JobsApi->container_security_jobs_job_status_by_image_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image_id** | **str**| The ID of the image for which you want the status. | 

### Return type

[**InlineResponse2002**](containerlegacy__InlineResponse2002.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the status of the job you specified. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_jobs_list_jobs**
> object container_security_jobs_list_jobs()

List active jobs

**Deprecated!** Tenable.io Container Security API v1 is deprecated. To determine the progress of an image analysis, use the [GET /container-security/api/v2/images/{repository}/{image}/{tag}](ref:container-security-v2-get-image-report) endpoint in Tenable.io Container Security API v2. Returns a list of active jobs.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv1
from tenableapi.containerv1.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv1.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv1.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv1.JobsApi(api_client)
    
    try:
        # List active jobs
        api_response = api_instance.container_security_jobs_list_jobs()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling JobsApi->container_security_jobs_list_jobs: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns an array of the statuses of all active jobs. |  -  |
**401** | Returns an error message if the request is not authorized. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

