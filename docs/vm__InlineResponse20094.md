# InlineResponse20094

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | [**list[InlineResponse20094Assets]**](vm__InlineResponse20094Assets.md) | An array of asset objects. | [optional] 
**total** | **int** | The total count of returned assets. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


