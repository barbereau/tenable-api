# InlineResponse20063

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**container_id** | **str** | The unique ID of your Tenable.io instance. | [optional] 
**owner_uuid** | **str** | The unique ID of the scan owner. | [optional] 
**uuid** | **str** | The UUID of the schedule for the scan. | [optional] 
**name** | **str** | The user-defined scan name. | [optional] 
**description** | **str** | A brief user-defined description of the scan. | [optional] 
**policy_id** | **int** | The unique ID of the policy associated with the scan. | [optional] 
**scanner_uuid** | **str** | The UUID of the scanner that the scan is configured to use, if the scan is *not* configured for [scan routing](doc:manage-scan-routing-tio). | [optional] 
**target_network_uuid** | **str** | The UUID of the network object that Tenable.io associates with the scan results if the scan is configured for [scan routing](doc:manage-scan-routing-tio). | [optional] 
**emails** | **str** | A comma-separated list of accounts that receive the email summary report. | [optional] 
**sms** | **str** | A comma-separated list of mobile phone numbers that receive notification of the scan. | [optional] 
**enabled** | **bool** | A value indicating whether the scan schedule is active (&#x60;true&#x60;) or inactive (&#x60;false&#x60;). | [optional] 
**dashboard_file** | **str** | The name of the dashboard file associated with the scan. | [optional] 
**include_aggregate** | **bool** | A value indicating whether the scan results appear in dashboards. | [optional] 
**scan_time_window** | **str** | The time frame, in minutes, during which agents must transmit scan results to Tenable.io in order to be included in dashboards and reports. If your request omits this parameter, the default value is 180 minutes. For non-agent scans, this attribute is null. | [optional] 
**custom_targets** | **str** | Targets specified in the &#x60;alt_targets&#x60; parameter of the [POST /scans/{scan_id}/launch](/reference#scans-launch) request body used to run the scan. | [optional] 
**starttime** | **str** | For one-time scans, the starting time and date for the scan. For recurrent scans, the first date on which the scan schedule is active and the time that recurring scans launch based on the &#x60;rrules&#x60; parameter.  This attribute has the following format: &#x60;YYYYMMDDTHHMMSS&#x60;. | [optional] 
**rrules** | **str** | The interval at which the scan repeats. The interval is formatted as a string of three values delimited by semi-colons. These values are: the frequency (FREQ&#x3D;ONETIME or DAILY or WEEKLY or MONTHLY or YEARLY), the interval (INTERVAL&#x3D;1 or 2 or 3 ... x), and the days of the week (BYDAY&#x3D;SU,MO,TU,WE,TH,FR,SA). For a scan that runs every three weeks on Monday Wednesday and Friday, the string would be &#x60;FREQ&#x3D;WEEKLY;INTERVAL&#x3D;3;BYDAY&#x3D;MO,WE,FR&#x60;. If the scan is not scheduled to recur, this attribute is &#x60;null&#x60;. For more information, see [rrules Format](example-assessment-scan-recurring#rrules-format).  **Note:** To set the &#x60;rrules&#x60; parameter for an agent scan, the request must also include the following body parameters:&lt;ul&gt;&lt;li&gt;The &#x60;uuid&#x60; parameter must specify an agent scan template. For more information, see [Tenable-Provided Agent Templates](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Scans/AgentTemplates.htm) and the [GET /editor/scan/templates](ref:editor-list-templates) endpoint.&lt;/li&gt;&lt;li&gt;The &#x60;agent_group_id&#x60; parameter must specify an agent group. For more information, see [Agent Groups](ref:agent-groups).&lt;/li&gt;&lt;/ul&gt;For an example request body for an agent scan, see [Example Agent Scan: Recurring](doc:example-agent-scan-recurring). | [optional] 
**timezone** | **str** | The timezone of the scheduled start time for the scan. | [optional] 
**notification_filters** | [**list[InlineResponse20063NotificationFilters]**](vm__InlineResponse20063NotificationFilters.md) | A list of filters that Tenable.io applies to determine whether it sends a notification email on scan completion to the recipients specified in the &#x60;emails&#x60; attribute. | [optional] 
**tag_targets** | **list[str]** | The list of asset tag identifiers the scan uses to determine which assets it evaluates. For more information about tag-based scans, see [Manage Tag-Based Scans](doc:manage-tag-based-scans-tio). | [optional] 
**shared** | **bool** | If &#x60;1&#x60;, the scan is shared with users other than the scan owner. The level of sharing is specified in the &#x60;acls&#x60; attribute of the scan details. | [optional] 
**user_permissions** | **int** | The sharing permissions for the scan. | [optional] 
**default_permissions** | **int** | The default permissions for the scan. | [optional] 
**owner** | **str** | The owner of the scan. | [optional] 
**owner_id** | **int** | The unique ID of the owner of the scan. | [optional] 
**last_modification_date** | **int** | For newly-created scans, the date on which the scan configuration was created. For scans that have been launched at least once, this attribute does not represent the date on which the scan configuration was last modified. Instead, it represents the date on which the scan was last launched, in Unix time format. Tenable.io updates this attribute each time the scan launches. | [optional] 
**creation_date** | **int** | For newly-created scans, the date on which the scan configuration was originally created. For scans that have been launched at least once, this attribute does not represent the date on which the scan configuration was originally created. Instead, it represents the date on which the scan was first launched, in Unix time format. | [optional] 
**type** | **str** | The type of scan. | [optional] 
**id** | **int** | The unique ID of the scan. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


