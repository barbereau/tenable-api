# InlineObject41

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alt_targets** | **list[str]** | If you include this parameter, Tenable.io scans these targets instead of the default. Value can be an array where each index is a target, or an array with a single index of comma-separated targets. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


