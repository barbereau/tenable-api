# InlineResponse2002Filters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operators** | **list[str]** | Corresponds to the operator component of the &#x60;f&#x60; parameter. | [optional] 
**control** | **str** | Indicates how the parameter appears in the Tenable.io user interface. | [optional] 
**name** | **str** | Corresponds to the field component of the &#x60;f&#x60; parameter. | [optional] 
**readable_name** | **str** | The name of the parameter as it appears in the Tenable.io user interface. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


