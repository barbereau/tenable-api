# InlineResponse20098

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | [**InlineResponse20092**](vm__InlineResponse20092.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


