# InlineResponse20086

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** | The UUID of the tag value. Use this value to assign the tag to assets. | [optional] 
**created_at** | **str** | An ISO timestamp indicating the date and time on which the tag value was created, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**created_by** | **str** | The name of the user who created the tag value. | [optional] 
**updated_at** | **str** | An ISO timestamp indicating the date and time on which the tag value was last updated, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. When you create a tag value, this date matches the &#x60;created_at&#x60; date. | [optional] 
**updated_by** | **str** | The name of the user who last updated the tag value. When you create a tag value, this name matches the &#x60;created_by&#x60; name. | [optional] 
**category_uuid** | **str** | The UUID of the category associated with the tag value. Use this value to create future tags in the same category. | [optional] 
**value** | **str** | The tag value. Must be unique within the category. | [optional] 
**description** | **str** | The description of the tag value. | [optional] 
**type** | **str** | The tag type:  - static—A user must manually apply the tag to assets.  - dynamic—Tenable.io automatically applies the tag based on asset attribute rules. | [optional] 
**category_name** | **str** | The name of the category associated with the tag value. | [optional] 
**category_description** | **str** | The description of the category associated with the tag value. | [optional] 
**filters** | [**InlineResponse20086Filters**](vm__InlineResponse20086Filters.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


