# AuditLogV1EventsFields

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pair** | [**AuditLogV1EventsFieldsPair**](vm__AuditLogV1EventsFieldsPair.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


