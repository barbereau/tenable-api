# InlineResponse20080

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**solutions** | [**list[InlineResponse20080Solutions]**](vm__InlineResponse20080Solutions.md) | A list of solutions. | [optional] 
**pagination** | [**InlineResponse20080Pagination**](vm__InlineResponse20080Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


