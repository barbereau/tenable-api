# InlineResponse20064History

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**history_id** | **int** | The unique ID of the historical data. | [optional] 
**owner_id** | **int** | The unique ID of the owner of the scan. | [optional] 
**creation_date** | **int** | The creation date for the historical data in Unix time. | [optional] 
**last_modification_date** | **int** | The last modification date for the historical data in Unix time. | [optional] 
**uuid** | **str** | The UUID of the historical data. | [optional] 
**type** | **str** | The type of scan: &#x60;local&#x60; (a credentialed scan performed over the network), &#x60;remote&#x60; (an uncredentialed scan performed over the network, &#x60;agent&#x60; (a scan on a local host that a Nessus agent performs directly), or &#x60;null&#x60; (the scan has never been launched, or the scan is imported). | [optional] 
**status** | **str** | The terminal status of the scan run. For a list of possible values, see [Scan Status](doc:scan-status-tio). | [optional] 
**scheduler** | **int** | If &#x60;true&#x60;, Tenable.io launched the scan automatically from a schedule. | [optional] 
**alt_targets_used** | **bool** | If &#x60;true&#x60;, Tenable.io did not not launched with a target list. This parameter is &#x60;true&#x60; for agent scans. | [optional] 
**is_archived** | **bool** | Indicates whether the scan results are older than 60 days (&#x60;true&#x60;). If this parameter is &#x60;true&#x60;, Tenable.io returns limited data for the scan run. For complete scan results that are older than 60 days, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


