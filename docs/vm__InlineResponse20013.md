# InlineResponse20013

The list of assets with details, and the total assets count.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | [**list[InlineResponse20013Assets]**](vm__InlineResponse20013Assets.md) | A list of assets with details. | [optional] 
**total** | **int** | The total number of assets in your Tenable.io instance. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


