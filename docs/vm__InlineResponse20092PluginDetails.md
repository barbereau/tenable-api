# InlineResponse20092PluginDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**family** | **str** | The plugin family. | [optional] 
**modification_date** | **str** | The ISO timestamp when Tenable last updated the plugin definition. | [optional] 
**name** | **str** | The name of the plugin. | [optional] 
**publication_date** | **str** | The ISO timestamp when Tenable first published the plugin definition. | [optional] 
**type** | **str** | The type of scan that uses the plugin, either a network scan (&#x60;remote&#x60;) or a credentialed scan (&#x60;local&#x60;). | [optional] 
**version** | **str** | The plugin version. | [optional] 
**severity** | **int** | The severity level of the plugin. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


