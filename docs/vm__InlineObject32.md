# InlineObject32

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name for the new scanner group. | 
**type** | **str** | The type of scanner group. If you omit this parameter, Tenable.io automatically uses the default (&#x60;load_balancing&#x60;). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


