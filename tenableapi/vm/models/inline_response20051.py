# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20051(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'id': 'int',
        'template_uuid': 'str',
        'name': 'str',
        'description': 'str',
        'owner_id': 'str',
        'owner': 'str',
        'shared': 'int',
        'user_permissions': 'int',
        'creation_date': 'int',
        'last_modification_date': 'int',
        'visibility': 'int',
        'no_target': 'bool'
    }

    attribute_map = {
        'id': 'id',
        'template_uuid': 'template_uuid',
        'name': 'name',
        'description': 'description',
        'owner_id': 'owner_id',
        'owner': 'owner',
        'shared': 'shared',
        'user_permissions': 'user_permissions',
        'creation_date': 'creation_date',
        'last_modification_date': 'last_modification_date',
        'visibility': 'visibility',
        'no_target': 'no_target'
    }

    def __init__(self, id=None, template_uuid=None, name=None, description=None, owner_id=None, owner=None, shared=None, user_permissions=None, creation_date=None, last_modification_date=None, visibility=None, no_target=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20051 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._id = None
        self._template_uuid = None
        self._name = None
        self._description = None
        self._owner_id = None
        self._owner = None
        self._shared = None
        self._user_permissions = None
        self._creation_date = None
        self._last_modification_date = None
        self._visibility = None
        self._no_target = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if template_uuid is not None:
            self.template_uuid = template_uuid
        if name is not None:
            self.name = name
        if description is not None:
            self.description = description
        if owner_id is not None:
            self.owner_id = owner_id
        if owner is not None:
            self.owner = owner
        if shared is not None:
            self.shared = shared
        if user_permissions is not None:
            self.user_permissions = user_permissions
        if creation_date is not None:
            self.creation_date = creation_date
        if last_modification_date is not None:
            self.last_modification_date = last_modification_date
        if visibility is not None:
            self.visibility = visibility
        if no_target is not None:
            self.no_target = no_target

    @property
    def id(self):
        """Gets the id of this InlineResponse20051.  # noqa: E501

        The unique ID of the policy.  # noqa: E501

        :return: The id of this InlineResponse20051.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20051.

        The unique ID of the policy.  # noqa: E501

        :param id: The id of this InlineResponse20051.  # noqa: E501
        :type id: int
        """

        self._id = id

    @property
    def template_uuid(self):
        """Gets the template_uuid of this InlineResponse20051.  # noqa: E501

        The UUID for the Tenable-provided template used to create the policy.  # noqa: E501

        :return: The template_uuid of this InlineResponse20051.  # noqa: E501
        :rtype: str
        """
        return self._template_uuid

    @template_uuid.setter
    def template_uuid(self, template_uuid):
        """Sets the template_uuid of this InlineResponse20051.

        The UUID for the Tenable-provided template used to create the policy.  # noqa: E501

        :param template_uuid: The template_uuid of this InlineResponse20051.  # noqa: E501
        :type template_uuid: str
        """

        self._template_uuid = template_uuid

    @property
    def name(self):
        """Gets the name of this InlineResponse20051.  # noqa: E501

        The name of the policy.  # noqa: E501

        :return: The name of this InlineResponse20051.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse20051.

        The name of the policy.  # noqa: E501

        :param name: The name of this InlineResponse20051.  # noqa: E501
        :type name: str
        """

        self._name = name

    @property
    def description(self):
        """Gets the description of this InlineResponse20051.  # noqa: E501

        The description of the policy.  # noqa: E501

        :return: The description of this InlineResponse20051.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this InlineResponse20051.

        The description of the policy.  # noqa: E501

        :param description: The description of this InlineResponse20051.  # noqa: E501
        :type description: str
        """

        self._description = description

    @property
    def owner_id(self):
        """Gets the owner_id of this InlineResponse20051.  # noqa: E501

        The unique ID of the owner of the policy.  # noqa: E501

        :return: The owner_id of this InlineResponse20051.  # noqa: E501
        :rtype: str
        """
        return self._owner_id

    @owner_id.setter
    def owner_id(self, owner_id):
        """Sets the owner_id of this InlineResponse20051.

        The unique ID of the owner of the policy.  # noqa: E501

        :param owner_id: The owner_id of this InlineResponse20051.  # noqa: E501
        :type owner_id: str
        """

        self._owner_id = owner_id

    @property
    def owner(self):
        """Gets the owner of this InlineResponse20051.  # noqa: E501

        The username for the owner of the policy.  # noqa: E501

        :return: The owner of this InlineResponse20051.  # noqa: E501
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this InlineResponse20051.

        The username for the owner of the policy.  # noqa: E501

        :param owner: The owner of this InlineResponse20051.  # noqa: E501
        :type owner: str
        """

        self._owner = owner

    @property
    def shared(self):
        """Gets the shared of this InlineResponse20051.  # noqa: E501

        The shared status of the policy (`1` if shared with users other than owner, `0` if not shared).  # noqa: E501

        :return: The shared of this InlineResponse20051.  # noqa: E501
        :rtype: int
        """
        return self._shared

    @shared.setter
    def shared(self, shared):
        """Sets the shared of this InlineResponse20051.

        The shared status of the policy (`1` if shared with users other than owner, `0` if not shared).  # noqa: E501

        :param shared: The shared of this InlineResponse20051.  # noqa: E501
        :type shared: int
        """

        self._shared = shared

    @property
    def user_permissions(self):
        """Gets the user_permissions of this InlineResponse20051.  # noqa: E501

        The sharing permissions for the policy.  # noqa: E501

        :return: The user_permissions of this InlineResponse20051.  # noqa: E501
        :rtype: int
        """
        return self._user_permissions

    @user_permissions.setter
    def user_permissions(self, user_permissions):
        """Sets the user_permissions of this InlineResponse20051.

        The sharing permissions for the policy.  # noqa: E501

        :param user_permissions: The user_permissions of this InlineResponse20051.  # noqa: E501
        :type user_permissions: int
        """

        self._user_permissions = user_permissions

    @property
    def creation_date(self):
        """Gets the creation_date of this InlineResponse20051.  # noqa: E501

        The creation date of the policy in Unix time format.  # noqa: E501

        :return: The creation_date of this InlineResponse20051.  # noqa: E501
        :rtype: int
        """
        return self._creation_date

    @creation_date.setter
    def creation_date(self, creation_date):
        """Sets the creation_date of this InlineResponse20051.

        The creation date of the policy in Unix time format.  # noqa: E501

        :param creation_date: The creation_date of this InlineResponse20051.  # noqa: E501
        :type creation_date: int
        """

        self._creation_date = creation_date

    @property
    def last_modification_date(self):
        """Gets the last_modification_date of this InlineResponse20051.  # noqa: E501

        The last modification date for the policy in Unix time format.  # noqa: E501

        :return: The last_modification_date of this InlineResponse20051.  # noqa: E501
        :rtype: int
        """
        return self._last_modification_date

    @last_modification_date.setter
    def last_modification_date(self, last_modification_date):
        """Sets the last_modification_date of this InlineResponse20051.

        The last modification date for the policy in Unix time format.  # noqa: E501

        :param last_modification_date: The last_modification_date of this InlineResponse20051.  # noqa: E501
        :type last_modification_date: int
        """

        self._last_modification_date = last_modification_date

    @property
    def visibility(self):
        """Gets the visibility of this InlineResponse20051.  # noqa: E501

        The visibility of the target (`private` or `shared`).  # noqa: E501

        :return: The visibility of this InlineResponse20051.  # noqa: E501
        :rtype: int
        """
        return self._visibility

    @visibility.setter
    def visibility(self, visibility):
        """Sets the visibility of this InlineResponse20051.

        The visibility of the target (`private` or `shared`).  # noqa: E501

        :param visibility: The visibility of this InlineResponse20051.  # noqa: E501
        :type visibility: int
        """

        self._visibility = visibility

    @property
    def no_target(self):
        """Gets the no_target of this InlineResponse20051.  # noqa: E501

        If `true`, the policy configuration does not include targets.  # noqa: E501

        :return: The no_target of this InlineResponse20051.  # noqa: E501
        :rtype: bool
        """
        return self._no_target

    @no_target.setter
    def no_target(self, no_target):
        """Sets the no_target of this InlineResponse20051.

        If `true`, the policy configuration does not include targets.  # noqa: E501

        :param no_target: The no_target of this InlineResponse20051.  # noqa: E501
        :type no_target: bool
        """

        self._no_target = no_target

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20051):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20051):
            return True

        return self.to_dict() != other.to_dict()
