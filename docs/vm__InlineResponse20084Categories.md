# InlineResponse20084Categories

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** | The UUID of the category. | [optional] 
**name** | **str** | The name of the category. The name must be unique within a Tenable.io instance. | [optional] 
**description** | **str** | The description of the category. | [optional] 
**created_at** | **str** | An ISO timestamp indicating the date and time on which the category was created, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**created_by** | **str** | The name of the user who created the category. | [optional] 
**updated_at** | **str** | An ISO timestamp indicating the date and time on which the category was last updated, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | [optional] 
**updated_by** | **str** | The name of the user who last updated the category. | [optional] 
**reserved** | **bool** | Indicates whether the tags in this category are reserved (cannot be updated). This is a read-only field set by the system. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


