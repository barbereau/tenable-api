# InlineResponse2007

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The unique ID of the folder. | [optional] 
**name** | **str** | The name of the folder. | [optional] 
**type** | **str** | The type of the folder (main, trash, custom). | [optional] 
**default_tag** | **int** | Whether or not the folder is the default (1 or 0). | [optional] 
**custom** | **int** | The custom status of the folder (1 or 0). | [optional] 
**unread_count** | **int** | The number of unread scans in the folder. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


