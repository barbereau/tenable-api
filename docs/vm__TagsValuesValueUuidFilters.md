# TagsValuesValueUuidFilters

The filters (conditional sets of rules) for automatically applying the tag to assets. For more information, see [Apply Dynamic Tags](doc:apply-dynamic-tags).
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset** | [**TagsValuesValueUuidFiltersAsset**](vm__TagsValuesValueUuidFiltersAsset.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


