# InlineResponse202

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scan_id** | **str** | The UUID of the scan. Retain this UUID to [view the scan details](ref:was-v2-scans-details), including the scan status. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


