# InlineResponse20045Scanners

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creation_date** | **int** | The date on which the scanner was linked to the Tenable.io instance. | [optional] 
**distro** | **str** | The scanner software distribution. | [optional] 
**engine_build** | **str** | The build of the engine running on the scanner. | [optional] 
**engine_version** | **str** | The version of the engine running on the scanner. | [optional] 
**group** | **bool** | Indicates whether the object represents a single scanner (&#x60;false&#x60;) or a scanner group (&#x60;true&#x60;). | [optional] 
**id** | **int** | The unique ID of the scanner. | [optional] 
**key** | **str** | An alpha-numeric sequence of characters used when linking a scanner to Tenable.io. | [optional] 
**last_connect** | **int** | The Unix timestamp when the scanner last connected to the Tenable.io instance. | [optional] 
**last_modification_date** | **int** | The Unix timestamp when the scanner was last updated. | [optional] 
**linked** | **int** | Indicates if the scanner is enabled (&#x60;1&#x60;) or not disabled (&#x60;0&#x60;). | [optional] 
**loaded_plugin_set** | **str** | The current plugin set on the scanner. | [optional] 
**name** | **str** | The user-defined name of the scanner. | [optional] 
**num_hosts** | **int** | The number of hosts that the scanner&#39;s analysis have discovered. | [optional] 
**num_scans** | **int** | The number of scan tasks the scanner is currently executing. | [optional] 
**num_sessions** | **int** | The number of active sessions between the scanner and hosts. | [optional] 
**num_tcp_sessions** | **int** | The number of active TCP sessions between the scanner and hosts. | [optional] 
**owner** | **str** | The scanner owner. | [optional] 
**owner_id** | **int** | The ID of the scanner owner. | [optional] 
**owner_name** | **str** | The name of the scanner owner. | [optional] 
**owner_uuid** | **str** | The UUID of the scanner owner. | [optional] 
**platform** | **str** | The platform of the scanner. | [optional] 
**pool** | **bool** | Indicates whether the scanner is part of a scanner group. | [optional] 
**report_frequency** | **int** | The frequency (in seconds) at which the scanner polls the Tenable.io instance. | [optional] 
**settings** | **object** |  | [optional] 
**scan_count** | **int** | The current number of scans currently running on the scanner. | [optional] 
**source** | **str** | Historical attribute. Always &#x60;service&#x60;. | [optional] 
**status** | **str** | The scanner&#39;s current status. Possible values are:  - on—The scanner has connected in the last five minutes.  - off—The scanner has not connected in the last five minutes. | [optional] 
**timestamp** | **int** | Equivalent to the last_modification_date. | [optional] 
**type** | **str** | The type of scanner (local or remote). | [optional] 
**uuid** | **str** | The UUID of the scanner. | [optional] 
**remote_uuid** | **str** | The UUID of the Nessus installation on the scanner. | [optional] 
**supports_remote_logs** | **bool** | Indicates whether the scanner supports remote logging. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


