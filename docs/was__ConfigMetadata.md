# ConfigMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config_id** | **str** | The UUID of the scan configuration. | 
**owner_id** | **str** | The UUID of the owner of the scan configuration. | [optional] 
**is_shared** | **bool** |  | [optional] 
**user_permissions** | [**PermissionLevel**](was__PermissionLevel.md) |  | 
**name** | **str** | The scan configuration name. | 
**target** | **str** | The URL of the target web application. | [optional] 
**description** | **str** | The description of the scan configuration. | [optional] 
**created_at** | **datetime** | An ISO timestamp indicating the date and time when the scan configuration was created, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | 
**updated_at** | **datetime** | An ISO timestamp indicating the date and time when the scan configuration was last updated, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | 
**schedule** | [**ScanSchedule**](was__ScanSchedule.md) |  | [optional] 
**template_id** | **str** | The UUID of the Tenable-provided or user-defined configuration template from which this configuration was derived. | [optional] 
**container_id** | **str** | The UUID of your organization&#39;s Tenable.io instance. | [optional] 
**last_scan** | [**Scan**](was__Scan.md) |  | [optional] 
**user_template** | [**ConfigMetadataUserTemplate**](ConfigMetadatawas__UserTemplate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


