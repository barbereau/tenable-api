# InlineObject38

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** | The UUID for the Tenable-provided scan template to use. Use the [GET /editor/scan/templates](ref:editor-list-templates) endpoint to find the template UUID. | 
**settings** | [**ScansSettings**](vm__ScansSettings.md) |  | 
**credentials** | [**ScansCredentials**](vm__ScansCredentials.md) |  | [optional] 
**plugins** | [**ScansPlugins**](vm__ScansPlugins.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


