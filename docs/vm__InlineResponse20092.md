# InlineResponse20092

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | A count of the vulnerability occurrences. | [optional] 
**vuln_count** | **int** |  | [optional] 
**description** | **str** | The description of the vulnerability. | [optional] 
**synopsis** | **str** | A brief summary of the vulnerability. | [optional] 
**solution** | **str** | Information on how to fix the vulnerability. | [optional] 
**discovery** | [**InlineResponse20092Discovery**](vm__InlineResponse20092Discovery.md) |  | [optional] 
**severity** | **int** | The severity level of the vulnerability. | [optional] 
**plugin_details** | [**InlineResponse20092PluginDetails**](vm__InlineResponse20092PluginDetails.md) |  | [optional] 
**reference_information** | [**list[InlineResponse20092ReferenceInformation]**](vm__InlineResponse20092ReferenceInformation.md) |  | [optional] 
**risk_information** | [**InlineResponse20092RiskInformation**](vm__InlineResponse20092RiskInformation.md) |  | [optional] 
**see_also** | **list[str]** | Links to external websites that contain helpful information about the vulnerability. | [optional] 
**vulnerability_information** | [**InlineResponse20092VulnerabilityInformation**](vm__InlineResponse20092VulnerabilityInformation.md) |  | [optional] 
**vpr** | [**InlineResponse20092Vpr**](vm__InlineResponse20092Vpr.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


