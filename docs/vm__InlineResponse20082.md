# InlineResponse20082

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vulnerabilities** | **list[str]** | A list of Common Vulnerabilities and Exposures (CVEs). | [optional] 
**pagination** | [**InlineResponse20082Pagination**](vm__InlineResponse20082Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


