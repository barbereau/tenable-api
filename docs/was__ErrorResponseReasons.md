# ErrorResponseReasons

A reason for the Tenable.io Web Application Scanning error, including a code and an extended description.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** | The Tenable.io Web Application Scanning error code. Error code values include:  - &#x60;NOT_FOUND&#x60;—Returned if Tenable.io Web Application Scanning could not find the resource you specified.  - &#x60;INVALID_ID_FORMAT&#x60;—Returned if you specify a resource ID in an invalid format.  - &#x60;INVALID_PARAMETER&#x60;—Returned if you specify an invalid URL parameter. - &#x60;INVALID_JSON_BODY&#x60;—Returned if you specify invalid JSON in request payload.  - &#x60;DUPLICATE_ENTITY&#x60;—Returned if you attempt to create a duplicate resource.  - &#x60;NOT_ALLOWED&#x60;—Returned if Tenable.io Web Application Scanning encounters a stateful conflict, for example, if you attempt to start a scan that is already in progress.  - &#x60;OPERATION_FORBIDDEN&#x60;—Returned if you do not have sufficient permissions to access a resource or complete a task. | 
**reason** | **str** | The extended description of the cause of the Tenable.io Web Application Scanning error. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


