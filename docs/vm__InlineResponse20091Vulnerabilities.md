# InlineResponse20091Vulnerabilities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | The number of times that a scan detected the vulnerability on an asset. | [optional] 
**plugin_family** | **str** | The plugin&#39;s family. | [optional] 
**plugin_id** | **int** | The unique plugin ID. | [optional] 
**plugin_name** | **str** | The name of the plugin that detected the vulnerability. | [optional] 
**vulnerability_state** | **str** | The current state of the reported plugin. Possible states include:  - Active—The vulnerability is currently present on an asset.  - New—The vulnerability is active on an asset, and was first detected within the last 14 days.  - Fixed—A subsequent scan detected that the formerly-active vulnerability is no longer present on an asset.  - Resurfaced—The vulnerability was previously marked as fixed on an asset, but a subsequent scan detected the vulnerability on the asset again. | [optional] 
**vpr_score** | **float** | The Vulnerability Priority Rating (VPR) for the vulnerability. If a plugin is designed to detect multiple vulnerabilities, the VPR represents the highest value calculated for a vulnerability associated with the plugin. For more information, see &lt;a href&#x3D;\&quot;https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Analysis/RiskMetrics.htm\&quot; target&#x3D;\&quot;_blank\&quot;&gt;Severity vs. VPR&lt;/a&gt; in the &lt;i&gt;Tenable.io Vulnerability Management User Guide&lt;/i&gt;. | [optional] 
**accepted_count** | **int** | The number of times that a user in the user interface created an accept rule for this vulnerability. For more information, see &lt;a href&#x3D;\&quot;https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/AboutRecastRules.htm\&quot; target&#x3D;\&quot;_blank\&quot;&gt;Recast Rules&lt;/a&gt; in the &lt;i&gt;Tenable.io Vulnerability Management User Guide&lt;/i&gt;. | [optional] 
**recasted_count** | **int** | The number of times that a user in the user interface created a recast rule for this vulnerability. For more information, see &lt;a href&#x3D;\&quot;https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/AboutRecastRules.htm\&quot; target&#x3D;\&quot;_blank\&quot;&gt;Recast Rules&lt;/a&gt; in the &lt;i&gt;Tenable.io Vulnerability Management User Guide&lt;/i&gt;. | [optional] 
**counts_by_severity** | [**list[InlineResponse20091CountsBySeverity]**](vm__InlineResponse20091CountsBySeverity.md) | The number of times that a scan detected the vulnerability on an asset, grouped by severity level. | [optional] 
**severity** | **int** | The severity level of the vulnerability, as defined using the Common Vulnerability Scoring System (CVSS) base score. Possible values include:   - 0—The vulnerability has a CVSS score of 0, which corresponds to the \&quot;info\&quot; severity level.  - 1—The vulnerability has a CVSS score between 0.1 and 3.9, which corresponds to the \&quot;low\&quot; severity level.  - 2—The vulnerability has a CVSS score between 4.0 and 6.9, which corresponds to the \&quot;medium\&quot; severity level.  - 3—The vulnerability has a CVSS score between 7.0 and 9.9, which corresponds to the \&quot;high\&quot; severity level.  - 4—The vulnerability has a CVSS score of 10.0, which corresponds to the \&quot;critical\&quot; severity level. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


