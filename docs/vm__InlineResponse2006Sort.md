# InlineResponse2006Sort

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sortable_fields** | **str** | The names of the fields you can use when constructing &#x60;sort&#x60; parameter values for the [GET /v2/access-groups](ref:io-v2-access-groups-list) endpoint. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


