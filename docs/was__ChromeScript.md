# ChromeScript

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**finish_wait** | **int** | The time (in milliseconds) that the scanner waits for the Selenium script to complete before the browser loads any remaining contents. | [optional] [default to 5000]
**page_load_wait** | **int** | The time (in milliseconds) that page rendering is delayed during the Selenium authentication process. It is recommended to wait for some time if a page is known to take time before its contents are rendered. | [optional] [default to 10000]
**command_wait** | **int** | The time (in milliseconds) to wait after running each command in the Selenium script. | [optional] [default to 500]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


