# InlineResponse20047DataAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plugin_modification_date** | **str** | The date when Tenable last updated the plugin. | [optional] 
**plugin_version** | **str** | The version of the plugin. | [optional] 
**exploited_by_malware** | **bool** | Indicates whether the vulnerability discovered by this plugin is known to be exploited by malware. | [optional] 
**description** | **str** | The extended description of the plugin. | [optional] 
**unsupported_by_vendor** | **bool** | Indicates whether the software found by this plugin is unsupported by the software&#39;s vendor (for example, Windows 95 or Firefox 3). | [optional] 
**cvss_temporal_score** | **float** | The raw CVSSv2 temporal metrics for the vulnerability. | [optional] 
**patch_publication_date** | **str** | The date when the vendor published a patch for the vulnerability. | [optional] 
**see_also** | **list[str]** | Links to external websites that contain helpful information about the vulnerability. | [optional] 
**default_account** | **str** | Indicates whether the plugin checks for default accounts requiring the use of credentials other than the credentials provided in the scan policy. For more information, see [What are the plugins that test for default accounts?](https://community.tenable.com/s/article/What-are-the-plugins-that-test-for-default-accounts) in the Tenable Community Portal. | [optional] 
**exploit_available** | **bool** | Indicates whether a known public exploit exists for the vulnerability. | [optional] 
**cve** | **list[str]** | A list of Common Vulnerabilities and Exposures (CVE) IDs for the vulnerabilities associated with the plugin. | [optional] 
**exploit_framework_canvas** | **bool** | Indicates whether an exploit exists in the Immunity CANVAS framework. | [optional] 
**cvss_base_score** | **str** | The CVSSv2 base score (intrinsic and fundamental characteristics of a vulnerability that are constant over time and user environments). | [optional] 
**solution** | **str** | Remediation information for the vulnerability. | [optional] 
**cvss_vector** | [**InlineResponse20047DataCvssVector**](vm__InlineResponse20047DataCvssVector.md) |  | [optional] 
**exploit_framework_exploithub** | **bool** | Indicates whether an exploit exists in the ExploitHub framework. | [optional] 
**cpe** | **list[str]** | A list of plugin target systems identified by Common Platform Enumeration (CPE). | [optional] 
**plugin_publication_date** | **str** | The date when Tenable originally published the plugin. | [optional] 
**exploit_framework_core** | **bool** | Indicates whether an exploit exists in the CORE Impact framework. | [optional] 
**in_the_news** | **bool** | Indicates whether this plugin has received media attention (for example, ShellShock, Meltdown). | [optional] 
**has_patch** | **bool** | Indicates whether the vendor has published a patch for the vulnerability. | [optional] 
**xref** | **list[str]** | References to third-party information about the vulnerability, exploit, or update associated with the plugin. Each reference includes a type, for example, &#39;FEDORA&#39;, and an ID, for example, &#39;2003-047&#39;. | [optional] 
**malware** | **bool** | Indicates whether the plugin targets potentially malicious files or processes. | [optional] 
**exploit_framework_d2_elliot** | **bool** | Indicates an exploit exists in the D2 Elliot Web Exploitation framework. | [optional] 
**xrefs** | **list[str]** | References to third-party information about the vulnerability, exploit, or update associated with the plugin. Each reference includes a type and an ID. For example, &#39;FEDORA&#39; and &#39;2003-047&#39;. | [optional] 
**risk_factor** | **str** | The risk factor associated with the plugin. Possible values are: Low, Medium, High, or Critical. | [optional] 
**synopsis** | **str** | A brief summary of the vulnerability or vulnerabilities associated with the plugin. | [optional] 
**cvss3_temporal_score** | **float** | The CVSSv3 temporal metrics for the vulnerability. | [optional] 
**exploited_by_nessus** | **bool** | Indicates whether Nessus exploited the vulnerability during the process of identification. | [optional] 
**cvss3_base_score** | **str** | The CVSSv3 base score (intrinsic and fundamental characteristics of a vulnerability that are constant over time and user environments). | [optional] 
**exploit_framework_metasploit** | **bool** | Indicates whether an exploit exists in the Metasploit framework. | [optional] 
**plugin_type** | **str** | Plugin type, for example, local, remote, or combined. For more information about plugin type, see [Nessus Plugin Types and Categories](https://community.tenable.com/s/article/Nessus-Plugin-Types-and-Categories) in the Tenable Community Portal. | [optional] 
**vpr** | [**InlineResponse20047DataVpr**](vm__InlineResponse20047DataVpr.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


