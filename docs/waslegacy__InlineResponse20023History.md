# InlineResponse20023History

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time_end** | **int** | The date the scan completed in Unix time. | [optional] 
**scan_uuid** | **str** | The scan history&#39;s UUID. | [optional] 
**time_start** | **int** | The date the scan started in Unix time. | [optional] 
**visibility** | **str** | The visibility of the scan in workbenches (public or private). | [optional] 
**targets** | [**InlineResponse20023Targets**](waslegacy__InlineResponse20023Targets.md) |  | [optional] 
**status** | **str** | The status of the scan (completed, aborted, imported, pending, running, resuming, canceling, canceled, pausing, paused, stopping, stopped). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


