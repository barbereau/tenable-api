# InlineResponse20064Notes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** | The title of the note. | [optional] 
**message** | **str** | The specific message of the note. | [optional] 
**severity** | **int** | The severity of the note. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


