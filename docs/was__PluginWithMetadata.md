# PluginWithMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plugin_id** | **int** | The ID of the plugin used to detect the vulnerability. | 
**name** | **str** | The name of the plugin. | 
**family** | **str** | The name of the plugin family. | 
**solution** | **str** | Remediation information for the vulnerability targeted by the plugin. | [optional] 
**description** | **str** | An extended description of the vulnerability targeted by the plugin. | 
**synopsis** | **str** | A short summary of the vulnerability targeted by the plugin. | 
**published** | **datetime** | The publication date of the plugin. | [optional] 
**patch_published** | **datetime** | The date a patch was published to fix the vulnerability targeted by the plugin. | [optional] 
**plugin_modified** | **datetime** | The date when Tenable last modified the plugin. | [optional] 
**risk_factor** | **str** | The risk factor associated with the plugin. Possible values are:  - low  - medium  - high  - critical. See the &#x60;risk_factor&#x60; attribute in [Tenable Plugin Attributes](doc:tenable-plugin-attributes). | 
**see_also** | **list[str]** | A reference link discussing the vulnerability targeted by the plugin, typically a vendor security advisory or news article. | [optional] 
**cvss3_base_score** | **float** | The CVSSv3 base score for the vulnerability targeted by the plugin (intrinsic and fundamental characteristics of a vulnerability that are constant over time and user environments). | [optional] 
**wasc** | **list[str]** | The list of Web Application Security Consortium (WASC) category associated with the vulnerability targeted by the plugin. | [optional] 
**cwe** | **list[str]** | The Common Weakness Enumeration (CWE) ID for the vulnerability targeted by the plugin. | [optional] 
**owasp** | [**list[PluginWithMetadataOwasp]**](was__PluginWithMetadataOwasp.md) | The Open Web Application Security Project (OWASP) category for the vulnerability targeted by the plugin. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


