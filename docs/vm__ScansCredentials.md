# ScansCredentials

An object that specifies credential parameters that enable a scanner to authenticate a connection to a target host.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**add** | [**ScansCredentialsAdd**](vm__ScansCredentialsAdd.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


