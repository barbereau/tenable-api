# Scan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scan_id** | **str** | The UUID of the scan that detected the vulnerability in the web application. | 
**asset_id** | **str** | The UUID of the asset being scanned. | 
**user_id** | **str** | The UUID of the user that created the scan. | 
**config_id** | **str** | The UUID of the scan configuration. | 
**application_uri** | **str** | The URI of the web application. **Deprecated:** This parameter is deprecated and will be retired on 2021/03/01. Tenable recommends that you use the &#x60;target&#x60; parameter instead. Please update any existing integrations that your organization has. | [optional] 
**target** | **str** | The URI of the web application. | 
**created_at** | **datetime** | An ISO timestamp indicating the date and time when the scan configuration was created, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | 
**updated_at** | **datetime** | An ISO timestamp indicating the date and time when the scan processing status changed, for example, &#x60;2018-12-31T13:51:17.243Z&#x60;. | 
**requested_action** | [**RequestedAction**](was__RequestedAction.md) |  | 
**status** | [**ScanStatus**](was__ScanStatus.md) |  | 
**metadata** | [**ScanMetadata**](was__ScanMetadata.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


