# InlineResponse20096

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | [**InlineResponse20096Info**](vm__InlineResponse20096Info.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


