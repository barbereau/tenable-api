# InlineResponse20047Params

The URL query parameters for the returned data set. If the request does not specify the parameters, contains default values.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page** | **int** | The result set page index. For example, if page size is 10, page 2 contains records 10-19. Default value is 1. | [optional] 
**size** | **int** | The result set page size. Default value is 1,000. | [optional] 
**last_updated** | **str** | The &#39;last_updated&#39; filter value. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


