# InlineResponse20062

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**folders** | [**list[InlineResponse20041]**](vm__InlineResponse20041.md) |  | [optional] 
**scans** | [**list[InlineResponse20062Scans]**](vm__InlineResponse20062Scans.md) |  | [optional] 
**timestamp** | **int** | The Unix timestamp when Tenable.io received the list request. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


