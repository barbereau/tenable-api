# ExclusionsSchedule

The schedule parameters for the exclusion.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** | If &#x60;true&#x60;, the exclusion schedule is active. | [optional] 
**starttime** | **str** | The start time of the exclusion formatted as &#x60;YYYY-MM-DD HH:MM:SS&#x60;. | [optional] 
**endtime** | **str** | The end time of the exclusion formatted as &#x60;YYYY-MM-DD HH:MM:SS&#x60;. | [optional] 
**timezone** | **str** | The timezone for the exclusion as returned by [scans: timezones](ref:scans-timezones). | [optional] 
**rrules** | [**ExclusionsScheduleRrules**](vm__ExclusionsScheduleRrules.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


