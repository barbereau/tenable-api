# AccessGroupsRules

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The type of asset rule. The asset rule type corresponds to the type of data you can specify in the &#x60;terms&#x60; parameter. For a complete list of supported rule types, use the [GET /access-groups/filters](ref:io-v1-access-groups-list-filters) endpoint. | [optional] 
**operator** | **str** | The operator that specifies how Tenable.io matches the terms value to asset data.   Possible operators include:   - eq—Tenable.io matches the rule to assets based on an exact match of the specified term. Note: Tenable.io interprets the operator as &#x60;equals&#x60; for ipv4 rules that specify a single IP address, but interprets the operator as &#x60;contains&#x60; for ipv4 rules that specify an IP range or CIDR range.  - match—Tenable.io matches the rule to assets based a partial match of the specified term.  - starts—Tenable.io matches the rule to assets that start with the specified term.  - ends—Tenable.io matches the rule to assets that end with the specified term.  For a complete list of operators by rule type, use the [GET /access-groups/rules/filters](ref:io-v1-access-groups-list-rule-filters) endpoint. | [optional] 
**terms** | **list[str]** | The values that Tenable.io uses to match an asset to the rule. A term must correspond to the rule type.  For example:  - If the rule type is &#x60;aws_account&#x60;, the term is an AWS account ID.  - If the rule type is &#x60;fqdn&#x60;, the term is a hostname or a fully-qualified domain name (FQDN).  - If the rule type is &#x60;ipv4&#x60;, the term is an individual IPv4 address, a range of IPv4 addresses (for example, 172.204.81.57-172.204.81.60), or a CIDR range (for example, 172.204.81.57/24).   For a complete list of supported values by rule type, use the [GET /access-groups/rules/filters](ref:io-v1-access-groups-list-rule-filters) endpoint.    If you specify multiple terms values, Tenable.io includes an asset in the access group if the asset&#39;s attributes match any of the terms in the rule. &lt;br &gt;You can specify up to 100,000 terms per asset rule. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


