# Address

The address of the owner of the provisioned account.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **str** | The primary contact&#39;s street address. | 
**street_two** | **str** | Additional street address information (such as a suite number, floor number, building name, or P.O. box). | [optional] 
**city** | **str** | The primary contact&#39;s city. | 
**country** | **str** | The primary contact&#39;s country in ISO 3166-1 alpha-2 format. | 
**state** | **str** | The primary contact&#39;s state. | 
**zip** | **str** | The primary contact&#39;s USPS postal code. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


