# InlineResponse20026

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**output** | [**list[InlineResponse20026Output]**](waslegacy__InlineResponse20026Output.md) |  | [optional] 
**info** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


