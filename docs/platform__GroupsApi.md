# tenableapi.platform.GroupsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**groups_add_user**](platform__GroupsApi.md#groups_add_user) | **POST** /groups/{group_id}/users/{user_id} | Add user to group
[**groups_create**](platform__GroupsApi.md#groups_create) | **POST** /groups | Create group
[**groups_delete**](platform__GroupsApi.md#groups_delete) | **DELETE** /groups/{group_id} | Delete group
[**groups_delete_user**](platform__GroupsApi.md#groups_delete_user) | **DELETE** /groups/{group_id}/users/{user_id} | Delete user from group
[**groups_edit**](platform__GroupsApi.md#groups_edit) | **PUT** /groups/{group_id} | Update group
[**groups_list**](platform__GroupsApi.md#groups_list) | **GET** /groups | List groups
[**groups_list_users**](platform__GroupsApi.md#groups_list_users) | **GET** /groups/{group_id}/users | List users in group


# **groups_add_user**
> object groups_add_user(group_id, user_id)

Add user to group

Add a user to the group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.GroupsApi(api_client)
    group_id = 56 # int | The unique ID of the group.
user_id = 56 # int | The unique ID of the user.

    try:
        # Add user to group
        api_response = api_instance.groups_add_user(group_id, user_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupsApi->groups_add_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique ID of the group. | 
 **user_id** | **int**| The unique ID of the user. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully added the user to the group. |  -  |
**403** | Returned if you do not have permission to add users to a group. |  -  |
**404** | Returned if Tenable.io cannot find the specified group or user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to add the user to the group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groups_create**
> InlineResponse2004 groups_create(inline_object8)

Create group

Create a group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.GroupsApi(api_client)
    inline_object8 = tenableapi.platform.InlineObject8() # InlineObject8 | 

    try:
        # Create group
        api_response = api_instance.groups_create(inline_object8)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupsApi->groups_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object8** | [**InlineObject8**](platform__InlineObject8.md)|  | 

### Return type

[**InlineResponse2004**](platform__InlineResponse2004.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates the user group. |  -  |
**400** | Returned if your request message contains an invalid parameter. |  -  |
**403** | Returned if you do not have permission to create a group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to add the group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groups_delete**
> object groups_delete(group_id)

Delete group

Delete a group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.GroupsApi(api_client)
    group_id = 56 # int | The unique ID of the group.

    try:
        # Delete group
        api_response = api_instance.groups_delete(group_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupsApi->groups_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique ID of the group. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deletes the specified user group. |  -  |
**400** | Returned if Tenable.io cannot find the specified user group. |  -  |
**403** | Returned if you do not have permission to delete the group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to delete the group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groups_delete_user**
> object groups_delete_user(group_id, user_id)

Delete user from group

Deletes a user from the group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.GroupsApi(api_client)
    group_id = 56 # int | The unique ID of the group.
user_id = 56 # int | The unique ID of the user.

    try:
        # Delete user from group
        api_response = api_instance.groups_delete_user(group_id, user_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupsApi->groups_delete_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique ID of the group. | 
 **user_id** | **int**| The unique ID of the user. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io removes the user from the group. |  -  |
**403** | Returned if you do not have permission to delete users from the group. |  -  |
**404** | Returned if Tenable.io cannot find the specified group or user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to remove the user from the group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groups_edit**
> InlineResponse2003 groups_edit(group_id, inline_object9)

Update group

Edit a group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.GroupsApi(api_client)
    group_id = 56 # int | The unique ID of the group.
inline_object9 = tenableapi.platform.InlineObject9() # InlineObject9 | 

    try:
        # Update group
        api_response = api_instance.groups_edit(group_id, inline_object9)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupsApi->groups_edit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique ID of the group. | 
 **inline_object9** | [**InlineObject9**](platform__InlineObject9.md)|  | 

### Return type

[**InlineResponse2003**](platform__InlineResponse2003.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io updates the user group. |  -  |
**400** | Returned if your request message contains an invalid parameter. |  -  |
**403** | Returned if you do not have permission to edit a group. |  -  |
**404** | Returned if Tenable.io cannot find the specified group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to edit the group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groups_list**
> list[InlineResponse2003] groups_list()

List groups

Returns the group list.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.GroupsApi(api_client)
    
    try:
        # List groups
        api_response = api_instance.groups_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupsApi->groups_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse2003]**](platform__InlineResponse2003.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the groups list. |  -  |
**403** | Returned if you do not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groups_list_users**
> list[InlineResponse200] groups_list_users(group_id)

List users in group

Return the group user list.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.GroupsApi(api_client)
    group_id = 56 # int | The unique ID of the group.

    try:
        # List users in group
        api_response = api_instance.groups_list_users(group_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling GroupsApi->groups_list_users: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique ID of the group. | 

### Return type

[**list[InlineResponse200]**](platform__InlineResponse200.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns if the group user list. |  -  |
**403** | Returned if you do not have permission to list a group&#39;s users. |  -  |
**404** | Returned if Tenable.io cannot find the specified group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

