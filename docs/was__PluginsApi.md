# tenableapi.was.PluginsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_v2_plugins_details**](was__PluginsApi.md#was_v2_plugins_details) | **GET** /was/v2/plugins/{plugin_id} | Get plugin details
[**was_v2_plugins_list**](was__PluginsApi.md#was_v2_plugins_list) | **GET** /was/v2/plugins | List plugins


# **was_v2_plugins_details**
> PluginWithMetadata was_v2_plugins_details(plugin_id)

Get plugin details

Returns details for the specified Tenable.io Web Application Scanning plugin. <p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.PluginsApi(api_client)
    plugin_id = 56 # int | The ID of a Tenable.io Web Application Scanning plugin.

    try:
        # Get plugin details
        api_response = api_instance.was_v2_plugins_details(plugin_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->was_v2_plugins_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plugin_id** | **int**| The ID of a Tenable.io Web Application Scanning plugin. | 

### Return type

[**PluginWithMetadata**](was__PluginWithMetadata.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns details for the specified Tenable.io Web Application Scanning plugin. |  -  |
**400** | Returned if your request specifies an invalid ID for a Tenable.io Web Application Scanning plugin. |  -  |
**404** | Returned if Tenable.io Web Application Scanning cannot find the specified plugin. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_v2_plugins_list**
> InlineResponse2007 was_v2_plugins_list()

List plugins

Returns a list of plugins used in Tenable.io Web Application Scanning scans. <p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.was
from tenableapi.was.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.was.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.was.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.was.PluginsApi(api_client)
    
    try:
        # List plugins
        api_response = api_instance.was_v2_plugins_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->was_v2_plugins_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2007**](was__InlineResponse2007.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of plugins. |  * Cache-Control -  <br>  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

