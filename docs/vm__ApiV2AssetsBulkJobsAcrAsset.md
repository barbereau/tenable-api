# ApiV2AssetsBulkJobsAcrAsset

Each object can contain a single instance of the properties described below. You can combine multiple instances of this object, each containing a different single property.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The UUID for a specific asset. Use this value as the unique key for the asset. | [optional] 
**fqdn** | **list[str]** | Fully-qualified domain names (FQDNs) associated with the asset or assets. | [optional] 
**mac_address** | **list[str]** | MAC addresses associated with the asset or assets. | [optional] 
**netbios_name** | **str** | The NetBIOS name for the asset. | [optional] 
**ipv4** | **list[str]** | IPv4 addresses associated with the asset or assets. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


