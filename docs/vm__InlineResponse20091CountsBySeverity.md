# InlineResponse20091CountsBySeverity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | The number of times that a scan detected the vulnerability on an asset while the vulnerability was assigned the specified severity level. | [optional] 
**value** | **int** | The severity level of the vulnerabilities in the group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


