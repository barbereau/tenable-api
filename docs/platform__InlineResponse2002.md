# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_key** | **str** | The access key for the user account in Tenable.io. Use this key in combination with the user&#39;s secret key to submit authorized API requests to Tenable.io. | [optional] 
**secret_key** | **str** | The secret key for the user account in Tenable.io. Use this key in combination with the user&#39;s access key to submit authorized API requests to Tenable.io. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


