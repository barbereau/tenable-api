# InlineResponse20011Pagination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | The total number of records which match any applied filters. This number may be approximate. | [optional] 
**offset** | **int** | The index of the first record retrieved. | [optional] 
**limit** | **int** | The number of records returned with this response. | [optional] 
**sort** | **list[str]** | The sorting parameters applied to response, in order of application. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


