# ConfigSettings

The scan configuration settings.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target** | **str** | The URL of the target web application.   **Deprecated:** The target parameter within the settings object is deprecated and will be retired on 2021/03/01. You can now define &#x60;target&#x60; in the main body of your request. Tenable recommends that you use the &#x60;target&#x60; parameter in the main body instead. Please update any existing integrations that your organization has. | [optional] 
**description** | **str** | The description of the scan configuration. | [optional] 
**timeout** | **str** | The maximum duration the scan runs before it stops automatically. The maximum duration you can set for your overall scan max time is 99:59:59 (hours:minutes:seconds). | [optional] 
**debug_mode** | **bool** | Indicates whether scan debug mode is enabled (&#x60;true&#x60;) or disabled (&#x60;false&#x60;). Scan debug mode provides additional information to users. | [optional] 
**input_force** | **bool** | Indicates whether to force input (&#x60;true&#x60;) or not (&#x60;false&#x60;). | [optional] 
**credentials** | [**Credentials**](was__Credentials.md) |  | [optional] 
**scope** | [**ScopeSettings**](was__ScopeSettings.md) |  | [optional] 
**plugin** | [**PluginSettings**](was__PluginSettings.md) |  | [optional] 
**browser** | [**BrowserSettings**](was__BrowserSettings.md) |  | [optional] 
**http** | [**HttpSettings**](was__HttpSettings.md) |  | [optional] 
**chrome_script** | [**ChromeScript**](was__ChromeScript.md) |  | [optional] 
**assessment** | [**AssessmentSettings**](was__AssessmentSettings.md) |  | [optional] 
**audit** | [**AuditSettings**](was__AuditSettings.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


