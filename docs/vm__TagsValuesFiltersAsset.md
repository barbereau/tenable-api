# TagsValuesFiltersAsset

The object containing conditional sets of rules for applying the tags.   **Note:** Tenable.io supports a maximum of 3,000 rules per tag. This limit means that you can specify a maximum of 3,000 `and` or `or` conditions for a single tag value. There is no limit on the number of values you can specify in a comma-delimited string for the value of an individual rule; however, the request body is limited to 1 MB. For more information about asset tags and their limitations, see [Manage Asset Tags](doc:manage-asset-tags-tio).
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_and** | [**list[TagsValuesFiltersAssetAnd]**](vm__TagsValuesFiltersAssetAnd.md) | To apply the tag to assets that match all of the rules, specify the rules inside the &#x60;and&#x60; object.  **Note:** The value you specify for the rule attribute is not case sensitive. For example, if you type the hostname \&quot;hostx,\&quot; the rule matches to assets with hostname \&quot;HOSTX\&quot; and \&quot;hostx.\&quot; | [optional] 
**_or** | [**list[TagsValuesFiltersAssetAnd]**](vm__TagsValuesFiltersAssetAnd.md) | To apply the tag to assets that match any of the rules, specify the rules inside the &#x60;or&#x60; object.  **Note:** The value you specify for the rule attribute is not case sensitive. For example, if you type the hostname \&quot;hostx,\&quot; the rule matches to assets with hostname \&quot;HOSTX\&quot; and \&quot;hostx.\&quot; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


