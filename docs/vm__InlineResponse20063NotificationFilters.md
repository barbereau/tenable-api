# InlineResponse20063NotificationFilters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** | The attribute value Tenable.io filters on. For example, when filtering on severity, this attribute might specify &#x60;Critical&#x60;. | [optional] 
**quality** | **str** | The operator Tenable.io applies to the filter value, for example, &#x60;eq&#x60;. | [optional] 
**filter** | **str** | The attribute name. For example, use the &#x60;risk_factor&#x60; attribute if you want to filter on vulnerability severity. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


