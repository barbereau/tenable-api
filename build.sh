#!/usr/bin/env bash
# This scripts fetch all reports as defined in datasource.csv and starts
# processing the reports
cd "$(dirname "$0")";

CMD_DATE="date +%Y%m%d%H%M"
echo `$CMD_DATE` ": " "Starting script $0"
if [ ! -f "openapi-generator-cli" ]; then
  curl https://raw.githubusercontent.com/OpenAPITools/openapi-generator/master/bin/utils/openapi-generator-cli.sh > openapi-generator-cli
  chmod u+x openapi-generator-cli
fi

YAPF=`command -v yapf`
if [[  $? ]]; then
  export PYTHON_POST_PROCESS_FILE="/usr/local/bin/yapf -i"
fi

mkdir api

URL_TENABLE_API=https://developer.tenable.com/openapi/5eaa0b91ca22f7001c19f215
SRC=tenable-wasv2-api
KEY=was
curl $URL_TENABLE_API > api/$SRC.json
./openapi-generator-cli generate -i api/$SRC.json -g python-legacy -o $SRC -c openapi-generator-config-$KEY.yaml --git-repo-id python-tenable --git-user-id barbich
./merge_source.py --source $SRC/ --key "$KEY"_ --destination .
cp -R $SRC/tenableapi .
cp "$KEY"_README.md docs/

URL_TENABLE_API=https://developer.tenable.com/openapi/5e7020f258e7d6001830e124
SRC=tenable-vm-api
KEY=vm
curl $URL_TENABLE_API > api/$SRC.json
./openapi-generator-cli generate -i api/$SRC.json -g python-legacy -o $SRC -c openapi-generator-config-$KEY.yaml --git-repo-id python-tenable --git-user-id barbich
./merge_source.py --source $SRC/ --key "$KEY"_ --destination .
cp -R $SRC/tenableapi .
cp "$KEY"_README.md docs/

URL_TENABLE_API=https://developer.tenable.com/openapi/5c13ef849fceb409440e41c4
SRC=tenable-platform-api
KEY=platform
curl $URL_TENABLE_API > api/$SRC.json
./openapi-generator-cli generate -i api/$SRC.json -g python-legacy -o $SRC -c openapi-generator-config-$KEY.yaml --git-repo-id python-tenable --git-user-id barbich
./merge_source.py --source $SRC/ --key "$KEY"_ --destination .
cp -R $SRC/tenableapi .
cp "$KEY"_README.md docs/

URL_TENABLE_API=https://developer.tenable.com/openapi/5c13f0649fceb409440e4221
SRC=tenable-containerv1-api
KEY=containerlegacy
curl $URL_TENABLE_API > api/$SRC.json
./openapi-generator-cli generate -i api/$SRC.json -g python-legacy -o $SRC -c openapi-generator-config-$KEY.yaml --git-repo-id python-tenable --git-user-id barbich
./merge_source.py --source $SRC/ --key "$KEY"_ --destination .
cp -R $SRC/tenableapi .
cp "$KEY"_README.md docs/

URL_TENABLE_API=https://developer.tenable.com/openapi/5d0a5de1e486ff0174d52b76
SRC=tenable-containerv2-api
KEY=container
curl $URL_TENABLE_API > api/$SRC.json
./openapi-generator-cli generate -i api/$SRC.json -g python-legacy -o $SRC -c openapi-generator-config-$KEY.yaml --git-repo-id python-tenable --git-user-id barbich
./merge_source.py --source $SRC/ --key "$KEY"_ --destination .
cp -R $SRC/tenableapi .
cp "$KEY"_README.md docs/

URL_TENABLE_API=https://developer.tenable.com/openapi/5c926ae6a9b73900ee2740cb
SRC=tenable-downloads-api
KEY=downloads
curl $URL_TENABLE_API > api/$SRC.json
./openapi-generator-cli generate -i api/$SRC.json -g python-legacy -o $SRC -c openapi-generator-config-$KEY.yaml --git-repo-id python-tenable --git-user-id barbich
./merge_source.py --source $SRC/ --key "$KEY"_ --destination .
cp -R $SRC/tenableapi .
cp "$KEY"_README.md docs/

URL_TENABLE_API=https://developer.tenable.com/openapi/5e8cf4f7b0c5640296df39c2
SRC=tenable-mssp-api
KEY=mssp
curl $URL_TENABLE_API > api/$SRC.json
./openapi-generator-cli generate -i api/$SRC.json -g python-legacy -o $SRC -c openapi-generator-config-$KEY.yaml --git-repo-id python-tenable --git-user-id barbich
./merge_source.py --source $SRC/ --key "$KEY"_ --destination .
cp -R $SRC/tenableapi .
cp "$KEY"_README.md docs/

URL_TENABLE_API=https://developer.tenable.com/openapi/5c13f072f4a52f07dc4488be
SRC=tenable-wasv1-api
KEY=waslegacy
curl $URL_TENABLE_API > api/$SRC.json
./openapi-generator-cli generate -i api/$SRC.json -g python-legacy -o $SRC -c openapi-generator-config-$KEY.yaml --git-repo-id python-tenable --git-user-id barbich
./merge_source.py --source $SRC/ --key "$KEY"_ --destination .
cp -R $SRC/tenableapi .
cp "$KEY"_README.md docs/

cp $SRC/setup.cfg .
cp $SRC/setup.py .
cp $SRC/requirements.txt .

# cleanup
rm -Rf tenable-*
