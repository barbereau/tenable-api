# InlineResponse2006

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page_number** | **int** | The starting record to retrieve. If your request omits the &#x60;page&#x60; query parameter, this value defaults to &#x60;0&#x60;. | 
**page_size** | **int** | The number of user-defined templates returned. If your request omits the &#x60;size&#x60; query parameter, this value defaults to &#x60;10&#x60;. | 
**order_by** | **str** | The field used to sort the returned user-defined templates. If your request omits the &#x60;order_by&#x60; parameter, this value defaults to &#x60;name&#x60; | 
**ordering** | **str** | The sort order applied when sorting by the &#x60;order_by&#x60; parameter. Values include:  - &#x60;asc&#x60;  - &#x60;desc&#x60;  If your request omits the &#x60;ordering&#x60; query parameter, this value defaults to &#x60;asc&#x60;. | 
**total_size** | **int** | The total number of user-defined templates returned. | 
**data** | [**list[UserTemplate]**](was__UserTemplate.md) | The list of user-defined templates. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


