# InlineObject4

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**software_update** | **bool** | If true, software updates are enabled for agents pursuant to any agent exclusions that are in effect. If false, software updates are disabled for all agents, even if no agent exclusions are in effect. | [optional] 
**auto_unlink** | [**ScannersScannerIdAgentsConfigAutoUnlink**](vm__ScannersScannerIdAgentsConfigAutoUnlink.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


