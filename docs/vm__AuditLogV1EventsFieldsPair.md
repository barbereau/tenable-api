# AuditLogV1EventsFieldsPair

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **str** | A key. The exact string varies based on the action that was taken. | [optional] 
**value** | **str** | A value that corresponds to a key. The value varies based on the request of the action that was taken. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


