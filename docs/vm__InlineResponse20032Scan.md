# InlineResponse20032Scan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**completed_at** | **str** | The ISO timestamp when the scan completed. | [optional] 
**schedule_uuid** | **str** | The schedule UUID for the scan that found the vulnerability. | [optional] 
**started_at** | **str** | The ISO timestamp when the scan started. | [optional] 
**uuid** | **str** | The UUID of the scan that found the vulnerability. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


