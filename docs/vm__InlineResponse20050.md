# InlineResponse20050

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plugins** | **list[object]** |  | [optional] 
**name** | **str** | The name of the family. | [optional] 
**id** | **int** | The unique ID of the family. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


