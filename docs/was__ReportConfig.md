# ReportConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config_id** | **str** | The UUID of the scan configuration. | [optional] 
**owner_id** | **str** | The UUID of owner of the scan configuration. | [optional] 
**settings** | [**ReportScanSettings**](was__ReportScanSettings.md) |  | [optional] 
**scanner_type** | **str** | Indicates the type of scanner that ran the scan, as configured in the scanner_id parameter of the scan configuration. Possible values are:   - &#x60;scanner&#x60; -- A single scanner running on premises in the client&#39;s own infrastructure or environment.   - &#x60;container_group&#x60; -- A group of client scanners running on premises.   - &#x60;cloud&#x60; -- A group of managed scanners running within Tenable&#39;s cloud infrastructure. | [optional] 
**scanner_instance_id** | **str** | The ID of the scanner (if the &#x60;type&#x60; is set to &#x60;scanner&#x60;), or scanner group (if the type is &#x60;container_group&#x60;) that performs the scan. | [optional] 
**name** | **str** | The name of the scan configuration. | [optional] 
**description** | **str** | The description of the scan configuration. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


