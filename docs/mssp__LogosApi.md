# tenableapi.mssp.LogosApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**io_mssp_logos_assign**](mssp__LogosApi.md#io_mssp_logos_assign) | **PUT** /mssp/accounts/logos | Assign logo
[**io_mssp_logos_base64_download**](mssp__LogosApi.md#io_mssp_logos_base64_download) | **GET** /mssp/logos/{logo_uuid}/logo.base64 | Download logo (Base64)
[**io_mssp_logos_create**](mssp__LogosApi.md#io_mssp_logos_create) | **POST** /mssp/logos | Add logo
[**io_mssp_logos_delete**](mssp__LogosApi.md#io_mssp_logos_delete) | **DELETE** /mssp/logos/{logo_uuid} | Delete logo
[**io_mssp_logos_details**](mssp__LogosApi.md#io_mssp_logos_details) | **GET** /mssp/logos/{logo_uuid} | Get logo details
[**io_mssp_logos_list**](mssp__LogosApi.md#io_mssp_logos_list) | **GET** /mssp/logos | List logos
[**io_mssp_logos_png_download**](mssp__LogosApi.md#io_mssp_logos_png_download) | **GET** /mssp/logos/{logo_uuid}/logo.png | Download logo (PNG)
[**io_mssp_logos_update**](mssp__LogosApi.md#io_mssp_logos_update) | **PATCH** /mssp/logos/{logo_uuid} | Update logo


# **io_mssp_logos_assign**
> io_mssp_logos_assign(logo_assignment_bulk=logo_assignment_bulk)

Assign logo

Assigns a logo to one or more customer accounts in the Tenable.io MSSP Portal. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.mssp
from tenableapi.mssp.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.mssp.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.mssp.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.mssp.LogosApi(api_client)
    logo_assignment_bulk = tenableapi.mssp.LogoAssignmentBulk() # LogoAssignmentBulk |  (optional)

    try:
        # Assign logo
        api_instance.io_mssp_logos_assign(logo_assignment_bulk=logo_assignment_bulk)
    except ApiException as e:
        print("Exception when calling LogosApi->io_mssp_logos_assign: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo_assignment_bulk** | [**LogoAssignmentBulk**](mssp__LogoAssignmentBulk.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io MSSP Portal successfully assigns the specified logo to the specified account(s). |  -  |
**400** | Returned if your request specifies an invalid &#x60;logo_uuid&#x60; or &#x60;account_uuids&#x60; query parameter value. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in the Tenable.io MSSP Portal. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_mssp_logos_base64_download**
> str io_mssp_logos_base64_download(logo_uuid)

Download logo (Base64)

Returns a logo file in Base64 format. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.mssp
from tenableapi.mssp.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.mssp.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.mssp.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.mssp.LogosApi(api_client)
    logo_uuid = 'logo_uuid_example' # str | The UUID of the logo to download.

    try:
        # Download logo (Base64)
        api_response = api_instance.io_mssp_logos_base64_download(logo_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LogosApi->io_mssp_logos_base64_download: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo_uuid** | [**str**](.md)| The UUID of the logo to download. | 

### Return type

**str**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain;charset=UTF-8, text/html, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the file is downloaded successfully. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in the Tenable.io MSSP Portal. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_mssp_logos_create**
> LogoUuidResponse io_mssp_logos_create(logo, name)

Add logo

Adds a logo to the Tenable.io MSSP Portal. After you add a logo to the Tenable.io MSSP Portal, use the [PUT /mssp/accounts/logos](ref:io-mssp-logos-assign) endpoint to assign the logo to a customer account.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.mssp
from tenableapi.mssp.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.mssp.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.mssp.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.mssp.LogosApi(api_client)
    logo = '/path/to/file' # file | The logo to upload. The logo must be in PNG format and no larger than 246x52 pixels.
name = 'name_example' # str | An identifiable, user-defined name for the logo.

    try:
        # Add logo
        api_response = api_instance.io_mssp_logos_create(logo, name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LogosApi->io_mssp_logos_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo** | **file**| The logo to upload. The logo must be in PNG format and no larger than 246x52 pixels. | 
 **name** | **str**| An identifiable, user-defined name for the logo. | 

### Return type

[**LogoUuidResponse**](mssp__LogoUuidResponse.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io MSSP Portal successfully added the logo. |  -  |
**400** | Returned if your request specifies invalid query parameter values. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in the Tenable.io MSSP Portal. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_mssp_logos_delete**
> io_mssp_logos_delete(logo_uuid)

Delete logo

Deletes the specified logo in the Tenable.io MSSP Portal. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.mssp
from tenableapi.mssp.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.mssp.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.mssp.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.mssp.LogosApi(api_client)
    logo_uuid = 'logo_uuid_example' # str | The UUID of the logo to delete.

    try:
        # Delete logo
        api_instance.io_mssp_logos_delete(logo_uuid)
    except ApiException as e:
        print("Exception when calling LogosApi->io_mssp_logos_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo_uuid** | [**str**](.md)| The UUID of the logo to delete. | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io MSSP Portal successfully deletes the specified logo. |  -  |
**404** | Returned if the Tenable.io MSSP Portal cannot find the specified logo. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in the Tenable.io MSSP Portal. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_mssp_logos_details**
> LogoObject io_mssp_logos_details(logo_uuid)

Get logo details

Returns details for the specified logo in the Tenable.io MSSP Portal. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.mssp
from tenableapi.mssp.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.mssp.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.mssp.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.mssp.LogosApi(api_client)
    logo_uuid = 'logo_uuid_example' # str | The UUID of the logo for which you want to view details.

    try:
        # Get logo details
        api_response = api_instance.io_mssp_logos_details(logo_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LogosApi->io_mssp_logos_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo_uuid** | [**str**](.md)| The UUID of the logo for which you want to view details. | 

### Return type

[**LogoObject**](mssp__LogoObject.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns logo details for the requested logo. |  -  |
**404** | Returned if the Tenable.io MSSP Portal cannot find the specified logo. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in the Tenable.io MSSP Portal. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_mssp_logos_list**
> LogoListResponse io_mssp_logos_list()

List logos

Returns a list of logos you have uploaded to the Tenable.io MSSP Portal. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.mssp
from tenableapi.mssp.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.mssp.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.mssp.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.mssp.LogosApi(api_client)
    
    try:
        # List logos
        api_response = api_instance.io_mssp_logos_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LogosApi->io_mssp_logos_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LogoListResponse**](mssp__LogoListResponse.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of logos in the Tenable.io MSSP Portal. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in the Tenable.io MSSP Portal. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_mssp_logos_png_download**
> io_mssp_logos_png_download(logo_uuid)

Download logo (PNG)

Returns a logo file in PNG format. <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.mssp
from tenableapi.mssp.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.mssp.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.mssp.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.mssp.LogosApi(api_client)
    logo_uuid = 'logo_uuid_example' # str | The UUID of the logo to download.

    try:
        # Download logo (PNG)
        api_instance.io_mssp_logos_png_download(logo_uuid)
    except ApiException as e:
        print("Exception when calling LogosApi->io_mssp_logos_png_download: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo_uuid** | [**str**](.md)| The UUID of the logo to download. | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/png, text/html, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the PNG file is downloaded successfully. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in the Tenable.io MSSP Portal. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **io_mssp_logos_update**
> LogoUuidResponse io_mssp_logos_update(logo_uuid, logo=logo, name=name)

Update logo

Updates a logo in the Tenable.io MSSP Portal. This update overwrites the existing logo.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.mssp
from tenableapi.mssp.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.mssp.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.mssp.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.mssp.LogosApi(api_client)
    logo_uuid = 'logo_uuid_example' # str | The UUID of the logo to update.
logo = '/path/to/file' # file | The logo to upload. The logo must be in PNG format and no larger than 246x52 pixels. (optional)
name = 'name_example' # str | An identifiable, user-defined name for the logo. (optional)

    try:
        # Update logo
        api_response = api_instance.io_mssp_logos_update(logo_uuid, logo=logo, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LogosApi->io_mssp_logos_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo_uuid** | [**str**](.md)| The UUID of the logo to update. | 
 **logo** | **file**| The logo to upload. The logo must be in PNG format and no larger than 246x52 pixels. | [optional] 
 **name** | **str**| An identifiable, user-defined name for the logo. | [optional] 

### Return type

[**LogoUuidResponse**](mssp__LogoUuidResponse.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io MSSP Portal successfully updated the logo. |  -  |
**400** | Returned if your request specifies invalid query parameter values. |  -  |
**404** | Returned if the Tenable.io MSSP Portal cannot find the specified logo. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if an internal error occurred in the Tenable.io MSSP Portal. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

