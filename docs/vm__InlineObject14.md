# InlineObject14

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | [**list[ImportAssetsAssets]**](vm__ImportAssetsAssets.md) | An array of asset objects to import. Each asset object requires a value for at least one of the following properties: fqdn, ipv4, netbios\\_name, mac\\_address.  For an example of this request body, see [Add Asset Data to Tenable.io](doc:add-asset-data-to-tenableio). For the complete list of importable asset attributes, see [Common Asset Attributes](doc:common-asset-attributes#section-asset-attribute-definitions). | 
**source** | **str** | A user-defined name for the source of the import containing the asset records. You can specify only one source for each import. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


