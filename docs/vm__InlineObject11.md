# InlineObject11

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | **list[str]** | Array of agent IDs or UUIDs to unlink. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


