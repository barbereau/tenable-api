# InlineResponse20083Filters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Corresponds to the field component of the &#x60;f&#x60; parameter in the [GET /solutions](ref:solutions-list) endpoint. | [optional] 
**readable_name** | **str** | The name of the parameter as it appears in the Tenable.io user interface. | [optional] 
**operators** | **list[str]** | Corresponds to the operator component of the &#x60;f&#x60; parameter in the [GET /solutions](ref:solutions-list) endpoint. | [optional] 
**control** | [**InlineResponse20083Control**](vm__InlineResponse20083Control.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


