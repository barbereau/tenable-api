# InlineResponse20032Asset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent_uuid** | **str** | The UUID of the agent that performed the scan where the vulnerability was found. | [optional] 
**bios_uuid** | **str** | The BIOS UUID of the asset where the vulnerability was found. | [optional] 
**device_type** | **str** | The type of asset where the vulnerability was found. | [optional] 
**fqdn** | **str** | The fully-qualified domain name of the asset where a scan found the vulnerability. | [optional] 
**hostname** | **str** | The host name of the asset where a scan found the vulnerability. | [optional] 
**uuid** | **str** | The UUID of the asset where a scan found the vulnerability. | [optional] 
**ipv6** | **str** | The IPv6 address of the asset where a scan found the vulnerability. | [optional] 
**last_authenticated_results** | **str** | The last date credentials were used successfully to scan the asset. | [optional] 
**last_unauthenticated_results** | **str** | The last date when the asset was scanned without using credentials | [optional] 
**mac_address** | **str** | The MAC address of the asset where a scan found the vulnerability. | [optional] 
**netbios_name** | **str** | The NETBIOS name of the asset where a scan found the vulnerability. | [optional] 
**netbios_workgroup** | **str** | The NETBIOS workgroup of the asset where a scan found the vulnerability. | [optional] 
**operating_system** | **str** | The operating system of the asset where a scan found the vulnerability. | [optional] 
**network_id** | **str** | The ID of the network object associated with scanners that identified the asset. The default network ID is &#x60;00000000-0000-0000-0000-000000000000&#x60;. For more information about network objects, see [Manage Networks](doc:manage-networks-tio). | [optional] 
**tracked** | **bool** | A value specifying whether Tenable.io tracks the asset in the asset management system. Tenable.io still assigns untracked assets identifiers in scan results, but these identifiers change with each new scan of the asset. This parameter is relevant to PCI-type scans and in certain cases where there is not enough information in a scan to identify the asset. Untracked assets appear in the scan history, but do not appear in workbenches or reports. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


