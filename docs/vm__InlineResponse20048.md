# InlineResponse20048

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**list[InlineResponse20048Attributes]**](vm__InlineResponse20048Attributes.md) | The plugin attributes. | [optional] 
**family_name** | **str** | The name of the plugin family. | [optional] 
**name** | **str** | The name of the plugin. | [optional] 
**id** | **int** | The ID of the plugin. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


