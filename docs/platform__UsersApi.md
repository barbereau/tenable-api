# tenableapi.platform.UsersApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_create**](platform__UsersApi.md#users_create) | **POST** /users | Create user
[**users_delete**](platform__UsersApi.md#users_delete) | **DELETE** /users/{user_id} | Delete user
[**users_details**](platform__UsersApi.md#users_details) | **GET** /users/{user_id} | Get user details
[**users_edit**](platform__UsersApi.md#users_edit) | **PUT** /users/{user_id} | Update user
[**users_enabled**](platform__UsersApi.md#users_enabled) | **PUT** /users/{user_id}/enabled | Enable user account
[**users_impersonate**](platform__UsersApi.md#users_impersonate) | **POST** /users/{user_id}/impersonate | Impersonate user
[**users_keys**](platform__UsersApi.md#users_keys) | **PUT** /users/{user_id}/keys | Generate API keys
[**users_list**](platform__UsersApi.md#users_list) | **GET** /users | List users
[**users_list_auths**](platform__UsersApi.md#users_list_auths) | **GET** /users/{user_id}/authorizations | Get user authorizations
[**users_password**](platform__UsersApi.md#users_password) | **PUT** /users/{user_id}/chpasswd | Change password
[**users_two_factor**](platform__UsersApi.md#users_two_factor) | **PUT** /users/{user_id}/two-factor | Configure two-factor authentication 
[**users_two_factor_enable**](platform__UsersApi.md#users_two_factor_enable) | **POST** /users/{user_id}/two-factor/send-verification | Send verification code
[**users_two_factor_enable_verify**](platform__UsersApi.md#users_two_factor_enable_verify) | **POST** /users/{user_id}/two-factor/verify-code | Validate verification code
[**users_update_auths**](platform__UsersApi.md#users_update_auths) | **PUT** /users/{user_id}/authorizations | Update user authorizations


# **users_create**
> InlineResponse200 users_create(inline_object)

Create user

Creates a new user.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    inline_object = tenableapi.platform.InlineObject() # InlineObject | 

    try:
        # Create user
        api_response = api_instance.users_create(inline_object)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object** | [**InlineObject**](platform__InlineObject.md)|  | 

### Return type

[**InlineResponse200**](platform__InlineResponse200.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates the user. |  -  |
**400** | Returned if a request parameter is invalid. |  -  |
**403** | Returned if you do not have permission to create a user. |  -  |
**409** | Returned if you attempt to create a duplicate user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_delete**
> object users_delete(user_id)

Delete user

Deletes a user.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 56 # int | The UUID (`uuid`) or unique ID (`id`) of the user.

    try:
        # Delete user
        api_response = api_instance.users_delete(user_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| The UUID (&#x60;uuid&#x60;) or unique ID (&#x60;id&#x60;) of the user. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io deletes the user. |  -  |
**403** | Returned if you do not have permission to delete the specified user. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**409** | Returned if you try to delete your own account. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to delete the user. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_details**
> InlineResponse200 users_details(user_id)

Get user details

Returns details for a specific user.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 56 # int | The UUID (`uuid`) or unique ID (`id`) of the user.

    try:
        # Get user details
        api_response = api_instance.users_details(user_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| The UUID (&#x60;uuid&#x60;) or unique ID (&#x60;id&#x60;) of the user. | 

### Return type

[**InlineResponse200**](platform__InlineResponse200.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the user details. |  -  |
**403** | Returned if you do not have permission to view the given user details. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_edit**
> InlineResponse200 users_edit(user_id, inline_object1)

Update user

Updates an existing user account.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 56 # int | The UUID (`uuid`) or unique ID (`id`) of the user.
inline_object1 = tenableapi.platform.InlineObject1() # InlineObject1 | 

    try:
        # Update user
        api_response = api_instance.users_edit(user_id, inline_object1)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_edit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| The UUID (&#x60;uuid&#x60;) or unique ID (&#x60;id&#x60;) of the user. | 
 **inline_object1** | [**InlineObject1**](platform__InlineObject1.md)|  | 

### Return type

[**InlineResponse200**](platform__InlineResponse200.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully updates the user. |  -  |
**400** | Returned if a request parameter is invalid. |  -  |
**403** | Returned if you do not have permission to update a user. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**409** | Returned if you attempt to change your own account&#39;s enabled or disabled status. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_enabled**
> InlineResponse200 users_enabled(user_id, inline_object3)

Enable user account

Enables or disables an existing user account.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 56 # int | The unique ID of the user.
inline_object3 = tenableapi.platform.InlineObject3() # InlineObject3 | 

    try:
        # Enable user account
        api_response = api_instance.users_enabled(user_id, inline_object3)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_enabled: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| The unique ID of the user. | 
 **inline_object3** | [**InlineObject3**](platform__InlineObject3.md)|  | 

### Return type

[**InlineResponse200**](platform__InlineResponse200.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns an array of user objects. |  -  |
**403** | Returned if you do not have permission to update a user. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**409** | Returned if you try to change your own permissions. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_impersonate**
> object users_impersonate(user_id)

Impersonate user

Allows the current administrator to impersonate the given user.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 56 # int | The unique ID of the user you want to impersonate.

    try:
        # Impersonate user
        api_response = api_instance.users_impersonate(user_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_impersonate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| The unique ID of the user you want to impersonate. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the impersonation was successful. |  -  |
**401** | Returned if Tenable.io cannot process the request for any reason, for example, wrong permissions or an invalid API key. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_keys**
> InlineResponse2002 users_keys(user_id)

Generate API keys

Generates the API keys for a user.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 56 # int | The unique ID of the user.

    try:
        # Generate API keys
        api_response = api_instance.users_keys(user_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| The unique ID of the user. | 

### Return type

[**InlineResponse2002**](platform__InlineResponse2002.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully generates the API keys for the user. |  -  |
**403** | Returned if you do not have permission to generate API keys for the user. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to generate the API keys. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_list**
> InlineResponse200 users_list()

List users

Returns a list of users.<p>Requires BASIC [16] user permissions. If the requesting user has ADMINISTRATOR [64] permissions, Tenable.io returns all attributes for individual user details. Otherwise, user details include only the `uuid`, `id`, `username`, and `email` attributes. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    
    try:
        # List users
        api_response = api_instance.users_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse200**](platform__InlineResponse200.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of users. |  -  |
**403** | Returned if you do not have permission to view the list of users. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_list_auths**
> InlineResponse2001 users_list_auths(user_id)

Get user authorizations

Returns user authorizations for accessing a Tenable.io instance. Access methods include user name and password, single sign-on (SSO) with SAML, and API. **Note:** All access methods are authorized by default.  For background information about managing user authorizations, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/ManageUserAccessAuthorizations.htm).<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 'user_id_example' # str | The UUID of the user. To determine the user UUID, use the [GET /users](ref:users-list) endpoint.

    try:
        # Get user authorizations
        api_response = api_instance.users_list_auths(user_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_list_auths: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | [**str**](.md)| The UUID of the user. To determine the user UUID, use the [GET /users](ref:users-list) endpoint. | 

### Return type

[**InlineResponse2001**](platform__InlineResponse2001.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns authorizations for the user. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_password**
> object users_password(user_id, inline_object2)

Change password

Changes the password for a user.   If you reset a user's password, Tenable.io automatically resets the user's `login_fail_count' and `lockout` attributes to `0`.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 56 # int | The unique ID of the user whose password you want to change.
inline_object2 = tenableapi.platform.InlineObject2() # InlineObject2 | 

    try:
        # Change password
        api_response = api_instance.users_password(user_id, inline_object2)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_password: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| The unique ID of the user whose password you want to change. | 
 **inline_object2** | [**InlineObject2**](platform__InlineObject2.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully changes the user password. |  -  |
**400** | Returned if Tenable.io cannot change the user password, because the new password is too short. |  -  |
**403** | Returned if you do not have the permissions needed to change the user&#39;s password. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to change the password. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_two_factor**
> object users_two_factor(user_id, inline_object7)

Configure two-factor authentication 

Enables or disables a user's two-factor authentication settings.   **Note:** The workflow for two-factor authentication is as follows:<ol><li>[Send a verification code](ref:users-two-factor-enable).</li><li>[Validate the verification code](ref:users-two-factor-enable-verify).</li><li>[Configure two-factor authentication](ref:users-two-factor).</li></ol><p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 56 # int | The unique ID of the user.
inline_object7 = tenableapi.platform.InlineObject7() # InlineObject7 | 

    try:
        # Configure two-factor authentication 
        api_response = api_instance.users_two_factor(user_id, inline_object7)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_two_factor: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| The unique ID of the user. | 
 **inline_object7** | [**InlineObject7**](platform__InlineObject7.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the two-factor authentication settings update is successful. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_two_factor_enable**
> object users_two_factor_enable(user_id, inline_object5)

Send verification code

Sends a one-time verification code to the user's phone number to start the process of enabling two-factor authentication.   **Note:** The workflow for two-factor authentication is as follows:<ol><li>[Send a verification code](ref:users-two-factor-enable).</li><li>[Validate the verification code](ref:users-two-factor-enable-verify).</li><li>[Configure two-factor authentication](ref:users-two-factor).</li></ol><p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 56 # int | The unique ID of the user.
inline_object5 = tenableapi.platform.InlineObject5() # InlineObject5 | 

    try:
        # Send verification code
        api_response = api_instance.users_two_factor_enable(user_id, inline_object5)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_two_factor_enable: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| The unique ID of the user. | 
 **inline_object5** | [**InlineObject5**](platform__InlineObject5.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io sends the one-time verification code successfully to the specified phone number. |  -  |
**400** | Returned if Tenable.io cannot send the verification code. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_two_factor_enable_verify**
> object users_two_factor_enable_verify(user_id, inline_object6)

Validate verification code

Validate the verification code sent to a phone number. If this request is successful, it enables two-factor authentication for the specified user.   **Note:** The workflow for two-factor authentication is as follows:<ol><li>[Send a verification code](ref:users-two-factor-enable).</li><li>[Validate the verification code](ref:users-two-factor-enable-verify).</li><li>[Configure two-factor authentication](ref:users-two-factor).</li></ol><p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 56 # int | The unique ID of the user.
inline_object6 = tenableapi.platform.InlineObject6() # InlineObject6 | 

    try:
        # Validate verification code
        api_response = api_instance.users_two_factor_enable_verify(user_id, inline_object6)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_two_factor_enable_verify: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| The unique ID of the user. | 
 **inline_object6** | [**InlineObject6**](platform__InlineObject6.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully validates the verification code and enables two-factor authentication. |  -  |
**400** | Returned if Tenable.io fails to validate the verification code because the verification code was empty, incorrect, or expired. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_update_auths**
> object users_update_auths(user_id, inline_object4)

Update user authorizations

Updates user authorizations for accessing a Tenable.io instance. Use the endpoint to grant and revoke authorizations.  **Note:** You cannot update authorizations for the current user.  For background information about managing user authorizations, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/ManageUserAccessAuthorizations.htm).<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.platform
from tenableapi.platform.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.platform.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.platform.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.platform.UsersApi(api_client)
    user_id = 'user_id_example' # str | The UUID of the user. To determine the UUID, use the [GET /users](ref:users-list) endpoint.
inline_object4 = tenableapi.platform.InlineObject4() # InlineObject4 | 

    try:
        # Update user authorizations
        api_response = api_instance.users_update_auths(user_id, inline_object4)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling UsersApi->users_update_auths: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | [**str**](.md)| The UUID of the user. To determine the UUID, use the [GET /users](ref:users-list) endpoint. | 
 **inline_object4** | [**InlineObject4**](platform__InlineObject4.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Returned if Tenable.io successfully updates the user&#39;s authorizations. |  -  |
**400** | Returned if your request specifies invalid parameters. |  -  |
**404** | Returned if Tenable.io cannot find the specified user. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

