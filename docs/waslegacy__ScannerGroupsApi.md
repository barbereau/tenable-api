# tenableapi.waslegacy.ScannerGroupsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_scanner_groups_add_scanner**](waslegacy__ScannerGroupsApi.md#was_scanner_groups_add_scanner) | **POST** /scanner-groups/{group_id}/scanners/{scanner_id} | Add scanner to scanner group
[**was_scanner_groups_create**](waslegacy__ScannerGroupsApi.md#was_scanner_groups_create) | **POST** /scanner-groups | Create scanner group
[**was_scanner_groups_delete**](waslegacy__ScannerGroupsApi.md#was_scanner_groups_delete) | **DELETE** /scanner-groups/{group_id} | Delete a scanner group
[**was_scanner_groups_delete_scanner**](waslegacy__ScannerGroupsApi.md#was_scanner_groups_delete_scanner) | **DELETE** /scanner-groups/{group_id}/scanners/{scanner_id} | Remove scanner from scanner group
[**was_scanner_groups_details**](waslegacy__ScannerGroupsApi.md#was_scanner_groups_details) | **GET** /scanner-groups/{group_id} | List scanner group details
[**was_scanner_groups_edit**](waslegacy__ScannerGroupsApi.md#was_scanner_groups_edit) | **PUT** /scanner-groups/{group_id} | Update scanner group
[**was_scanner_groups_list**](waslegacy__ScannerGroupsApi.md#was_scanner_groups_list) | **GET** /scanner-groups | List scanner groups
[**was_scanner_groups_list_scanners**](waslegacy__ScannerGroupsApi.md#was_scanner_groups_list_scanners) | **GET** /scanner-groups/{group_id}/scanners | List scanners within scanner group


# **was_scanner_groups_add_scanner**
> object was_scanner_groups_add_scanner(group_id, scanner_id)

Add scanner to scanner group

Adds a scanner to the scanner group. Use the [GET /scanner-groups/{group_id}/scanners](ref:was-scanner-groups-list-scanners) endpoint to verify that the scanner was added to the specified scanner group.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.
scanner_id = 56 # int | The ID of the scanner to add to the scanner group.

    try:
        # Add scanner to scanner group
        api_response = api_instance.was_scanner_groups_add_scanner(group_id, scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->was_scanner_groups_add_scanner: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 
 **scanner_id** | **int**| The ID of the scanner to add to the scanner group. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the scanner has been successfully added to the scanner group. |  -  |
**400** | Returned if an attempt is made to add a scanner group to another scanner group. |  -  |
**409** | Returned if you attempt to add a scanner to a scanner group that the scanner is already a member of. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if the server failed to add the scanner to the scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scanner_groups_create**
> InlineResponse2008 was_scanner_groups_create(inline_object3)

Create scanner group

Creates a new scanner group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScannerGroupsApi(api_client)
    inline_object3 = tenableapi.waslegacy.InlineObject3() # InlineObject3 | 

    try:
        # Create scanner group
        api_response = api_instance.was_scanner_groups_create(inline_object3)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->was_scanner_groups_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object3** | [**InlineObject3**](waslegacy__InlineObject3.md)|  | 

### Return type

[**InlineResponse2008**](waslegacy__InlineResponse2008.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the scanner group has been successfully created. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if the server failed to create the scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scanner_groups_delete**
> object was_scanner_groups_delete(group_id)

Delete a scanner group

Deletes a scanner group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.

    try:
        # Delete a scanner group
        api_response = api_instance.was_scanner_groups_delete(group_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->was_scanner_groups_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the scanner group has been successfully deleted. |  -  |
**404** | Returned if the scanner group does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if the server failed to delete the scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scanner_groups_delete_scanner**
> object was_scanner_groups_delete_scanner(group_id, scanner_id)

Remove scanner from scanner group

Remove a scanner from the scanner group.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.
scanner_id = 56 # int | The ID of the scanner to remove from the scanner group.

    try:
        # Remove scanner from scanner group
        api_response = api_instance.was_scanner_groups_delete_scanner(group_id, scanner_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->was_scanner_groups_delete_scanner: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 
 **scanner_id** | **int**| The ID of the scanner to remove from the scanner group. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the scanner has been successfully removed from the scanner group. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if the server failed to remove the scanner from the scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scanner_groups_details**
> InlineResponse2008 was_scanner_groups_details(group_id)

List scanner group details

Retruns details for the given scanner group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.

    try:
        # List scanner group details
        api_response = api_instance.was_scanner_groups_details(group_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->was_scanner_groups_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 

### Return type

[**InlineResponse2008**](waslegacy__InlineResponse2008.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scanner group details. |  -  |
**403** | Returned if user does not have permission to view the scanner group. |  -  |
**404** | Returned if the scanner group does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scanner_groups_edit**
> object was_scanner_groups_edit(group_id, inline_object4)

Update scanner group

Updates a scanner group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.
inline_object4 = tenableapi.waslegacy.InlineObject4() # InlineObject4 | 

    try:
        # Update scanner group
        api_response = api_instance.was_scanner_groups_edit(group_id, inline_object4)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->was_scanner_groups_edit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 
 **inline_object4** | [**InlineObject4**](waslegacy__InlineObject4.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the scanner group has been successfully updated. |  -  |
**404** | Returned if the scanner group does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if the server failed to update the scanner group. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scanner_groups_list**
> InlineResponse2008 was_scanner_groups_list()

List scanner groups

Lists scanner groups within the current container.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScannerGroupsApi(api_client)
    
    try:
        # List scanner groups
        api_response = api_instance.was_scanner_groups_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->was_scanner_groups_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2008**](waslegacy__InlineResponse2008.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the scanner group list. |  -  |
**403** | Returned if the user does not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_scanner_groups_list_scanners**
> list[InlineResponse2009] was_scanner_groups_list_scanners(group_id)

List scanners within scanner group

Lists scanners associated with the scanner group.<p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.ScannerGroupsApi(api_client)
    group_id = 56 # int | The ID of the scanner group.

    try:
        # List scanners within scanner group
        api_response = api_instance.was_scanner_groups_list_scanners(group_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ScannerGroupsApi->was_scanner_groups_list_scanners: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The ID of the scanner group. | 

### Return type

[**list[InlineResponse2009]**](waslegacy__InlineResponse2009.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the list of scanners in the group. |  -  |
**403** | Returned if the user does not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

