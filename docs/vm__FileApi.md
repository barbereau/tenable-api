# tenableapi.vm.FileApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**file_upload**](vm__FileApi.md#file_upload) | **POST** /file/upload | Upload file


# **file_upload**
> InlineResponse20038 file_upload(no_enc=no_enc, filedata=filedata)

Upload file

Uploads a file.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.FileApi(api_client)
    no_enc = 56 # int | Send value of `1` when uploading an encrypted file. (optional)
filedata = '/path/to/file' # file | The file to upload. (optional)

    try:
        # Upload file
        api_response = api_instance.file_upload(no_enc=no_enc, filedata=filedata)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling FileApi->file_upload: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **no_enc** | **int**| Send value of &#x60;1&#x60; when uploading an encrypted file. | [optional] 
 **filedata** | **file**| The file to upload. | [optional] 

### Return type

[**InlineResponse20038**](vm__InlineResponse20038.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the name of the successfully uploaded file. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io cannot upload the file. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

