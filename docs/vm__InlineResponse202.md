# InlineResponse202

Bulk operations results. Contains the number of assets affected by the operation (moved or deleted).
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_count** | **int** | The number of assets affected by the operation. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


