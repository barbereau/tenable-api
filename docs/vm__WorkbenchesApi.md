# tenableapi.vm.WorkbenchesApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**workbenches_asset_info**](vm__WorkbenchesApi.md#workbenches_asset_info) | **GET** /workbenches/assets/{asset_id}/info | Get asset information
[**workbenches_asset_vulnerabilities**](vm__WorkbenchesApi.md#workbenches_asset_vulnerabilities) | **GET** /workbenches/assets/{asset_id}/vulnerabilities | List asset vulnerabilities
[**workbenches_asset_vulnerability_info**](vm__WorkbenchesApi.md#workbenches_asset_vulnerability_info) | **GET** /workbenches/assets/{asset_id}/vulnerabilities/{plugin_id}/info | Get asset vulnerability details
[**workbenches_asset_vulnerability_output**](vm__WorkbenchesApi.md#workbenches_asset_vulnerability_output) | **GET** /workbenches/assets/{asset_id}/vulnerabilities/{plugin_id}/outputs | List asset vulnerabilties for plugin
[**workbenches_assets**](vm__WorkbenchesApi.md#workbenches_assets) | **GET** /workbenches/assets | List assets
[**workbenches_assets_activity**](vm__WorkbenchesApi.md#workbenches_assets_activity) | **GET** /workbenches/assets/{asset_uuid}/activity | Get asset activity log
[**workbenches_assets_delete**](vm__WorkbenchesApi.md#workbenches_assets_delete) | **DELETE** /workbenches/assets/{asset_uuid} | Delete asset
[**workbenches_assets_vulnerabilities**](vm__WorkbenchesApi.md#workbenches_assets_vulnerabilities) | **GET** /workbenches/assets/vulnerabilities | List assets with vulnerabilities
[**workbenches_export_download**](vm__WorkbenchesApi.md#workbenches_export_download) | **GET** /workbenches/export/{file_id}/download | Download export file
[**workbenches_export_request**](vm__WorkbenchesApi.md#workbenches_export_request) | **GET** /workbenches/export | Export workbench
[**workbenches_export_status**](vm__WorkbenchesApi.md#workbenches_export_status) | **GET** /workbenches/export/{file_id}/status | Check export status
[**workbenches_vulnerabilities**](vm__WorkbenchesApi.md#workbenches_vulnerabilities) | **GET** /workbenches/vulnerabilities | List vulnerabilities
[**workbenches_vulnerability_info**](vm__WorkbenchesApi.md#workbenches_vulnerability_info) | **GET** /workbenches/vulnerabilities/{plugin_id}/info | Get plugin details
[**workbenches_vulnerability_output**](vm__WorkbenchesApi.md#workbenches_vulnerability_output) | **GET** /workbenches/vulnerabilities/{plugin_id}/outputs | List plugin outputs


# **workbenches_asset_info**
> InlineResponse20096 workbenches_asset_info(asset_id, all_fields=all_fields)

Get asset information

Returns information about the specified asset.   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    asset_id = 'asset_id_example' # str | The UUID of the asset. You can find the UUID by examining the output of the [GET /workbenches/assets](ref:workbenches-assets) endpoint.
all_fields = 'all_fields_example' # str | A value specifying whether you want the returned data to include all fields (`full`) or only the default fields (`default`).  The schema for this endpoint defines the `default` fields only. For a definition of the `full` fields, see [Common Asset Attributes](doc:common-asset-attributes). (optional)

    try:
        # Get asset information
        api_response = api_instance.workbenches_asset_info(asset_id, all_fields=all_fields)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_asset_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The UUID of the asset. You can find the UUID by examining the output of the [GET /workbenches/assets](ref:workbenches-assets) endpoint. | 
 **all_fields** | **str**| A value specifying whether you want the returned data to include all fields (&#x60;full&#x60;) or only the default fields (&#x60;default&#x60;).  The schema for this endpoint defines the &#x60;default&#x60; fields only. For a definition of the &#x60;full&#x60; fields, see [Common Asset Attributes](doc:common-asset-attributes). | [optional] 

### Return type

[**InlineResponse20096**](vm__InlineResponse20096.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns asset information. |  -  |
**403** | Returned if you do not have permission to view information for the specified asset. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_asset_vulnerabilities**
> InlineResponse20091 workbenches_asset_vulnerabilities(asset_id, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)

List asset vulnerabilities

Retrieves a list of the vulnerabilities recorded for a specified asset. By default, this list is sorted by vulnerability count in descending order. The list returned is limited to 5,000. To retrieve more than 5,000 vulnerabilities, use the [export-request API](ref:workbenches-export-request).   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    asset_id = 'asset_id_example' # str | The UUID of the asset. You can find the UUID by examining the output of the [GET /workbenches/assets](ref:workbenches-assets) endpoint.
date_range = 56 # int | The number of days of data prior to and including today that should be returned. (optional)
filter_0_filter = '?filter.0.filter=plugin.name' # str | The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_quality = '&filter.0.quality=match' # str | The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_value = '&filter.0.value=RHEL' # str | The value of the filter to apply to the exported scan report. You can find valid values for the filter in the 'control' attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_search_type = 'filter_search_type_example' # str | For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). (optional)

    try:
        # List asset vulnerabilities
        api_response = api_instance.workbenches_asset_vulnerabilities(asset_id, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_asset_vulnerabilities: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The UUID of the asset. You can find the UUID by examining the output of the [GET /workbenches/assets](ref:workbenches-assets) endpoint. | 
 **date_range** | **int**| The number of days of data prior to and including today that should be returned. | [optional] 
 **filter_0_filter** | **str**| The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_quality** | **str**| The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_value** | **str**| The value of the filter to apply to the exported scan report. You can find valid values for the filter in the &#39;control&#39; attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_search_type** | **str**| For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 

### Return type

[**InlineResponse20091**](vm__InlineResponse20091.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of vulnerabilities for the specified asset. |  -  |
**403** | Returned if you do not have permission to list vulnerabilities for the specified asset. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_asset_vulnerability_info**
> InlineResponse20098 workbenches_asset_vulnerability_info(asset_id, plugin_id, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)

Get asset vulnerability details

Retrieves the details for a vulnerability recorded on a specified asset.   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    asset_id = 'asset_id_example' # str | The UUID of the asset.
plugin_id = 56 # int | The ID of the plugin.
date_range = 56 # int | The number of days of data prior to and including today that should be returned. (optional)
filter_0_filter = '?filter.0.filter=plugin.name' # str | The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_quality = '&filter.0.quality=match' # str | The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_value = '&filter.0.value=RHEL' # str | The value of the filter to apply to the exported scan report. You can find valid values for the filter in the 'control' attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_search_type = 'filter_search_type_example' # str | For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). (optional)

    try:
        # Get asset vulnerability details
        api_response = api_instance.workbenches_asset_vulnerability_info(asset_id, plugin_id, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_asset_vulnerability_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The UUID of the asset. | 
 **plugin_id** | **int**| The ID of the plugin. | 
 **date_range** | **int**| The number of days of data prior to and including today that should be returned. | [optional] 
 **filter_0_filter** | **str**| The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_quality** | **str**| The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_value** | **str**| The value of the filter to apply to the exported scan report. You can find valid values for the filter in the &#39;control&#39; attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_search_type** | **str**| For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 

### Return type

[**InlineResponse20098**](vm__InlineResponse20098.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns details for the specified vulnerability recorded on the specified asset. |  -  |
**403** | Returned if you do not have permission to view vulnerability details for the specified asset. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_asset_vulnerability_output**
> InlineResponse20099 workbenches_asset_vulnerability_output(asset_id, plugin_id, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)

List asset vulnerabilties for plugin

Retrieves the vulnerability outputs for a plugin recorded on a specified asset.   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    asset_id = 'asset_id_example' # str | The UUID of the asset.
plugin_id = 56 # int | The ID of the plugin.
date_range = 56 # int | The number of days of data prior to and including today that should be returned. (optional)
filter_0_filter = '?filter.0.filter=plugin.name' # str | The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_quality = '&filter.0.quality=match' # str | The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_value = '&filter.0.value=RHEL' # str | The value of the filter to apply to the exported scan report. You can find valid values for the filter in the 'control' attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_search_type = 'filter_search_type_example' # str | For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). (optional)

    try:
        # List asset vulnerabilties for plugin
        api_response = api_instance.workbenches_asset_vulnerability_output(asset_id, plugin_id, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_asset_vulnerability_output: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The UUID of the asset. | 
 **plugin_id** | **int**| The ID of the plugin. | 
 **date_range** | **int**| The number of days of data prior to and including today that should be returned. | [optional] 
 **filter_0_filter** | **str**| The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_quality** | **str**| The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_value** | **str**| The value of the filter to apply to the exported scan report. You can find valid values for the filter in the &#39;control&#39; attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_search_type** | **str**| For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 

### Return type

[**InlineResponse20099**](vm__InlineResponse20099.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of asset vulnerabilties for the specified plugin. |  -  |
**403** | Returned if you do not have permission to list of asset vulnerabilties for the specified plugin. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_assets**
> InlineResponse20094 workbenches_assets(date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type, all_fields=all_fields)

List assets

Retrieves a list of assets. The list can be modified using filters. The list returned is limited to 5,000. To retrieve more than 5,000 assets, use the [export-request API](ref:exports-assets-request-export).   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    date_range = 56 # int | The number of days of data prior to and including today that should be returned. (optional)
filter_0_filter = '?filter.0.filter=plugin.name' # str | The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. If you specify the name of the filter, you must specify the operator as the filter.0.quality parameter and the value as the filter.0.value parameter. To use multiple filters, increment the `<INDEX>` portion of `filter.<INDEX>.filter`, for example, `filter.0.filter`. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_quality = '&filter.0.quality=match' # str | The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_value = '&filter.0.value=RHEL' # str | The value of the filter to apply to the exported scan report. You can find valid values for the filter in the 'control' attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_search_type = 'filter_search_type_example' # str | For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
all_fields = 'all_fields_example' # str | A value specifying whether you want the returned data to include all fields (`full`) or only the default fields (`default`). The schema for this endpoint defines the `default` fields only. For a definition of the `full` fields, see [Common Asset Attributes](doc:common-asset-attributes). (optional)

    try:
        # List assets
        api_response = api_instance.workbenches_assets(date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type, all_fields=all_fields)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_assets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date_range** | **int**| The number of days of data prior to and including today that should be returned. | [optional] 
 **filter_0_filter** | **str**| The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. If you specify the name of the filter, you must specify the operator as the filter.0.quality parameter and the value as the filter.0.value parameter. To use multiple filters, increment the &#x60;&lt;INDEX&gt;&#x60; portion of &#x60;filter.&lt;INDEX&gt;.filter&#x60;, for example, &#x60;filter.0.filter&#x60;. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_quality** | **str**| The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_value** | **str**| The value of the filter to apply to the exported scan report. You can find valid values for the filter in the &#39;control&#39; attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_search_type** | **str**| For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **all_fields** | **str**| A value specifying whether you want the returned data to include all fields (&#x60;full&#x60;) or only the default fields (&#x60;default&#x60;). The schema for this endpoint defines the &#x60;default&#x60; fields only. For a definition of the &#x60;full&#x60; fields, see [Common Asset Attributes](doc:common-asset-attributes). | [optional] 

### Return type

[**InlineResponse20094**](vm__InlineResponse20094.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns an array of asset objects. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_assets_activity**
> list[InlineResponse20097] workbenches_assets_activity(asset_uuid)

Get asset activity log

Returns the activity log for the specified asset. Event types include:<ul><li>discovered—Asset created (for example, by a network scan or import).</li><li>seen—Asset observed by a network scan without any changes to its attributes.</li><li>tagging—Tag added to or removed from asset.</li><li>attribute_change—A scan identified new or changed attributes for the asset (for example, new software applications installed on the asset).</li><li>updated—Asset updated either manually by a user or automatically by a new scan.</li></ul>   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    asset_uuid = 'asset_uuid_example' # str | The UUID of the asset. You can find the UUID by examining the output of the [GET /workbenches/assets](ref:workbenches-assets) endpoint.

    try:
        # Get asset activity log
        api_response = api_instance.workbenches_assets_activity(asset_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_assets_activity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_uuid** | **str**| The UUID of the asset. You can find the UUID by examining the output of the [GET /workbenches/assets](ref:workbenches-assets) endpoint. | 

### Return type

[**list[InlineResponse20097]**](vm__InlineResponse20097.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns activity for an asset. |  -  |
**401** | Returned if you do not have permission to view activity for an asset. |  -  |
**404** | Returned if Tenable.io cannot find the specified asset. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to return activity for an asset. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_assets_delete**
> object workbenches_assets_delete(asset_uuid)

Delete asset

Deletes the specified asset. When you delete an asset, Tenable.io deletes vulnerability data associated with the asset. Deleting an asset does not immediately subtract the asset from your licensed assets count. Deleted assets continue to be included in the count until they automatically age out as inactive.   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires SCAN OPERATOR [24] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    asset_uuid = 'asset_uuid_example' # str | The UUID of the asset. You can find the UUID by examining the output of the [GET /workbenches/assets](ref:workbenches-assets) endpoint.

    try:
        # Delete asset
        api_response = api_instance.workbenches_assets_delete(asset_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_assets_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_uuid** | **str**| The UUID of the asset. You can find the UUID by examining the output of the [GET /workbenches/assets](ref:workbenches-assets) endpoint. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Returned if Tenable.io successfully deletes the asset. |  -  |
**403** | Returned if you do not have permission to delete the asset. |  -  |
**404** | Returned if Tenable.io cannot find the specified asset. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to delete the specified asset. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_assets_vulnerabilities**
> list[InlineResponse20095] workbenches_assets_vulnerabilities(date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)

List assets with vulnerabilities

Returns a list of assets with vulnerabilities. The list is limited to 5,000. To retrieve more than 5,000 assets, use the [export-request API](ref:workbenches-export-request).   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    date_range = 56 # int | The number of days of data prior to and including today that should be returned. (optional)
filter_0_filter = '?filter.0.filter=plugin.name' # str | The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_quality = '&filter.0.quality=match' # str | The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint.For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_value = '&filter.0.value=RHEL' # str | The value of the filter to apply to the exported scan report. You can find valid values for the filter in the 'control' attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_search_type = 'filter_search_type_example' # str | For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). (optional)

    try:
        # List assets with vulnerabilities
        api_response = api_instance.workbenches_assets_vulnerabilities(date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_assets_vulnerabilities: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date_range** | **int**| The number of days of data prior to and including today that should be returned. | [optional] 
 **filter_0_filter** | **str**| The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_quality** | **str**| The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint.For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_value** | **str**| The value of the filter to apply to the exported scan report. You can find valid values for the filter in the &#39;control&#39; attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_search_type** | **str**| For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 

### Return type

[**list[InlineResponse20095]**](vm__InlineResponse20095.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned an array of assets with vulnerabilities. |  -  |
**403** | Returned if you do not have permission to view assets with vulnerabilities. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_export_download**
> object workbenches_export_download(file_id)

Download export file

Downloads a file that has been prepared for export. For more information about workbench export files, see [Export File Formats](doc:export-file-formats).   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    file_id = 56 # int | The unique identifier of the workbench report being downloaded. The value for this parameter can be obtained from the response of the initial export request.

    try:
        # Download export file
        api_response = api_instance.workbenches_export_download(file_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_export_download: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file_id** | **int**| The unique identifier of the workbench report being downloaded. The value for this parameter can be obtained from the response of the initial export request. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if the file downloaded successfully. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_export_request**
> InlineResponse200100 workbenches_export_request(format, report, chapter, start_date=start_date, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type, minimum_vuln_info=minimum_vuln_info, plugin_id=plugin_id, asset_id=asset_id)

Export workbench

Exports the specified workbench to a file. Once requested, the file can be downloaded using the [export download](ref:workbenches-export-download) method upon receiving a \"ready\" status from the [export status](ref:workbenches-export-status) method.  For more information about workbench export files, see [Export File Formats](doc:export-file-formats).   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    format = 'format_example' # str | The file format to use (Nessus, HTML, PDF, or CSV).  **Note:** Tag-based filters are supported for the `CSV` file format type only.
report = 'report_example' # str | The type of workbench report to be exported
chapter = 'vuln_by_asset' # str | Semicolon-separated list of chapters to include for vulnerabilities/hosts reports (vuln\\_by\\_plugin, vuln\\_by\\_asset, vuln\\_hosts\\_summary) or a single chapter for Executive Summary (exec\\_summary). Currently, only vuln\\_by\\_asset is supported for .nessus workbench exports.
start_date = 56 # int | The date (in unixtime) at which the exported results should begin to be included. Defaults to today. (optional)
date_range = 56 # int | The number of days of data prior to and including start\\_date that should be returned. If not specified, data for all dates is returned. (optional)
filter_0_filter = '?filter.0.filter=plugin.name' # str | The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_quality = '&filter.0.quality=match' # str | The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_value = '&filter.0.value=RHEL' # str | The value of the filter to apply to the exported scan report. You can find valid values for the filter in the 'control' attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_search_type = 'filter_search_type_example' # str | For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
minimum_vuln_info = True # bool | When `true`, Tenable.io returns only a minimal subset of scan details for each result, excluding plugin attributes. In this case, only plugin\\_output and vulnerability\\_state fields are always returned; first\\_found, last\\_found and last\\_fixed are also returned if possible. (optional)
plugin_id = 56 # int | A plugin ID. Restricts the export data to vulnerabilities that only the specified plugin detects. (optional)
asset_id = 'asset_id_example' # str | The UUID of an asset. Restricts the export data to findings on the specified asset only. (optional)

    try:
        # Export workbench
        api_response = api_instance.workbenches_export_request(format, report, chapter, start_date=start_date, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type, minimum_vuln_info=minimum_vuln_info, plugin_id=plugin_id, asset_id=asset_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_export_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **str**| The file format to use (Nessus, HTML, PDF, or CSV).  **Note:** Tag-based filters are supported for the &#x60;CSV&#x60; file format type only. | 
 **report** | **str**| The type of workbench report to be exported | 
 **chapter** | **str**| Semicolon-separated list of chapters to include for vulnerabilities/hosts reports (vuln\\_by\\_plugin, vuln\\_by\\_asset, vuln\\_hosts\\_summary) or a single chapter for Executive Summary (exec\\_summary). Currently, only vuln\\_by\\_asset is supported for .nessus workbench exports. | 
 **start_date** | **int**| The date (in unixtime) at which the exported results should begin to be included. Defaults to today. | [optional] 
 **date_range** | **int**| The number of days of data prior to and including start\\_date that should be returned. If not specified, data for all dates is returned. | [optional] 
 **filter_0_filter** | **str**| The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_quality** | **str**| The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_value** | **str**| The value of the filter to apply to the exported scan report. You can find valid values for the filter in the &#39;control&#39; attribute of the objects returned by the [GET /filters/workbenches/assets](ref:filters-assets-filter) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_search_type** | **str**| For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **minimum_vuln_info** | **bool**| When &#x60;true&#x60;, Tenable.io returns only a minimal subset of scan details for each result, excluding plugin attributes. In this case, only plugin\\_output and vulnerability\\_state fields are always returned; first\\_found, last\\_found and last\\_fixed are also returned if possible. | [optional] 
 **plugin_id** | **int**| A plugin ID. Restricts the export data to vulnerabilities that only the specified plugin detects. | [optional] 
 **asset_id** | **str**| The UUID of an asset. Restricts the export data to findings on the specified asset only. | [optional] 

### Return type

[**InlineResponse200100**](vm__InlineResponse200100.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully queues the export. |  -  |
**400** | Returned if you attempt to use a tag-based filter on a non-CSV file format. |  -  |
**403** | Returned if you do not have permission to export the workbench. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_export_status**
> InlineResponse200101 workbenches_export_status(file_id)

Check export status

Returns the status of a pending export. When an export has been requested, it is necessary to poll this endpoint until a \"ready\" status is returned, at which point the file is complete and can be downloaded using the [export download](ref:workbenches-export-download) endpoint.   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    file_id = 56 # int | The unique identifier of the workbench report being exported. The value for this parameter can be obtained from the response of the initial export request.

    try:
        # Check export status
        api_response = api_instance.workbenches_export_status(file_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_export_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file_id** | **int**| The unique identifier of the workbench report being exported. The value for this parameter can be obtained from the response of the initial export request. | 

### Return type

[**InlineResponse200101**](vm__InlineResponse200101.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the export status. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_vulnerabilities**
> InlineResponse20091 workbenches_vulnerabilities(age=age, authenticated=authenticated, date_range=date_range, exploitable=exploitable, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type, resolvable=resolvable, severity=severity)

List vulnerabilities

Returns a list of recorded vulnerabilities. The list returned is limited to 5,000. To retrieve more than 5,000 vulnerabilities, use the [export-request API](ref:workbenches-export-request).   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    age = 56 # int | Lists only those vulnerabilities older than a certain number of days. (optional)
authenticated = True # bool | Lists only authenticated vulnerabilities. (optional)
date_range = 56 # int | The number of days of data prior to and including today that should be returned. (optional)
exploitable = True # bool | Lists only exploitable vulnerabilities. (optional)
filter_0_filter = '?filter.0.filter=plugin.name' # str | The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_quality = '&filter.0.quality=match' # str | The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_value = '&filter.0.value=RHEL' # str | The value of the filter to apply to the exported scan report. You can find valid values for the filter in the 'control' attribute of the objects returned by the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_search_type = 'filter_search_type_example' # str | For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
resolvable = True # bool | Lists only those vulnerabilities with a remediation path. (optional)
severity = 'severity_example' # str | Lists only vulnerabilities of a specific severity (critical, high, medium or low) (optional)

    try:
        # List vulnerabilities
        api_response = api_instance.workbenches_vulnerabilities(age=age, authenticated=authenticated, date_range=date_range, exploitable=exploitable, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type, resolvable=resolvable, severity=severity)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_vulnerabilities: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **age** | **int**| Lists only those vulnerabilities older than a certain number of days. | [optional] 
 **authenticated** | **bool**| Lists only authenticated vulnerabilities. | [optional] 
 **date_range** | **int**| The number of days of data prior to and including today that should be returned. | [optional] 
 **exploitable** | **bool**| Lists only exploitable vulnerabilities. | [optional] 
 **filter_0_filter** | **str**| The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_quality** | **str**| The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_value** | **str**| The value of the filter to apply to the exported scan report. You can find valid values for the filter in the &#39;control&#39; attribute of the objects returned by the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_search_type** | **str**| For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **resolvable** | **bool**| Lists only those vulnerabilities with a remediation path. | [optional] 
 **severity** | **str**| Lists only vulnerabilities of a specific severity (critical, high, medium or low) | [optional] 

### Return type

[**InlineResponse20091**](vm__InlineResponse20091.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of vulnerabilities. |  -  |
**403** | Returned if you do not have permission to list vulnerabilities. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_vulnerability_info**
> InlineResponse20092 workbenches_vulnerability_info(plugin_id, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)

Get plugin details

Retrieves the details for a plugin.   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    plugin_id = 56 # int | The ID of the plugin. You can find the plugin ID by examining the output of the [GET /workbenches/vulnerabilities](ref:workbenches-vulnerabilities) endpoint.
date_range = 56 # int | The number of days of data prior to and including today that should be returned. (optional)
filter_0_filter = '?filter.0.filter=plugin.name' # str | The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_quality = '&filter.0.quality=match' # str | The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_value = '&filter.0.value=RHEL' # str | The value of the filter to apply to the exported scan report. You can find valid values for the filter in the 'control' attribute of the objects returned by the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_search_type = 'filter_search_type_example' # str | For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). (optional)

    try:
        # Get plugin details
        api_response = api_instance.workbenches_vulnerability_info(plugin_id, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_vulnerability_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plugin_id** | **int**| The ID of the plugin. You can find the plugin ID by examining the output of the [GET /workbenches/vulnerabilities](ref:workbenches-vulnerabilities) endpoint. | 
 **date_range** | **int**| The number of days of data prior to and including today that should be returned. | [optional] 
 **filter_0_filter** | **str**| The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_quality** | **str**| The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_value** | **str**| The value of the filter to apply to the exported scan report. You can find valid values for the filter in the &#39;control&#39; attribute of the objects returned by the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_search_type** | **str**| For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 

### Return type

[**InlineResponse20092**](vm__InlineResponse20092.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns plugin details. |  -  |
**403** | Returned if you do not have permission to view plugin details. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **workbenches_vulnerability_output**
> list[InlineResponse20093] workbenches_vulnerability_output(plugin_id, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)

List plugin outputs

Retrieves the vulnerability outputs for a plugin. The list returned is limited to 5,000. To retrieve more than 5,000 vulnerability outputs, use the [export-request API](ref:workbenches-export-request).   **Note:** This endpoint is not intended for large or frequent exports of vulnerability or assets data. If you experience errors, reduce the volume, [rate](doc:rate-limiting), or [concurrency](doc:concurrency-limiting) of your requests or narrow your filters. Contact support if you continue to experience errors. Additionally, Tenable recommends the [POST /vulns/export](ref:exports-vulns-request-export) endpoint for large or frequent exports of vulnerability data, and the [POST /assets/export](ref:exports-assets-request-export) endpoint for large or frequent exports of assets data.  For information and best practices for retrieving vulnerability and assets data from Tenable.io, see [Retrieve Vulnerability Data from Tenable.io](doc:retrieve-vulnerability-data-from-tenableio) and [Retrieve Asset Data from Tenable.io](doc:retrieve-asset-data-from-tenableio).<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.WorkbenchesApi(api_client)
    plugin_id = 56 # int | The ID of the plugin. You can find the plugin ID by examining the output of the [GET /workbenches/vulnerabilities](ref:workbenches-vulnerabilities) endpoint.
date_range = 56 # int | The number of days of data prior to and including today that should be returned. (optional)
filter_0_filter = '?filter.0.filter=plugin.name' # str | The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_quality = '&filter.0.quality=match' # str | The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_0_value = '&filter.0.value=RHEL' # str | The value of the filter to apply to the exported scan report. You can find valid values for the filter in the 'control' attribute of the objects returned by the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). (optional)
filter_search_type = 'filter_search_type_example' # str | For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). (optional)

    try:
        # List plugin outputs
        api_response = api_instance.workbenches_vulnerability_output(plugin_id, date_range=date_range, filter_0_filter=filter_0_filter, filter_0_quality=filter_0_quality, filter_0_value=filter_0_value, filter_search_type=filter_search_type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling WorkbenchesApi->workbenches_vulnerability_output: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plugin_id** | **int**| The ID of the plugin. You can find the plugin ID by examining the output of the [GET /workbenches/vulnerabilities](ref:workbenches-vulnerabilities) endpoint. | 
 **date_range** | **int**| The number of days of data prior to and including today that should be returned. | [optional] 
 **filter_0_filter** | **str**| The name of the filter to apply to the exported scan report. You can find available filters by using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_quality** | **str**| The operator of the filter to apply to the exported scan report. You can find the operators for the filter using the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_0_value** | **str**| The value of the filter to apply to the exported scan report. You can find valid values for the filter in the &#39;control&#39; attribute of the objects returned by the [GET /filters/workbenches/vulnerabilities](ref:workbenches-vulnerabilities-filters) endpoint. For more information about the format of this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 
 **filter_search_type** | **str**| For multiple filters, specifies whether to use the AND or the OR logical operator. The default is AND. For more information about this parameter, see [Workbench Filters](doc:workbench-filters). | [optional] 

### Return type

[**list[InlineResponse20093]**](vm__InlineResponse20093.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns plugin outputs. |  -  |
**403** | Returned if you do not have permission to view plugin outputs. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

