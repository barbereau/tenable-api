# InlineResponse20015

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scanner_uuid** | **str** | The UUID of the scanner the scan belongs to. | [optional] 
**name** | **str** | The name of the scan. | [optional] 
**status** | **str** | Scan status. Can be: pending, processing, stopping, pausing, paused, resuming, or running. | [optional] 
**id** | **str** | The scan UUID. | [optional] 
**scan_id** | **int** | The ID of the scan. | [optional] 
**user** | **str** | The username of the scan owner. | [optional] 
**last_modification_date** | **int** | The last time the scan was modified in Unix time. | [optional] 
**start_time** | **int** | The date and time when the scan was started. | [optional] 
**remote** | **bool** | True if the scan is running remotely; false, otherwise. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


