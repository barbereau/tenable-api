# InlineObject44

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**folder_id** | **int** | The ID of the destination folder. If you don&#39;t specify a folder ID, Tenable.io creates the copy in the same folder as the original. | [optional] 
**name** | **str** | The name of the copied scan. If you don&#39;t specify a name, Tenable.io uses the same name as the original with \&quot;Copy of\&quot; prefix. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


