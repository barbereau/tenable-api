# LogoAssignmentBulk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logo_uuid** | **str** | The UUID of a logo in the Tenable.io MSSP Portal. | [optional] 
**account_uuids** | **list[str]** | An array of customer account UUIDs in the Tenable.io MSSP Portal. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


