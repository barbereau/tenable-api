# InlineResponse20084PaginationSort

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the sort field. | [optional] 
**order** | **str** | The direction in which Tenable.io sorts on the field, &#x60;asc&#x60; for ascending or &#x60;desc&#x60; for descending. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


