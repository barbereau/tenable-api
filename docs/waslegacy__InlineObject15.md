# InlineObject15

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | **str** | The file format to use. For Web Application Scanning, supported export formats are Nessus, CSV, and DB. | 
**password** | **str** | The password used to encrypt database exports (\\*Required when exporting as DB). | [optional] 
**chapters** | **str** | The chapters to include in the export (expecting a semi-colon delimited string comprised of some combination of the following options: vuln\\_hosts\\_summary, vuln\\_by\\_host, compliance\\_exec, remediations, vuln\\_by\\_plugin, compliance) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


