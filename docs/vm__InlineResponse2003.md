# InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | [**list[InlineResponse2003Filters]**](vm__InlineResponse2003Filters.md) | An array specifying values to use when constructing an asset rule for the [POST /access-groups](ref:io-v1-access-groups-create) and [PUT /access-groups/{id}](ref:io-v1-access-groups-edit) methods. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


