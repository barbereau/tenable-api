# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineObject28(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'scanner_uuids': 'list[str]'
    }

    attribute_map = {
        'scanner_uuids': 'scanner_uuids'
    }

    def __init__(self, scanner_uuids=None, local_vars_configuration=None):  # noqa: E501
        """InlineObject28 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._scanner_uuids = None
        self.discriminator = None

        self.scanner_uuids = scanner_uuids

    @property
    def scanner_uuids(self):
        """Gets the scanner_uuids of this InlineObject28.  # noqa: E501

        A list of UUIDs for the scanners and scanner groups you want to bulk move to a network object. To get values for this list, use the [GET /networks/{network_id}/assignable-scanners](ref:networks-list-assignable-scanners) endpoint.  # noqa: E501

        :return: The scanner_uuids of this InlineObject28.  # noqa: E501
        :rtype: list[str]
        """
        return self._scanner_uuids

    @scanner_uuids.setter
    def scanner_uuids(self, scanner_uuids):
        """Sets the scanner_uuids of this InlineObject28.

        A list of UUIDs for the scanners and scanner groups you want to bulk move to a network object. To get values for this list, use the [GET /networks/{network_id}/assignable-scanners](ref:networks-list-assignable-scanners) endpoint.  # noqa: E501

        :param scanner_uuids: The scanner_uuids of this InlineObject28.  # noqa: E501
        :type scanner_uuids: list[str]
        """
        if self.local_vars_configuration.client_side_validation and scanner_uuids is None:  # noqa: E501
            raise ValueError("Invalid value for `scanner_uuids`, must not be `None`")  # noqa: E501

        self._scanner_uuids = scanner_uuids

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineObject28):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineObject28):
            return True

        return self.to_dict() != other.to_dict()
