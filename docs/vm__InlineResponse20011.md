# InlineResponse20011

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agents** | [**list[InlineResponse20011Agents]**](vm__InlineResponse20011Agents.md) |  | [optional] 
**pagination** | [**InlineResponse20011Pagination**](vm__InlineResponse20011Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


