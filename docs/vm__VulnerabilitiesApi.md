# tenableapi.vm.VulnerabilitiesApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vulnerabilities_import**](vm__VulnerabilitiesApi.md#vulnerabilities_import) | **POST** /import/vulnerabilities | Import vulnerabilities v1
[**vulnerabilities_import_v2**](vm__VulnerabilitiesApi.md#vulnerabilities_import_v2) | **POST** /api/v2/vulnerabilities | Import vulnerabilities v2


# **vulnerabilities_import**
> object vulnerabilities_import(inline_object56)

Import vulnerabilities v1

**Deprecated!** This endpoint is deprecated. Use the [POST /vulnerabilities](#vulnerabilities-import-v2) v2 endpoint instead.  Imports a list of vulnerabilities in JSON format. The request cannot exceed 15 MB in total size. In addition, the request can contain a maximum of 50 asset objects. For request body examples, see [Add Vulnerability Data to Tenable.io](doc:add-vulnerability-data-to-tenableio). <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.VulnerabilitiesApi(api_client)
    inline_object56 = tenableapi.vm.InlineObject56() # InlineObject56 | 

    try:
        # Import vulnerabilities v1
        api_response = api_instance.vulnerabilities_import(inline_object56)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VulnerabilitiesApi->vulnerabilities_import: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object56** | [**InlineObject56**](vm__InlineObject56.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully imports the vulnerabilities. |  -  |
**400** | Returned if you submitted an invalid request. |  -  |
**403** | Returned if you do not have permission to import vulnerabilities. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vulnerabilities_import_v2**
> InlineResponse20090 vulnerabilities_import_v2(inline_object57)

Import vulnerabilities v2

Imports a list of vulnerabilities in JSON format. The request cannot exceed 15 MB in total size. In addition, the request can contain a maximum of 50 asset objects. For request body examples, see [Add Vulnerability Data to Tenable.io](doc:add-vulnerability-data-to-tenableio). <p>Requires ADMINISTRATOR [64] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.VulnerabilitiesApi(api_client)
    inline_object57 = tenableapi.vm.InlineObject57() # InlineObject57 | 

    try:
        # Import vulnerabilities v2
        api_response = api_instance.vulnerabilities_import_v2(inline_object57)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VulnerabilitiesApi->vulnerabilities_import_v2: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object57** | [**InlineObject57**](vm__InlineObject57.md)|  | 

### Return type

[**InlineResponse20090**](vm__InlineResponse20090.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully imports the vulnerabilities. Currently all responses will not have a job id. |  -  |
**400** | Returned if you submit an invalid request. |  -  |
**403** | Returned if you do not have permission to import vulnerabilities. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encounters an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

