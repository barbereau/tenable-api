# ProvisioningForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_name** | **str** | The name of the customer account in the Tenable.io MSSP Portal. | 
**sso_username** | **str** | The username for the user account that the Tenable.io MSSP Portal uses to access the customer&#39;s Tenable.io instance. | 
**po_number** | **str** | The reference number for the purchase order (PO) submitted when purchasing the Tenable.io MSSP Portal from either Tenable or a reseller. | 
**distributor** | **str** | The value-added reseller through which you purchased Tenable.io MSSP Portal. | [optional] 
**anticipated_license_assets** | **int** | The number of assets which the customer is licensed to scan. | [optional] 
**notes** | **str** | User-defined text; for example, internal tracking or customer contact information. | [optional] 
**logo_uuid** | **str** | The UUID of the logo you want to assign to the customer account in the Tenable.io MSSP Portal. | [optional] 
**provisioned_account_details** | [**ProvisionedAccountDetails**](mssp__ProvisionedAccountDetails.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


