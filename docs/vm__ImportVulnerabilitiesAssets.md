# ImportVulnerabilitiesAssets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**network_interfaces** | [**list[ImportVulnerabilitiesNetworkInterfaces]**](vm__ImportVulnerabilitiesNetworkInterfaces.md) | A valid network_interface object must contain at least one of the following parameters: &#x60;ipv4&#x60;, &#x60;netbios_name&#x60;, &#x60;fqdn&#x60;. | [optional] 
**hostname** | **str** | The asset&#39;s hostname. | [optional] 
**servicenow_sysid** | **str** | The unique record identifier of the asset in ServiceNow. For more information, see the ServiceNow documentation. | [optional] 
**ssh_fingerprint** | **str** | The SSH key fingerprint that the scan has associated with the asset. | [optional] 
**bios_uuid** | **str** | The BIOS UUID of the asset. | [optional] 
**netbios_name** | **str** | The NetBIOS name that the scan has associated with the asset. | [optional] 
**tenable_agent_id** | **int** | The unique ID of the Nessus agent installed on the asset. This parameter is supported only if the &#x60;source&#x60; parameter for the request is &#x60;security_center&#x60;. | [optional] 
**vulnerabilities** | [**list[ImportVulnerabilitiesVulnerabilities]**](vm__ImportVulnerabilitiesVulnerabilities.md) | A valid vulnerability object must contain at least one of the following parameters: &#x60;tenable_plugin_id&#x60; or &#x60;cve&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


