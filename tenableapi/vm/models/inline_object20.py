# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineObject20(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'name': 'str',
        'description': 'str',
        'members': 'str',
        'schedule': 'ExclusionsSchedule',
        'network_id': 'str'
    }

    attribute_map = {
        'name': 'name',
        'description': 'description',
        'members': 'members',
        'schedule': 'schedule',
        'network_id': 'network_id'
    }

    def __init__(self, name=None, description=None, members=None, schedule=None, network_id=None, local_vars_configuration=None):  # noqa: E501
        """InlineObject20 - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._name = None
        self._description = None
        self._members = None
        self._schedule = None
        self._network_id = None
        self.discriminator = None

        if name is not None:
            self.name = name
        if description is not None:
            self.description = description
        if members is not None:
            self.members = members
        if schedule is not None:
            self.schedule = schedule
        if network_id is not None:
            self.network_id = network_id

    @property
    def name(self):
        """Gets the name of this InlineObject20.  # noqa: E501

        The name of the exclusion.  # noqa: E501

        :return: The name of this InlineObject20.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineObject20.

        The name of the exclusion.  # noqa: E501

        :param name: The name of this InlineObject20.  # noqa: E501
        :type name: str
        """

        self._name = name

    @property
    def description(self):
        """Gets the description of this InlineObject20.  # noqa: E501

        The description of the exclusion.  # noqa: E501

        :return: The description of this InlineObject20.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this InlineObject20.

        The description of the exclusion.  # noqa: E501

        :param description: The description of this InlineObject20.  # noqa: E501
        :type description: str
        """

        self._description = description

    @property
    def members(self):
        """Gets the members of this InlineObject20.  # noqa: E501

        The targets that you want excluded from scans. Specify multiple targets as a comma-separated string. Targets can be in the following formats:  - an individual IPv4 address (192.168.1.1)  - a range of IPv4 addresses (192.168.1.1-192.168.1.255)  - CIDR notation (192.168.2.0/24)  - a fully-qualified domain name (FQDN) (host.domain.com)  # noqa: E501

        :return: The members of this InlineObject20.  # noqa: E501
        :rtype: str
        """
        return self._members

    @members.setter
    def members(self, members):
        """Sets the members of this InlineObject20.

        The targets that you want excluded from scans. Specify multiple targets as a comma-separated string. Targets can be in the following formats:  - an individual IPv4 address (192.168.1.1)  - a range of IPv4 addresses (192.168.1.1-192.168.1.255)  - CIDR notation (192.168.2.0/24)  - a fully-qualified domain name (FQDN) (host.domain.com)  # noqa: E501

        :param members: The members of this InlineObject20.  # noqa: E501
        :type members: str
        """

        self._members = members

    @property
    def schedule(self):
        """Gets the schedule of this InlineObject20.  # noqa: E501


        :return: The schedule of this InlineObject20.  # noqa: E501
        :rtype: ExclusionsSchedule
        """
        return self._schedule

    @schedule.setter
    def schedule(self, schedule):
        """Sets the schedule of this InlineObject20.


        :param schedule: The schedule of this InlineObject20.  # noqa: E501
        :type schedule: ExclusionsSchedule
        """

        self._schedule = schedule

    @property
    def network_id(self):
        """Gets the network_id of this InlineObject20.  # noqa: E501

        The ID of the network object associated with scanners where Tenable.io applies the exclusion. The default network ID is `00000000-0000-0000-0000-000000000000`. To determine the ID of a custom network, use the [GET /networks](ref:networks-list) endpoint. For more information about network objects, see [Manage Networks](doc:manage-networks-tio).  # noqa: E501

        :return: The network_id of this InlineObject20.  # noqa: E501
        :rtype: str
        """
        return self._network_id

    @network_id.setter
    def network_id(self, network_id):
        """Sets the network_id of this InlineObject20.

        The ID of the network object associated with scanners where Tenable.io applies the exclusion. The default network ID is `00000000-0000-0000-0000-000000000000`. To determine the ID of a custom network, use the [GET /networks](ref:networks-list) endpoint. For more information about network objects, see [Manage Networks](doc:manage-networks-tio).  # noqa: E501

        :param network_id: The network_id of this InlineObject20.  # noqa: E501
        :type network_id: str
        """

        self._network_id = network_id

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineObject20):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineObject20):
            return True

        return self.to_dict() != other.to_dict()
