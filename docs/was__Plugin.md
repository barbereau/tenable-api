# Plugin

The plugin properties.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plugin_id** | **int** | The ID of the plugin. | 
**name** | **str** | The name of the plugin. | 
**family** | **str** | The name of the plugin family. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


