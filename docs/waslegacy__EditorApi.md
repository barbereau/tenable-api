# tenableapi.waslegacy.EditorApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**was_editor_details**](waslegacy__EditorApi.md#was_editor_details) | **GET** /editor/{type}/{id} | Get configuration details
[**was_editor_list**](waslegacy__EditorApi.md#was_editor_list) | **GET** /editor/{type}/templates | List templates
[**was_editor_plugin_description**](waslegacy__EditorApi.md#was_editor_plugin_description) | **GET** /editor/policy/{policy_id}/families/{family_id}/plugins/{plugin_id} | Get plugin details
[**was_editor_template_details**](waslegacy__EditorApi.md#was_editor_template_details) | **GET** /editor/{type}/templates/{template_uuid} | Get template details


# **was_editor_details**
> InlineResponse2003 was_editor_details(type, id)

Get configuration details

Gets the configuration details for the scan or policy.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.EditorApi(api_client)
    type = 'type_example' # str | The type of object (scan or policy).
id = 56 # int | The unique ID of the object.

    try:
        # Get configuration details
        api_response = api_instance.was_editor_details(type, id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling EditorApi->was_editor_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| The type of object (scan or policy). | 
 **id** | **int**| The unique ID of the object. | 

### Return type

[**InlineResponse2003**](waslegacy__InlineResponse2003.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the object data. |  -  |
**403** | Returned if the user does not have permission to open the object. |  -  |
**404** | Returned if the object does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_editor_list**
> list[InlineResponse2004] was_editor_list(type)

List templates

Lists scan or policy templates, including non-WAS templates.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.EditorApi(api_client)
    type = 'type_example' # str | The type of templates to retrieve (scan or policy).

    try:
        # List templates
        api_response = api_instance.was_editor_list(type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling EditorApi->was_editor_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| The type of templates to retrieve (scan or policy). | 

### Return type

[**list[InlineResponse2004]**](waslegacy__InlineResponse2004.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the template list. |  -  |
**403** | Returned if the user does not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_editor_plugin_description**
> InlineResponse2005 was_editor_plugin_description(policy_id, family_id, plugin_id)

Get plugin details

Gets the details of the plugin associated with the scan or policy.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.EditorApi(api_client)
    policy_id = 56 # int | The ID of the policy to lookup.
family_id = 56 # int | The ID of the family to lookup within the policy.
plugin_id = 56 # int | The ID of the plugin to lookup within the family.

    try:
        # Get plugin details
        api_response = api_instance.was_editor_plugin_description(policy_id, family_id, plugin_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling EditorApi->was_editor_plugin_description: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **policy_id** | **int**| The ID of the policy to lookup. | 
 **family_id** | **int**| The ID of the family to lookup within the policy. | 
 **plugin_id** | **int**| The ID of the plugin to lookup within the family. | 

### Return type

[**InlineResponse2005**](waslegacy__InlineResponse2005.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the plugin output. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **was_editor_template_details**
> InlineResponse2006 was_editor_template_details(type, template_uuid)

Get template details

Gets details for the given template.<p>Requires STANDARD [32] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.waslegacy
from tenableapi.waslegacy.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.waslegacy.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.waslegacy.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.waslegacy.EditorApi(api_client)
    type = 'type_example' # str | The type of template to retrieve (scan or policy).
template_uuid = 'template_uuid_example' # str | The UUID for the template.

    try:
        # Get template details
        api_response = api_instance.was_editor_template_details(type, template_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling EditorApi->was_editor_template_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| The type of template to retrieve (scan or policy). | 
 **template_uuid** | **str**| The UUID for the template. | 

### Return type

[**InlineResponse2006**](waslegacy__InlineResponse2006.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the template details. |  -  |
**403** | Returned if the user does not have permission to open the template. |  -  |
**404** | Returned if the template does not exist. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

