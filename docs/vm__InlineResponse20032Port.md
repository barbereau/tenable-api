# InlineResponse20032Port

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**port** | **str** | The port the scanner used to communicate with the asset. | [optional] 
**protocol** | **str** | The protocol the scanner used to communicate with the asset. | [optional] 
**service** | **str** | The service the scanner used to communicate with the asset. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


