# InlineResponse20066

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scan_uuid** | **str** | The UUID of the scan launched. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


