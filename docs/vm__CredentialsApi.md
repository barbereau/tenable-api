# tenableapi.vm.CredentialsApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**credentials_create**](vm__CredentialsApi.md#credentials_create) | **POST** /credentials | Create managed credential
[**credentials_delete**](vm__CredentialsApi.md#credentials_delete) | **DELETE** /credentials/{uuid} | Delete managed credential
[**credentials_details**](vm__CredentialsApi.md#credentials_details) | **GET** /credentials/{uuid} | Get managed credential details
[**credentials_file_upload**](vm__CredentialsApi.md#credentials_file_upload) | **POST** /credentials/files | Upload credentials file
[**credentials_list**](vm__CredentialsApi.md#credentials_list) | **GET** /credentials | List managed credentials
[**credentials_list_credential_types**](vm__CredentialsApi.md#credentials_list_credential_types) | **GET** /credentials/types | List credential types
[**credentials_update**](vm__CredentialsApi.md#credentials_update) | **PUT** /credentials/{uuid} | Update managed credential


# **credentials_create**
> InlineResponse20019 credentials_create(inline_object15)

Create managed credential

Creates a managed credential object that you can use when configuring and running scans. You can grant other users the permission to use the managed credential object in scans and to edit the managed credential configuration.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.CredentialsApi(api_client)
    inline_object15 = tenableapi.vm.InlineObject15() # InlineObject15 | 

    try:
        # Create managed credential
        api_response = api_instance.credentials_create(inline_object15)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling CredentialsApi->credentials_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object15** | [**InlineObject15**](vm__InlineObject15.md)|  | 

### Return type

[**InlineResponse20019**](vm__InlineResponse20019.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully creates a managed credential object. |  -  |
**400** | Returned if Tenable.io encounters invalid JSON in request body. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have permission to create managed credential objects. |  -  |
**409** | Returned if a managed credential object with the same name already exists. |  -  |
**415** | Returned if the request payload is in an unsupported format. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **credentials_delete**
> InlineResponse20022 credentials_delete(uuid)

Delete managed credential

Deletes the specified managed credential object. When you delete a managed credential object, Tenable.io also removes the credential from any scan that uses the credential.<P>Requires CAN EDIT [64] credential permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.CredentialsApi(api_client)
    uuid = 'uuid_example' # str | The UUID for the managed credential object you want to delete.

    try:
        # Delete managed credential
        api_response = api_instance.credentials_delete(uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling CredentialsApi->credentials_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **str**| The UUID for the managed credential object you want to delete. | 

### Return type

[**InlineResponse20022**](vm__InlineResponse20022.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deletes the specified managed credential object. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have sufficient permissions to delete the specified managed credential object. |  -  |
**404** | Returned if Tenable.io could not find the managed credential object you specified. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **credentials_details**
> InlineResponse20020 credentials_details(uuid)

Get managed credential details

Returns details of the specified managed credential object.<p>Requires CAN USE [32] credential permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.CredentialsApi(api_client)
    uuid = 'uuid_example' # str | The UUID of the managed credential for which you want to view details.

    try:
        # Get managed credential details
        api_response = api_instance.credentials_details(uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling CredentialsApi->credentials_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **str**| The UUID of the managed credential for which you want to view details. | 

### Return type

[**InlineResponse20020**](vm__InlineResponse20020.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the details of the specified managed credential object. |  -  |
**403** | Returned if you do not have sufficient permissions to view the specified managed credential object. |  -  |
**404** | Returned if Tenable.io cannot find a managed credential object with the specified UUID. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |
**504** | Returned if Tenable.io is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **credentials_file_upload**
> InlineResponse20024 credentials_file_upload(filedata=filedata)

Upload credentials file

Uploads a file for use with a managed credential (for example, as a private key file for an SSH credential). For more information about using this file, see [Create a Managed Credential](doc:create-managed-credential). <p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.CredentialsApi(api_client)
    filedata = '/path/to/file' # file | The file to upload. (optional)

    try:
        # Upload credentials file
        api_response = api_instance.credentials_file_upload(filedata=filedata)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling CredentialsApi->credentials_file_upload: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filedata** | **file**| The file to upload. | [optional] 

### Return type

[**InlineResponse20024**](vm__InlineResponse20024.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the name of the successfully uploaded file. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io cannot upload the file. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **credentials_list**
> InlineResponse20018 credentials_list(f=f, ft=ft, limit=limit, offset=offset, sort=sort, referrer_owner_uuid=referrer_owner_uuid)

List managed credentials

Lists managed credentials where you have been assigned at least CAN USE (32) permissions.     **Note:** This endpoint does not list scan-specific or policy-specific credentials (that is, credentials stored in either a scan or a policy). To view a list of scan-specific or policy-specific credentials, use the editor details endpoint (GET /editor/{type}/{id}). <p>Requires CAN USE [32] credential permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.CredentialsApi(api_client)
    f = 'f_example' # str | A filter condition in the following format: `field:operator:value`. For managed credentials, you can only filter on the `name` field, using the following operators:  * eq—The name of the returned credential is equal to the text you specify.  * neq—The returned list of managed credentials excludes the credential object where the name is equal to the text you specify.  * match—The returned list includes managed credentials where the name contains the text you specify at least partially.  You can specify multiple `f` parameters, separated by ampersand (&) characters. If you specify multiple `f` parameters, use the `ft` parameter to specify how Tenable.io applies the multiple filter conditions. (optional)
ft = 'ft_example' # str | The operator that Tenable.io applies if multiple \\`f\\` parameters are present. The `OR` operator is the only supported value. If you omit this parameter and multiple `f` parameters are present, Tenable.io applies the `OR` operator by default. (optional)
limit = 56 # int | Maximum number of objects requested (or service imposed limit if not in request). Must be in the int32 format. (optional)
offset = 56 # int | Offset from request (or zero). Must be in the int32 format. (optional)
sort = [tenableapi.vm.Sort()] # list[Sort] | An array of objects specifyfing the sort order for the returned data. (optional)
referrer_owner_uuid = 'referrer_owner_uuid_example' # str | The UUID of a scan owner. This parameter limits the returned data to managed credentials assigned to scans owned by the specified user. (optional)

    try:
        # List managed credentials
        api_response = api_instance.credentials_list(f=f, ft=ft, limit=limit, offset=offset, sort=sort, referrer_owner_uuid=referrer_owner_uuid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling CredentialsApi->credentials_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **f** | **str**| A filter condition in the following format: &#x60;field:operator:value&#x60;. For managed credentials, you can only filter on the &#x60;name&#x60; field, using the following operators:  * eq—The name of the returned credential is equal to the text you specify.  * neq—The returned list of managed credentials excludes the credential object where the name is equal to the text you specify.  * match—The returned list includes managed credentials where the name contains the text you specify at least partially.  You can specify multiple &#x60;f&#x60; parameters, separated by ampersand (&amp;) characters. If you specify multiple &#x60;f&#x60; parameters, use the &#x60;ft&#x60; parameter to specify how Tenable.io applies the multiple filter conditions. | [optional] 
 **ft** | **str**| The operator that Tenable.io applies if multiple \\&#x60;f\\&#x60; parameters are present. The &#x60;OR&#x60; operator is the only supported value. If you omit this parameter and multiple &#x60;f&#x60; parameters are present, Tenable.io applies the &#x60;OR&#x60; operator by default. | [optional] 
 **limit** | **int**| Maximum number of objects requested (or service imposed limit if not in request). Must be in the int32 format. | [optional] 
 **offset** | **int**| Offset from request (or zero). Must be in the int32 format. | [optional] 
 **sort** | [**list[Sort]**](vm__Sort.md)| An array of objects specifyfing the sort order for the returned data. | [optional] 
 **referrer_owner_uuid** | **str**| The UUID of a scan owner. This parameter limits the returned data to managed credentials assigned to scans owned by the specified user. | [optional] 

### Return type

[**InlineResponse20018**](vm__InlineResponse20018.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of managed credentials. |  -  |
**400** | Returned if the query parameters in your request were invalid. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **credentials_list_credential_types**
> InlineResponse20023 credentials_list_credential_types()

List credential types

Lists all credential types supported for managed credentials in Tenable.io. For more information about using the data returned by this endpoint to create managed credentials, see [Determine Settings for a Credential Type](doc:determine-settings-for-credential-type). <p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.CredentialsApi(api_client)
    
    try:
        # List credential types
        api_response = api_instance.credentials_list_credential_types()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling CredentialsApi->credentials_list_credential_types: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20023**](vm__InlineResponse20023.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of supported credential types and associated settings. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **credentials_update**
> InlineResponse20021 credentials_update(uuid, inline_object16)

Update managed credential

Updates a managed credential object.   **Note:** You cannot use this endpoint to update the credential type. If you create a managed credential with the incorrect type, create a new managed credential with the correct credential type, and delete the incorrect managed credential.<p>Requires CAN EDIT [64] credential permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.CredentialsApi(api_client)
    uuid = 'uuid_example' # str | The UUID of the managed credential object you want to update.
inline_object16 = tenableapi.vm.InlineObject16() # InlineObject16 | 

    try:
        # Update managed credential
        api_response = api_instance.credentials_update(uuid, inline_object16)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling CredentialsApi->credentials_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **str**| The UUID of the managed credential object you want to update. | 
 **inline_object16** | [**InlineObject16**](vm__InlineObject16.md)|  | 

### Return type

[**InlineResponse20021**](vm__InlineResponse20021.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully updates the managed credential object. |  -  |
**400** | Returned if Tenable.io encounters invalid JSON in the request body. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**403** | Returned if you do not have permission to update the specified managed credential object. |  -  |
**404** | Returned if Tenable.io could not find the specified managed credential object, either because the object does not exist or because the object has been deleted. |  -  |
**409** | Returned if a managed credential object with the same name already exists. |  -  |
**415** | Returned if the request payload is in an unsupported format. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io encountered an internal server error. Wait a moment, and try your request again. |  -  |
**503** | Returned if a Tenable.io service is unavailable. Wait a moment, and try your request again. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

