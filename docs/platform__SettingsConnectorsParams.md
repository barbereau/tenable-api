# SettingsConnectorsParams

The connector parameters:   * For AWS connectors, the parameters include the `access_key`, `secret_key`, associated accounts, and cloudtrails.   * For keyless AWS connectors, the parameters include associated AWS accounts (sub-accounts) and cloudtrails.   * For Azure connectors, the parameters include the `application_ID`, `tenant_ID`, `client_secret` key, and an optional list of subscription IDs. If your request omits subscription IDs, Tenable.io automatically discovers them. * For GCP connectors, the `service_account_key`.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_key** | **str** | The AWS access key. &lt;b&gt;Note: &lt;/b&gt;The &#x60;access_key&#x60; is not included in the keyless AWS connector parameters. | [optional] 
**secret_key** | **str** | For AWS connectors, the AWS secret key. | [optional] 
**trails** | [**list[InlineResponse2005ParamsTrails]**](platform__InlineResponse2005ParamsTrails.md) | For AWS connectors, a list of AWS cloudtrails associated with the connector. The trails must be available to be used by the connector. Use the [POST /settings/connectors/aws/cloudtrails](ref:connectors-get-aws-cloudtrails) endpoint to check the &#x60;availability&#x60; property of cloudtrail objects. | [optional] 
**sub_accounts** | [**list[InlineResponse2005ParamsSubAccounts]**](platform__InlineResponse2005ParamsSubAccounts.md) | For AWS connectors, a list of AWS accounts associated with the connector. | [optional] 
**application_id** | **str** | For Azure connectors, Azure application ID. | [optional] 
**tenant_id** | **str** | For Azure connectors, Azure tenant ID. | [optional] 
**subscription_id** | **str** | For Azure connectors, Azure subscription ID. If you do not provide subscription IDs, Tenable.io automatically discovers them. | [optional] 
**service_account_key** | **str** | For GCP connectors, Base64-encoded string value of the service account key JSON file. For more information, see [GCP documentation](https://cloud.google.com/iam/docs/creating-managing-service-account-keys).  **Important!** The &#x60;service_account_key&#x60; value must include only the literal encoded string. Do not include the &#x60;data:application/json;base64&#x60; prefix. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


