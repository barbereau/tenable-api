# tenableapi.containerv2.ImagesApi

All URIs are relative to *https://cloud.tenable.com/container-security/api/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**container_security_v2_delete_image**](container__ImagesApi.md#container_security_v2_delete_image) | **DELETE** /images/{repository}/{image}/{tag} | Delete image
[**container_security_v2_get_image_details**](container__ImagesApi.md#container_security_v2_get_image_details) | **GET** /images/{repository}/{image}/{tag} | Get image details
[**container_security_v2_list_images**](container__ImagesApi.md#container_security_v2_list_images) | **GET** /images | List images


# **container_security_v2_delete_image**
> container_security_v2_delete_image(repository, image, tag)

Delete image

Deletes an image specified by repository, name, and tag.<p>Requires SCAN OPERATOR [24] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv2
from tenableapi.containerv2.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com/container-security/api/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv2.Configuration(
    host = "https://cloud.tenable.com/container-security/api/v2"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv2.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv2.ImagesApi(api_client)
    repository = 'repository_example' # str | The name of the Tenable.io Container Security repository where the image is stored. The value is case-sensitive.
image = 'image_example' # str | The name of the image. The value is case-sensitive.
tag = 'tag_example' # str | The tag identifying the image version. The value is case-sensitive. **Note**: Image tags are not equivalent to Tenable.io [asset tags](ref:tags).

    try:
        # Delete image
        api_instance.container_security_v2_delete_image(repository, image, tag)
    except ApiException as e:
        print("Exception when calling ImagesApi->container_security_v2_delete_image: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **repository** | **str**| The name of the Tenable.io Container Security repository where the image is stored. The value is case-sensitive. | 
 **image** | **str**| The name of the image. The value is case-sensitive. | 
 **tag** | **str**| The tag identifying the image version. The value is case-sensitive. **Note**: Image tags are not equivalent to Tenable.io [asset tags](ref:tags). | 

### Return type

void (empty response body)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Returned if Tenable.io successfully deletes the specified image. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**404** | Returned if Tenable.io cannot find the specified image. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_v2_get_image_details**
> ImageDetails container_security_v2_get_image_details(repository, image, tag)

Get image details

Returns the details for an image specified by repository, name, and tag.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv2
from tenableapi.containerv2.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com/container-security/api/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv2.Configuration(
    host = "https://cloud.tenable.com/container-security/api/v2"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv2.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv2.ImagesApi(api_client)
    repository = 'repository_example' # str | The name of the Tenable.io Container Security repository where the image is stored. The value is case-sensitive.
image = 'image_example' # str | The name of the image. The value is case-sensitive.
tag = 'tag_example' # str | The tag identifying the image version. The value is case-sensitive. **Note**: Image tags are not equivalent to Tenable.io [asset tags](ref:tags).

    try:
        # Get image details
        api_response = api_instance.container_security_v2_get_image_details(repository, image, tag)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ImagesApi->container_security_v2_get_image_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **repository** | **str**| The name of the Tenable.io Container Security repository where the image is stored. The value is case-sensitive. | 
 **image** | **str**| The name of the image. The value is case-sensitive. | 
 **tag** | **str**| The tag identifying the image version. The value is case-sensitive. **Note**: Image tags are not equivalent to Tenable.io [asset tags](ref:tags). | 

### Return type

[**ImageDetails**](container__ImageDetails.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the image details. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |
**404** | Returned if Tenable.io cannot find the specified image. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **container_security_v2_list_images**
> ImageListResponse container_security_v2_list_images(offset=offset, limit=limit, name=name, repo=repo, tag=tag, has_malware=has_malware, score=score, score_operator=score_operator, os=os)

List images

Returns a paginated list of images. Use URL query parameters to filter the list. <p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.containerv2
from tenableapi.containerv2.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com/container-security/api/v2
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.containerv2.Configuration(
    host = "https://cloud.tenable.com/container-security/api/v2"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.containerv2.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.containerv2.ImagesApi(api_client)
    offset = 0 # int | The number of skipped records in the returned result set. Must be in the int32 format. (optional) (default to 0)
limit = 50 # int | The number of records to include in the result set. Must be in the int32 format. Default is 50. The maximum limit is 1,000. (optional) (default to 50)
name = 'name_example' # str | The image name to filter on. Tenable.io returns only the images with names that exactly match the parameter value. The value is case-sensitive. (optional)
repo = 'repo_example' # str | The repository name to filter on. Tenable.io returns only the images from repositories that exactly match the parameter value. The value is case-sensitive. (optional)
tag = 'tag_example' # str | The tag to filter on. Tenable.io returns only the images with tags that exactly match the parameter value. The value is case-sensitive. (optional)
has_malware = True # bool | Specifies whether to return only the images with associated malware (images with the `numberOfMalware` attribute greater than 0). (optional)
score = 56 # int | The score to filter on. Tenable.io limits results based on the parameter value and the comparison operator specified by the `scoreOperator` parameter. For more information about the risk score metric, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/containersecurity/Content/ContainerSecurity/RiskMetrics.htm). (optional)
score_operator = 'score_operator_example' # str | The comparison operator for the value specified by the `score` parameter. Operators include:  - EQ—equals  - GT—greater than  - EQ—less than (optional)
os = 'os_example' # str | The operating system to filter on. Tenable.io returns only the images with an operating system that exactly matches the parameter value. (optional)

    try:
        # List images
        api_response = api_instance.container_security_v2_list_images(offset=offset, limit=limit, name=name, repo=repo, tag=tag, has_malware=has_malware, score=score, score_operator=score_operator, os=os)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ImagesApi->container_security_v2_list_images: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int**| The number of skipped records in the returned result set. Must be in the int32 format. | [optional] [default to 0]
 **limit** | **int**| The number of records to include in the result set. Must be in the int32 format. Default is 50. The maximum limit is 1,000. | [optional] [default to 50]
 **name** | **str**| The image name to filter on. Tenable.io returns only the images with names that exactly match the parameter value. The value is case-sensitive. | [optional] 
 **repo** | **str**| The repository name to filter on. Tenable.io returns only the images from repositories that exactly match the parameter value. The value is case-sensitive. | [optional] 
 **tag** | **str**| The tag to filter on. Tenable.io returns only the images with tags that exactly match the parameter value. The value is case-sensitive. | [optional] 
 **has_malware** | **bool**| Specifies whether to return only the images with associated malware (images with the &#x60;numberOfMalware&#x60; attribute greater than 0). | [optional] 
 **score** | **int**| The score to filter on. Tenable.io limits results based on the parameter value and the comparison operator specified by the &#x60;scoreOperator&#x60; parameter. For more information about the risk score metric, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/containersecurity/Content/ContainerSecurity/RiskMetrics.htm). | [optional] 
 **score_operator** | **str**| The comparison operator for the value specified by the &#x60;score&#x60; parameter. Operators include:  - EQ—equals  - GT—greater than  - EQ—less than | [optional] 
 **os** | **str**| The operating system to filter on. Tenable.io returns only the images with an operating system that exactly matches the parameter value. | [optional] 

### Return type

[**ImageListResponse**](container__ImageListResponse.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a paginated list of images in your Tenable.io Container Security instance. |  -  |
**401** | Returned if Tenable.io cannot authenticate the user account that submitted the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

