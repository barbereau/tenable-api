# tenableapi.vm.FoldersApi

All URIs are relative to *https://cloud.tenable.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**folders_create**](vm__FoldersApi.md#folders_create) | **POST** /folders | Create folder
[**folders_delete**](vm__FoldersApi.md#folders_delete) | **DELETE** /folders/{folder_id} | Delete folder
[**folders_edit**](vm__FoldersApi.md#folders_edit) | **PUT** /folders/{folder_id} | Rename folder
[**folders_list**](vm__FoldersApi.md#folders_list) | **GET** /folders | List folders


# **folders_create**
> InlineResponse20042 folders_create(inline_object24)

Create folder

Creates a new custom folder for the current user.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.FoldersApi(api_client)
    inline_object24 = tenableapi.vm.InlineObject24() # InlineObject24 | 

    try:
        # Create folder
        api_response = api_instance.folders_create(inline_object24)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling FoldersApi->folders_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object24** | [**InlineObject24**](vm__InlineObject24.md)|  | 

### Return type

[**InlineResponse20042**](vm__InlineResponse20042.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the new folder ID. |  -  |
**400** | Returned if the folder name is invalid. |  -  |
**403** | Returned if you do not have permission to create a folder. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to create the folder. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **folders_delete**
> object folders_delete(folder_id)

Delete folder

Deletes a folder. You cannot delete:  - Tenable-provided folders (`main` or `trash`)  - a custom folder that belongs to another user (even if you use an administrator account)  If you delete a folder that contains scans, Tenable.io automatically moves those scans to the trash folder. <p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.FoldersApi(api_client)
    folder_id = 56 # int | The ID of the custom folder to delete. Use the [GET /folders](/reference#folders-list) endpoint to determine the ID of the custom folder you want to delete.

    try:
        # Delete folder
        api_response = api_instance.folders_delete(folder_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling FoldersApi->folders_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **folder_id** | **int**| The ID of the custom folder to delete. Use the [GET /folders](/reference#folders-list) endpoint to determine the ID of the custom folder you want to delete. | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully deletes the folder. |  -  |
**403** | Returned if you attempt to delete a Tenable-provided folder (a folder where the &#x60;custom&#x60; attribute is set to &#x60;0&#x60;). |  -  |
**404** | Returned if Tenable.io cannot find the folder specified in the request. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to delete the folder. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **folders_edit**
> object folders_edit(folder_id, inline_object25)

Rename folder

Renames a folder for the current user. You cannot rename:  - system-provided scan folders  - a custom folder that belongs to another user (even if your account has administrator privileges)<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.FoldersApi(api_client)
    folder_id = 56 # int | The ID of the folder to edit.
inline_object25 = tenableapi.vm.InlineObject25() # InlineObject25 | 

    try:
        # Rename folder
        api_response = api_instance.folders_edit(folder_id, inline_object25)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling FoldersApi->folders_edit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **folder_id** | **int**| The ID of the folder to edit. | 
 **inline_object25** | [**InlineObject25**](vm__InlineObject25.md)|  | 

### Return type

**object**

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returned if Tenable.io successfully renames the folder. |  -  |
**403** | Returned if you attempted to rename a system-provided folder. For system-provided folders, the &#x60;custom&#x60; attribute is set to &#x60;0&#x60;. |  -  |
**404** | Returned if Tenable.io cannot find the specified folder. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |
**500** | Returned if Tenable.io fails to rename the folder. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **folders_list**
> list[InlineResponse20041] folders_list()

List folders

Lists the Tenable-provided folders, as well as the current user's custom folders.<p>Requires BASIC [16] user permissions. See [Permissions](doc:permissions).</p>

### Example

* Api Key Authentication (cloud):
```python
from __future__ import print_function
import time
import tenableapi.vm
from tenableapi.vm.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://cloud.tenable.com
# See configuration.py for a list of all supported configuration parameters.
configuration = tenableapi.vm.Configuration(
    host = "https://cloud.tenable.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: cloud
configuration.api_key['cloud'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['cloud'] = 'Bearer'

# Enter a context with an instance of the API client
with tenableapi.vm.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tenableapi.vm.FoldersApi(api_client)
    
    try:
        # List folders
        api_response = api_instance.folders_list()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling FoldersApi->folders_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[InlineResponse20041]**](vm__InlineResponse20041.md)

### Authorization

[cloud](../README.md#cloud)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the folder list for the current user. |  -  |
**403** | Returned if you do not have permission to view the list. |  -  |
**429** | Returned if you attempt to send too many requests in a specific period of time. For more information, see [Rate Limiting](doc:rate-limiting). |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

