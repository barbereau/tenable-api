# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class InlineResponse20064Info(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'owner': 'str',
        'name': 'str',
        'no_target': 'bool',
        'folder_id': 'int',
        'control': 'bool',
        'user_permissions': 'int',
        'schedule_uuid': 'str',
        'edit_allowed': 'bool',
        'scanner_name': 'str',
        'policy': 'str',
        'shared': 'bool',
        'object_id': 'int',
        'tag_targets': 'list[str]',
        'acls': 'list[ScansSettingsAcls]',
        'hostcount': 'int',
        'uuid': 'str',
        'status': 'str',
        'scan_type': 'str',
        'targets': 'str',
        'alt_targets_used': 'bool',
        'pci_can_upload': 'bool',
        'scan_start': 'int',
        'timestamp': 'int',
        'is_archived': 'bool',
        'scan_end': 'int',
        'haskb': 'bool',
        'hasaudittrail': 'bool',
        'scanner_start': 'str',
        'scanner_end': 'str'
    }

    attribute_map = {
        'owner': 'owner',
        'name': 'name',
        'no_target': 'no_target',
        'folder_id': 'folder_id',
        'control': 'control',
        'user_permissions': 'user_permissions',
        'schedule_uuid': 'schedule_uuid',
        'edit_allowed': 'edit_allowed',
        'scanner_name': 'scanner_name',
        'policy': 'policy',
        'shared': 'shared',
        'object_id': 'object_id',
        'tag_targets': 'tag_targets',
        'acls': 'acls',
        'hostcount': 'hostcount',
        'uuid': 'uuid',
        'status': 'status',
        'scan_type': 'scan_type',
        'targets': 'targets',
        'alt_targets_used': 'alt_targets_used',
        'pci_can_upload': 'pci-can-upload',
        'scan_start': 'scan_start',
        'timestamp': 'timestamp',
        'is_archived': 'is_archived',
        'scan_end': 'scan_end',
        'haskb': 'haskb',
        'hasaudittrail': 'hasaudittrail',
        'scanner_start': 'scanner_start',
        'scanner_end': 'scanner_end'
    }

    def __init__(self, owner=None, name=None, no_target=None, folder_id=None, control=None, user_permissions=None, schedule_uuid=None, edit_allowed=None, scanner_name=None, policy=None, shared=None, object_id=None, tag_targets=None, acls=None, hostcount=None, uuid=None, status=None, scan_type=None, targets=None, alt_targets_used=None, pci_can_upload=None, scan_start=None, timestamp=None, is_archived=None, scan_end=None, haskb=None, hasaudittrail=None, scanner_start=None, scanner_end=None, local_vars_configuration=None):  # noqa: E501
        """InlineResponse20064Info - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._owner = None
        self._name = None
        self._no_target = None
        self._folder_id = None
        self._control = None
        self._user_permissions = None
        self._schedule_uuid = None
        self._edit_allowed = None
        self._scanner_name = None
        self._policy = None
        self._shared = None
        self._object_id = None
        self._tag_targets = None
        self._acls = None
        self._hostcount = None
        self._uuid = None
        self._status = None
        self._scan_type = None
        self._targets = None
        self._alt_targets_used = None
        self._pci_can_upload = None
        self._scan_start = None
        self._timestamp = None
        self._is_archived = None
        self._scan_end = None
        self._haskb = None
        self._hasaudittrail = None
        self._scanner_start = None
        self._scanner_end = None
        self.discriminator = None

        if owner is not None:
            self.owner = owner
        if name is not None:
            self.name = name
        if no_target is not None:
            self.no_target = no_target
        if folder_id is not None:
            self.folder_id = folder_id
        if control is not None:
            self.control = control
        if user_permissions is not None:
            self.user_permissions = user_permissions
        if schedule_uuid is not None:
            self.schedule_uuid = schedule_uuid
        if edit_allowed is not None:
            self.edit_allowed = edit_allowed
        if scanner_name is not None:
            self.scanner_name = scanner_name
        if policy is not None:
            self.policy = policy
        if shared is not None:
            self.shared = shared
        if object_id is not None:
            self.object_id = object_id
        if tag_targets is not None:
            self.tag_targets = tag_targets
        if acls is not None:
            self.acls = acls
        if hostcount is not None:
            self.hostcount = hostcount
        if uuid is not None:
            self.uuid = uuid
        if status is not None:
            self.status = status
        if scan_type is not None:
            self.scan_type = scan_type
        if targets is not None:
            self.targets = targets
        if alt_targets_used is not None:
            self.alt_targets_used = alt_targets_used
        if pci_can_upload is not None:
            self.pci_can_upload = pci_can_upload
        if scan_start is not None:
            self.scan_start = scan_start
        if timestamp is not None:
            self.timestamp = timestamp
        if is_archived is not None:
            self.is_archived = is_archived
        if scan_end is not None:
            self.scan_end = scan_end
        if haskb is not None:
            self.haskb = haskb
        if hasaudittrail is not None:
            self.hasaudittrail = hasaudittrail
        if scanner_start is not None:
            self.scanner_start = scanner_start
        if scanner_end is not None:
            self.scanner_end = scanner_end

    @property
    def owner(self):
        """Gets the owner of this InlineResponse20064Info.  # noqa: E501

        The owner of the scan.  # noqa: E501

        :return: The owner of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this InlineResponse20064Info.

        The owner of the scan.  # noqa: E501

        :param owner: The owner of this InlineResponse20064Info.  # noqa: E501
        :type owner: str
        """

        self._owner = owner

    @property
    def name(self):
        """Gets the name of this InlineResponse20064Info.  # noqa: E501

        The name of the scan.  # noqa: E501

        :return: The name of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse20064Info.

        The name of the scan.  # noqa: E501

        :param name: The name of this InlineResponse20064Info.  # noqa: E501
        :type name: str
        """

        self._name = name

    @property
    def no_target(self):
        """Gets the no_target of this InlineResponse20064Info.  # noqa: E501

        Indicates whether the scan based on this policy can specify targets.  # noqa: E501

        :return: The no_target of this InlineResponse20064Info.  # noqa: E501
        :rtype: bool
        """
        return self._no_target

    @no_target.setter
    def no_target(self, no_target):
        """Sets the no_target of this InlineResponse20064Info.

        Indicates whether the scan based on this policy can specify targets.  # noqa: E501

        :param no_target: The no_target of this InlineResponse20064Info.  # noqa: E501
        :type no_target: bool
        """

        self._no_target = no_target

    @property
    def folder_id(self):
        """Gets the folder_id of this InlineResponse20064Info.  # noqa: E501

        The unique ID of the destination folder for the scan.  # noqa: E501

        :return: The folder_id of this InlineResponse20064Info.  # noqa: E501
        :rtype: int
        """
        return self._folder_id

    @folder_id.setter
    def folder_id(self, folder_id):
        """Sets the folder_id of this InlineResponse20064Info.

        The unique ID of the destination folder for the scan.  # noqa: E501

        :param folder_id: The folder_id of this InlineResponse20064Info.  # noqa: E501
        :type folder_id: int
        """

        self._folder_id = folder_id

    @property
    def control(self):
        """Gets the control of this InlineResponse20064Info.  # noqa: E501

        If `true`, the scan has a schedule and can be launched.  # noqa: E501

        :return: The control of this InlineResponse20064Info.  # noqa: E501
        :rtype: bool
        """
        return self._control

    @control.setter
    def control(self, control):
        """Sets the control of this InlineResponse20064Info.

        If `true`, the scan has a schedule and can be launched.  # noqa: E501

        :param control: The control of this InlineResponse20064Info.  # noqa: E501
        :type control: bool
        """

        self._control = control

    @property
    def user_permissions(self):
        """Gets the user_permissions of this InlineResponse20064Info.  # noqa: E501

        The sharing permissions for the scan.  # noqa: E501

        :return: The user_permissions of this InlineResponse20064Info.  # noqa: E501
        :rtype: int
        """
        return self._user_permissions

    @user_permissions.setter
    def user_permissions(self, user_permissions):
        """Sets the user_permissions of this InlineResponse20064Info.

        The sharing permissions for the scan.  # noqa: E501

        :param user_permissions: The user_permissions of this InlineResponse20064Info.  # noqa: E501
        :type user_permissions: int
        """

        self._user_permissions = user_permissions

    @property
    def schedule_uuid(self):
        """Gets the schedule_uuid of this InlineResponse20064Info.  # noqa: E501

        The UUID for a specific instance in the scan schedule.  # noqa: E501

        :return: The schedule_uuid of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._schedule_uuid

    @schedule_uuid.setter
    def schedule_uuid(self, schedule_uuid):
        """Sets the schedule_uuid of this InlineResponse20064Info.

        The UUID for a specific instance in the scan schedule.  # noqa: E501

        :param schedule_uuid: The schedule_uuid of this InlineResponse20064Info.  # noqa: E501
        :type schedule_uuid: str
        """

        self._schedule_uuid = schedule_uuid

    @property
    def edit_allowed(self):
        """Gets the edit_allowed of this InlineResponse20064Info.  # noqa: E501

        If `true`, the requesting user can edit this scan configuration.  # noqa: E501

        :return: The edit_allowed of this InlineResponse20064Info.  # noqa: E501
        :rtype: bool
        """
        return self._edit_allowed

    @edit_allowed.setter
    def edit_allowed(self, edit_allowed):
        """Sets the edit_allowed of this InlineResponse20064Info.

        If `true`, the requesting user can edit this scan configuration.  # noqa: E501

        :param edit_allowed: The edit_allowed of this InlineResponse20064Info.  # noqa: E501
        :type edit_allowed: bool
        """

        self._edit_allowed = edit_allowed

    @property
    def scanner_name(self):
        """Gets the scanner_name of this InlineResponse20064Info.  # noqa: E501

        The name of the scanner configured to run the scan.  # noqa: E501

        :return: The scanner_name of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._scanner_name

    @scanner_name.setter
    def scanner_name(self, scanner_name):
        """Sets the scanner_name of this InlineResponse20064Info.

        The name of the scanner configured to run the scan.  # noqa: E501

        :param scanner_name: The scanner_name of this InlineResponse20064Info.  # noqa: E501
        :type scanner_name: str
        """

        self._scanner_name = scanner_name

    @property
    def policy(self):
        """Gets the policy of this InlineResponse20064Info.  # noqa: E501

        The name of the scan template associated with the scan.  # noqa: E501

        :return: The policy of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._policy

    @policy.setter
    def policy(self, policy):
        """Sets the policy of this InlineResponse20064Info.

        The name of the scan template associated with the scan.  # noqa: E501

        :param policy: The policy of this InlineResponse20064Info.  # noqa: E501
        :type policy: str
        """

        self._policy = policy

    @property
    def shared(self):
        """Gets the shared of this InlineResponse20064Info.  # noqa: E501

        If `true`, the scan is shared with users other than the owner. The level of sharing is specified in the `acls` attribute of the scan details.  # noqa: E501

        :return: The shared of this InlineResponse20064Info.  # noqa: E501
        :rtype: bool
        """
        return self._shared

    @shared.setter
    def shared(self, shared):
        """Sets the shared of this InlineResponse20064Info.

        If `true`, the scan is shared with users other than the owner. The level of sharing is specified in the `acls` attribute of the scan details.  # noqa: E501

        :param shared: The shared of this InlineResponse20064Info.  # noqa: E501
        :type shared: bool
        """

        self._shared = shared

    @property
    def object_id(self):
        """Gets the object_id of this InlineResponse20064Info.  # noqa: E501


        :return: The object_id of this InlineResponse20064Info.  # noqa: E501
        :rtype: int
        """
        return self._object_id

    @object_id.setter
    def object_id(self, object_id):
        """Sets the object_id of this InlineResponse20064Info.


        :param object_id: The object_id of this InlineResponse20064Info.  # noqa: E501
        :type object_id: int
        """

        self._object_id = object_id

    @property
    def tag_targets(self):
        """Gets the tag_targets of this InlineResponse20064Info.  # noqa: E501

        The list of asset tag identifiers the scan uses to determine which assets it evaluates. For more information about tag-based scans, see [Manage Tag-Based Scans](doc:manage-tag-based-scans-tio).  # noqa: E501

        :return: The tag_targets of this InlineResponse20064Info.  # noqa: E501
        :rtype: list[str]
        """
        return self._tag_targets

    @tag_targets.setter
    def tag_targets(self, tag_targets):
        """Sets the tag_targets of this InlineResponse20064Info.

        The list of asset tag identifiers the scan uses to determine which assets it evaluates. For more information about tag-based scans, see [Manage Tag-Based Scans](doc:manage-tag-based-scans-tio).  # noqa: E501

        :param tag_targets: The tag_targets of this InlineResponse20064Info.  # noqa: E501
        :type tag_targets: list[str]
        """

        self._tag_targets = tag_targets

    @property
    def acls(self):
        """Gets the acls of this InlineResponse20064Info.  # noqa: E501

        An array of objects that control sharing permissions for the scan.  # noqa: E501

        :return: The acls of this InlineResponse20064Info.  # noqa: E501
        :rtype: list[ScansSettingsAcls]
        """
        return self._acls

    @acls.setter
    def acls(self, acls):
        """Sets the acls of this InlineResponse20064Info.

        An array of objects that control sharing permissions for the scan.  # noqa: E501

        :param acls: The acls of this InlineResponse20064Info.  # noqa: E501
        :type acls: list[ScansSettingsAcls]
        """

        self._acls = acls

    @property
    def hostcount(self):
        """Gets the hostcount of this InlineResponse20064Info.  # noqa: E501

        The total number of assets scanned for vulnerabilities.  # noqa: E501

        :return: The hostcount of this InlineResponse20064Info.  # noqa: E501
        :rtype: int
        """
        return self._hostcount

    @hostcount.setter
    def hostcount(self, hostcount):
        """Sets the hostcount of this InlineResponse20064Info.

        The total number of assets scanned for vulnerabilities.  # noqa: E501

        :param hostcount: The hostcount of this InlineResponse20064Info.  # noqa: E501
        :type hostcount: int
        """

        self._hostcount = hostcount

    @property
    def uuid(self):
        """Gets the uuid of this InlineResponse20064Info.  # noqa: E501

        The UUID of the scan.  # noqa: E501

        :return: The uuid of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._uuid

    @uuid.setter
    def uuid(self, uuid):
        """Sets the uuid of this InlineResponse20064Info.

        The UUID of the scan.  # noqa: E501

        :param uuid: The uuid of this InlineResponse20064Info.  # noqa: E501
        :type uuid: str
        """

        self._uuid = uuid

    @property
    def status(self):
        """Gets the status of this InlineResponse20064Info.  # noqa: E501

        The status of the scan. For a list of possible values, see [Scan Status](doc:scan-status-tio).  # noqa: E501

        :return: The status of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this InlineResponse20064Info.

        The status of the scan. For a list of possible values, see [Scan Status](doc:scan-status-tio).  # noqa: E501

        :param status: The status of this InlineResponse20064Info.  # noqa: E501
        :type status: str
        """

        self._status = status

    @property
    def scan_type(self):
        """Gets the scan_type of this InlineResponse20064Info.  # noqa: E501

        The type of scan: `ps` (a scan performed over the network by a cloud scanner), `remote` (a  scan performed over the network by a local scanner), `agent` (a scan on a local host that a Nessus agent performs directly), or `null` (the scan has never been launched, or the scan is imported).  # noqa: E501

        :return: The scan_type of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._scan_type

    @scan_type.setter
    def scan_type(self, scan_type):
        """Sets the scan_type of this InlineResponse20064Info.

        The type of scan: `ps` (a scan performed over the network by a cloud scanner), `remote` (a  scan performed over the network by a local scanner), `agent` (a scan on a local host that a Nessus agent performs directly), or `null` (the scan has never been launched, or the scan is imported).  # noqa: E501

        :param scan_type: The scan_type of this InlineResponse20064Info.  # noqa: E501
        :type scan_type: str
        """

        self._scan_type = scan_type

    @property
    def targets(self):
        """Gets the targets of this InlineResponse20064Info.  # noqa: E501

        A comma-delimited list of IPv4 addresses that are configured as targets for the scan.  # noqa: E501

        :return: The targets of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._targets

    @targets.setter
    def targets(self, targets):
        """Sets the targets of this InlineResponse20064Info.

        A comma-delimited list of IPv4 addresses that are configured as targets for the scan.  # noqa: E501

        :param targets: The targets of this InlineResponse20064Info.  # noqa: E501
        :type targets: str
        """

        self._targets = targets

    @property
    def alt_targets_used(self):
        """Gets the alt_targets_used of this InlineResponse20064Info.  # noqa: E501

        If `true`, Tenable.io did not not launched with a target list. This parameter is `true` for agent scans.  # noqa: E501

        :return: The alt_targets_used of this InlineResponse20064Info.  # noqa: E501
        :rtype: bool
        """
        return self._alt_targets_used

    @alt_targets_used.setter
    def alt_targets_used(self, alt_targets_used):
        """Sets the alt_targets_used of this InlineResponse20064Info.

        If `true`, Tenable.io did not not launched with a target list. This parameter is `true` for agent scans.  # noqa: E501

        :param alt_targets_used: The alt_targets_used of this InlineResponse20064Info.  # noqa: E501
        :type alt_targets_used: bool
        """

        self._alt_targets_used = alt_targets_used

    @property
    def pci_can_upload(self):
        """Gets the pci_can_upload of this InlineResponse20064Info.  # noqa: E501

        If `true`, you can submit the results of the scan for PCI ASV review. For more information, see [PCI ASV](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/PCI_ASV/Welcome.htm) in the Tenable.io Vulnerability Management User Guide.  # noqa: E501

        :return: The pci_can_upload of this InlineResponse20064Info.  # noqa: E501
        :rtype: bool
        """
        return self._pci_can_upload

    @pci_can_upload.setter
    def pci_can_upload(self, pci_can_upload):
        """Sets the pci_can_upload of this InlineResponse20064Info.

        If `true`, you can submit the results of the scan for PCI ASV review. For more information, see [PCI ASV](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/PCI_ASV/Welcome.htm) in the Tenable.io Vulnerability Management User Guide.  # noqa: E501

        :param pci_can_upload: The pci_can_upload of this InlineResponse20064Info.  # noqa: E501
        :type pci_can_upload: bool
        """

        self._pci_can_upload = pci_can_upload

    @property
    def scan_start(self):
        """Gets the scan_start of this InlineResponse20064Info.  # noqa: E501

        The Unix timestamp when the scan run started.  # noqa: E501

        :return: The scan_start of this InlineResponse20064Info.  # noqa: E501
        :rtype: int
        """
        return self._scan_start

    @scan_start.setter
    def scan_start(self, scan_start):
        """Sets the scan_start of this InlineResponse20064Info.

        The Unix timestamp when the scan run started.  # noqa: E501

        :param scan_start: The scan_start of this InlineResponse20064Info.  # noqa: E501
        :type scan_start: int
        """

        self._scan_start = scan_start

    @property
    def timestamp(self):
        """Gets the timestamp of this InlineResponse20064Info.  # noqa: E501

        The Unix timestamp when the scan run finished.  # noqa: E501

        :return: The timestamp of this InlineResponse20064Info.  # noqa: E501
        :rtype: int
        """
        return self._timestamp

    @timestamp.setter
    def timestamp(self, timestamp):
        """Sets the timestamp of this InlineResponse20064Info.

        The Unix timestamp when the scan run finished.  # noqa: E501

        :param timestamp: The timestamp of this InlineResponse20064Info.  # noqa: E501
        :type timestamp: int
        """

        self._timestamp = timestamp

    @property
    def is_archived(self):
        """Gets the is_archived of this InlineResponse20064Info.  # noqa: E501

        Indicates whether the scan results are older than 60 days (`true`). If this attribute is `true`, the response message for this endpoint excludes the `hosts`, `vulnerabilities`, `comphosts`, `compliance`, and `filters` objects. For complete scan results older than 60 days, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead.  # noqa: E501

        :return: The is_archived of this InlineResponse20064Info.  # noqa: E501
        :rtype: bool
        """
        return self._is_archived

    @is_archived.setter
    def is_archived(self, is_archived):
        """Sets the is_archived of this InlineResponse20064Info.

        Indicates whether the scan results are older than 60 days (`true`). If this attribute is `true`, the response message for this endpoint excludes the `hosts`, `vulnerabilities`, `comphosts`, `compliance`, and `filters` objects. For complete scan results older than 60 days, use the [POST /scans/{scan_id}/export](ref:scans-export-request) endpoint instead.  # noqa: E501

        :param is_archived: The is_archived of this InlineResponse20064Info.  # noqa: E501
        :type is_archived: bool
        """

        self._is_archived = is_archived

    @property
    def scan_end(self):
        """Gets the scan_end of this InlineResponse20064Info.  # noqa: E501

        The Unix timestamp when the scan run finished.  # noqa: E501

        :return: The scan_end of this InlineResponse20064Info.  # noqa: E501
        :rtype: int
        """
        return self._scan_end

    @scan_end.setter
    def scan_end(self, scan_end):
        """Sets the scan_end of this InlineResponse20064Info.

        The Unix timestamp when the scan run finished.  # noqa: E501

        :param scan_end: The scan_end of this InlineResponse20064Info.  # noqa: E501
        :type scan_end: int
        """

        self._scan_end = scan_end

    @property
    def haskb(self):
        """Gets the haskb of this InlineResponse20064Info.  # noqa: E501

        Indicates whether a scan has a Knowledge Base (KB) associated with it. A KB is an ASCII text file containing a log of information relevant to the scan performed and results found.  # noqa: E501

        :return: The haskb of this InlineResponse20064Info.  # noqa: E501
        :rtype: bool
        """
        return self._haskb

    @haskb.setter
    def haskb(self, haskb):
        """Sets the haskb of this InlineResponse20064Info.

        Indicates whether a scan has a Knowledge Base (KB) associated with it. A KB is an ASCII text file containing a log of information relevant to the scan performed and results found.  # noqa: E501

        :param haskb: The haskb of this InlineResponse20064Info.  # noqa: E501
        :type haskb: bool
        """

        self._haskb = haskb

    @property
    def hasaudittrail(self):
        """Gets the hasaudittrail of this InlineResponse20064Info.  # noqa: E501

        Indicates whether the scan is configured to create an audit trail.  # noqa: E501

        :return: The hasaudittrail of this InlineResponse20064Info.  # noqa: E501
        :rtype: bool
        """
        return self._hasaudittrail

    @hasaudittrail.setter
    def hasaudittrail(self, hasaudittrail):
        """Sets the hasaudittrail of this InlineResponse20064Info.

        Indicates whether the scan is configured to create an audit trail.  # noqa: E501

        :param hasaudittrail: The hasaudittrail of this InlineResponse20064Info.  # noqa: E501
        :type hasaudittrail: bool
        """

        self._hasaudittrail = hasaudittrail

    @property
    def scanner_start(self):
        """Gets the scanner_start of this InlineResponse20064Info.  # noqa: E501

        The scan's start time, if the scan is imported.  # noqa: E501

        :return: The scanner_start of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._scanner_start

    @scanner_start.setter
    def scanner_start(self, scanner_start):
        """Sets the scanner_start of this InlineResponse20064Info.

        The scan's start time, if the scan is imported.  # noqa: E501

        :param scanner_start: The scanner_start of this InlineResponse20064Info.  # noqa: E501
        :type scanner_start: str
        """

        self._scanner_start = scanner_start

    @property
    def scanner_end(self):
        """Gets the scanner_end of this InlineResponse20064Info.  # noqa: E501

        The scan's end time, if the scan is imported.  # noqa: E501

        :return: The scanner_end of this InlineResponse20064Info.  # noqa: E501
        :rtype: str
        """
        return self._scanner_end

    @scanner_end.setter
    def scanner_end(self, scanner_end):
        """Sets the scanner_end of this InlineResponse20064Info.

        The scan's end time, if the scan is imported.  # noqa: E501

        :param scanner_end: The scanner_end of this InlineResponse20064Info.  # noqa: E501
        :type scanner_end: str
        """

        self._scanner_end = scanner_end

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20064Info):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, InlineResponse20064Info):
            return True

        return self.to_dict() != other.to_dict()
