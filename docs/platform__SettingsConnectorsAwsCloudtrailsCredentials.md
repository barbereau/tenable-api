# SettingsConnectorsAwsCloudtrailsCredentials

For AWS connectors, the credentials that the Tenable.io connector uses to communicate with the AWS API, including `access_key` and `secret_key`.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_key** | **str** | The AWS access key. | [optional] 
**secret_key** | **str** | The AWS secret key. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


