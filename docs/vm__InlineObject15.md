# InlineObject15

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the managed credential. This name must be unique within your Tenable.io instance. | 
**description** | **str** | The description of the managed credential object. | [optional] 
**type** | **str** | The type of credential object. For a list of supported credential types, use the GET /credentials/types endpoint. | 
**settings** | [**CredentialsSettings**](vm__CredentialsSettings.md) |  | 
**permissions** | [**list[CredentialsPermissions]**](vm__CredentialsPermissions.md) | A list of user permissions for the managed credential. If a request message omits this parameter, Tenable.io automatically creates a &#x60;permissions&#x60; object for the user account that submits the request. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


