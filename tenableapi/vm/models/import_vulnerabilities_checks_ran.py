# coding: utf-8

"""
    Vulnerability Management

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import inspect
import pprint
import re  # noqa: F401
import six

from tenableapi.vm.configuration import Configuration


class ImportVulnerabilitiesChecksRan(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'tenable_plugin_id': 'str',
        'port': 'int',
        'protocol': 'str'
    }

    attribute_map = {
        'tenable_plugin_id': 'tenable_plugin_id',
        'port': 'port',
        'protocol': 'protocol'
    }

    def __init__(self, tenable_plugin_id=None, port=None, protocol=None, local_vars_configuration=None):  # noqa: E501
        """ImportVulnerabilitiesChecksRan - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._tenable_plugin_id = None
        self._port = None
        self._protocol = None
        self.discriminator = None

        if tenable_plugin_id is not None:
            self.tenable_plugin_id = tenable_plugin_id
        if port is not None:
            self.port = port
        if protocol is not None:
            self.protocol = protocol

    @property
    def tenable_plugin_id(self):
        """Gets the tenable_plugin_id of this ImportVulnerabilitiesChecksRan.  # noqa: E501

        The Tenable plugin ID.  # noqa: E501

        :return: The tenable_plugin_id of this ImportVulnerabilitiesChecksRan.  # noqa: E501
        :rtype: str
        """
        return self._tenable_plugin_id

    @tenable_plugin_id.setter
    def tenable_plugin_id(self, tenable_plugin_id):
        """Sets the tenable_plugin_id of this ImportVulnerabilitiesChecksRan.

        The Tenable plugin ID.  # noqa: E501

        :param tenable_plugin_id: The tenable_plugin_id of this ImportVulnerabilitiesChecksRan.  # noqa: E501
        :type tenable_plugin_id: str
        """

        self._tenable_plugin_id = tenable_plugin_id

    @property
    def port(self):
        """Gets the port of this ImportVulnerabilitiesChecksRan.  # noqa: E501

        The port on the asset where the check ran.  # noqa: E501

        :return: The port of this ImportVulnerabilitiesChecksRan.  # noqa: E501
        :rtype: int
        """
        return self._port

    @port.setter
    def port(self, port):
        """Sets the port of this ImportVulnerabilitiesChecksRan.

        The port on the asset where the check ran.  # noqa: E501

        :param port: The port of this ImportVulnerabilitiesChecksRan.  # noqa: E501
        :type port: int
        """

        self._port = port

    @property
    def protocol(self):
        """Gets the protocol of this ImportVulnerabilitiesChecksRan.  # noqa: E501

        The protocol used to communicate with the asset while running the check.  # noqa: E501

        :return: The protocol of this ImportVulnerabilitiesChecksRan.  # noqa: E501
        :rtype: str
        """
        return self._protocol

    @protocol.setter
    def protocol(self, protocol):
        """Sets the protocol of this ImportVulnerabilitiesChecksRan.

        The protocol used to communicate with the asset while running the check.  # noqa: E501

        :param protocol: The protocol of this ImportVulnerabilitiesChecksRan.  # noqa: E501
        :type protocol: str
        """

        self._protocol = protocol

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = inspect.getargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ImportVulnerabilitiesChecksRan):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, ImportVulnerabilitiesChecksRan):
            return True

        return self.to_dict() != other.to_dict()
