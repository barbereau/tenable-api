# AssetsExportFilters

Specifies filters for exported assets. To return all assets, omit the filters object. If your request specifies multiple filters, the system combines the filters using the AND search operator.  **Note:** If you submit an asset export request with filters that are identical to a previously submitted asset export request then the old export is canceled and a new export is submitted.
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **int** | Returns all assets created later than the date specified. The specified date must be in the Unix timestamp format. | [optional] 
**updated_at** | **int** | Returns all assets updated later than the date specified. The specified date must be in the Unix timestamp format. | [optional] 
**terminated_at** | **int** | Returns all assets terminated later than the date specified. The specified date must be in the Unix timestamp format. | [optional] 
**is_terminated** | **bool** | When set to &#x60;true&#x60;, returns assets which have any value for the &#x60;terminated_at&#x60; attribute. | [optional] 
**deleted_at** | **int** | Returns all assets deleted later than the date specified. The specified date must in the Unix timestamp format. | [optional] 
**is_deleted** | **bool** | When set to &#x60;true&#x60;, returns assets which have any value for the &#x60;deleted_at&#x60; attribute. | [optional] 
**is_licensed** | **bool** | Specifies whether the asset is included in the asset count for the Tenable.io instance. If &#x60;true&#x60;, Tenable.io returns only licensed assets. If &#x60;false&#x60;, Tenable.io returns all assets, both licensed and unlicensed. | [optional] 
**first_scan_time** | **int** | Returns all assets with a first scan time later than the date specified. The specified date must be in the Unix timestamp format. | [optional] 
**last_authenticated_scan_time** | **int** | Returns all assets with a last credentialed scan time later than the date specified. The specified date must be in the Unix timestamp format. | [optional] 
**last_assessed** | **int** | Returns all assets with a last assessed time later than the date specified. Tenable.io considers an asset assessed if it has been scanned by a credentialed or non-credentialed scan. The specified date must be in the Unix timestamp format. | [optional] 
**servicenow_sysid** | **bool** | If &#x60;true&#x60;, returns all assets that have a ServiceNow Sys ID, regardless of value. If &#x60;false&#x60;, returns all assets that do not have a ServiceNow Sys ID. | [optional] 
**sources** | **list[str]** | Returns assets that have the specified source. An asset source is the entity that reported the asset details. Sources can include sensors, connectors, and API imports. If your request specifies multiple sources, Tenable.io returns all assets that have been seen by any of the specified sources.  The items in the sources array must correspond to the names of the sources as defined in your organization&#39;s implementation of Tenable.io. Commonly used names include:  - AWS—You obtained the asset data from an Amazon Web Services connector.  - NESSUS_AGENT—You obtained the asset data obtained from a Nessus agent scan.  - PVS—You obtained the asset data from a Nessus Network Monitor (NNM) scan.  - NESSUS_SCAN—You obtained the asset data from a Nessus scan.  - WAS—You obtained the asset data from a  Web Application Scanning scan. | [optional] 
**has_plugin_results** | **bool** | Filter by whether or not the asset has plugin results associated with it. If &#x60;true&#x60;, Tenable.io returns all assets that have plugin results. If &#x60;false&#x60;, Tenable.io returns all assets that do not have plugin results. An asset may not have plugin results if the asset details originated from a connector or an API import. | [optional] 
**tag_category** | **str** | Returns all assets with the specified tag. The filter is defined as \&quot;tag\&quot;, a period (\&quot;.\&quot;), and the tag category name. The value of the filter is the tag value. For more information about tags, see [Tenable.io Vulnerability Management User Guide](https://docs.tenable.com/tenableio/vulnerabilitymanagement/Content/Settings/TagFormatAndApplication.htm). | [optional] 
**network_id** | **str** | The ID of the network object associated with scanners that identified the assets you want to export. The default network ID is &#x60;00000000-0000-0000-0000-000000000000&#x60;. To determine the ID of a custom network, use the [GET /networks](ref:networks-list) endpoint. For more information about network objects, see [Manage Networks](doc:manage-networks-tio). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


