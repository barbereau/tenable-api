# InlineResponse20025

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_was** | **bool** | Indicates whether the scan or user-defined template (policy) can be used in Tenable.io Web Application Scanning. | [optional] 
**user_permissions** | **int** | The scan or policy permissions that the requesting user has for the specified scan or user-defined template (policy). For more information, see [Permissions](doc:permissions). | [optional] 
**owner** | **str** | The username of the owner of the scan or user-defined template (policy). | [optional] 
**title** | **str** | For scans, the standard text, &#x60;Custom Scan&#x60;. For user-defined templates (policies), the name of the Tenable-provided template used to create the user-defined template.  | [optional] 
**is_agent** | **bool** | Indicates whether the scan or user-defined template (policy) can be used for agent scans. | [optional] 
**uuid** | **str** | The UUID of the scan or user-defined template (policy). | [optional] 
**filter_attributes** | [**list[InlineResponse20025FilterAttributes]**](vm__InlineResponse20025FilterAttributes.md) |  | [optional] 
**settings** | [**InlineResponse20025Settings**](vm__InlineResponse20025Settings.md) |  | [optional] 
**credentials** | **object** | Credentials that grant the scanner access to the target system without requiring an agent. Credentialed scans can perform a wider variety of checks than non-credentialed scans, which can result in more accurate scan results. This facilitates scanning of a very large network to determine local exposures or compliance violations. You can configure credentials for Cloud Services, Database, Host, Miscellaneous, Mobile Device Management, and Plaintext Authentication. | [optional] 
**compliance** | **object** | Plugins options enables you to select security checks by Plugin Family or individual plugins checks. | [optional] 
**plugins** | **object** | The settings for compliance audit checks. | [optional] 
**name** | **str** | For scans, the standard text, &#x60;custom&#x60;. For user-defined templates (policies), the system name for the Tenable-provided template used to create the scan or user-defined template. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


