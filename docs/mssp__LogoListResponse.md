# LogoListResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logos** | [**list[LogoObject]**](mssp__LogoObject.md) | An array of logo objects. | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


